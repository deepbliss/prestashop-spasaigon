<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ContactControllerCore extends FrontController
{
    public $php_self = 'contact';
    public $ssl = true;

    /**
    * Start forms process
    * @see FrontController::postProcess()
    */
    public function postProcess()
    {
        if (Tools::isSubmit('submitMessage')) {
            $saveContactKey = $this->context->cookie->contactFormKey;
            $extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
            $file_attachment = Tools::fileAttachment('fileUpload');
            $message = Tools::getValue('message'); // Html entities is not usefull, iscleanHtml check there is no bad html tags.
            $url = Tools::getValue('url');
            if (!($from = trim(Tools::getValue('from'))) || !Validate::isEmail($from)) {
                $this->errors[] = Tools::displayError('Invalid email address.');
            } elseif (!$message) {
                $this->errors[] = Tools::displayError('The message cannot be blank.');
            } elseif (!Validate::isCleanHtml($message)) {
                $this->errors[] = Tools::displayError('Invalid message');
            } elseif (!($id_contact = (int)Tools::getValue('id_contact')) || !(Validate::isLoadedObject($contact = new Contact($id_contact, $this->context->language->id)))) {
                $this->errors[] = Tools::displayError('Please select a subject from the list provided. ');
            } elseif (!empty($file_attachment['name']) && $file_attachment['error'] != 0) {
                $this->errors[] = Tools::displayError('An error occurred during the file-upload process.');
            } elseif (!empty($file_attachment['name']) && !in_array(Tools::strtolower(substr($file_attachment['name'], -4)), $extension) && !in_array(Tools::strtolower(substr($file_attachment['name'], -5)), $extension)) {
                $this->errors[] = Tools::displayError('Bad file extension');
            } elseif ($url === false || !empty($url) || $saveContactKey != (Tools::getValue('contactKey'))) {
                $this->errors[] = Tools::displayError('An error occurred while sending the message.');
            } else {
                $customer = $this->context->customer;
                if (!$customer->id) {
                    $customer->getByEmail($from);
                }

                $id_order = (int)$this->getOrder();

                if (!((
                        ($id_customer_thread = (int)Tools::getValue('id_customer_thread'))
                        && (int)Db::getInstance()->getValue('
						SELECT cm.id_customer_thread FROM '._DB_PREFIX_.'customer_thread cm
						WHERE cm.id_customer_thread = '.(int)$id_customer_thread.' AND cm.id_shop = '.(int)$this->context->shop->id.' AND token = \''.pSQL(Tools::getValue('token')).'\'')
                    ) || (
                        $id_customer_thread = CustomerThread::getIdCustomerThreadByEmailAndIdOrder($from, $id_order)
                    ))) {
                    $fields = Db::getInstance()->executeS('
					SELECT cm.id_customer_thread, cm.id_contact, cm.id_customer, cm.id_order, cm.id_product, cm.email
					FROM '._DB_PREFIX_.'customer_thread cm
					WHERE email = \''.pSQL($from).'\' AND cm.id_shop = '.(int)$this->context->shop->id.' AND ('.
                        ($customer->id ? 'id_customer = '.(int)$customer->id.' OR ' : '').'
						id_order = '.(int)$id_order.')');
                    $score = 0;
                    foreach ($fields as $key => $row) {
                        $tmp = 0;
                        if ((int)$row['id_customer'] && $row['id_customer'] != $customer->id && $row['email'] != $from) {
                            continue;
                        }
                        if ($row['id_order'] != 0 && $id_order != $row['id_order']) {
                            continue;
                        }
                        if ($row['email'] == $from) {
                            $tmp += 4;
                        }
                        if ($row['id_contact'] == $id_contact) {
                            $tmp++;
                        }
                        if (Tools::getValue('id_product') != 0 && $row['id_product'] == Tools::getValue('id_product')) {
                            $tmp += 2;
                        }
                        if ($tmp >= 5 && $tmp >= $score) {
                            $score = $tmp;
                            $id_customer_thread = $row['id_customer_thread'];
                        }
                    }
                }
                $old_message = Db::getInstance()->getValue('
					SELECT cm.message FROM '._DB_PREFIX_.'customer_message cm
					LEFT JOIN '._DB_PREFIX_.'customer_thread cc on (cm.id_customer_thread = cc.id_customer_thread)
					WHERE cc.id_customer_thread = '.(int)$id_customer_thread.' AND cc.id_shop = '.(int)$this->context->shop->id.'
					ORDER BY cm.date_add DESC');
                if ($old_message == $message) {
                    $this->context->smarty->assign('alreadySent', 1);
                    $contact->email = '';
                    $contact->customer_service = 0;
                }

                if ($contact->customer_service) {
                    if ((int)$id_customer_thread) {
                        $ct = new CustomerThread($id_customer_thread);
                        $ct->status = 'open';
                        $ct->id_lang = (int)$this->context->language->id;
                        $ct->id_contact = (int)$id_contact;
                        $ct->id_order = (int)$id_order;
                        if ($id_product = (int)Tools::getValue('id_product')) {
                            $ct->id_product = $id_product;
                        }
                        $ct->update();
                    } else {
                        $ct = new CustomerThread();
                        if (isset($customer->id)) {
                            $ct->id_customer = (int)$customer->id;
                        }
                        $ct->id_shop = (int)$this->context->shop->id;
                        $ct->id_order = (int)$id_order;
                        if ($id_product = (int)Tools::getValue('id_product')) {
                            $ct->id_product = $id_product;
                        }
                        $ct->id_contact = (int)$id_contact;
                        $ct->id_lang = (int)$this->context->language->id;
                        $ct->email = $from;
                        $ct->status = 'open';
                        $ct->token = Tools::passwdGen(12);
                        $ct->add();
                    }

                    if ($ct->id) {
                        $cm = new CustomerMessage();
                        $cm->id_customer_thread = $ct->id;
                        $cm->message = $message;
                        if (isset($file_attachment['rename']) && !empty($file_attachment['rename']) && rename($file_attachment['tmp_name'], _PS_UPLOAD_DIR_.basename($file_attachment['rename']))) {
                            $cm->file_name = $file_attachment['rename'];
                            @chmod(_PS_UPLOAD_DIR_.basename($file_attachment['rename']), 0664);
                        }
                        $cm->ip_address = (int)ip2long(Tools::getRemoteAddr());
                        $cm->user_agent = $_SERVER['HTTP_USER_AGENT'];
                        if (!$cm->add()) {
                            $this->errors[] = Tools::displayError('An error occurred while sending the message.');
                        }
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred while sending the message.');
                    }
                }

                if (!count($this->errors)) {
                    $var_list = array(
                                    '{order_name}' => '-',
                                    '{attached_file}' => '-',
                                    '{message}' => Tools::nl2br(stripslashes($message)),
                                    '{email}' =>  $from,
                                    '{product_name}' => '',
                                );

                    if (isset($file_attachment['name'])) {
                        $var_list['{attached_file}'] = $file_attachment['name'];
                    }

                    $id_product = (int)Tools::getValue('id_product');

                    if (isset($ct) && Validate::isLoadedObject($ct) && $ct->id_order) {
                        $order = new Order((int)$ct->id_order);
                        $var_list['{order_name}'] = $order->getUniqReference();
                        $var_list['{id_order}'] = (int)$order->id;
                    }

                    if ($id_product) {
                        $product = new Product((int)$id_product);
                        if (Validate::isLoadedObject($product) && isset($product->name[Context::getContext()->language->id])) {
                            $var_list['{product_name}'] = $product->name[Context::getContext()->language->id];
                        }
                    }

                    if (empty($contact->email)) {
                        Mail::Send($this->context->language->id, 'contact_form', ((isset($ct) && Validate::isLoadedObject($ct)) ? sprintf(Mail::l('Your message has been correctly sent #ct%1$s #tc%2$s'), $ct->id, $ct->token) : Mail::l('Your message has been correctly sent')), $var_list, $from, null, null, null, $file_attachment);
                    } else {
                        if (!Mail::Send($this->context->language->id, 'contact', Mail::l('Message from contact form').' [no_sync]',
                            $var_list, $contact->email, $contact->name, null, null,
                                    $file_attachment, null,    _PS_MAIL_DIR_, false, null, null, $from) ||
                                !Mail::Send($this->context->language->id, 'contact_form', ((isset($ct) && Validate::isLoadedObject($ct)) ? sprintf(Mail::l('Your message has been correctly sent #ct%1$s #tc%2$s'), $ct->id, $ct->token) : Mail::l('Your message has been correctly sent')), $var_list, $from, null, null, null, $file_attachment, null, _PS_MAIL_DIR_, false, null, null, $contact->email)) {
                            $this->errors[] = Tools::displayError('An error occurred while sending the message.');
                        }
                    }
                }

                if (count($this->errors) > 1) {
                    array_unique($this->errors);
                } elseif (!count($this->errors)) {
                    $this->context->smarty->assign('confirmation', 1);
                }
            }
        }
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_THEME_CSS_DIR_.'contact-form.css');
        $this->addJS(_THEME_JS_DIR_.'contact-form.js');
        $this->addJS(_PS_JS_DIR_.'validate.js');
           parent::setMedia();
            $this->addCSS(_THEME_CSS_DIR_.'stores.css');
  
    if (!Configuration::get('PS_STORES_SIMPLIFIED')) {
        $default_country = new Country((int)Tools::getCountry());
        $this->addJS('http'.((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? 's' : '').'://maps.google.com/maps/api/js?sensor=true&region='.substr($default_country->iso_code, 0, 2));
        $this->addJS(_THEME_JS_DIR_.'stores.js');
    }
    }

    /**
    * Assign template vars related to page content
    * @see FrontController::initContent()
    */
public function initContent()
{
    parent::initContent();
 
    $this->assignOrderList();
 
    $email = Tools::safeOutput(Tools::getValue('from',
    ((isset($this->context->cookie) && isset($this->context->cookie->email) && Validate::isEmail($this->context->cookie->email)) ? $this->context->cookie->email : '')));
    $this->context->smarty->assign(array(
        'errors' => $this->errors,
        'email' => $email,
        'fileupload' => Configuration::get('PS_CUSTOMER_SERVICE_FILE_UPLOAD'),
        'max_upload_size' => (int)Tools::getMaxUploadSize()
    ));
 
    if (($id_customer_thread = (int)Tools::getValue('id_customer_thread')) && $token = Tools::getValue('token')) {
        $customer_thread = Db::getInstance()->getRow('
            SELECT cm.*
            FROM '._DB_PREFIX_.'customer_thread cm
            WHERE cm.id_customer_thread = '.(int)$id_customer_thread.'
            AND cm.id_shop = '.(int)$this->context->shop->id.'
            AND token = \''.pSQL($token).'\'
        ');
 
        $order = new Order((int)$customer_thread['id_order']);
        if (Validate::isLoadedObject($order)) {
            $customer_thread['reference'] = $order->getUniqReference();
        }
        $this->context->smarty->assign('customerThread', $customer_thread);
    }
 
    $this->context->smarty->assign(array(
        'contacts' => Contact::getContacts($this->context->language->id),
        'message' => html_entity_decode(Tools::getValue('message'))
    ));
 
 
    if (Configuration::get('PS_STORES_SIMPLIFIED')) {
        $this->assignStoresSimplified();
    } else {
        $this->assignStores();
    }
 
    $this->context->smarty->assign(array(
        'mediumSize' => Image::getSize(ImageType::getFormatedName('medium')),
        'defaultLat' => (float)Configuration::get('PS_STORES_CENTER_LAT'),
        'defaultLong' => (float)Configuration::get('PS_STORES_CENTER_LONG'),
        'searchUrl' => $this->context->link->getPageLink('stores'),
        'logo_store' => Configuration::get('PS_STORES_ICON')
    ));
 
    $this->setTemplate(_PS_THEME_DIR_.'contact-form.tpl');
}

    /**
    * Assign template vars related to order list and product list ordered by the customer
    */
    protected function assignOrderList()
    {
        if ($this->context->customer->isLogged()) {
            $this->context->smarty->assign('isLogged', 1);

            $products = array();
            $result = Db::getInstance()->executeS('
			SELECT id_order
			FROM '._DB_PREFIX_.'orders
			WHERE id_customer = '.(int)$this->context->customer->id.Shop::addSqlRestriction(Shop::SHARE_ORDER).' ORDER BY date_add');

            $orders = array();

            foreach ($result as $row) {
                $order = new Order($row['id_order']);
                $date = explode(' ', $order->date_add);
                $tmp = $order->getProducts();
                foreach ($tmp as $key => $val) {
                    $products[$row['id_order']][$val['product_id']] = array('value' => $val['product_id'], 'label' => $val['product_name']);
                }

                $orders[] = array('value' => $order->id, 'label' => $order->getUniqReference().' - '.Tools::displayDate($date[0], null) , 'selected' => (int)$this->getOrder() == $order->id);
            }

            $this->context->smarty->assign('orderList', $orders);
            $this->context->smarty->assign('orderedProductList', $products);
        }
    }

    protected function getOrder()
    {
        $id_order = false;
        if (!is_numeric($reference = Tools::getValue('id_order'))) {
            $reference = ltrim($reference, '#');
            $orders = Order::getByReference($reference);
            if ($orders) {
                foreach ($orders as $order) {
                    $id_order = (int)$order->id;
                    break;
                }
            }
        } elseif (Order::getCartIdStatic((int)Tools::getValue('id_order'))) {
            $id_order = (int)Tools::getValue('id_order');
        }
        return (int)$id_order;
    }
    protected function processStoreAddress($store)
{
    $ignore_field = array(
        'firstname',
        'lastname'
    );
 
    $out_datas = array();
 
    $address_datas = AddressFormat::getOrderedAddressFields($store['id_country'], false, true);
    $state = (isset($store['id_state'])) ? new State($store['id_state']) : null;
 
    foreach ($address_datas as $data_line) {
        $data_fields = explode(' ', $data_line);
        $addr_out = array();
 
        $data_fields_mod = false;
        foreach ($data_fields as $field_item) {
            $field_item = trim($field_item);
            if (!in_array($field_item, $ignore_field) && !empty($store[$field_item])) {
                $addr_out[] = ($field_item == 'city' && $state && isset($state->iso_code) && strlen($state->iso_code)) ?
                    $store[$field_item].', '.$state->iso_code : $store[$field_item];
                $data_fields_mod = true;
            }
        }
        if ($data_fields_mod) {
            $out_datas[] = implode(' ', $addr_out);
        }
    }
 
    $out = implode('<br />', $out_datas);
    return $out;
}
 
/**
 * Assign template vars for simplified stores
 */
protected function assignStoresSimplified()
{
    $stores = Db::getInstance()->executeS('
    SELECT s.*, cl.name country, st.iso_code state
    FROM '._DB_PREFIX_.'store s
    '.Shop::addSqlAssociation('store', 's').'
    LEFT JOIN '._DB_PREFIX_.'country_lang cl ON (cl.id_country = s.id_country)
    LEFT JOIN '._DB_PREFIX_.'state st ON (st.id_state = s.id_state)
    WHERE s.active = 1 AND cl.id_lang = '.(int)$this->context->language->id);
 
    $addresses_formated = array();
 
    foreach ($stores as &$store) {
        $address = new Address();
        $address->country = Country::getNameById($this->context->language->id, $store['id_country']);
        $address->address1 = $store['address1'];
        $address->address2 = $store['address2'];
        $address->postcode = $store['postcode'];
        $address->city = $store['city'];
 
        $addresses_formated[$store['id_store']] = AddressFormat::getFormattedLayoutData($address);
 
        $store['has_picture'] = file_exists(_PS_STORE_IMG_DIR_.(int)$store['id_store'].'.jpg');
        if ($working_hours = $this->renderStoreWorkingHours($store)) {
            $store['working_hours'] = $working_hours;
        }
    }
 
    $this->context->smarty->assign(array(
        'simplifiedStoresDiplay' => true,
        'stores' => $stores,
        'addresses_formated' => $addresses_formated,
    ));
}
 
public function renderStoreWorkingHours($store)
{
    global $smarty;
 
    $days[1] = 'Monday';
    $days[2] = 'Tuesday';
    $days[3] = 'Wednesday';
    $days[4] = 'Thursday';
    $days[5] = 'Friday';
    $days[6] = 'Saturday';
    $days[7] = 'Sunday';
 
    $days_datas = array();
    $hours = array();
 
    if ($store['hours']) {
        $hours = Tools::unSerialize($store['hours']);
        if (is_array($hours)) {
            $hours = array_filter($hours);
        }
    }
 
    if (!empty($hours)) {
        for ($i = 1; $i < 8; $i++) {
            if (isset($hours[(int)$i - 1])) {
                $hours_datas = array();
                $hours_datas['hours'] = $hours[(int)$i - 1];
                $hours_datas['day'] = $days[$i];
                $days_datas[] = $hours_datas;
            }
        }
        $smarty->assign('days_datas', $days_datas);
        $smarty->assign('id_country', $store['id_country']);
        return $this->context->smarty->fetch(_PS_THEME_DIR_.'store_infos.tpl');
    }
    return false;
}
 
public function getStores()
{
    $distance_unit = Configuration::get('PS_DISTANCE_UNIT');
    if (!in_array($distance_unit, array('km', 'mi'))) {
        $distance_unit = 'km';
    }
 
    if (Tools::getValue('all') == 1) {
        $stores = Db::getInstance()->executeS('
        SELECT s.*, cl.name country, st.iso_code state
        FROM '._DB_PREFIX_.'store s
        '.Shop::addSqlAssociation('store', 's').'
        LEFT JOIN '._DB_PREFIX_.'country_lang cl ON (cl.id_country = s.id_country)
        LEFT JOIN '._DB_PREFIX_.'state st ON (st.id_state = s.id_state)
        WHERE s.active = 1 AND cl.id_lang = '.(int)$this->context->language->id);
    } else {
        $distance = (int)Tools::getValue('radius', 100);
        $multiplicator = ($distance_unit == 'km' ? 6371 : 3959);
 
        $stores = Db::getInstance()->executeS('
        SELECT s.*, cl.name country, st.iso_code state,
        ('.(int)$multiplicator.'
            * acos(
                cos(radians('.(float)Tools::getValue('latitude').'))
                * cos(radians(latitude))
                * cos(radians(longitude) - radians('.(float)Tools::getValue('longitude').'))
                + sin(radians('.(float)Tools::getValue('latitude').'))
                * sin(radians(latitude))
            )
        ) distance,
        cl.id_country id_country
        FROM '._DB_PREFIX_.'store s
        '.Shop::addSqlAssociation('store', 's').'
        LEFT JOIN '._DB_PREFIX_.'country_lang cl ON (cl.id_country = s.id_country)
        LEFT JOIN '._DB_PREFIX_.'state st ON (st.id_state = s.id_state)
        WHERE s.active = 1 AND cl.id_lang = '.(int)$this->context->language->id.'
        HAVING distance < '.(int)$distance.'
        ORDER BY distance ASC
        LIMIT 0,20');
    }
 
    return $stores;
}
 
/**
 * Assign template vars for classical stores
 */
protected function assignStores()
{
    $this->context->smarty->assign('hasStoreIcon', file_exists(_PS_IMG_DIR_.Configuration::get('PS_STORES_ICON')));
 
    $distance_unit = Configuration::get('PS_DISTANCE_UNIT');
    if (!in_array($distance_unit, array('km', 'mi'))) {
        $distance_unit = 'km';
    }
 
    $this->context->smarty->assign(array(
        'distance_unit' => $distance_unit,
        'simplifiedStoresDiplay' => false,
        'stores' => $this->getStores(),
    ));
}
}
