<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
class Hook extends HookCore
{
    /*
    * module: cookienotification
    * date: 2018-05-21 15:16:16
    * version: 1.1.0
    */
    public static function getHookModuleExecList($hook_name = null)
    {
        $context = Context::getContext();
        $list = parent::getHookModuleExecList($hook_name);
        if ($list &&
            Module::isEnabled('cookienotification') &&
            !isset($context->employee) &&
            !isset($context->cookie->{Configuration::get('CN_COOKIE_IDENTIFIER')}))
        {
            $blocked_module_ids = Configuration::get('CN_BLOCKED_MODULE_IDS');
            $blocked_module_ids = $blocked_module_ids ? Tools::jsonDecode($blocked_module_ids, true) : array();
            foreach ($list as $k => $module)
            {
                if (in_array($module['id_module'], $blocked_module_ids))
                    unset($list[$k]);
            }
        }
        return $list;
    }
}
