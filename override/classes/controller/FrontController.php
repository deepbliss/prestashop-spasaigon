<?php
class FrontControllerCore extends Controller
{
    public function setMedia()
    {
        parent::setMedia(); // This will take all code in setMedia() of the original classes/controller/FrontController.php
        $this->addCSS(_THEME_CSS_DIR_.'myoverride/animate.css');
        
        if ($this->assignCase == 1)
             $this->addJS(_THEME_JS_DIR_.'myoverride/wow.min.js');
        
    }
}