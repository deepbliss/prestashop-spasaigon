
$(document).ready(function(){
	
	fixHeader();

		/*mobile*/
	$('.mobile-btn, .cart-close').click(function() {
		$('.cart_block').toggleClass('active');
		$('html').toggleClass('cart-active');
	});
	$('.js-menu-btn').click(function() {
		$('html').toggleClass('scroll-lock');
		if ($('.cart_block').hasClass('active')){
			$('.cart_block').removeClass('active');
		}
	});
	/*end mobile*/
	
	searchFocusMenu();
	$(window).resize(function(){
		searchFocusMenu();
		fixHeader();
	});

	$('#back-top').click(function() {
		$('body,html').animate({scrollTop: 0}, 1000);
	});

		/*count items more-info-tabs */
		var itemsTabs = $('#more_info_tabs').find('li');
		if(itemsTabs.length > 3)
			$('#more_info_tabs').addClass('total-more-than-3');
		/*end count items more-info-tabs */

		// video
	var $video = $('#video-in');
	if ($video.length){
		var $videoDOM = $video[0];
		$video.closest('#video-promo-wrap').on('click', function(){
			$(this).toggleClass('play pause');
			if ($videoDOM.paused){
				$videoDOM.play();
				$(this).addClass('play').removeClass('pause');
			} else {
				$videoDOM.pause();
				$(this).addClass('pause').removeClass('play');
			}
		});
	}

});

function searchFocusMenu(){
	var getSearch = $('#amazing_block_top');
	if (getSearch.length != 0){
		if(scrollCompensate() + $(window).width()>768){
			$('.header_search_input').click(function(){
				$(this).parent().parent().addClass('search-active-menu');
			});
				$(document).on('click',function(e){
					 if ($(e.target).closest('.amazing-search').length) return; 
					$('.amazing-search').removeClass('search-active-menu');
					e.stopPropagation();
				});
		}else{
			$('.header_search_input').off();
			$('.amazing-search').removeClass('search-active-menu');
		}
	}
}
function fixHeader(){
	var scrollTimer, resizeTimer;
	// sticky menu
	if(scrollCompensate() + $(window).width()>768){
		$(window).scroll(function() {
			clearTimeout(scrollTimer);
			scrollTimer=setTimeout(function() {
				var scrollY=$(window).scrollTop();
				if (scrollY > 51) {
					$('.main_panel').addClass('fixedHeader');
				} else {
					$('.main_panel').removeClass('fixedHeader');
				}
			}
			, 0);
		}).trigger('scroll');
	}
}$(document).ready(function(){$('.twa_posts').bxSlider({  controls:true,    auto: true,    pager: true  });});