var isMobileOrTablet = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
(function(jQuery) {
	'use strict';
	jQuery.stylingSettings = {
		chooseFileTxt : 'Choose file',
		noFiletxt : 'No file chosen',
	};
	// defined in product.js
	if (typeof product_fileButtonHtml != 'undefined')
		jQuery.stylingSettings.chooseFileTxt = product_fileButtonHtml;
	if (typeof product_fileDefaultHtml != 'undefined')
		jQuery.stylingSettings.noFiletxt = product_fileDefaultHtml;

	// defined in contact-form.js
	if (typeof contact_fileButtonHtml != 'undefined')
		jQuery.stylingSettings.chooseFileTxt = contact_fileButtonHtml;
	if (typeof contact_fileDefaultHtml != 'undefined')
		jQuery.stylingSettings.noFiletxt = contact_fileDefaultHtml;

	jQuery.fn.addStyling = function(settings) {
		settings = jQuery.extend({}, jQuery.stylingSettings, settings);
		this.each(function(){
			if (this.tagName == 'SELECT' && !isMobileOrTablet){
				if (!jQuery(this).parent().hasClass('styled-select'))
					jQuery(this).addClass('wrapped').wrap('<div class="styled-select"></div>');
				jQuery(this).parent().find('.dl').remove();
				var options = jQuery(this).find('option, optgroup');
				if (!options.length)
					return;
				var optionsHTML = '',
					longestTxt = '',
					maxChars = 0;
				options.each(function(i){
					var val = jQuery(this).val();
					var optClass = 'option '+jQuery.trim(jQuery(this).attr('class') || '');
					if (!i)
						optClass += ' first';
					var optContent = jQuery(jQuery(this)).text();
					if (this.nodeName == 'OPTGROUP'){
						val = '';
						optContent = jQuery(jQuery(this)).attr('label');
						optClass = 'optgroup '+jQuery.trim(jQuery(this).attr('class') || '');
					}
					var data = jQuery(jQuery(this)).data();
					var dataHTML = '';
					for (var i in data)
						dataHTML += ' data-'+i+'="'+data[i]+'"';
					optionsHTML += '<li data-val="'+val+'" class="'+optClass+'"'+dataHTML+'>'+optContent+'</li>';
					var txt = jQuery(this).text();
					if (txt.length > maxChars) {
						maxChars = txt.length;
						longestTxt = jQuery(this).text();
					}
				});
				var newHTML = '<div class="dl closed"><dt class="option"><span class="longest-txt">'+longestTxt+'</span><span class="selected_name">'+jQuery(this).find('option:selected').text()+'</span><i class="toggle"></i></dt><ul class="styled-options">';
				newHTML += optionsHTML;
				newHTML += '</ul></div>';
				jQuery(this).parent().append(newHTML);
				// fieldset fix
				if (jQuery(this).closest('fieldset').length)
				{
					var resizeTimer;
					var jQuerycontainer = jQuery(this).closest('.styledSelect');
					jQuerycontainer.css('max-width', jQuerycontainer.closest('fieldset').parent().innerWidth()+'px');
					jQuery(window).resize(function(){
						clearTimeout(resizeTimer);
						resizeTimer = setTimeout(function() {
							jQuerycontainer.css('max-width', jQuerycontainer.closest('fieldset').parent().innerWidth()+'px');
						}, 200);
					});
				}
			}
			else if (this.tagName == 'INPUT'){
				var type = jQuery(this).attr('type'),
					cls = jQuery(this).attr('class') || '';
				if ( type == 'checkbox' || type == 'radio' || type == 'file'){
					//brain 27/06/2016 exclude input.star
					if(jQuery(this).hasClass('not-styling'))
						return;
					if (!jQuery(this).parent().hasClass('styled-'+type)){
						jQuery(this).wrap('<span class="styled-'+type+' '+cls+'"></span>');
						if (type == 'file'){
							jQuery(this).before('<span class="file-button">'+settings.chooseFileTxt+'</span><span class="file-name">'+settings.noFiletxt+'</span>');
						}
					}
					if (type != 'file' && jQuery(this).prop('checked'))
						jQuery(this).parent().addClass('checked');
				}
			}
			if (jQuery(this).is(':disabled'))
				jQuery(this).parent().addClass('disabled');
			if (isMobileOrTablet)
				jQuery(this).parent().addClass('mobile');
		});
	}

	// select events
	jQuery(document).on('click', function(e) {
		if (!jQuery(e.target).closest('.styled-select').length)
			jQuery('.styled-select .dl').addClass('closed');
	}).on('click', '.styled-select dt', function(){
		var closed = jQuery(this).parent().hasClass('closed');
		jQuery('.styled-select .dl').addClass('closed');
		if (closed){
			jQuery(this).parent().removeClass('closed');
			var jQueryul = jQuery(this).closest('.styled-select').find('ul');
			if (!jQueryul.hasClass('prepared')){
				var maxHeight = 5;
				jQueryul.find('li:visible').each(function(i){
					if (i > 5)
						return false;
					maxHeight += jQuery(this).outerHeight();
				});
				jQueryul.css('max-height', maxHeight+'px').addClass('prepared');
			}
			try {
				var viewPortPosition = this.getBoundingClientRect();
				var toTop = viewPortPosition.top;
				var toBottom = jQuery(window).height() - viewPortPosition.bottom;
				if (toTop > toBottom && toBottom < jQueryul.outerHeight())
					jQueryul.addClass('above');
				else
					jQueryul.removeClass('above');
			}catch(err){};

		}
	}).on('click', '.styled-select li.option', function(){
		jQuery(this).closest('.dl').addClass('closed').find('.selected_name').html(jQuery(this).html());
		if (!jQuery(this).hasClass('dont-bubble')) {
			jQuery(this).closest('.styled-select').find('select').val(jQuery(this).attr('data-val')).change().click();
		} else {
			jQuery(this).removeClass('dont-bubble');

		}
	}).on('change', 'select.wrapped', function(){
		var val = jQuery(this).val();
		jQuery(this).parent().find('li[data-val="'+val+'"]').addClass('dont-bubble').click();
	}).on('mousewheel.styling DOMMouseScroll.styling', '.styled-options', function(e){
		/*
		* copied from jquery.chosen
		*/
		var delta, _ref1, _ref2;
		delta = -((_ref1 = e.originalEvent) != null ? _ref1.wheelDelta : void 0) || ((_ref2 = e.originialEvent) != null ? _ref2.detail : void 0);
		if (delta != null) {
			e.preventDefault();
			if (e.type === 'DOMMouseScroll') {
				delta = delta * 40;
			}
			jQuery(this).scrollTop(delta + jQuery(this).scrollTop());
		}
	});

	// checkbox/radio events
	// in some cases click or change can be unbound, so bind both of them here
	jQuery(document).on('change click', '.styled-checkbox input, .styled-radio input', function(){
		if (jQuery(this).attr('type') == 'radio')
			jQuery('[type="radio"][name="'+jQuery(this).attr('name')+'"]').each(function(){
				jQuery(this).parent().toggleClass('checked', jQuery(this).prop('checked'));
			});
		jQuery(this).parent().toggleClass('checked', jQuery(this).prop('checked'));
	});

	// file events
	jQuery(document).on('change', '.styled-file input', function(){
		var fileName = [];
		for (var i in this.files)
			if (jQuery.isNumeric(i))
				fileName.push(this.files[i].name);
		fileName = fileName.join(', ');
		jQuery(this).siblings('.file-name').html(fileName);
	});

	// in some places uniform is called without pre-check
	jQuery.fn.uniform = function (settings) {
		this.addStyling(settings);
	}
	jQuery.uniform = {defaults:{},update:function(){}};
}(jQuery));

jQuery(window).load(function(){
	jQuery('select, input:not(.not-styling)').addStyling();
});
