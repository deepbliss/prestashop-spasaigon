/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

// <![CDATA[
$('document').ready(function() {
	var $input_search = $("#amazingsearch_query_top");
	$input_search.on('keyup', function() {
		if ($(this).val().length > 4){
			var $select_c = $("#amazing_search_select").val();
			var search_value = $(this).val();
			ajaxQuery = $.ajax({
				type: 'GET',
				url: baseDir + 'modules/amazingsearch/amazingsearch-ajax.php',
				data: 's='+search_value.toLowerCase()+'&id_lang='+lang_id+'&id_shop='+shop_id+'&id_category='+$select_c,
				dataType: 'json',
				cache: false,
				success: function(result){
					if(result.length > 0){
						$(".amazingsearch_result").html(result);
						$(".amazingsearch_result").show();
					}else{
						$(".amazingsearch_result").html('');
						$(".amazingsearch_result").hide();
					}
				}					
			});	
		}
	});
	$("#amazing_search_select").on("change", function(){
		if ($("#amazingsearch_query_top").val().length > 4){
			var $select_c = $("#amazing_search_select").val();
			var search_value = $(this).val();
			ajaxQuery = $.ajax({
				type: 'GET',
				url: baseDir + 'modules/amazingsearch/amazingsearch-ajax.php',
				data: 's='+search_value.toLowerCase()+'&id_lang='+lang_id+'&id_shop='+shop_id+'&id_category='+$select_c,
				dataType: 'json',
				cache: false,
				success: function(result){
					if(result.length > 0){
						$(".amazingsearch_result").html(result);
						$(".amazingsearch_result").show();
					}else{
						$(".amazingsearch_result").html('');
						$(".amazingsearch_result").hide();
					}
				}					
			});	
		}
	});

	$(document).mouseup(function (e){
	    var container = $("#amazing_block_top");
		if (!container.is(e.target) && container.has(e.target).length === 0){
	        $("#amazing_block_top .amazingsearch_result").hide().html('');
	    }
    });
});
// ]]>