{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"><![endif]-->
<html lang="{$lang_iso}">
    <head>
        <meta charset="utf-8" />
        <title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
        <meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
        <meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
        <meta name="generator" content="PrestaShop" />
        <meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes" /> 
        <link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
        <link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
        <link href='http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Roboto:500,900,400italic,300,700,500italic,300italic,400&subset=cyrillic,cyrillic-ext,latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700,900" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        
{if isset($css_files)}
    {foreach from=$css_files key=css_uri item=media}
        <link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
    {/foreach}
{/if}
{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
    {$js_def}
    {foreach from=$js_files item=js_uri}
    <script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
    {/foreach}
{/if}
        {$HOOK_HEADER}
        
        
        <!--[if IE 8]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if $page_name !='index'} no-index{/if} {if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso} {if $page_name == 'index' && (!empty($left_column_size) || !empty($right_column_size))} columns-active{/if}">
    {if !isset($content_only) || !$content_only}
        {if isset($restricted_country_mode) && $restricted_country_mode}
            <div id="restricted-country">
                <p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span></p>
            </div>
        {/if}
        <div id="page">      
             <input id="menu-checkbox" class="menu-checkbox not-styling" type="checkbox">
                <header id="header">       
  {hook h='displayCustomBanners1'}
                             
                    <div id="topMain" class="nav">                         
                        <div class="container-fluid">
                            <div class="row">         
                                <nav class="clearfix">     
                                    {hook h="displayNav"}
                                    <!--<label class="js-menu-btn round" for="menu-checkbox">
                                        <span class="menu-btn"></span>
                                    </label>-->
                                </nav>
<!--                                new div-->
                                <div class="header-custom">
                                     <div class="user-info__item">
                                           <form id="searchbox" method="get" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" >
                                                <input type="hidden" name="controller" value="search" />
                                                <input type="hidden" name="orderby" value="position" />
                                                <input type="hidden" name="orderway" value="desc" />
                                                <button type="submit" name="submit_search" class="button-search icon-search">
                                                </button>
                                                <input class="search_query" type="text" id="search_query_top" name="search_query"  placeholder="{l s='Search L\'Apothiquaire' mod='blocksearch'}"/>
                                            </form>
                                    </div>  
                                    <div id="header_logo">
                                        <a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
                                            <img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"/>
                                        </a> 
                                    </div>   
                                              
                            <div id="header_contact"  class="len">
                                        <a href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='Contact' mod='blockpermanentlinks'}">
                                        {l s='Contact' mod='blockpermanentlinks'}</a>
                                    </div>   
           
                                    <div id="header_contact"  class="lvn">
                                        <a href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='Contact' mod='blockpermanentlinks'}">
                                        {l s='Liên hệ' mod='blockpermanentlinks'}</a>                                        
                                    </div>   
 
                                     <!--div class="cart">
                                        <a href="{$link->getPageLink($order_process, true)|escape:'html':'UTF-8'}" title="{l s='View my shopping cart' mod='blockuserinfo'}" rel="nofollow" class="shop_cart_user icon-shopping-cart">
                                        {l s='Shopping Bag' mod='blockuserinfo'}
                                        </a>
                                    </div-->        
                                    <div class="clear"></div>                                 
                                </div>
                            </div>
                        </div>
                    </div>
                             {if isset($HOOK_TOP)}{$HOOK_TOP}{/if}       
                                       
                    <div class="main_panel">
                        <div class="container-fluid">
                            <div class="row">
                                <!--div id="header_logo" class="col-md-2 col-xs-6">
                                    <a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
                                        <img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"/>
                                    </a>
                                </div-->  
                                <div class="mobile-wrapper-menu">
                                    <div class="mobile-inner-menu">
                                        <div class="inner-menu-cell">
                                            <div class="box-relative">
                                                <div class="js-box-table box-table">
                                                    <h4 class="mobile-title font-remove">{l s='Menu'}</h4>
                                                </div>
                                                <div class="js-mobile_search mobile_search"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            <div class="columns-container">
                {if $page_name !='index' && $page_name !='pagenotfound'}
                {hook h="displayBanner"}
                <div class="wrap_breadcrumb">
                    <div class="container">
                        {include file="$tpl_dir./breadcrumb.tpl"}
                    </div>
                </div>
                {/if}
                <div id="columns" class="clearfix {if $page_name !='index'}container{/if} {if $page_name == 'index' && (!empty($left_column_size) || !empty($right_column_size))}container{/if}">
                    <div id="slider_row" class="{if $page_name !='index'}row{/if} {if $page_name == 'index' && (!empty($left_column_size) || !empty($right_column_size))}row{/if}">
                     <!-- Category image -->
                    <div class="content_scene_cat_bg"><div class="container">
                        <h1 class="title_main_section category-name">
                                <span>
                                    {strip}
                                        {$category->name|escape:'html':'UTF-8'}
                                        {if isset($categoryNameComplement)}
                                            {$categoryNameComplement|escape:'html':'UTF-8'}
                                        {/if}
                                    {/strip}
                                </span>
                            </h1>
                        {if $category->description}
                            <div class="cat_desc">
                            {if Tools::strlen($category->description) > 350}
                                <!--No short, whole view should be  seen
                                <div id="category_description_short" class="rte">{$description_short}</div>
                                <!--no more button direct content should be seen, initially it was in this manner, but now i have changed to 
                                
                                <div id="category_description_full" class="unvisible rte">{$category->description}</div>
                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                                -->
                                <div id="category_description_full" class="rte">{$category->description}</div>
                            {else}
                                <div class="rte">{$category->description}</div>
                            {/if}
                            </div>
                        {/if}
                        {if $category->id_image} 
                            <img src="{$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}" class="img-responsive" alt=""/> 
                        {/if}
                     </div></div>
                      <!-- ends Category image -->
                    
                        <div id="top_column" class="center_column col-xs-12 col-sm-12">
                            {if $page_name == 'index'}
                                <div class="row">
                                <!-- hook displayTopColumn -->
                                    {hook h='displayTopColumn'}
                                <!-- end hook DisplayTopColumn -->
                                </div>
                                <!-- end hook displayHomeLine -->
                            {/if}
                        </div>
                    </div>
                    <div class="{if $page_name !='index' && $page_name !='pagenotfound'}row{/if}">
                        {if isset($left_column_size) && !empty($left_column_size)}
                        <div id="left_column" class="column column-side col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
                        {/if}
                        {if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
                        <div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">
    {/if}