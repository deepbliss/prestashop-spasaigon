{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if isset($category)}
	{if $category->id AND $category->active}
		{if $scenes || $category->description || $category->id_image}
			<div class="content_scene_cat">
				 {if $scenes}
					<div class="content_scene">
						<!-- Scenes -->
						{include file="$tpl_dir./scenes.tpl" scenes=$scenes}
						{if $category->description}
							<div class="cat_desc rte">
							{if Tools::strlen($category->description) > 350}
								<div id="category_description_short">{$description_short}</div>
								<div id="category_description_full" class="unvisible">{$category->description}</div>
								<a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
							{else}
								<div>{$category->description}</div>
							{/if}
							</div>
						{/if}
					</div>
				{else}
                  
					<!-- Category image
					<div class="content_scene_cat_bg">
						<h1 class="title_main_section category-name">
								<span>
									{strip}
										{$category->name|escape:'html':'UTF-8'}
										{if isset($categoryNameComplement)}
											{$categoryNameComplement|escape:'html':'UTF-8'}
										{/if}
									{/strip}
								</span>
							</h1>
						{if $category->description}
							<div class="cat_desc">
							{if Tools::strlen($category->description) > 350}
								<div id="category_description_short" class="rte">{$description_short}</div>
								<div id="category_description_full" class="unvisible rte">{$category->description}</div>
								<a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
							{else}
								<div class="rte">{$category->description}</div>
							{/if}
							</div>
						{/if}
						{if $category->id_image} 
							<img src="{$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}" class="img-responsive" alt=""/> 
						{/if}
					 </div-->
				  {/if}
			</div>
		{else}
		<!--removing heading from product listing page
        <h1 class="title_main_section category-name">
								<span>
									{strip}
										{$category->name|escape:'html':'UTF-8'}
										{if isset($categoryNameComplement)}
											{$categoryNameComplement|escape:'html':'UTF-8'}
										{/if}
									{/strip}
								</span>
			</h1>-->
		{/if}
		{*<h1 class="page-heading{if (isset($subcategories) && !$products) || (isset($subcategories) && $products) || !isset($subcategories) && $products} product-listing{/if}"><span class="cat-name">{$category->name|escape:'html':'UTF-8'}{if isset($categoryNameComplement)}&nbsp;{$categoryNameComplement|escape:'html':'UTF-8'}{/if}</span>{include file="$tpl_dir./category-count.tpl"}</h1>*}
        
        <!--For removing subcategories commented below code-->
	<!--	{if isset($subcategories)}
		{if (isset($display_subcategories) && $display_subcategories eq 1) || !isset($display_subcategories) }
	
		<div id="subcategories" class="subcategories">
			<p class="subcategory-heading">{l s='Subcategories'}</p>
			<ul class="clearfix subcategories__list">
			{foreach from=$subcategories item=subcategory}
				<li class="subcategories__item">
					<div class="subcategory__img-wrap">
						<a class="subcategories__item__link" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">
						{if $subcategory.id_image}
							<img class="subcategories__item__img replace-2x img-responsive" src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'medium_default')|escape:'html':'UTF-8'}" alt=""/>
						{else}
							<img class="subcategories__item__img replace-2x img-responsive" src="{$img_cat_dir}default-medium_default.jpg" alt=""/>
						{/if}
						<h5 class="subcategories__item__name">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'|truncate:350}</h5>
					{if $subcategory.description}
						{*<div class="cat_desc">{$subcategory.description}</div>*}
					{/if}
					</a>
					</div>
				</li>
			{/foreach}
			</ul>
		</div>
		{/if}
		{/if}-->
		{if $products}
			<div class="content_sortPagiBar clearfix">
				<div class="sortPagiBar clearfix">
					{*{include file="./product-compare.tpl"}*}
					{include file="./nbr-product-page.tpl"}
					{include file="./product-sort.tpl"}
				</div>
				<div class="top-pagination-content clearfix">
					{include file="$tpl_dir./pagination.tpl"}
				</div>
			</div>
			{include file="./product-list.tpl" products=$products}
			<div class="content_sortPagiBar clearfix bottom_pagi">
				<!--bottom sorting
                div class="sortPagiBar clearfix">
					{include file="./product-compare.tpl" paginationId='bottom'}
					{include file="./product-sort.tpl"}
					{*{include file="./nbr-product-page.tpl"}*}
				</div>
				<div class="bottom-pagination-content clearfix">
					{include file="./pagination.tpl" paginationId='bottom'}
				</div-->
			</div>
		{/if}{*
			{hook h='displayCustomBanners2'}*}
	{elseif $category->id}
		<p class="alert alert-warning">{l s='This category is currently unavailable.'}</p>
	{/if}
{/if}
