{if $MENU != ''}
	{if $MENU_SEARCH}
		<div class="sf-search noBack">
			<form class="searchbox" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="get">
				<div class="searchbox_inner">
					<input type="hidden" name="controller" value="search" />
					<input type="hidden" value="position" name="orderby"/>
					<input type="hidden" value="desc" name="orderway"/>
					<input type="text" name="search_query" placeholder="{l s='Search' mod='blocktopmenu'}" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" />
					<button type="submit" name="submit_search" class="button-search icon-search"></button>
				</div>
			</form>
		</div>
	{/if}
	<!-- Menu -->
	<div id="block_top_menu" class="sf-contener clearfix col-md-8 col-xs-12">
		{*<div class="cat-title">{l s="Categories" mod="blocktopmenu"}</div>*}
		<ul class="sf-menu clearfix menu-content">
			{$MENU}
		</ul>
	</div>
	<!--/ Menu -->
{/if}