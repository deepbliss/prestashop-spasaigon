<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'Blocco info utente';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_970a31aa19d205f92ccfd1913ca04dc0'] = 'Aggiungi un blocco che visualizza le informazioni sul cliente';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_0c3bf3014aafb90201805e45b5e62881'] = 'Vedi il mio carrello';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_a85eba4c6c699122b2bb1387ea4813ad'] = 'Carrello';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_deb10517653c255364175796ace3553f'] = 'Prodotto';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_068f80c7519d0528fb08e82137a72131'] = 'Prodotti';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(vuoto)';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_2cbfb6731610056e1d0aaacde07096c1'] = 'Vedi il mio account';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Il tuo account';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_83218ac34c1834c26781fe4bde918ee4'] = 'Benvenuto';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_4b877ba8588b19f1b278510bf2b57ebb'] = 'Fammi uscire';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_c87aacf5673fada1108c9f809d354311'] = 'Esci';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_d4151a9a3959bdd43690735737034f27'] = 'Accedi al tuo account cliente';
$_MODULE['<{blockuserinfo}cosmetico>blockuserinfo_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Entra';
$_MODULE['<{blockuserinfo}cosmetico>nav_2cbfb6731610056e1d0aaacde07096c1'] = 'Vedi il mio account';
$_MODULE['<{blockuserinfo}cosmetico>nav_4b877ba8588b19f1b278510bf2b57ebb'] = 'Fammi uscire';
$_MODULE['<{blockuserinfo}cosmetico>nav_c87aacf5673fada1108c9f809d354311'] = 'Esci';
$_MODULE['<{blockuserinfo}cosmetico>nav_d4151a9a3959bdd43690735737034f27'] = 'Accedi al tuo account cliente';
$_MODULE['<{blockuserinfo}cosmetico>nav_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Entra';
$_MODULE['<{blockuserinfo}cosmetico>nav_09a763df3edf590f70369bf8f7b65222'] = 'Elenco delle informazioni link utente';
$_MODULE['<{blockuserinfo}cosmetico>nav_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'il mio conto';
$_MODULE['<{blockuserinfo}cosmetico>nav_7ec9cceb94985909c6994e95c31c1aa8'] = 'Wishlist';
$_MODULE['<{blockuserinfo}cosmetico>nav_9f7b857dbd2834e6dbae6d8de2611789'] = 'Wishlist';
$_MODULE['<{blockuserinfo}cosmetico>nav_0c3bf3014aafb90201805e45b5e62881'] = 'Il mio carrello';
$_MODULE['<{blockuserinfo}cosmetico>nav_b75443a19207ed3a3552edda86536857'] = 'Carrello';
$_MODULE['<{blockuserinfo}cosmetico>nav_4394c8d8e63c470de62ced3ae85de5ae'] = 'Uscire';
$_MODULE['<{blockuserinfo}cosmetico>nav_99dea78007133396a7b8ed70578ac6ae'] = 'Login';
