<!-- Block user information module NAV  -->
<div class="header_user_info  mobile-view">
	<label class="drop_links icon-user" for="state-user-2">
		<i class="drop-icon icon-caret-down"></i>
	</label>
	<input id="state-user-2" type="checkbox" class="not-styling"/>
	<ul class="drop_content_user" class="row">
		<li class="user-info__item item-account">
			<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account icon-user" rel="nofollow"><span>{l s='My account' mod='blockuserinfo'}</span></a>
		</li>
		<li class="user-info__item">
			<a class="wish_link icon-heart" href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='My wishlists' mod='blockuserinfo'}">
			{l s='Wish lists' mod='blockuserinfo'}
			</a>
		</li>
		<li class="user-info__item">
			<a href="{$link->getPageLink($order_process, true)|escape:'html':'UTF-8'}" title="{l s='View my shopping cart' mod='blockuserinfo'}" rel="nofollow" class="shop_cart_user icon-shopping-cart">
			{l s='Shopping Cart' mod='blockuserinfo'}
			</a>
		</li>
		<li class="user-info__item log">
			{if $is_logged}
				<a class="logout log icon-sign-out" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
					{l s='Log out' mod='blockuserinfo'}
				</a>
			{else}
				<a class="login log icon-key" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
					{l s='Login' mod='blockuserinfo'}
				</a>
			{/if}
		</li>
	</ul>
</div>
<!-- /Block usmodule NAV -->
