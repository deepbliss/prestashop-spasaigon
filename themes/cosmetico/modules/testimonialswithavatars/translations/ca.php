<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_903c194cc3db26fefde75efdded1edbc'] = 'Testimonis amb avatars';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_0064ea4f9d171db6ab86d5cd7a19f1a0'] = 'Testimonis amb uploadable avatars i de qualificació.';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_d2d72c90bb88220a1e17c2eae0b8c4db'] = 'Taula de base de dades no va ser instal · lat correctament';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_499322717c2e3272efd40d1e62fce681'] = 'Configuració de les dades no era guardat correctament';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_bfb8cecbc23205e1dc83fc9808e931a1'] = 'Els ganxos no es van asssigned correctament';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_30e2cc87132004f90fee18ac974e2281'] = 'S\'ha produït un error mentre es prepara per defecte de les imatges';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_6ac2cabe9402ea3042b1036897fae83b'] = 'Usuari frienly URL';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_8c7155a68de87e28a9f34ae7fe80cbb3'] = 'Utilitzades per a la pàgina de testimonis';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_bad743e6b5a71dca5b951e94fb17813d'] = 'Correu electrònic per a notificacions';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_4267df868e6750f472702506f5aed149'] = 'Deixeu-ho buit si voleu desactivar les notificacions';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_60dc1ba73f433b6aaee17108a1423584'] = 'Nombre de qualificació sars';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_77c1ec5e187e8e698a778b462b7f4499'] = 'Ús de 0 a desactivar qualificació';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_8de609fcd6362e3f757732ffe0a63683'] = 'Qualificació símbol';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_5eb2caca74bb8e00b8d4d5442a44a638'] = 'Max personatges en revisió';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_f394b90c7d42d46440595bb44b49b1af'] = 'Interval de temps entre els comentaris';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_e1b019b659cc281a03e51315d08967ac'] = 'Valor d\'entrada en qüestió de segons. Per exemple, si voleu permetre que els clients publicar el segon lloc després de 24 hores, d\'entrada aquest número: 86400';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_3765b99341991f9e12e6b70870e81f38'] = 'Permetre HTML en el front-offce?';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_5e4d5df59236c3d76e5a0abfeab1cd21'] = 'negreta, cursiva, subratllat i cares d\'estat d\'ànim';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_ea623486b149a03caacbc52cde79699a'] = 'Nous posts publicats a l\'instant?';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_50c4760d559c1f628f553d482ff65bd9'] = 'Si es defineix a SÍ, usuari missatges es publicaran a l\'instant, en cas contrari es farà només després de la seva aprovació';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_eddb3294ad19b14ca5e7a9b9c912211f'] = 'Nombre de missatges visibles';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_365119d429fc230c8e9b5c3c94842c19'] = 'Pantalla tipus';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_225bf3d9290b5f536b2e442259e78652'] = 'Cavallets';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_5174d1309f275ba6f275db3af9eb3e18'] = 'Graella';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_3f16cf037b71f365559773d1e74ca41c'] = 'Simple llista';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_01fda57aa6c7e9f07f5aa36b108e95cb'] = 'Ordenar per';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_a92faaeaca0d7d9a7a067c8159f16e11'] = 'Forçat de les posicions';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_85de9a3fcc45082345f6cfe7c8c98e0f'] = 'Data afegit';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_64663f4646781c9c0110838b905daa23'] = 'L\'atzar';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_248336101b461380a4b2391a7625493d'] = 'Salvat';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_6eab35de98861cb638cf72751a404a05'] = 'Valor incorrecte';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_6609dd239e143d00002123326c5e4aad'] = 'Alguna cosa ha anat malament';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_42084a9c15b730c62bfa38dadf7ebc9c'] = 'No pots publicar missatges perquè sovint';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_76f4eb0a24b07260fca07c260323529a'] = 'Ordenació fallat';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_4639535533b580ec40681c150f324c00'] = 'Nova revisió de presentar';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_454c7c4d16d25425b693202d14502fa0'] = 'Si us plau, ompliu aquest camp';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_0da39117166c24f53b9ccc9c26110e6b'] = 'Max personatges limit exceeded';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_7773984d8fadf94d152e4e94a27b965d'] = 'El fitxer no existeix';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_43a05ccc9c67a0e787bb061e47318ec2'] = 'Imatge dimentions no podia ser definit';
$_MODULE['<{testimonialswithavatars}cosmetico>testimonialswithavatars_ed630d1c37212e8cecc2790a1fb36eb9'] = 'No suficient memòria per a processar la imatge';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_6f9970727831d5815b65ef5af01527bf'] = 'Opcions de visualització per a diferents hooks';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_04abd16ae777606f5bdf9a953f123ae8'] = 'Testimonis de la pàgina';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_ba67e1148665270ecfbf9215b00f56c7'] = 'Per tal de mostrar aquest ganxo, utilitzeu el següent codi';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_aabdb03cda3c0213dfd6c27972cec8aa'] = 'Podeu inserir el codi en qualsevol lloc que vulgueu - se directament a qualsevol àrea editable (CMS pàgina, la descripció del producte, etc.), o en qualsevol tpl fitxer';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_15f138542517985950f9d3782effaa29'] = 'Utilitzar aquest ganxo';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_93cba07454f06a4a960172bbd6e2a435'] = 'Sí';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_de62775a71fc2bf7a13d7530ae24a7ed'] = 'Configuració General';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_882fb2e36d34a8cbacdf184efe047b4c'] = 'Llista de comentaris';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_7e129c6b2a469b6523013703fcb1db1c'] = 'Carrega Més';
$_MODULE['<{testimonialswithavatars}cosmetico>configure_9e8a0bc76fc409ee81f26ece7962bb37'] = 'Això és tot';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_602c70ce92350566afe4475fbe64f486'] = 'Arrossegar-lo';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_967e2c53868b83102fa97c0dfe2d49ef'] = 'Publicat a %s';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_7dce122004969d56ae2e0245cb754d35'] = 'Edita';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_ea4788705e6873b424c65e91c2846b19'] = 'Cancel·la';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_f2a6c498fb90ee345d997f888fce3b18'] = 'Esborrar';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_320b39251600fbebe992446192156507'] = 'Activar/Desactivar';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_c7892ebbb139886662c6f2fc8c450710'] = 'Tema';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_dda9c06f33071c9b6fc237ee164109d8'] = 'Qualificació';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_44749712dbec183e983dcd78a7736c41'] = 'Data';
$_MODULE['<{testimonialswithavatars}cosmetico>post-list_f15c1cae7882448b3fb0404682e17e61'] = 'Contingut';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_13ea583f6474cdce6c0c9b289b652ffe'] = 'Testimonis';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_b24344b6a2c4a94c9b0d0ebd4602aeca'] = 'Veure més';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_ef61fb324d729c341ea8ab9901e23566'] = 'Afegir nou';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_e9798e2562816917813c3064c5d4e56b'] = 'Gràcies per la ressenya! Serà publicat a la dreta després de la seva aprovació';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_1b1250190c5f7678fc7f50cc48713887'] = 'Pujar el teu avatar';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_614cffa523202658a898e34a5d94d05e'] = 'El Teu Nom';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_a41ca0a0f501b04880e59d924d3701c3'] = 'Post subject';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_fd5f26ebf9f1b469e3a52fbb4bf11842'] = 'Post text';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_1383580b72c68136773cc9b7fa56f042'] = 'Deixa la teva valoració';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_e0aa021e21dddbd6d8cecec71e9cf564'] = 'D\'ACORD';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_hook_13ea583f6474cdce6c0c9b289b652ffe'] = 'Testimonis';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_hook_0b4db271fc4624853e634ef6882ea8be'] = 'Veure tots els';
$_MODULE['<{testimonialswithavatars}cosmetico>twa_hook_66a9b0d3210e5127ba68ebb4e3ee8bf4'] = 'El que la gent diu';
