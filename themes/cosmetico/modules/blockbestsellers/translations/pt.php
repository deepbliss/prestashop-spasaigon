<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_9862f1949f776f69155b6e6b330c7ee1'] = 'Top-sellers bloco';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_ed6476843a865d9daf92e409082b76e1'] = 'Adiciona um bloco que mostra os produtos mais vendidos na loja.';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_c888438d14855d7d96a2724ee9c306bd'] = 'Definições atualizadas';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_f4f70727dc34561dfde1a3c529b6205c'] = 'Definições';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_26986c3388870d4148b1b5375368a83d'] = 'Produtos a mostrar';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_2b21378492166b0e5a855e2da611659c'] = 'Indique o número de produtos a mostrar neste bloco';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_24ff4e4d39bb7811f6bdf0c189462272'] = 'Mostrar sempre este bloco';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_84b0c5fdef19ab8ef61cd809f9250d85'] = 'Mostrar o bloco mesmo que não existam produtos mais vendidos.';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Ativo';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_b9f5c797ebbf55adccdd8539a65a0241'] = 'Desativado';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers-home_09a5fe24fe0fc9ce90efc4aa507c66e7'] = 'Não existem produtos mais vendidos neste momento.';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_1d0a2e1f62ccf460d604ccbc9e09da95'] = 'Ver produtos mais vendidos';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_3cb29f0ccc5fd220a97df89dafe46290'] = 'Mais Vendidos';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_eae99cd6a931f3553123420b16383812'] = 'Todos os mais vendidos';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_f7be84d6809317a6eb0ff3823a936800'] = 'Sem produtos mais vendidos neste momento';
$_MODULE['<{blockbestsellers}cosmetico>tab_d7b2933ba512ada478c97fa43dd7ebe6'] = 'Mais Vendidos';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_2d8ee3c32fffdf39872b844a50f08ad9'] = 'Melhor preço';
$_MODULE['<{blockbestsellers}cosmetico>blockbestsellers_66e6708dd7144b48efa2735da93f4051'] = 'Todos os melhores';
$_MODULE['<{blockbestsellers}cosmetico>tab_68ef004de6166492c1d668eb8efe09bd'] = 'Melhor';
