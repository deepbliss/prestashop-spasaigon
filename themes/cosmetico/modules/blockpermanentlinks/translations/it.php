<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-footer_5813ce0ec7196c492c97596718f71969'] = 'Mappa del sito';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-footer_bbaff12800505b22a853e8b7f4eb6a22'] = 'Contatto';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-footer_714814d37531916c293a8a4007e8418c'] = 'Segna questa pagina.';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-header_2f8a6bf31f3bd67bd2d9720c58b19c9a'] = 'Contattaci';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-header_e1da49db34b0bdfdddaba2ad6552f848'] = 'Mappa del sito';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-header_fad9383ed4698856ed467fd49ecf4820'] = 'segnalibro';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks_39355c36cfd8f1d048a1f84803963534'] = 'Blocco link permanenti';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks_6c5b993889148d10481569e55f8f7c6d'] = 'Aggiunge un blocco che visualizza i link permanenti come mappa del sito, contatti, ecc.';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks_c46000dc35dcefd6cc33237fa52acc48'] = 'Mappa del negozio';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks_5813ce0ec7196c492c97596718f71969'] = 'Mappa del sito';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks_c661cf76442d8d2cb318d560285a2a57'] = 'Modulo di contatto';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks_bbaff12800505b22a853e8b7f4eb6a22'] = 'Contatto';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks_2d29fbe96aaeb9737b355192f4683690'] = 'Aggiungi ai preferiti';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-footer_13348442cc6a27032d2b4aa28b75a5d3'] = 'Ricerca';
$_MODULE['<{blockpermanentlinks}cosmetico>blockpermanentlinks-footer_5f30702c50f64d0a952d11470cdd9447'] = 'Pagamento sicuro';
