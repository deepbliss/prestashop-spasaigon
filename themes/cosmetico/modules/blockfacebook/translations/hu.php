<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockfacebook}cosmetico>blockfacebook_43d541d80b37ddb75cde3906b1ded452'] = 'Facebook, Mint a Box blokk';
$_MODULE['<{blockfacebook}cosmetico>blockfacebook_e2887a32ddafab9926516d8cb29aab76'] = 'Megjelenik egy blokk az előfizető a Facebook Oldal.';
$_MODULE['<{blockfacebook}cosmetico>blockfacebook_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Konfiguráció frissítve';
$_MODULE['<{blockfacebook}cosmetico>blockfacebook_f4f70727dc34561dfde1a3c529b6205c'] = 'Beállítások';
$_MODULE['<{blockfacebook}cosmetico>blockfacebook_c98cf18081245e342d7929c117066f0b'] = 'Facebook hivatkozás (teljes URL kell)';
$_MODULE['<{blockfacebook}cosmetico>blockfacebook_c9cc8cce247e49bae79f15173ce97354'] = 'Mentés';
$_MODULE['<{blockfacebook}cosmetico>blockfacebook_374fe11018588d3d27e630b2dffb7909'] = 'Kövess minket Facebook-on';
$_MODULE['<{blockfacebook}cosmetico>preview_31fde7b05ac8952dacf4af8a704074ec'] = 'Előnézet';
$_MODULE['<{blockfacebook}cosmetico>preview_374fe11018588d3d27e630b2dffb7909'] = 'Kövess minket Facebook-on';
