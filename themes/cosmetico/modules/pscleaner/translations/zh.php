<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pscleaner}cosmetico>pscleaner_e5a8af934462c05509c7de5f2f2c18a3'] = 'PrestaShop清洁';
$_MODULE['<{pscleaner}cosmetico>pscleaner_4bcb9cc248b7f6c8dc7f5c323bde76de'] = '检查和修复功能完整性的限制和消除默认数据';
$_MODULE['<{pscleaner}cosmetico>pscleaner_752369f18aebeed9ae8384d8f1b5dc5e'] = '非常小心这个工具，就不可能取得成就后出现后退!';
$_MODULE['<{pscleaner}cosmetico>pscleaner_550b877b1a255ba717cfad4b82057731'] = '以下提出的询问成功地固定破碎的数据：';
$_MODULE['<{pscleaner}cosmetico>pscleaner_14a7ab23d566b4505d0c711338c19a08'] = '%d线(s)';
$_MODULE['<{pscleaner}cosmetico>pscleaner_d1ff3c9d57acd4283d2793a36737479e'] = '没什么需要固定';
$_MODULE['<{pscleaner}cosmetico>pscleaner_53d097f11855337bb74f1444d6c47c99'] = '以下提出的询问成功地给你清理数据库：';
$_MODULE['<{pscleaner}cosmetico>pscleaner_098c3581a731f08d24311bbf515adbbb'] = '没什么，需要清理';
$_MODULE['<{pscleaner}cosmetico>pscleaner_1bb7c5eb8682aeada82c407b40ec09c8'] = '目录短员仲裁';
$_MODULE['<{pscleaner}cosmetico>pscleaner_ed6ecb7169d5476ef5251524bb17552a'] = '令和短员仲裁的客户';
$_MODULE['<{pscleaner}cosmetico>pscleaner_43364f357f96e8b70be4a44d44196807'] = '请读的声明和点\"是\"上';
$_MODULE['<{pscleaner}cosmetico>pscleaner_6c69628e1d57fa6e39162b039a82133b'] = '你确定要删除所有目录的数据吗?';
$_MODULE['<{pscleaner}cosmetico>pscleaner_6a68264705f23c8e3d505fd2c93a87ba'] = '你确定要删除所有销售数据吗?';
$_MODULE['<{pscleaner}cosmetico>pscleaner_c32516babc5b6c47eb8ce1bfc223253c'] = '分类';
$_MODULE['<{pscleaner}cosmetico>pscleaner_da69e50b7440e12fe63287904819eaa3'] = '我的理解是，所有目录的数据将被取消，而不可能减少造成的：产品的特点、职类、标签、形象、价格、附加物、现场储存的属性团体和价值观、制造商、供应商...';
$_MODULE['<{pscleaner}cosmetico>pscleaner_00d23a76e43b46dae9ec7aa9dcbebb32'] = '启用';
$_MODULE['<{pscleaner}cosmetico>pscleaner_b9f5c797ebbf55adccdd8539a65a0241'] = '禁用';
$_MODULE['<{pscleaner}cosmetico>pscleaner_b2d7c99e984831bd36221baf34e9c26e'] = '删除目录';
$_MODULE['<{pscleaner}cosmetico>pscleaner_3300d0bf086fa38cf593fe4feff351f1'] = '订单和客户';
$_MODULE['<{pscleaner}cosmetico>pscleaner_a01f9a9a340c3c68a2dc4663f46d8637'] = '我的理解是，所有的命令以及小额供资教育网络的顾客将是消除了不可能减少造成的：顾客、货运马车、命令、关系，客人信息、统计数据...';
$_MODULE['<{pscleaner}cosmetico>pscleaner_17ca7f22baf84821b6b73462c96fb1e3'] = '删除的命令&amp;客户';
$_MODULE['<{pscleaner}cosmetico>pscleaner_3535aa31bd9005bde626ad4312b67d6b'] = '功能完整性的制约因素';
$_MODULE['<{pscleaner}cosmetico>pscleaner_e84c6595e849214a70b35ed8f95d7d16'] = '看看&amp;修好';
$_MODULE['<{pscleaner}cosmetico>pscleaner_ccc27439e3e08c444690af3bed668e2d'] = '数据库的清洁';
$_MODULE['<{pscleaner}cosmetico>pscleaner_39707b9cfefe433d64f695623d2d3fd7'] = '清洁&amp;优化';
