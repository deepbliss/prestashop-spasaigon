<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{loremipsum}cosmetico>loremipsum_d8acb9272677b376f85fde36b8a3e762'] = 'LoremIpsum';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bc3439422360594e977f63ad3fea77b3'] = 'Combler les vides des descriptions de produits avec un bruit de «Lorem Ipsum» texte';
$_MODULE['<{loremipsum}cosmetico>loremipsum_876f23178c29dc2552c0b48bf23cd9bd'] = 'Êtes-vous sûr de vouloir désinstaller?';
$_MODULE['<{loremipsum}cosmetico>loremipsum_dae8ace18bdcbcc6ae5aece263e14fe8'] = 'Options';
$_MODULE['<{loremipsum}cosmetico>loremipsum_c5486bc90fea37dc964c30e17c334880'] = 'Si le prix est de 0, jeu de hasard';
$_MODULE['<{loremipsum}cosmetico>loremipsum_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Pas de';
$_MODULE['<{loremipsum}cosmetico>loremipsum_b9bbefd932cceeaf33d6ab76c1c26350'] = 'Prix Minimum';
$_MODULE['<{loremipsum}cosmetico>loremipsum_e6d1a870c9dea4bc70be18c6d3f357d4'] = 'Prix Maximum';
$_MODULE['<{loremipsum}cosmetico>loremipsum_3088e091d0cd9798dfb547becc545faf'] = 'Certain nombre de paragraphes pour une complète description de l\'élément';
$_MODULE['<{loremipsum}cosmetico>loremipsum_fe111b79fdbd95a88b6825da86b16154'] = 'Nombre de condamnations pour une courte description de l\'élément';
$_MODULE['<{loremipsum}cosmetico>loremipsum_2807574e69e5588dd6fae6883ca3c3d2'] = 'Lancer la numérisation';
