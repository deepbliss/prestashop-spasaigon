<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{loremipsum}cosmetico>loremipsum_d8acb9272677b376f85fde36b8a3e762'] = 'LoremIpsum';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bc3439422360594e977f63ad3fea77b3'] = 'Töltse ki az üres termék leírások a generált \"Lorem Ipsum\" szöveg,';
$_MODULE['<{loremipsum}cosmetico>loremipsum_876f23178c29dc2552c0b48bf23cd9bd'] = 'Biztos, hogy azt akarod, hogy távolítsa el?';
$_MODULE['<{loremipsum}cosmetico>loremipsum_dae8ace18bdcbcc6ae5aece263e14fe8'] = 'Lehetőségek';
$_MODULE['<{loremipsum}cosmetico>loremipsum_c5486bc90fea37dc964c30e17c334880'] = 'Ha az ár 0, állítsa be a véletlenszerű';
$_MODULE['<{loremipsum}cosmetico>loremipsum_93cba07454f06a4a960172bbd6e2a435'] = 'Igen';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nem';
$_MODULE['<{loremipsum}cosmetico>loremipsum_b9bbefd932cceeaf33d6ab76c1c26350'] = 'Minimum ár';
$_MODULE['<{loremipsum}cosmetico>loremipsum_e6d1a870c9dea4bc70be18c6d3f357d4'] = 'Maximális ár';
$_MODULE['<{loremipsum}cosmetico>loremipsum_3088e091d0cd9798dfb547becc545faf'] = 'Bekezdések száma a teljes tétel leírása';
$_MODULE['<{loremipsum}cosmetico>loremipsum_fe111b79fdbd95a88b6825da86b16154'] = 'Számos mondatokat, rövid tétel leírása';
$_MODULE['<{loremipsum}cosmetico>loremipsum_2807574e69e5588dd6fae6883ca3c3d2'] = 'Keresés elindításához';
