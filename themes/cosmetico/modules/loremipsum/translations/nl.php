<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{loremipsum}cosmetico>loremipsum_d8acb9272677b376f85fde36b8a3e762'] = 'LoremIpsum';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bc3439422360594e977f63ad3fea77b3'] = 'Vul lege product beschrijvingen gegenereerd met \"Lorem Ipsum\" tekst';
$_MODULE['<{loremipsum}cosmetico>loremipsum_876f23178c29dc2552c0b48bf23cd9bd'] = 'Weet u zeker dat u wilt verwijderen?';
$_MODULE['<{loremipsum}cosmetico>loremipsum_dae8ace18bdcbcc6ae5aece263e14fe8'] = 'Opties';
$_MODULE['<{loremipsum}cosmetico>loremipsum_c5486bc90fea37dc964c30e17c334880'] = 'Als prijs 0 is ingesteld op willekeurig';
$_MODULE['<{loremipsum}cosmetico>loremipsum_93cba07454f06a4a960172bbd6e2a435'] = 'Ja';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Geen';
$_MODULE['<{loremipsum}cosmetico>loremipsum_b9bbefd932cceeaf33d6ab76c1c26350'] = 'Minimum prijs';
$_MODULE['<{loremipsum}cosmetico>loremipsum_e6d1a870c9dea4bc70be18c6d3f357d4'] = 'Maximale prijs';
$_MODULE['<{loremipsum}cosmetico>loremipsum_3088e091d0cd9798dfb547becc545faf'] = 'Aantal leden voor het volledige item beschrijving';
$_MODULE['<{loremipsum}cosmetico>loremipsum_fe111b79fdbd95a88b6825da86b16154'] = 'Aantal zinnen voor een kort item beschrijving';
$_MODULE['<{loremipsum}cosmetico>loremipsum_2807574e69e5588dd6fae6883ca3c3d2'] = 'Starten met scannen';
