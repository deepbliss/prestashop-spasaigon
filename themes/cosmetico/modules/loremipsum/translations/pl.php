<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{loremipsum}cosmetico>loremipsum_d8acb9272677b376f85fde36b8a3e762'] = 'LoremIpsum';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bc3439422360594e977f63ad3fea77b3'] = 'Wypełnić puste opisy produktów z utworzonymi \"spółka \" koordynator\" tekst';
$_MODULE['<{loremipsum}cosmetico>loremipsum_876f23178c29dc2552c0b48bf23cd9bd'] = 'Jesteś pewien, że chcesz usunąć?';
$_MODULE['<{loremipsum}cosmetico>loremipsum_dae8ace18bdcbcc6ae5aece263e14fe8'] = 'Opcje';
$_MODULE['<{loremipsum}cosmetico>loremipsum_c5486bc90fea37dc964c30e17c334880'] = 'Jeśli cena 0, wartość przypadkową';
$_MODULE['<{loremipsum}cosmetico>loremipsum_93cba07454f06a4a960172bbd6e2a435'] = 'Tak';
$_MODULE['<{loremipsum}cosmetico>loremipsum_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nie';
$_MODULE['<{loremipsum}cosmetico>loremipsum_b9bbefd932cceeaf33d6ab76c1c26350'] = 'Cena minimalna';
$_MODULE['<{loremipsum}cosmetico>loremipsum_e6d1a870c9dea4bc70be18c6d3f357d4'] = 'Cena maksymalna';
$_MODULE['<{loremipsum}cosmetico>loremipsum_3088e091d0cd9798dfb547becc545faf'] = 'Ilość punktów dla pełnego opisu elementu';
$_MODULE['<{loremipsum}cosmetico>loremipsum_fe111b79fdbd95a88b6825da86b16154'] = 'Liczba ofert za Krótki opis produktu';
$_MODULE['<{loremipsum}cosmetico>loremipsum_2807574e69e5588dd6fae6883ca3c3d2'] = 'Uruchom proces skanowania';
