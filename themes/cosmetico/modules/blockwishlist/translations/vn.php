<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockwishlist}cosmetico>blockwishlist-extra_2d96bb66d8541a89620d3c158ceef42b'] = 'Thêm vào danh sách yêu thích';
$_MODULE['<{blockwishlist}cosmetico>blockwishlist_9ae79c1fccd231ac7fbbf3235dbf6326'] = 'Danh sách mong muốn ';
$_MODULE['<{blockwishlist}cosmetico>blockwishlist_7ec9cceb94985909c6994e95c31c1aa8'] = 'Danh sách mong muốn ';
$_MODULE['<{blockwishlist}cosmetico>blockwishlist_button_2d96bb66d8541a89620d3c158ceef42b'] = 'Thêm vào danh sách yêu thích';
$_MODULE['<{blockwishlist}cosmetico>blockwishlist_button_15b94c64c4d5a4f7172e5347a36b94fd'] = 'Danh sách mong muốn ';
$_MODULE['<{blockwishlist}cosmetico>blockwishlist_button_6a5373df703ab2827a4ba7facdfcf779'] = 'Thêm vào danh sách yêu thích';
$_MODULE['<{blockwishlist}cosmetico>my-account_7ec9cceb94985909c6994e95c31c1aa8'] = 'Danh sách mong muốn ';
$_MODULE['<{blockwishlist}cosmetico>mywishlist_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Tài khoản của tôi';
$_MODULE['<{blockwishlist}cosmetico>mywishlist_7ec9cceb94985909c6994e95c31c1aa8'] = 'Danh sách mong muốn ';
$_MODULE['<{blockwishlist}cosmetico>mywishlist_06c335f27f292a096a9bf39e3a58e97b'] = 'Danh sách mong muốn mới';
$_MODULE['<{blockwishlist}cosmetico>mywishlist_49ee3087348e8d44e1feda1917443987'] = 'Tên';
$_MODULE['<{blockwishlist}cosmetico>mywishlist_c9cc8cce247e49bae79f15173ce97354'] = 'Tiết kiệm';
$_MODULE['<{blockwishlist}cosmetico>mywishlist_0b3db27bc15f682e92ff250ebb167d4b'] = 'trở lại tài khoản của bạn';
$_MODULE['<{blockwishlist}cosmetico>mywishlist_8cf04a9734132302f96da8e113e80ce5'] = 'Trang chủ';
$_MODULE['<{blockwishlist}cosmetico>view_2d0f6b8300be19cf35e89e66f0677f95'] = 'Thêm vào giỏ hàng';
$_MODULE['<{blockwishlist}cosmetico>managewishlist_2d0f6b8300be19cf35e89e66f0677f95'] = 'Thêm vào giỏ hàng';
$_MODULE['<{blockwishlist}cosmetico>view_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Tài khoản của tôi';
$_MODULE['<{blockwishlist}cosmetico>view_7ec9cceb94985909c6994e95c31c1aa8'] = 'Danh sách mong muốn ';
