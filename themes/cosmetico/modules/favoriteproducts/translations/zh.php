<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts_c249aeb21294d5e97598462b550e73eb'] = '最喜欢的产品';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts_018770f4456d82ec755cd9d0180e4cce'] = '展示一页介绍的客户喜欢的产品。';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-account_d95cf4ab2cbf1dfb63f066b50558b07d'] = '我的帐户';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-account_e5090b68524b1bbfd7983bfa9500b7c9'] = '我最喜欢的产品。';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-account_44fd621c6504b8a5346946650682a842'] = '没有最喜欢的产品已经确定了。 ';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-account_6f16227b3e634872e87b0501948310b6'] = '回你的账户。';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-extra_8128d66be9351da562f9b01f591e00cd'] = '添加此商品到我的收藏夹';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-extra_b895a379bd5ea51680ead24ee13a1bb7'] = '从收藏夹删除此商品';
$_MODULE['<{favoriteproducts}cosmetico>my-account_e5090b68524b1bbfd7983bfa9500b7c9'] = '我最爱的商品';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-account_a34e0796b9c15d09300f67d972379722'] = '我最爱的商品';
$_MODULE['<{favoriteproducts}cosmetico>favoriteproducts-account_a958dbb46713e59856c35f89e5092a5e'] = '回到你的帐户';
$_MODULE['<{favoriteproducts}cosmetico>my-account_a34e0796b9c15d09300f67d972379722'] = '我最爱的商品';
