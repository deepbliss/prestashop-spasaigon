<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssales}cosmetico>statssales_45c4b3e103155326596d6ccd2fea0f25'] = '销售与订单';
$_MODULE['<{statssales}cosmetico>statssales_d2fb07753354576172a2b144c373a610'] = '增加的图表介绍的演变，出售品编号和命令的统计数据的仪表板上';
$_MODULE['<{statssales}cosmetico>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = '向导';
$_MODULE['<{statssales}cosmetico>statssales_bdaa0cab56c2880f8f60e6a2cef40e63'] = '关于秩序状态';
$_MODULE['<{statssales}cosmetico>statssales_fdfa8599f3887bef99e9572f3611260f'] = '在你回办公室，你可以改变的顺序如下状况：有待检查的付款、付款接受，准备工作的进展、运输、交付、取消退款，支付错误，股票和等待银行的电汇付款。';
$_MODULE['<{statssales}cosmetico>statssales_4b75384caa4e6830c22f15e06e0bfac0'] = '这些秩序的的状态不可能消除来自后面的办公室；但是你的选择增多。';
$_MODULE['<{statssales}cosmetico>statssales_ddb21e1caa84c463bc744c412a7b05f5'] = '下列图表所代表的的发展演变你的店的命令和销售营业额的一些选定的期间。';
$_MODULE['<{statssales}cosmetico>statssales_ef9c3c65723819a9c183d857a39ff403'] = '你应该经常进行协商这部电影，因为它允许你们快点监测你的店的可持续性。 这也让你来监测多个时期。';
$_MODULE['<{statssales}cosmetico>statssales_5cc6f5194e3ef633bcab4869d79eeefa'] = '唯一有效的命令就是生动的代表。';
$_MODULE['<{statssales}cosmetico>statssales_c3987e4cac14a8456515f0d200da04ee'] = '所有国家';
$_MODULE['<{statssales}cosmetico>statssales_d7778d0c64b6ba21494c97f77a66885a'] = '过滤';
$_MODULE['<{statssales}cosmetico>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = '成功下单';
$_MODULE['<{statssales}cosmetico>statssales_156e5c5872c9af24a5c982da07a883c2'] = '成功购买产品';
$_MODULE['<{statssales}cosmetico>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'CSV导出';
$_MODULE['<{statssales}cosmetico>statssales_ec3e48bb9aa902ba2ad608547fdcbfdc'] = '出售品编号:';
$_MODULE['<{statssales}cosmetico>statssales_f6825178a5fef0a97dacf963409829f0'] = '你能认为分配秩序的的状态下。';
$_MODULE['<{statssales}cosmetico>statssales_da80af4de99df74dd59e665adf1fac8f'] = '期间没有订单';
$_MODULE['<{statssales}cosmetico>statssales_c58114720bcd52bfe96fd801cee77e93'] = '订单';
$_MODULE['<{statssales}cosmetico>statssales_c8be451a5698956a0e78b5c2caab4821'] = '产品买的';
$_MODULE['<{statssales}cosmetico>statssales_b52b44c9d23e141b067d7e83b44bb556'] = '产品';
$_MODULE['<{statssales}cosmetico>statssales_497a2a4cf0a780ff5b60a7a6e43ea533'] = '销售货币：%s';
$_MODULE['<{statssales}cosmetico>statssales_17833fb3783b26e0a9bc8b21ee85302a'] = '百分比单%的地位。';
