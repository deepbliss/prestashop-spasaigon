<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{autotranslator}cosmetico>autotranslator_d21eb4f0cf1b5354e99603ad5d152add'] = 'Автоматические переводы';
$_MODULE['<{autotranslator}cosmetico>autotranslator_4a206e35c4a907c8473bc40010b01c0c'] = 'Пожалуйста, введите ключ';
$_MODULE['<{autotranslator}cosmetico>autotranslator_068f80c7519d0528fb08e82137a72131'] = 'Продукты';
$_MODULE['<{autotranslator}cosmetico>autotranslator_775a3ab6add326ef93f2382f49f9e500'] = 'Группы атрибутов';
$_MODULE['<{autotranslator}cosmetico>autotranslator_287234a1ff35a314b5b6bc4e5828e745'] = 'Атрибуты';
$_MODULE['<{autotranslator}cosmetico>autotranslator_98f770b0af18ca763421bac22b4b6805'] = 'Особенности';
$_MODULE['<{autotranslator}cosmetico>autotranslator_5ad99668d69a36913c5c2cf5c8c9617c'] = 'Характеристика значения';
$_MODULE['<{autotranslator}cosmetico>autotranslator_af1b98adf7f686b84cd0b443e022b7a0'] = 'Категории';
$_MODULE['<{autotranslator}cosmetico>autotranslator_71b16ec913e2b2febe4542160717aaeb'] = 'CMS-страницы';
$_MODULE['<{autotranslator}cosmetico>autotranslator_40857405e3cb47e2aad653d218acc675'] = 'CMS категории';
$_MODULE['<{autotranslator}cosmetico>autotranslator_2377be3c2ad9b435ba277a73f0f1ca76'] = 'Производители';
$_MODULE['<{autotranslator}cosmetico>autotranslator_1814d65a76028fdfbadab64a5a8076df'] = 'Поставщики';
$_MODULE['<{autotranslator}cosmetico>autotranslator_f388bc3eaa4b9f72756a75693a12559d'] = 'Установленные модули';
$_MODULE['<{autotranslator}cosmetico>autotranslator_83915d1254927f41241e8630890bec6e'] = 'Темы';
$_MODULE['<{autotranslator}cosmetico>autotranslator_f40f0a146b015acb0b4c5f5e933d8055'] = 'Перевести';
$_MODULE['<{autotranslator}cosmetico>autotranslator_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Все';
$_MODULE['<{autotranslator}cosmetico>autotranslator_11a755d598c0c417f9a36758c3da7481'] = 'Стоп';
$_MODULE['<{autotranslator}cosmetico>autotranslator_49ee3087348e8d44e1feda1917443987'] = 'Название';
$_MODULE['<{autotranslator}cosmetico>autotranslator_e7187e855e77081f015200a88effeaf2'] = 'Никакие действия не выполнены';
$_MODULE['<{autotranslator}cosmetico>autotranslator_8e8ec4010ecadf61b5cde35798382499'] = 'Новый файл был создан: %s';
$_MODULE['<{autotranslator}cosmetico>autotranslator_a98025c398d91265ae9427e539422e37'] = '%d слов за %s секунд';
$_MODULE['<{autotranslator}cosmetico>autotranslator_f2692b1ded2db1e9ec8628b457ccc463'] = 'Класс %s не доступен';
$_MODULE['<{autotranslator}cosmetico>autotranslator_0144b1736895c4fcd0ccf099d07a78d4'] = '%s не может быть загружен';
$_MODULE['<{autotranslator}cosmetico>autotranslator_902b0d55fddef6f8d651fe1035b7d4bd'] = 'Ошибка';
$_MODULE['<{autotranslator}cosmetico>autotranslator_aee9784c03b80d38d3271cde2b252b8d'] = 'Неизвестная ошибка';
$_MODULE['<{autotranslator}cosmetico>autotranslator_85749614de5f612ca58d875b3317543b'] = 'Ошибка при попытке массового перевода';
$_MODULE['<{autotranslator}cosmetico>autotranslator_1128ad4d17119d287baa047d2e091730'] = 'расширения Curl необходим для автоматического перевода';
$_MODULE['<{autotranslator}cosmetico>autotranslator_a3c085f0d457ae1104bf59365beb675a'] = 'Каталог \"%s\" не может быть создан';
$_MODULE['<{autotranslator}cosmetico>content-types-form_04d1449f96136f5e8c02e93c2163f20c'] = 'Типы Контента';
$_MODULE['<{autotranslator}cosmetico>content-types-form_b73865c5d1977721411526d9cd7cb357'] = 'Ядро (без темы)';
$_MODULE['<{autotranslator}cosmetico>content-types-form_e0626222614bdee31951d84c64e5e9ff'] = 'Выберите';
$_MODULE['<{autotranslator}cosmetico>content-types-form_5705375c3527e4be741c5039993cb526'] = 'Перезаписать существующие переводы';
$_MODULE['<{autotranslator}cosmetico>key-form_656a6828d7ef1bb791e42087c4b5ee6e'] = 'API-ключ';
$_MODULE['<{autotranslator}cosmetico>key-form_e0aa021e21dddbd6d8cecec71e9cf564'] = 'ОК';
$_MODULE['<{autotranslator}cosmetico>key-form_70542a6ed3a7b9e3ef8be9cc44b68ba2'] = 'Получите бесплатный ключ';
$_MODULE['<{autotranslator}cosmetico>key-form_7a52e36bf4a1caa031c75a742fb9927a'] = 'Питание от';
