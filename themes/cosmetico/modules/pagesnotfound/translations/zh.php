<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_251295238bdf7693252f2804c8d3707e'] = '找不到此页';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_607cc8b8993662a37cac86032fb071d2'] = '增加一个标签的仪表板上的数据，显示的网页请求你的访客，有没有找到。';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_dc3a3db6b98723bf91f924537a630600'] = '\"页上找不到的\"库已经空无一人。';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_b323790d8ee3c43d317d19aea5012626'] = '\"页上找不到的\"藏匿处已被删除。';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_6602bbeb2956c035fb4cb5e844a4861b'] = '向导';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_3604249130acf7fda296e16edc996e5b'] = '404错误';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_675f1f46497aeeee35832a5d9d095976'] = '404错误是一个HTTP:错误的法典中，这意味着该文件要求的用户找不到了。 以你的情况这意味着你的访客进入了一个错误的统一资源定位地址在于解决酒吧或者你或另一个网站上有一个死去的联系。 在可能的情况下referrer显示了所以你能不能找到页数/址，其中载有该死的联系。 如果没有，一般来说，它意味着这是一个直接接触，所以可能是有人收藏了一个联系中已经不存在了';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_a90083861c168ef985bf70763980aa60'] = '如何察觉这些错误？';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_4f803d59ee120b11662027e049cba1f3'] = '如果你webhost支持。htaccess的档案，你就能创造一个，进入根目录的PrestaShop，并插入以下条线进行：\"%s\"这些词语。';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_07e7f83ae625fe216a644d09feab4573'] = '一个用户要求的网页，并不存在将转向下列网页:%s的。 这一单元记录的获得这个网页。';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_01bd0bf7c5a68ad6ee4423118be3f7b6'] = '你必须使用的。htaccess档案转404错误的\"404.php\"页。';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_193cfc9be3b995831c6af2fea6650e60'] = '页面';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_b6f05e5ddde1ec63d992d61144452dfa'] = '提交页面';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_64d129224a5377b63e9727479ec987d9'] = '计数器';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_4a7a7e7cda40454cee7ec247660f8017'] = '没有\"网页上找不到的\"问题登记。';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_d8847bc418fc4f5a3e37c2e8390bb9ed'] = '清空数据库';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_b9ae3636d6e672413a163f7cb34beb84'] = '空\"的网页没有找到\"通知的这一期间';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_0cf5c3a279c0e8c57b232d8c6bc3f06a'] = '空\"的网页没有找到\"通知的';
