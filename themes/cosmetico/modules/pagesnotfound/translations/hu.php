<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_251295238bdf7693252f2804c8d3707e'] = 'Nem található oldalak';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_607cc8b8993662a37cac86032fb071d2'] = 'A látogatók által lekért, de nem található oldalakról ad hozzá egy fület a Statisztika vezérlőpulthoz.';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_dc3a3db6b98723bf91f924537a630600'] = 'A nem megjeleníthető oldalak gyorsítótára ürítve lett.';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_b323790d8ee3c43d317d19aea5012626'] = 'Az \"oldal nem található\" gyorsítótár ürítve lett.';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_6602bbeb2956c035fb4cb5e844a4861b'] = 'Útmutató';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_3604249130acf7fda296e16edc996e5b'] = '404 hibák';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_675f1f46497aeeee35832a5d9d095976'] = 'A 404-es hiba HTTP-hiba kód, ami azt jelenti, hogy a fájl a felhasználó által kért nem található. Az ön esetében ez azt jelenti, hogy a látogató belépett egy rossz URL-címét a címsorba, vagy ön, vagy egy másik honlapján van egy halott link. Ha lehetséges, a hivatkozó látható, hogy megtalálja az oldal/oldal, amely tartalmazza a halott link. Ha nem, akkor ez általában azt jelenti, hogy ez egy közvetlen hozzáférés, szóval lehet, hogy valaki könyvjelzővel egy link, amely már nem létezik.';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_a90083861c168ef985bf70763980aa60'] = 'Hogyan tudom ezeket a hibákat elkapni?';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_4f803d59ee120b11662027e049cba1f3'] = 'Amennyiben a szolgáltatód támogatja a .htaccess fájlt, akkor azt létre tudod hozni a PrestaShop gyökérmappájában, melybe a következő sort kell beleírni: \"%s\".';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_07e7f83ae625fe216a644d09feab4573'] = 'Ha egy felhasználó egy nem létező oldalt kér le, akkor át lesz irányítva erre az oldalra: %s. A modul naplózza az oldalhozzáférést.';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_01bd0bf7c5a68ad6ee4423118be3f7b6'] = 'Hogy a 404 hibákat a \"404.php\" oldalra tudd irányítani, egy .htaccess fájlt kell használni.';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_193cfc9be3b995831c6af2fea6650e60'] = 'Oldal';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_b6f05e5ddde1ec63d992d61144452dfa'] = 'Beajánló';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_64d129224a5377b63e9727479ec987d9'] = 'Számláló';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_4a7a7e7cda40454cee7ec247660f8017'] = 'Egyelőre még nincs rögzített \"oldal nem található\" probléma.';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_d8847bc418fc4f5a3e37c2e8390bb9ed'] = 'Adatbázis kiürítése';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_b9ae3636d6e672413a163f7cb34beb84'] = 'MINDEN \"oldal nem található\" bejegyzés törlése erre az intervallumra';
$_MODULE['<{pagesnotfound}cosmetico>pagesnotfound_0cf5c3a279c0e8c57b232d8c6bc3f06a'] = 'MINDEN \"oldal nem található\" bejegyzés törlése';
