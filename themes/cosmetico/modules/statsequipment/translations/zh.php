<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsequipment}cosmetico>statsequipment_247270d410e2b9de01814b82111becda'] = '浏览器和操作系统';
$_MODULE['<{statsequipment}cosmetico>statsequipment_2876718a648dea03aaafd4b5a63b1efe'] = '在仪表盘添加一个关于Web浏览器和操作系统的使用图表标签。';
$_MODULE['<{statsequipment}cosmetico>statsequipment_6602bbeb2956c035fb4cb5e844a4861b'] = '向导';
$_MODULE['<{statsequipment}cosmetico>statsequipment_854c8e126f839cc861cde822b641230e'] = '确保你的网站上可以获得尽可能多的人';
$_MODULE['<{statsequipment}cosmetico>statsequipment_0d5f13106dec10bb8a9301541052278c'] = '时管理网站是很重要的跟踪软件使用的游客，以便能确保该网站展示的同样方式的每一个人。 PrestaShop的建立符合最近的网络浏览器和计算机操作系统(OS). 但是，因为你可能增加先进的特点你的网或甚至修改的核心PrestaShop法典》，添加这些词语可能不是人人可以使用的。 这就是为什么这是个好主意跟踪用户百分率的各类软件之前增加或改变的东西只有数量有限的用户将能够获得的。';
$_MODULE['<{statsequipment}cosmetico>statsequipment_11db1362a88c5e3e74c8f699c14d6798'] = '显示的百分比每个网络浏览器使用的客户。';
$_MODULE['<{statsequipment}cosmetico>statsequipment_998e4c5c80f27dec552e99dfed34889a'] = 'CSV导出';
$_MODULE['<{statsequipment}cosmetico>statsequipment_90c58bfe4872fc9ca7bf6a181c3e5edd'] = '显示的百分数每一个操作系统使用的客户。';
$_MODULE['<{statsequipment}cosmetico>statsequipment_bb38096ab39160dc20d44f3ea6b44507'] = '插件';
$_MODULE['<{statsequipment}cosmetico>statsequipment_9ffafc9e090c8e1c06f928ef2817efd6'] = '网络浏览器使用';
$_MODULE['<{statsequipment}cosmetico>statsequipment_0241b7aaaa5f76afd585bb6cdae314d1'] = '操作系统使用';
