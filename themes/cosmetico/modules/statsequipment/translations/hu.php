<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsequipment}cosmetico>statsequipment_247270d410e2b9de01814b82111becda'] = 'Böngészők és operációs rendszerek';
$_MODULE['<{statsequipment}cosmetico>statsequipment_2876718a648dea03aaafd4b5a63b1efe'] = 'Böngésző és operációs rendszer-használati grafikont tartalmazó fület ad hozzá a Statisztika vezérlőpulthoz.';
$_MODULE['<{statsequipment}cosmetico>statsequipment_6602bbeb2956c035fb4cb5e844a4861b'] = 'Útmutató';
$_MODULE['<{statsequipment}cosmetico>statsequipment_854c8e126f839cc861cde822b641230e'] = 'Ügyelve arra, hogy a honlapon elérhető, hogy a lehető legtöbb embert';
$_MODULE['<{statsequipment}cosmetico>statsequipment_0d5f13106dec10bb8a9301541052278c'] = 'Amikor kezelése a honlapon, fontos, hogy nyomon tudjuk követni a szoftver által használt látogatók, így győződjön meg róla, hogy az oldalon jeleníti meg, ugyanúgy, ahogy mindenki. PrestaShop épült, hogy kompatibilis a legújabb Web-böngésző, operációs rendszer (OS). Azonban, mert lehet, hogy a végén, hozzátéve, speciális funkciók, a weboldal vagy akár módosítása a mag PrestaShop kód, ezeket a kiegészítéseket nem lehet mindenki számára hozzáférhető. Ezért ez egy jó ötlet, hogy kövesd nyomon a százalékos aránya a felhasználók minden típusú szoftver hozzáadása előtt, vagy változik valami, hogy csak korlátozott számú felhasználó férhet hozzá.';
$_MODULE['<{statsequipment}cosmetico>statsequipment_11db1362a88c5e3e74c8f699c14d6798'] = 'Azt jelzi, hogy a százalékos aránya, az egyes web böngésző által használt ügyfelek.';
$_MODULE['<{statsequipment}cosmetico>statsequipment_998e4c5c80f27dec552e99dfed34889a'] = 'CSV exportálás';
$_MODULE['<{statsequipment}cosmetico>statsequipment_90c58bfe4872fc9ca7bf6a181c3e5edd'] = 'Azt jelzi, hogy a százalékos aránya, az egyes operációs rendszer által használt ügyfelek.';
$_MODULE['<{statsequipment}cosmetico>statsequipment_bb38096ab39160dc20d44f3ea6b44507'] = 'Beépülők';
$_MODULE['<{statsequipment}cosmetico>statsequipment_9ffafc9e090c8e1c06f928ef2817efd6'] = 'Web böngésző használt';
$_MODULE['<{statsequipment}cosmetico>statsequipment_0241b7aaaa5f76afd585bb6cdae314d1'] = 'Használt operációs rendszer';
