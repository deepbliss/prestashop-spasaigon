<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_9ae42413e3cb9596efe3857f75bad3df'] = 'Beszállítók blokk';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_e72b2501ba75e9ab754d3294d43c2590'] = 'Blokk hozzáadása beszállítók megjelenítéséhez.';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_85ab0c0d250e58397e95c96277a3f8e3'] = 'Hibás elemszám.';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_914b0cd4e4aa7cba84a3fd47b880fd2a'] = 'Legalább egy listát aktiválj.';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_f38f5974cdc23279ffe6d203641a8bdf'] = 'Beállítások frissítése megtörtént';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_f4f70727dc34561dfde1a3c529b6205c'] = 'Beállítások';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_bfdff752293014f11f17122c92909ad5'] = 'Egyszerű lista használata';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_78b2098aa1d513b5e1852b3140c7ee26'] = 'Beszállítók listájának megjelenítése egyszerű listaként.';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Bekapcsolt';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_b9f5c797ebbf55adccdd8539a65a0241'] = 'Kikapcsolt';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_2eef734f174a02ae3d7aaafefeeedb42'] = 'Megjelenítendő elemek száma';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_b0fa976774d2acf72f9c62e9ab73de38'] = 'Legördülő lista használata';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_e490c3b296d2f54d65d3d20803f71b55'] = 'Beszállítók listájának megjelenítése legördülő listában';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_c9cc8cce247e49bae79f15173ce97354'] = 'Mentés';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_1814d65a76028fdfbadab64a5a8076df'] = 'Beszállítók';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_49fa2426b7903b3d4c89e2c1874d9346'] = 'Több infó';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_ecf253735ac0cba84a9d2eeff1f1b87c'] = 'Összes beszállító';
$_MODULE['<{blocksupplier}cosmetico>blocksupplier_496689fbd342f80d30f1f266d060415a'] = 'Nincs beszállító';
