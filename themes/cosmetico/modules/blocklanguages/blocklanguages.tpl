{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block languages module -->
{if count($languages) > 1}
<div id="languages-block-top">
	<select onchange="window.location.href=this.options[this.selectedIndex].value">
		{*<option style="display: none;" class="current">
			{foreach from=$languages key=k item=language name="languages"}
				{if $language.iso_code == $lang_iso}
					{$language.iso_code}
				{/if}
			{/foreach}
		</option>*}
		{foreach from=$languages key=k item=language name="languages"}
			<option {if $language.iso_code == $lang_iso}selected disabled{/if} class="{if $language.iso_code == $lang_iso}selected{/if}" value="{if $language.iso_code != $lang_iso}{assign var=indice_lang value=$language.id_lang}{if isset($lang_rewrite_urls.$indice_lang)}{$lang_rewrite_urls.$indice_lang|escape:'html':'UTF-8'}{else}{$link->getLanguageLink($language.id_lang)|escape:'html':'UTF-8'}{/if}{/if}">
				{*{$language.name|regex_replace:"/\s\(.*\)$/":""}*}
				{$language.iso_code}
			</option>
		{/foreach}
	</select>
</div>
{/if}
<!-- /Block languages module -->
