<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{amazingsearch}cosmetico>amazingsearch_18bb5286c14aa64f4ab555f5fb060770'] = 'Verbazingwekkend zoeken';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_068f80c7519d0528fb08e82137a72131'] = 'Producten';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_03de921a8ea82897e13d33d66c28b4db'] = 'Alleen Online';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_ca2bf12169883f4982d8fe34b7e3c618'] = 'In prijs verlaagd!!';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_69d08bd5f8cf4e228930935c3f13e42f'] = 'In Stock';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'Op voorraad';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_cb3c718c905f00adbb6735f55bfb38ef'] = 'Product is beschikbaar met verschillende opties';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-top_13348442cc6a27032d2b4aa28b75a5d3'] = 'Zoeken';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_2d0f6b8300be19cf35e89e66f0677f95'] = 'Toevoegen aan winkelwagen';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_4351cfebe4b61d8aa5efa1d020710005'] = 'Bekijk';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_63a78ed4647f7c63c2929e35ec1c95e3'] = 'Aanpassen';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_d3da97e2d9aee5c8fbe03156ad051c99'] = 'Meer';
