<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{amazingsearch}cosmetico>amazingsearch_18bb5286c14aa64f4ab555f5fb060770'] = 'Удивительные поиск';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_068f80c7519d0528fb08e82137a72131'] = 'Продукты';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_03de921a8ea82897e13d33d66c28b4db'] = 'Онлайн только';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_ca2bf12169883f4982d8fe34b7e3c618'] = 'Цена снижена!';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_69d08bd5f8cf4e228930935c3f13e42f'] = 'В Наличии';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'В наличии';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_cb3c718c905f00adbb6735f55bfb38ef'] = 'Продукт доступен с различными вариантами';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-top_13348442cc6a27032d2b4aa28b75a5d3'] = 'Поиск';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_2d0f6b8300be19cf35e89e66f0677f95'] = 'Добавить в корзину';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_4351cfebe4b61d8aa5efa1d020710005'] = 'Смотреть';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_63a78ed4647f7c63c2929e35ec1c95e3'] = 'Настройка';
$_MODULE['<{amazingsearch}cosmetico>amazingsearch-result_d3da97e2d9aee5c8fbe03156ad051c99'] = 'Подробнее';
