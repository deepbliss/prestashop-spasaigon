<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbanner}cosmetico>blockbanner_4b92fcfe6f0ec26909935aa960b7b81f'] = 'Banner blokk';
$_MODULE['<{blockbanner}cosmetico>blockbanner_9ec3d0f07db2d25a37ec4b7a21c788f8'] = 'Megjelenik egy banner a tetején a bolt.';
$_MODULE['<{blockbanner}cosmetico>blockbanner_df7859ac16e724c9b1fba0a364503d72'] = 'Hiba történt, miközben megpróbálja feltölteni a fájlt.';
$_MODULE['<{blockbanner}cosmetico>blockbanner_efc226b17e0532afff43be870bff0de7'] = 'A beállítások frissült.';
$_MODULE['<{blockbanner}cosmetico>blockbanner_f4f70727dc34561dfde1a3c529b6205c'] = 'Beállítások';
$_MODULE['<{blockbanner}cosmetico>blockbanner_9edcdbdff24876b0dac92f97397ae497'] = 'Felső banner kép';
$_MODULE['<{blockbanner}cosmetico>blockbanner_e90797453e35e4017b82e54e2b216290'] = 'A kép feltöltése a felső banner. Az ajánlott méretek 1170 x 65px, ha használja az alapértelmezett téma.';
$_MODULE['<{blockbanner}cosmetico>blockbanner_46fae48f998058600248a16100acfb7e'] = 'Banner Link';
$_MODULE['<{blockbanner}cosmetico>blockbanner_084fa1da897dfe3717efa184616ff91c'] = 'Adja meg a link jár, hogy a banner. Ha rákattint a bannerre, hogy a link megnyit az ugyanabban az ablakban. Ha nincs kapcsolat a bevitt átirányítja a honlap.';
$_MODULE['<{blockbanner}cosmetico>blockbanner_ff09729bee8a82c374f6b61e14a4af76'] = 'Banner leírás';
$_MODULE['<{blockbanner}cosmetico>blockbanner_112f6f9a1026d85f440e5ca68d8e2ec5'] = 'Kérjük, adj meg egy rövid, de tartalmas leírás a banner.';
$_MODULE['<{blockbanner}cosmetico>blockbanner_c9cc8cce247e49bae79f15173ce97354'] = 'Mentés';
$_MODULE['<{blockbanner}cosmetico>form_92fbf0e5d97b8afd7e73126b52bdc4bb'] = 'Válasszon ki egy fájlt';
