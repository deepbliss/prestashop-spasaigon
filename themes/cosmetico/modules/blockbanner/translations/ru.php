<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbanner}technoshop>blockbanner_4b92fcfe6f0ec26909935aa960b7b81f'] = 'Баннер блок';
$_MODULE['<{blockbanner}technoshop>blockbanner_9d9becee392c0fbcc66ff4981b8ae2f7'] = 'Отображает баннер в верхней части магазина.';
$_MODULE['<{blockbanner}technoshop>blockbanner_126b21ce46c39d12c24058791a236777'] = 'Неверное изображение';
$_MODULE['<{blockbanner}technoshop>blockbanner_df7859ac16e724c9b1fba0a364503d72'] = 'ошибка при загрузке файла';
$_MODULE['<{blockbanner}technoshop>blockbanner_efc226b17e0532afff43be870bff0de7'] = 'Настройки обновлены';
$_MODULE['<{blockbanner}technoshop>blockbanner_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{blockbanner}technoshop>blockbanner_89ca5c48bbc6b7a648a5c1996767484c'] = 'Блок изображения';
$_MODULE['<{blockbanner}technoshop>blockbanner_296dce46079fc6cabe69b7e7edb25506'] = 'Вы можете либо загрузить файл изображения или введите абсолютную ссылку в поле \"ссылка на картинку\" вариант ниже.';
$_MODULE['<{blockbanner}technoshop>blockbanner_9ce38727cff004a058021a6c7351a74a'] = 'Ссылка на картинку';
$_MODULE['<{blockbanner}technoshop>blockbanner_5b79f7f033924a348f025924820988cb'] = 'Вы можете либо войти в образ абсолютного ссылку или загрузить файл изображения в \"блокировать изображение\" вариант выше.';
$_MODULE['<{blockbanner}technoshop>blockbanner_18f2ae2bda9a34f06975d5c124643168'] = 'Изображение описание';
$_MODULE['<{blockbanner}technoshop>blockbanner_112f6f9a1026d85f440e5ca68d8e2ec5'] = 'Пожалуйста, введите короткое, но чёткое описание для баннера.';
$_MODULE['<{blockbanner}technoshop>blockbanner_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
