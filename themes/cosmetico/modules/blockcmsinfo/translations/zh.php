<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_988659f6c5d3210a3f085ecfecccf5d3'] = '客户服务信息模块';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_cd4abd29bdc076fb8fabef674039cd6e'] = '在您店铺添加客户信息模块。';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_666f6333e43c215212b916fef3d94af0'] = '所有字段为必填';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_86432715902fbaf53de469fed3fa6c53'] = '你必须选定的至少一个商店。';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_d52eaeff31af37a4a7e0550008aff5df'] = '试图保存时发生错误。';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_6f16c729fadd8aa164c6c47853983dd2'] = '新制定客服模块';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_9dffbf69ffba8bc38bc4e01abf4b1675'] = '文字';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_c9cc8cce247e49bae79f15173ce97354'] = '保存';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_630f6dc397fe74e52d5189e2c80f282b'] = '返回列表　';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_9d55fc80bbb875322aa67fd22fc98469'] = '商店项目关联：';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_6fcdef6ca2bb47a0cf61cd41ccf274f4'] = '阻挡身份证';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_9f82518d468b9fee614fcc92f76bb163'] = '店铺';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_56425383198d22fc8bb296bcca26cecf'] = '街区的案文';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_ef61fb324d729c341ea8ab9901e23566'] = '添加新的';
