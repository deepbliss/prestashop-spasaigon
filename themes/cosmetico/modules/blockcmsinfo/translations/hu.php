<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_988659f6c5d3210a3f085ecfecccf5d3'] = 'Egyéni CMS információs blokk';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_cd4abd29bdc076fb8fabef674039cd6e'] = 'Hozzáteszi, egyedi információs blokkok ebben a boltban.';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_666f6333e43c215212b916fef3d94af0'] = 'Minden mezőt ki kell töltened.';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_86432715902fbaf53de469fed3fa6c53'] = 'Ki kell választani legalább egy bolt.';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_d52eaeff31af37a4a7e0550008aff5df'] = 'Hiba történt, miközben megpróbálja megmenteni.';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_6f16c729fadd8aa164c6c47853983dd2'] = 'Új egyéni CMS blokk';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'Szöveg';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_c9cc8cce247e49bae79f15173ce97354'] = 'Mentés';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_630f6dc397fe74e52d5189e2c80f282b'] = 'Vissza a listához';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_9d55fc80bbb875322aa67fd22fc98469'] = 'Bolthozzárendelés';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_6fcdef6ca2bb47a0cf61cd41ccf274f4'] = 'Blokk AZONOSÍTÓ';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_9f82518d468b9fee614fcc92f76bb163'] = 'Bolt';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_56425383198d22fc8bb296bcca26cecf'] = 'Szöveg blokk';
$_MODULE['<{blockcmsinfo}cosmetico>blockcmsinfo_ef61fb324d729c341ea8ab9901e23566'] = 'Új';
