<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockstore}cosmetico>blockstore_68e9ecb0ab69b1121fe06177868b8ade'] = 'Üzleteink blokk';
$_MODULE['<{blockstore}cosmetico>blockstore_c1104fe0bdaceb2e1c6f77b04977b64b'] = 'Az üzletkeresőre mutató kép hívatkozást jelenít meg.';
$_MODULE['<{blockstore}cosmetico>blockstore_b786bfc116ecf9a6d47ce1114ca6abb7'] = 'Ez a modul kell kapcsolni egy oszlopban, de a téma nem hajtja végre.';
$_MODULE['<{blockstore}cosmetico>blockstore_7107f6f679c8d8b21ef6fce56fef4b93'] = 'Érvénytelen kép.';
$_MODULE['<{blockstore}cosmetico>blockstore_df7859ac16e724c9b1fba0a364503d72'] = 'Hiba történt, miközben megpróbálja feltölteni a fájlt.';
$_MODULE['<{blockstore}cosmetico>blockstore_efc226b17e0532afff43be870bff0de7'] = 'A beállítások frissült.';
$_MODULE['<{blockstore}cosmetico>blockstore_f4f70727dc34561dfde1a3c529b6205c'] = 'Beállítások';
$_MODULE['<{blockstore}cosmetico>blockstore_4d100d8b1b9bcb5a376f78365340cdbe'] = 'Kép a boltkereső blokk';
$_MODULE['<{blockstore}cosmetico>blockstore_a34202cc413553fe0fe2d46f706db435'] = 'A szöveg a boltkereső blokk';
$_MODULE['<{blockstore}cosmetico>blockstore_c9cc8cce247e49bae79f15173ce97354'] = 'Mentés';
$_MODULE['<{blockstore}cosmetico>blockstore_8c0caec5616160618b362bcd4427d97b'] = 'Üzleteink!';
$_MODULE['<{blockstore}cosmetico>blockstore_28fe12f949fd191685071517628df9b3'] = 'Fedezd fel az áruházainkat!';
$_MODULE['<{blockstore}cosmetico>blockstore_34c869c542dee932ef8cd96d2f91cae6'] = 'Üzleteink';
$_MODULE['<{blockstore}cosmetico>blockstore_61d5070a61ce6eb6ad2a212fdf967d92'] = 'Fedezd fel az áruházainkat';
