<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_f7c34fc4a48bc683445c1e7bbc245508'] = 'Nieuwe producten blok';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_d3ee346c7f6560faa13622b6fef26f96'] = 'Toont een blok met uw winkels nieuwste producten.';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_1cd777247f2a6ed79534d4ace72d78ce'] = 'Vul alstublieft het \"te weergeven producten\"-veld in.';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_73293a024e644165e9bf48f270af63a0'] = 'Ongeldig aantal.';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_c888438d14855d7d96a2724ee9c306bd'] = 'Instellingen bijgewerkt';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_f4f70727dc34561dfde1a3c529b6205c'] = 'Instellingen';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_26986c3388870d4148b1b5375368a83d'] = 'Producten om weer te geven';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_3ea7689283770958661c27c37275b89c'] = 'Stel het aantal producten in die moeten worden weergegeven in dit blok.';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_85dd6b2059e1ff8fbefcc9cf6e240933'] = 'Aantal dagen dat een product als \'nieuw\' wordt beschouwd';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_24ff4e4d39bb7811f6bdf0c189462272'] = 'Blok altijd weergeven';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_d68e7b860a7dba819fa1c75225c284b5'] = 'Toon het blok, zelfs als er geen producten beschikbaar zijn.';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Ingeschakeld';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_b9f5c797ebbf55adccdd8539a65a0241'] = 'Uitgeschakeld';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_c9cc8cce247e49bae79f15173ce97354'] = 'Opslaan';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_home_0af0aac2e9f6bd1d5283eed39fe265cc'] = 'Er zijn op dit moment geen nieuwe producten.';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_9ff0635f5737513b1a6f559ac2bff745'] = 'Nieuwe producten';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_43340e6cc4e88197d57f8d6d5ea50a46'] = 'Lees verder';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_60efcc704ef1456678f77eb9ee20847b'] = 'Alle nieuwe producten';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_18cc24fb12f89c839ab890f8188febe8'] = 'Er zijn op dit moment geen nieuwe producten.';
$_MODULE['<{blocknewproducts}cosmetico>tab_a0d0ebc37673b9ea77dd7c1a02160e2d'] = 'Nieuw binnen';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nieuw';
$_MODULE['<{blocknewproducts}cosmetico>blocknewproducts_f5174c99099735a431a0b0cb95b0412c'] = 'Alle nieuwe';
$_MODULE['<{blocknewproducts}cosmetico>tab_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nieuw';
