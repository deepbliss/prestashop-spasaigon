<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:42
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/easycarousels/views/templates/hook/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12054298045bad98a6c7a980-16359869%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e70e2b8eb6e61d0c7e375dfe473b5ee797028fc5' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/easycarousels/views/templates/hook/layout.tpl',
      1 => 1528149836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12054298045bad98a6c7a980-16359869',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'carousels_in_hook' => 0,
    'display_settings' => 0,
    'id_wrapper' => 0,
    'wrappers_settings' => 0,
    'w_settings' => 0,
    'carousels' => 0,
    'hook_name' => 0,
    'k' => 0,
    'carousel' => 0,
    'carousel_tpl' => 0,
    'custom_class' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a6d10224_67287468',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a6d10224_67287468')) {function content_5bad98a6d10224_67287468($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['carousels_in_hook']->value) {?>
<div class="easycarousels <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['display_settings']->value['custom_class'], ENT_QUOTES, 'UTF-8', true);?>
">
	<?php  $_smarty_tpl->tpl_vars['carousels'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carousels']->_loop = false;
 $_smarty_tpl->tpl_vars['id_wrapper'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['carousels_in_hook']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carousels']->key => $_smarty_tpl->tpl_vars['carousels']->value) {
$_smarty_tpl->tpl_vars['carousels']->_loop = true;
 $_smarty_tpl->tpl_vars['id_wrapper']->value = $_smarty_tpl->tpl_vars['carousels']->key;
?>
		<?php $_smarty_tpl->tpl_vars['w_settings'] = new Smarty_variable(array(), null, 0);?><?php if (!empty($_smarty_tpl->tpl_vars['wrappers_settings']->value[$_smarty_tpl->tpl_vars['id_wrapper']->value])) {?><?php $_smarty_tpl->tpl_vars['w_settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['wrappers_settings']->value[$_smarty_tpl->tpl_vars['id_wrapper']->value], null, 0);?><?php }?>
		<div class="c-wrapper w-<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
<?php if (!empty($_smarty_tpl->tpl_vars['w_settings']->value['custom_class'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['w_settings']->value['custom_class'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
		<?php if (!empty($_smarty_tpl->tpl_vars['carousels']->value['in_tabs'])) {?>
			<div class="in_tabs clearfix<?php if ($_smarty_tpl->tpl_vars['display_settings']->value['compact_tabs']) {?> compact_on<?php }?>">
				<ul id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true);?>
_<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
_easycarousel_tabs" class="game-tabs nav nav-tabs ec-tabs closed">
					<?php  $_smarty_tpl->tpl_vars['carousel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carousel']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = array_values($_smarty_tpl->tpl_vars['carousels']->value['in_tabs']); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carousel']->key => $_smarty_tpl->tpl_vars['carousel']->value) {
$_smarty_tpl->tpl_vars['carousel']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['carousel']->key;
?>
						<?php if (!$_smarty_tpl->tpl_vars['k']->value) {?>
							<li class="responsive_tabs_selection title_block">
								<a href="" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" onclick="event.preventDefault();">
									<span class="selection"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</span>
									<i class="icon icon-angle-down"></i>
								</a>
							</li>
						<?php }?>
						<li class="<?php if (!$_smarty_tpl->tpl_vars['k']->value) {?>first active<?php }?> carousel_title">
							<a href="#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['identifier'], ENT_QUOTES, 'UTF-8', true);?>
" class="ec-tab-link"><?php if (isset($_smarty_tpl->tpl_vars['carousel']->value['name'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></a>
						</li>
					<?php } ?>
				</ul>
				<div class="tab-content clearfix ec-tabs-content">
				<?php $_smarty_tpl->tpl_vars['custom_class'] = new Smarty_variable('active', null, 0);?>
				<?php  $_smarty_tpl->tpl_vars['carousel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carousel']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['carousels']->value['in_tabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carousel']->key => $_smarty_tpl->tpl_vars['carousel']->value) {
$_smarty_tpl->tpl_vars['carousel']->_loop = true;
?>
					<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['carousel_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('carousel'=>$_smarty_tpl->tpl_vars['carousel']->value,'in_tabs'=>true,'custom_class'=>$_smarty_tpl->tpl_vars['custom_class']->value), 0);?>

					<?php $_smarty_tpl->tpl_vars['custom_class'] = new Smarty_variable('', null, 0);?>
				<?php } ?>
				</div>
			</div>
		<?php }?>
		<?php if (!empty($_smarty_tpl->tpl_vars['carousels']->value['one_by_one'])) {?>
			<div class="one_by_one clearfix">
				<?php  $_smarty_tpl->tpl_vars['carousel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carousel']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['carousels']->value['one_by_one']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carousel']->key => $_smarty_tpl->tpl_vars['carousel']->value) {
$_smarty_tpl->tpl_vars['carousel']->_loop = true;
?>
					<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['carousel_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('carousel'=>$_smarty_tpl->tpl_vars['carousel']->value,'in_tabs'=>false), 0);?>

				<?php } ?>
			</div>
		<?php }?>
		</div>
	<?php } ?>
</div>
<?php }?>

<?php }} ?>
