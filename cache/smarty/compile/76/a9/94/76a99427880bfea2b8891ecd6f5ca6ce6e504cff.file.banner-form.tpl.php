<?php /* Smarty version Smarty-3.1.19, created on 2018-09-19 13:37:15
         compiled from "/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/banner-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12009289285ba1ee9bf2f1b8-62429931%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '76a99427880bfea2b8891ecd6f5ca6ce6e504cff' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/banner-form.tpl',
      1 => 1528149244,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12009289285ba1ee9bf2f1b8-62429931',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'banner' => 0,
    'id_banner' => 0,
    'input_fields' => 0,
    'field' => 0,
    'input_name' => 0,
    'languages' => 0,
    'lang' => 0,
    'id_lang' => 0,
    'id_lang_current' => 0,
    'value' => 0,
    'k' => 0,
    'type' => 0,
    'key' => 0,
    'selector' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba1ee9c228f49_17190223',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba1ee9c228f49_17190223')) {function content_5ba1ee9c228f49_17190223($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['id_banner'] = new Smarty_variable($_smarty_tpl->tpl_vars['banner']->value['id_banner'], null, 0);?>
<div class="banner-item clearfix" data-id="<?php echo intval($_smarty_tpl->tpl_vars['id_banner']->value);?>
">
	<form method="post" action="" class="form-horizontal">
	<div class="banner-header clearfix">
		<input type="checkbox" value="<?php echo intval($_smarty_tpl->tpl_vars['id_banner']->value);?>
" class="banner-box" title="<?php echo intval($_smarty_tpl->tpl_vars['id_banner']->value);?>
">
		<span class="banner-name">
			<span class="banner-preview">
				<span <?php if (isset($_smarty_tpl->tpl_vars['banner']->value['header_img'])) {?>style="background-image:url(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['header_img'], ENT_QUOTES, 'UTF-8', true);?>
)"<?php }?>>
					<?php if (isset($_smarty_tpl->tpl_vars['banner']->value['header_html'])) {?>HTML<?php }?>
				</span>
			</span>
			<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

			<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value['exc_note'])) {?><span class="exc-note"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['exc_note'], ENT_QUOTES, 'UTF-8', true);?>
</span><?php }?>
		</span>

		<span class="actions pull-right">
			<a class="activateBanner list-action-enable action-<?php if ($_smarty_tpl->tpl_vars['banner']->value['active']) {?>enabled<?php } else { ?>disabled<?php }?>" href="#" title="<?php echo smartyTranslate(array('s'=>'Active','mod'=>'custombanners'),$_smarty_tpl);?>
">
				<i class="icon-check"></i>
				<i class="icon-remove"></i>
				<input type="checkbox" name="active" value="1" class="toggleable_param hidden"<?php if ($_smarty_tpl->tpl_vars['banner']->value['active']) {?> checked<?php }?>>
			</a>
			<i class="dragger act icon icon-arrows-v icon-2x"></i>
			<div class="btn-group pull-right">
				<button title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'custombanners'),$_smarty_tpl);?>
" class="editBanner btn btn-default">
					<i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'custombanners'),$_smarty_tpl);?>

				</button>
				<button title="<?php echo smartyTranslate(array('s'=>'Scroll Up','mod'=>'custombanners'),$_smarty_tpl);?>
" class="scrollUp btn btn-default">
					<i class="icon icon-minus"></i> <?php echo smartyTranslate(array('s'=>'Cancel','mod'=>'custombanners'),$_smarty_tpl);?>

				</button>
				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="icon-caret-down"></i>
				</button>
				<ul class="dropdown-menu">
					<li class="dont-hide">
						
						<div class="toggle-hook-list dropdown-action"><i class="icon-copy"></i> <?php echo smartyTranslate(array('s'=>'Copy to hook','mod'=>'custombanners'),$_smarty_tpl);?>
</div>
						<div class="dynamic-hook-list" style="display:none;">
							<button class="btn btn-default copyToAnotherHook"><?php echo smartyTranslate(array('s'=>'OK','mod'=>'custombanners'),$_smarty_tpl);?>
</button>
						</div>
					</li>
					<li class="dont-hide">
						<div class="toggle-hook-list dropdown-action"><i class="icon-arrow-left"></i> <?php echo smartyTranslate(array('s'=>'Move to hook','mod'=>'custombanners'),$_smarty_tpl);?>
</div>
						<div class="dynamic-hook-list" style="display:none;">
							<button class="btn btn-default moveToAnotherHook"><?php echo smartyTranslate(array('s'=>'OK','mod'=>'custombanners'),$_smarty_tpl);?>
</button>
						</div>
					</li>
					<li>
						<div class="deleteBanner dropdown-action">
							<i class="icon icon-trash"></i>
							<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'custombanners'),$_smarty_tpl);?>

						</div>
					</li>
				</ul>
			</div>

		</span>
	</div>

	<div class="banner-details" style="display:none;">
		<div class="ajax-errors alert alert-danger" style="display:none"></div>
		<?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_smarty_tpl->tpl_vars['input_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['input_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
 $_smarty_tpl->tpl_vars['input_name']->value = $_smarty_tpl->tpl_vars['field']->key;
?>
		<?php if (isset($_smarty_tpl->tpl_vars['field']->value['display_name'])) {?>
		<div class="form-group<?php if (!isset($_smarty_tpl->tpl_vars['banner']->value['content'][$_smarty_tpl->tpl_vars['input_name']->value])&&$_smarty_tpl->tpl_vars['input_name']->value!='exceptions') {?> empty<?php }?>">
			<label class="control-label col-lg-1">
				<span<?php if (isset($_smarty_tpl->tpl_vars['field']->value['tooltip'])) {?> class="label-tooltip" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['tooltip'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
					<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['display_name'], ENT_QUOTES, 'UTF-8', true);?>

				</span>
				<a href="#" class="show-field" title="<?php echo smartyTranslate(array('s'=>'Add','mod'=>'custombanners'),$_smarty_tpl);?>
"><i class="icon-plus"></i></a>
			</label>
			<div class="col-lg-10 clearfix">
			<?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['lang']->_loop = true;
?>
				<?php $_smarty_tpl->tpl_vars['id_lang'] = new Smarty_variable($_smarty_tpl->tpl_vars['lang']->value['id_lang'], null, 0);?>
				<?php if (isset($_smarty_tpl->tpl_vars['banner']->value['content'][$_smarty_tpl->tpl_vars['input_name']->value][$_smarty_tpl->tpl_vars['id_lang']->value])) {?>
					<?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['banner']->value['content'][$_smarty_tpl->tpl_vars['input_name']->value][$_smarty_tpl->tpl_vars['id_lang']->value], null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable(0, null, 0);?>
				<?php }?>
				<div class="multilang lang-<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
" data-lang="<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
" style="<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['id_lang_current']->value) {?>display: none;<?php }?>">
				<?php if ($_smarty_tpl->tpl_vars['input_name']->value=='img') {?>
					<div class="img-holder<?php if ($_smarty_tpl->tpl_vars['value']->value) {?> has-img<?php }?>">
						<div class="img-uploader">
							<i class="icon-file-image-o"></i>
							<?php echo smartyTranslate(array('s'=>'Drag your image here, or','mod'=>'custombanners'),$_smarty_tpl);?>

							<a href="#" class="img-browse"><?php echo smartyTranslate(array('s'=>'browse','mod'=>'custombanners'),$_smarty_tpl);?>
</a>
							<input type="file" name="banner_img_<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
" style="display:none;">
							<input type="hidden" class="banner_img_name" name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][img]" value="<?php if ($_smarty_tpl->tpl_vars['value']->value) {?><?php echo htmlspecialchars(basename($_smarty_tpl->tpl_vars['value']->value), ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
						</div>
						<?php if ($_smarty_tpl->tpl_vars['value']->value) {?>
							<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8', true);?>
">
						<?php }?>
						<i class="icon-upload upload-new act" title="<?php echo smartyTranslate(array('s'=>'Upload new','mod'=>'custombanners'),$_smarty_tpl);?>
"></i>
					</div>
				<?php } elseif ($_smarty_tpl->tpl_vars['input_name']->value=='link') {?>
					<select name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][link][type]" class="col-lg-3 linkTypeSelector">
						<?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['field']->value['selector']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['type']->key;
?>
							<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value['type']==$_smarty_tpl->tpl_vars['k']->value) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true);?>
</option>
						<?php } ?>
					</select>
					<div class="input-group link-type col-lg-9" data-type="<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value['type']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value['type'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>custom<?php }?>">
						<span class="input-group-addon">
							<span class="any"><?php echo smartyTranslate(array('s'=>'Any URL','mod'=>'custombanners'),$_smarty_tpl);?>
</span>
							<span class="by_id">
								<span class="label-tooltip" data-toggle="tooltip" title="<?php echo smartyTranslate(array('s'=>'Just add the ID of selected resource. For example: \"10\"','mod'=>'custombanners'),$_smarty_tpl);?>
">
									<i class="icon-info-circle"></i>
								</span>
								<?php echo smartyTranslate(array('s'=>'ID','mod'=>'custombanners'),$_smarty_tpl);?>

							</span>
						</span>
						<input type="text" name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][link][href]" value="<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value['href']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value['href'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
						<span class="input-group-addon">
							<label class="label-checkbox">
								<input type="checkbox" name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][link][_blank]" value="1"<?php if ($_smarty_tpl->tpl_vars['value']->value&&isset($_smarty_tpl->tpl_vars['value']->value['_blank'])) {?> checked="checked"<?php }?>>
								<?php echo smartyTranslate(array('s'=>'new window','mod'=>'custombanners'),$_smarty_tpl);?>

							</label>
						</span>
					</div>
				<?php } elseif ($_smarty_tpl->tpl_vars['input_name']->value=='html') {?>
					<textarea class="mce" name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
]" id="banner_<?php echo intval($_smarty_tpl->tpl_vars['id_banner']->value);?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
_<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
"><?php if ($_smarty_tpl->tpl_vars['value']->value) {?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php }?></textarea>
				<?php } elseif ($_smarty_tpl->tpl_vars['input_name']->value=='class') {?>
					<div class="input-group">
						<input type="text" name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][class]" value="<?php if ($_smarty_tpl->tpl_vars['value']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" class="all-langs" data-all="txt-class">
						<span class="input-group-addon act show-classes">
							<?php echo smartyTranslate(array('s'=>'Predefined classes','mod'=>'custombanners'),$_smarty_tpl);?>

							<i class="icon-angle-down"></i>
						</span>
					</div>
				<?php } elseif ($_smarty_tpl->tpl_vars['input_name']->value=='exceptions') {?>
					<?php  $_smarty_tpl->tpl_vars['selector'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['selector']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['field']->value['selectors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['selector']->key => $_smarty_tpl->tpl_vars['selector']->value) {
$_smarty_tpl->tpl_vars['selector']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['selector']->key;
?>
						<div class="exceptions-block <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['ids']) {?> has-ids<?php }?>">
							<select name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][exceptions][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
][type]" class="exc all-langs <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
" data-all="select-exc-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
">
								<?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['selector']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['type']->key;
?>
									<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['type']==$_smarty_tpl->tpl_vars['k']->value) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true);?>
</option>
								<?php } ?>
							</select>
							<div class="input-group exc-ids">
								<span class="input-group-addon">
									<span class="label-tooltip" data-toggle="tooltip" title="<?php echo smartyTranslate(array('s'=>'For example: 11, 15, 18','mod'=>'custombanners'),$_smarty_tpl);?>
">
										<?php echo smartyTranslate(array('s'=>'IDs','mod'=>'custombanners'),$_smarty_tpl);?>

									</span>
								</span>
								<input type="text" name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][exceptions][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
][ids]" value="<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['ids']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['ids'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" class="ids all-langs" data-all="txt-exc-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
">
							</div>
						</div>
					<?php } ?>
				<?php } else { ?>
					<input type="text" name="banner_data[<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
]" id="banner_<?php echo intval($_smarty_tpl->tpl_vars['id_banner']->value);?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
_<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
" value="<?php if ($_smarty_tpl->tpl_vars['value']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" class="<?php if (isset($_smarty_tpl->tpl_vars['field']->value['all_langs'])) {?>all-langs<?php }?>">
				<?php }?>
				</div>
			<?php } ?>
			</div>
			<div class="col-lg-1">
				<?php if (!isset($_smarty_tpl->tpl_vars['field']->value['all_langs'])) {?>
				<div class="banner-langs">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['lang']->_loop = true;
?>
							<span class="multilang lang-<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
" style="<?php if ($_smarty_tpl->tpl_vars['lang']->value['id_lang']!=$_smarty_tpl->tpl_vars['id_lang_current']->value) {?>display:none;<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lang']->value['iso_code'], ENT_QUOTES, 'UTF-8', true);?>
</span>
						<?php } ?>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['lang']->_loop = true;
?>
						<li>
							<a href="#" onclick="event.preventDefault(); selectLanguage($(this), <?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
)">
								<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lang']->value['name'], ENT_QUOTES, 'UTF-8', true);?>

							</a>
						</li>
						<?php } ?>
					</ul>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['input_name']->value!='exceptions') {?>
				<i class="icon-times hide-field act" title="<?php echo smartyTranslate(array('s'=>'Remove','mod'=>'custombanners'),$_smarty_tpl);?>
"></i>
				<?php }?>
			</div>
		</div>
		<?php }?>
		<?php } ?>
		<div class="p-footer">
			<input type="hidden" name="id_banner" value="<?php echo intval($_smarty_tpl->tpl_vars['id_banner']->value);?>
">
			<input type="hidden" name="hook_name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['hook_name'], ENT_QUOTES, 'UTF-8', true);?>
">
			<input type="hidden" name="id_wrapper" value="<?php echo intval($_smarty_tpl->tpl_vars['banner']->value['id_wrapper']);?>
">
			<button class="saveBanner btn btn-default">
				<i class="process-icon-save"></i>
				<?php echo smartyTranslate(array('s'=>'Save','mod'=>'custombanners'),$_smarty_tpl);?>

			</button>
			<label class="label-checkbox">
				<input type="checkbox" name="lang_source" value="<?php echo intval($_smarty_tpl->tpl_vars['id_lang_current']->value);?>
" >
				<?php echo smartyTranslate(array('s'=>'Copy all data from selected language to others','mod'=>'custombanners'),$_smarty_tpl);?>

			</label>
		</div>
	</div>
	</form>
</div>

<?php }} ?>
