<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:42
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/easycarousels/views/templates/hook/carousel.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19196859525bad98a6d1a266-87466009%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '34726ddd64fc4b7f5dec1eebd03fe8c88bf3152d' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/easycarousels/views/templates/hook/carousel.tpl',
      1 => 1528149836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19196859525bad98a6d1a266-87466009',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'carousel' => 0,
    'in_tabs' => 0,
    'custom_class' => 0,
    'is_17' => 0,
    'hook_name' => 0,
    'c_settings' => 0,
    'k' => 0,
    'column_items' => 0,
    'type' => 0,
    'product_item_tpl' => 0,
    'i' => 0,
    'item_tpl' => 0,
    'total' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a6dff563_88422033',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a6dff563_88422033')) {function content_5bad98a6dff563_88422033($_smarty_tpl) {?>

<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['identifier'], ENT_QUOTES, 'UTF-8', true);?>
" class="easycarousel <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['settings']['tpl']['custom_class'], ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['in_tabs']->value) {?> tab-pane ec-tab-pane<?php } else { ?> carousel_block<?php }?><?php if (!empty($_smarty_tpl->tpl_vars['custom_class']->value)) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['custom_class']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['is_17']->value&&!empty($_smarty_tpl->tpl_vars['carousel']->value['name'])&&$_smarty_tpl->tpl_vars['carousel']->value['settings']['carousel']['n']) {?> nav_without_name<?php }?> <?php if ($_smarty_tpl->tpl_vars['in_tabs']->value) {?>grid-style-arrows<?php }?> easy-carousel-style">
	<?php if (!$_smarty_tpl->tpl_vars['in_tabs']->value&&!empty($_smarty_tpl->tpl_vars['carousel']->value['name'])) {?>
		<h3 class="<?php if (htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)!='displayEasyCarousel2'&htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)!='displayLeftColumn'&&htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)!='displayRightColumn') {?>title_main_section <?php } else { ?> title-box title_block<?php }?> carousel_title""><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</h3>
	<?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['carousel']->value['description'])) {?><div class="carousel-description"><?php echo $_smarty_tpl->tpl_vars['carousel']->value['description'];?>
</div><?php }?>
	<?php $_smarty_tpl->tpl_vars['c_settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['carousel']->value['settings']['carousel'], null, 0);?>
	<div class="block_content grid<?php if ($_smarty_tpl->tpl_vars['c_settings']->value['type']==2) {?> scroll-x-wrapper<?php }?>">
		<div class="c_container<?php if (empty($_smarty_tpl->tpl_vars['c_settings']->value['type'])) {?> simple-grid xl-<?php echo intval($_smarty_tpl->tpl_vars['c_settings']->value['i']);?>
 l-<?php echo intval($_smarty_tpl->tpl_vars['c_settings']->value['i_1200']);?>
 m-<?php echo intval($_smarty_tpl->tpl_vars['c_settings']->value['i_992']);?>
 s-<?php echo intval($_smarty_tpl->tpl_vars['c_settings']->value['i_768']);?>
 xs-<?php echo intval($_smarty_tpl->tpl_vars['c_settings']->value['i_480']);?>
 clearfix<?php } elseif ($_smarty_tpl->tpl_vars['c_settings']->value['type']==1) {?> carousel<?php } elseif ($_smarty_tpl->tpl_vars['c_settings']->value['type']==2) {?> scroll-x<?php }?>" data-settings="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['c_settings']->value), ENT_QUOTES, 'UTF-8', true);?>
">
			<?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(count($_smarty_tpl->tpl_vars['carousel']->value['items']), null, 0);?>
			<?php  $_smarty_tpl->tpl_vars['column_items'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['column_items']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = array_values($_smarty_tpl->tpl_vars['carousel']->value['items']); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['column_items']->key => $_smarty_tpl->tpl_vars['column_items']->value) {
$_smarty_tpl->tpl_vars['column_items']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['column_items']->key;
?>
				
				<?php if ($_smarty_tpl->tpl_vars['k']->value) {?></div><?php }?><div class="c_col">
					<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['column_items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
						<div class="c_item <?php if ($_smarty_tpl->tpl_vars['carousel']->value['type']=='manufacturers'||$_smarty_tpl->tpl_vars['carousel']->value['type']=='suppliers') {?>man-sup-item<?php } else { ?> ajax_block_product col-xs-12<?php }?>">
							<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable($_smarty_tpl->tpl_vars['carousel']->value['type'], null, 0);?>
							<?php if ($_smarty_tpl->tpl_vars['type']->value!='suppliers'&&$_smarty_tpl->tpl_vars['type']->value!='manufacturers'&&$_smarty_tpl->tpl_vars['type']->value!='categories'&&$_smarty_tpl->tpl_vars['type']->value!='subcategories') {?>
								<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['product_item_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['i']->value,'settings'=>$_smarty_tpl->tpl_vars['carousel']->value['settings']['tpl']), 0);?>

							<?php } else { ?>
								<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['item_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('item'=>$_smarty_tpl->tpl_vars['i']->value,'type'=>$_smarty_tpl->tpl_vars['type']->value,'settings'=>$_smarty_tpl->tpl_vars['carousel']->value['settings']['tpl']), 0);?>

							<?php }?>
						</div>
					<?php } ?>
				<?php if ($_smarty_tpl->tpl_vars['k']->value+1==$_smarty_tpl->tpl_vars['total']->value) {?></div><?php }?>
			<?php } ?>
		</div>
	</div>
	<?php if (!empty($_smarty_tpl->tpl_vars['carousel']->value['view_all_link'])) {?>
		<div class="text-center">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['view_all_link'], ENT_QUOTES, 'UTF-8', true);?>
" class="view_all btn btn-default"><?php echo smartyTranslate(array('s'=>'View all','mod'=>'easycarousels'),$_smarty_tpl);?>
</a>
		</div>
	<?php }?>
</div>

<?php }} ?>
