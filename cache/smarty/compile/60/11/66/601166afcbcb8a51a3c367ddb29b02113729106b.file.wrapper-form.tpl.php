<?php /* Smarty version Smarty-3.1.19, created on 2018-09-19 13:35:42
         compiled from "/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/wrapper-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8398217785ba1ee3e0e0f49-28961685%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '601166afcbcb8a51a3c367ddb29b02113729106b' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/wrapper-form.tpl',
      1 => 1528149244,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8398217785ba1ee3e0e0f49-28961685',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id_wrapper' => 0,
    'cb' => 0,
    'banners' => 0,
    'settings' => 0,
    'k' => 0,
    'field' => 0,
    'carousel_settings' => 0,
    'total' => 0,
    'half' => 0,
    'first_column' => 0,
    'second_column' => 0,
    'banner' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba1ee3e155e79_87039899',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba1ee3e155e79_87039899')) {function content_5ba1ee3e155e79_87039899($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['cb']->value->getWrapperSettingsFields($_smarty_tpl->tpl_vars['id_wrapper']->value), null, 0);?>
<?php $_smarty_tpl->tpl_vars['carousel_settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['cb']->value->getWrapperSettingsFields($_smarty_tpl->tpl_vars['id_wrapper']->value,'carousel'), null, 0);?>
<div class="cb-wrapper<?php if (!$_smarty_tpl->tpl_vars['banners']->value) {?> empty<?php }?>" data-id="<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
">
    <div class="w-actions">
        <form class="w-settings-form">
            <input type="hidden" name="id_wrapper" value="<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
">
            <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['settings']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['field']->key;
?>
                <?php echo $_smarty_tpl->getSubTemplate ("./form-group.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('name'=>$_smarty_tpl->tpl_vars['k']->value,'field'=>$_smarty_tpl->tpl_vars['field']->value,'form_identifier'=>$_smarty_tpl->tpl_vars['id_wrapper']->value,'group_class'=>'inline-block wrapper-form-group','label_class'=>'inline-label','input_wrapper_class'=>'inline-block','input_class'=>'save-on-the-fly'), 0);?>

            <?php } ?>
            <a href="#" class="callSettings btn btn-default<?php if ($_smarty_tpl->tpl_vars['settings']->value['display_type']['value']!=2) {?> hidden<?php }?>" data-settings="carousel">
                <i class="icon-wrench"></i> <?php echo smartyTranslate(array('s'=>'Carousel settings','mod'=>'custombanners'),$_smarty_tpl);?>

            </a>
            <a href="#" class="addBanner pull-right">
                <i class="icon-plus"></i>
                <span class="btn-txt"><?php echo smartyTranslate(array('s'=>'New banner','mod'=>'custombanners'),$_smarty_tpl);?>
</span>
            </a>
        </form>
        <form class="carousel-settings-form form-horizontal panel" style="display:none">
            <input type="hidden" name="id_wrapper" value="<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
">
            <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable(count($_smarty_tpl->tpl_vars['carousel_settings']->value), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['half'] = new Smarty_variable($_smarty_tpl->tpl_vars['total']->value/2, null, 0);?>
            <?php $_smarty_tpl->tpl_vars['first_column'] = new Smarty_variable(array_slice($_smarty_tpl->tpl_vars['carousel_settings']->value,0,$_smarty_tpl->tpl_vars['half']->value), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['second_column'] = new Smarty_variable(array_slice($_smarty_tpl->tpl_vars['carousel_settings']->value,$_smarty_tpl->tpl_vars['half']->value,$_smarty_tpl->tpl_vars['total']->value), null, 0);?>
            <?php  $_smarty_tpl->tpl_vars['carousel_settings'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carousel_settings']->_loop = false;
 $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = array($_smarty_tpl->tpl_vars['first_column']->value,$_smarty_tpl->tpl_vars['second_column']->value); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carousel_settings']->key => $_smarty_tpl->tpl_vars['carousel_settings']->value) {
$_smarty_tpl->tpl_vars['carousel_settings']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['carousel_settings']->key;
?>
                <div class="col-lg-6">
                    <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['carousel_settings']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['field']->key;
?>
                        <?php echo $_smarty_tpl->getSubTemplate ("./form-group.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('name'=>$_smarty_tpl->tpl_vars['k']->value,'field'=>$_smarty_tpl->tpl_vars['field']->value,'form_identifier'=>$_smarty_tpl->tpl_vars['id_wrapper']->value,'label_class'=>'control-label col-lg-6','input_wrapper_class'=>'col-lg-6'), 0);?>

                    <?php } ?>
                </div>
            <?php } ?>
            <div class="p-footer">
                <button class="btn btn-default saveCarouselSettings"><i class="process-icon-save"></i> <?php echo smartyTranslate(array('s'=>'Save','mod'=>'custombanners'),$_smarty_tpl);?>
</button>
            </div>
        </form>

        <a href="#" class="deleteWrapper" title="<?php echo smartyTranslate(array('s'=>'Delete wrapper','mod'=>'custombanners'),$_smarty_tpl);?>
"><i class="icon-trash"></i></a>
        <a href="#" class="dragger w-dragger">
            <i class="icon icon-arrows-v"></i>
        </a>
        <div class="settings-container" style="display:none;"></div>
    </div>
    <div class="banner-list">
        <?php  $_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['banner']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['banners']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['banner']->key => $_smarty_tpl->tpl_vars['banner']->value) {
$_smarty_tpl->tpl_vars['banner']->_loop = true;
?>
            <?php echo $_smarty_tpl->getSubTemplate ("./banner-form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('banner'=>$_smarty_tpl->tpl_vars['banner']->value), 0);?>

        <?php } ?>
    </div>
</div>
<?php }} ?>
