<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:43
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/hook/blocks.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2060864005bad98a72f8b26-89951966%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cc27f0a212e547a9ebddea1f059b7a2b853089ec' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/hook/blocks.tpl',
      1 => 1533704504,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2060864005bad98a72f8b26-89951966',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'hook_name' => 0,
    'is_column_hook' => 0,
    'blocks' => 0,
    'block' => 0,
    'settings' => 0,
    'all_link' => 0,
    'blog' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a7384d72_11728017',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a7384d72_11728017')) {function content_5bad98a7384d72_11728017($_smarty_tpl) {?>

<div class="amazzingblog blocks <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['is_column_hook']->value) {?> column-hook<?php }?>">
<?php  $_smarty_tpl->tpl_vars['block'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['block']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['blocks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['block']->key => $_smarty_tpl->tpl_vars['block']->value) {
$_smarty_tpl->tpl_vars['block']->_loop = true;
?>
	<?php $_smarty_tpl->tpl_vars['settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['settings'], null, 0);?>
	<div class="post-block block <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['display_type'], ENT_QUOTES, 'UTF-8', true);?>
-view<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['compact'])) {?> compact<?php }?><?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['class'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['class'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" data-id="<?php echo intval($_smarty_tpl->tpl_vars['block']->value['id_block']);?>
"<?php if (!empty($_smarty_tpl->tpl_vars['block']->value['encoded_carousel_settings'])) {?> data-carousel-settings="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['encoded_carousel_settings'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
		<div class="block-title<?php if ($_smarty_tpl->tpl_vars['is_column_hook']->value) {?> title_block<?php }?>">
			<h2 class="title-box">
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_link']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View all','mod'=>'amazzingblog'),$_smarty_tpl);?>
">
					<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

				</a>
			</h2>
		</div>
		<div class="<?php if ($_smarty_tpl->tpl_vars['is_column_hook']->value&&$_smarty_tpl->tpl_vars['settings']->value['display_type']!='carousel') {?>block_content<?php }?> <?php if ($_smarty_tpl->tpl_vars['settings']->value['display_type']=='carousel') {?>theme-carousel<?php }?>">
			<?php if ($_smarty_tpl->tpl_vars['settings']->value['display_type']=='presentation') {?>
				<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('post-list-presentation.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('posts'=>$_smarty_tpl->tpl_vars['block']->value['posts'],'settings'=>$_smarty_tpl->tpl_vars['settings']->value), 0);?>

			<?php } else { ?>
				<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('post-list.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('posts'=>$_smarty_tpl->tpl_vars['block']->value['posts'],'settings'=>$_smarty_tpl->tpl_vars['settings']->value,'no_pagination'=>true), 0);?>

			<?php }?>
		</div>
		<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['all_link'])&&$_smarty_tpl->tpl_vars['settings']->value['type']!='related') {?>
			<a class="btn btn-default block-viewall" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_link']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View all','mod'=>'amazzingblog'),$_smarty_tpl);?>
">
					<?php echo smartyTranslate(array('s'=>'View all','mod'=>'amazzingblog'),$_smarty_tpl);?>

				</a>
		<?php }?>
	</div>
<?php } ?>
</div>

<?php }} ?>
