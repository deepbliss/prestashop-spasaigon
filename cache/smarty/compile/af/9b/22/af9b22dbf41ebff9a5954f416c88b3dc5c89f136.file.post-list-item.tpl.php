<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:43
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/front/post-list-item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18697160625bad98a7dcb912-37052207%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af9b22dbf41ebff9a5954f416c88b3dc5c89f136' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/front/post-list-item.tpl',
      1 => 1529326410,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18697160625bad98a7dcb912-37052207',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'post' => 0,
    'blog' => 0,
    'col' => 0,
    'k' => 0,
    'settings' => 0,
    'cover_src' => 0,
    'link' => 0,
    'tags_tpl_path' => 0,
    'split_date' => 0,
    'i' => 0,
    'fragment' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a7eff1d0_16104216',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a7eff1d0_16104216')) {function content_5bad98a7eff1d0_16104216($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['link'] = new Smarty_variable($_smarty_tpl->tpl_vars['blog']->value->getPostLink($_smarty_tpl->tpl_vars['post']->value['id_post'],$_smarty_tpl->tpl_vars['post']->value['link_rewrite']), null, 0);?>
<div class="post-item-wrapper<?php if ($_smarty_tpl->tpl_vars['col']->value) {?> col-md-<?php echo intval($_smarty_tpl->tpl_vars['col']->value);?>
<?php if ($_smarty_tpl->tpl_vars['k']->value%$_smarty_tpl->tpl_vars['settings']->value['col_num']==0) {?> first-in-line<?php }?><?php }?>">
	<div class="post-item<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_date'])) {?> has-date<?php }?>">
		<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_cover'])) {?>
			<?php $_smarty_tpl->tpl_vars['cover_src'] = new Smarty_variable($_smarty_tpl->tpl_vars['blog']->value->getImgSrc('post',$_smarty_tpl->tpl_vars['post']->value['id_post'],$_smarty_tpl->tpl_vars['settings']->value['cover_type'],$_smarty_tpl->tpl_vars['post']->value['cover']), null, 0);?>
			<?php if (!empty($_smarty_tpl->tpl_vars['cover_src']->value)) {?>
				<div class="post-item-cover">
					<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['link_cover'])) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
"><?php }?>
						<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cover_src']->value, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
					<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['link_cover'])) {?></a><?php }?>
				</div>
			<?php }?>
		<?php }?>
		<div class="post-content">
		<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_author'])||!empty($_smarty_tpl->tpl_vars['settings']->value['show_views'])||!empty($_smarty_tpl->tpl_vars['settings']->value['show_comments'])||!empty($_smarty_tpl->tpl_vars['settings']->value['show_readmore'])||!empty($_smarty_tpl->tpl_vars['settings']->value['show_tags'])) {?>
				<div class="post-item-footer clearfix">
					<div class="post-item-infos pull-left">
						<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_author'])) {?>
							<span class="post-item-info author">
								<i class="icon-user"></i>
								<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['firstname'], ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['lastname'], ENT_QUOTES, 'UTF-8', true);?>

							</span>
						<?php }?>
						<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_views'])) {?>
							<span class="post-item-info views-num">
								<i class="icon-eye"></i>
								<?php echo intval($_smarty_tpl->tpl_vars['post']->value['views']);?>

							</span>
						<?php }?>
						<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_comments'])) {?>
							<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
#post-comments" class="post-item-info comments-num">
								<i class="icon-comment"></i>
								<?php echo intval($_smarty_tpl->tpl_vars['post']->value['comments']);?>

							</a>
						<?php }?>
						<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_tags'])&&!empty($_smarty_tpl->tpl_vars['post']->value['tags'])) {?>
							<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['tags_tpl_path']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tags'=>$_smarty_tpl->tpl_vars['post']->value['tags']), 0);?>

						<?php }?>
					</div>
				</div>
			<?php }?>
			<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_title'])) {?>
				<h3 class="post-item-title<?php if ($_smarty_tpl->tpl_vars['settings']->value['display_type']=='grid'||$_smarty_tpl->tpl_vars['settings']->value['display_type']=='carousel') {?> overflow-ellipsis<?php }?>">
					<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['link_title'])) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
"><?php }?>
						<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['title_truncate'])) {?><?php $_smarty_tpl->createLocalArrayVariable('post', null, 0);
$_smarty_tpl->tpl_vars['post']->value['title'] = $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['post']->value['title'],$_smarty_tpl->tpl_vars['settings']->value['title_truncate'],'...');?><?php }?>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

					<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['link_title'])) {?></a><?php }?>
				</h3>
			<?php }?>
		        <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_date'])) {?>
                <?php if ($_smarty_tpl->tpl_vars['post']->value['publish_from']==$_smarty_tpl->tpl_vars['blog']->value->empty_date) {?><?php $_smarty_tpl->createLocalArrayVariable('post', null, 0);
$_smarty_tpl->tpl_vars['post']->value['publish_from'] = $_smarty_tpl->tpl_vars['post']->value['date_add'];?><?php }?>
                <?php $_smarty_tpl->tpl_vars['split_date'] = new Smarty_variable($_smarty_tpl->tpl_vars['blog']->value->prepareDate($_smarty_tpl->tpl_vars['post']->value['publish_from']), null, 0);?>
                <div class="post-item-date">
                    <?php  $_smarty_tpl->tpl_vars['fragment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fragment']->_loop = false;
 $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['split_date']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fragment']->key => $_smarty_tpl->tpl_vars['fragment']->value) {
$_smarty_tpl->tpl_vars['fragment']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['fragment']->key;
?>
                        <div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value, ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['fragment']->value, ENT_QUOTES, 'UTF-8', true);?>
</div>
                    <?php } ?>
                </div>
            <?php }?>
        	<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_intro'])) {?>
				<div class="post-item-content"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['post']->value['content']),$_smarty_tpl->tpl_vars['settings']->value['truncate'],'...'), ENT_QUOTES, 'UTF-8', true);?>

				<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_intro'])&&!empty($_smarty_tpl->tpl_vars['settings']->value['show_readmore'])) {?>
						<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Read more','mod'=>'amazzingblog'),$_smarty_tpl);?>
" class="item-readmore">
							<?php echo smartyTranslate(array('s'=>'Read more','mod'=>'amazzingblog'),$_smarty_tpl);?>

						</a>
					<?php }?>
				</div>
			<?php }?>      
		</div>
	</div>
</div>

<?php }} ?>
