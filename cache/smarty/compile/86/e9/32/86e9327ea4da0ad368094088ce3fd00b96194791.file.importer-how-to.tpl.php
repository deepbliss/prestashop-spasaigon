<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 20:12:22
         compiled from "/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/importer-how-to.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4830816995ba0f9b6e7b699-73290456%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '86e9327ea4da0ad368094088ce3fd00b96194791' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/importer-how-to.tpl',
      1 => 1528149272,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4830816995ba0f9b6e7b699-73290456',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'iso_lang_current' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0f9b6e83af9_06017365',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0f9b6e83af9_06017365')) {function content_5ba0f9b6e83af9_06017365($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['iso_lang_current']->value=='some_other_lang') {?>
<?php } else { ?>
	<p>In order to export all current carousel data, just click "Export carousels" and save file. This file contains all multilingual data including hook positions and page exceptions.</p>
	</p>In order to import, just upload the file using "Import carousels" button. You can use this file on the same store as a backup, or you can upload it to any other store. When you upload the file, data is processed in a smart way to synchronize with installed languages/shops.</p>
	<p>Note: If you are using multistore, data is imported only to shops that are currently selected. It may be a single shop, a group of shops, or all shops. </p>
	<h4>Advanced use:</h4>
	<p>If you want, you can change pre-installed demo content, that is used every time on module reset. Same content is pre-installed when you export module as part of your theme. In order to do that, follow these steps:</p>
	<ol>
		<li>Make a regular export and save the file</li>
		<li>Rename file to "carousels-custom.txt"</li>
		<li>Move file to "/modules/easycaroulels/democontent/carousels-custom.txt"</li>
	</ol>
<?php }?>

<?php }} ?>
