<?php /* Smarty version Smarty-3.1.19, created on 2018-09-26 10:51:36
         compiled from "/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/faq.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5288134125bab02481a9dc8-78025378%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f2fe86593c0934a85e0c27591aced1f6a29d331f' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/faq.tpl',
      1 => 1536668036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5288134125bab02481a9dc8-78025378',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'apifaq' => 0,
    'categorie' => 0,
    'QandA' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bab0248247c56_06676922',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bab0248247c56_06676922')) {function content_5bab0248247c56_06676922($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/pjmyczpl/public_html/demo/tools/smarty/plugins/modifier.replace.php';
?>

<div class="tab-pane panel" id="faq">
    <div class="panel-heading"><i class="icon-question"></i> <?php echo smartyTranslate(array('s'=>'FAQ','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</div>
    <?php  $_smarty_tpl->tpl_vars['categorie'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['categorie']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['apifaq']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['categorie']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['categorie']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['categorie']->key => $_smarty_tpl->tpl_vars['categorie']->value) {
$_smarty_tpl->tpl_vars['categorie']->_loop = true;
 $_smarty_tpl->tpl_vars['categorie']->iteration++;
 $_smarty_tpl->tpl_vars['categorie']->last = $_smarty_tpl->tpl_vars['categorie']->iteration === $_smarty_tpl->tpl_vars['categorie']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['faq']['last'] = $_smarty_tpl->tpl_vars['categorie']->last;
?>
        <span class="faq-h1"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['categorie']->value->title, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
        <ul>
            <?php  $_smarty_tpl->tpl_vars['QandA'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['QandA']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categorie']->value->blocks; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['QandA']->key => $_smarty_tpl->tpl_vars['QandA']->value) {
$_smarty_tpl->tpl_vars['QandA']->_loop = true;
?>
                <?php if (!empty($_smarty_tpl->tpl_vars['QandA']->value->question)) {?>
                    <li>
                        <span class="faq-h2"><i class="icon-info-circle"></i> <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['QandA']->value->question, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
                        <p class="faq-text hide">
                            <?php echo smarty_modifier_replace(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['QandA']->value->answer, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'),"\n","<br />");?>

                        </p>
                    </li>
                <?php }?>
            <?php } ?>
        </ul>
        <?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['faq']['last']) {?><hr/><?php }?>
    <?php } ?>
</div>
<?php }} ?>
