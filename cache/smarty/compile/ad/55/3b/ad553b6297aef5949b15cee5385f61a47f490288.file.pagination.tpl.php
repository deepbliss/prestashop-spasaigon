<?php /* Smarty version Smarty-3.1.19, created on 2018-09-25 19:16:28
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/front/pagination.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8929555425baa271c0ee473-58125906%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad553b6297aef5949b15cee5385f61a47f490288' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/front/pagination.tpl',
      1 => 1528149758,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8929555425baa271c0ee473-58125906',
  'function' => 
  array (
    'getPageLink' => 
    array (
      'parameter' => 
      array (
        'page' => 1,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'settings' => 0,
    'ab_first_page_url' => 0,
    'page' => 0,
    'blog' => 0,
    'opt' => 0,
    'p_max' => 0,
    'p_type' => 0,
    'prev' => 0,
    'next' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5baa271c1b07e9_01346213',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5baa271c1b07e9_01346213')) {function content_5baa271c1b07e9_01346213($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['p_max'] = new Smarty_variable($_smarty_tpl->tpl_vars['settings']->value['npp']=='all' ? 0 : ceil($_smarty_tpl->tpl_vars['settings']->value['total']/$_smarty_tpl->tpl_vars['settings']->value['npp']), null, 0);?>
<?php $_smarty_tpl->tpl_vars['prev'] = new Smarty_variable($_smarty_tpl->tpl_vars['settings']->value['p']-1, null, 0);?>
<?php $_smarty_tpl->tpl_vars['next'] = new Smarty_variable($_smarty_tpl->tpl_vars['settings']->value['p']+1, null, 0);?>


<?php if (!function_exists('smarty_template_function_getPageLink')) {
    function smarty_template_function_getPageLink($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['getPageLink']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?><?php if (isset($_smarty_tpl->tpl_vars['ab_first_page_url']->value)) {?><?php echo $_smarty_tpl->tpl_vars['blog']->value->addPageNumber($_smarty_tpl->tpl_vars['ab_first_page_url']->value,$_smarty_tpl->tpl_vars['page']->value);?>
<?php } else { ?>#<?php }?><?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>


<div class="pagination">
	<div class="npp-holder pull-left">
		<div class="inline-block">
			<select class="npp form-control">
				<?php  $_smarty_tpl->tpl_vars['opt'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['opt']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['npp_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['opt']->key => $_smarty_tpl->tpl_vars['opt']->value) {
$_smarty_tpl->tpl_vars['opt']->_loop = true;
?>
					<option value="<?php echo intval($_smarty_tpl->tpl_vars['opt']->value);?>
"<?php if ($_smarty_tpl->tpl_vars['settings']->value['npp']==$_smarty_tpl->tpl_vars['opt']->value) {?> selected<?php }?>><?php echo intval($_smarty_tpl->tpl_vars['opt']->value);?>
</option>
				<?php } ?>
				<option value="all"<?php if ($_smarty_tpl->tpl_vars['settings']->value['npp']=='all') {?> selected<?php }?>><?php echo smartyTranslate(array('s'=>'All','mod'=>'amazzingblog'),$_smarty_tpl);?>
</option>
			</select>
		</div>
		<span class="total inline-block">
            <input type="hidden" name="posts_total" class="posts_total" value="<?php echo intval($_smarty_tpl->tpl_vars['settings']->value['total']);?>
">
            <?php echo smartyTranslate(array('s'=>'of %d','mod'=>'amazzingblog','sprintf'=>array($_smarty_tpl->tpl_vars['settings']->value['total'])),$_smarty_tpl);?>

        </span>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['p_max']->value>1) {?>
	<div class="pages pull-left<?php if ($_smarty_tpl->tpl_vars['p_type']->value=='ajax') {?> ajax<?php }?>">
		<label><?php echo smartyTranslate(array('s'=>'Pages:','mod'=>'amazzingblog'),$_smarty_tpl);?>
</label>
		
		<?php if ($_smarty_tpl->tpl_vars['prev']->value) {?>
			<a href="<?php smarty_template_function_getPageLink($_smarty_tpl,array('page'=>1));?>
" class="go-to-page first" data-page="1">1</a>
			<?php if ($_smarty_tpl->tpl_vars['prev']->value>1) {?>
				<?php if ($_smarty_tpl->tpl_vars['prev']->value>2) {?>...<?php }?>
				<a href="<?php smarty_template_function_getPageLink($_smarty_tpl,array('page'=>$_smarty_tpl->tpl_vars['prev']->value));?>
" class="go-to-page" data-page="<?php echo intval($_smarty_tpl->tpl_vars['prev']->value);?>
"><?php echo intval($_smarty_tpl->tpl_vars['prev']->value);?>
</a>
			<?php }?>
		<?php }?>
		<span class="current-page" data-page="<?php echo intval($_smarty_tpl->tpl_vars['settings']->value['p']);?>
"><?php echo intval($_smarty_tpl->tpl_vars['settings']->value['p']);?>
</span>
		<?php if ($_smarty_tpl->tpl_vars['next']->value<=$_smarty_tpl->tpl_vars['p_max']->value) {?>
			<?php if ($_smarty_tpl->tpl_vars['next']->value<$_smarty_tpl->tpl_vars['p_max']->value) {?>
				<a href="<?php smarty_template_function_getPageLink($_smarty_tpl,array('page'=>$_smarty_tpl->tpl_vars['next']->value));?>
" class="go-to-page" data-page="<?php echo intval($_smarty_tpl->tpl_vars['next']->value);?>
"><?php echo intval($_smarty_tpl->tpl_vars['next']->value);?>
</a>
				<?php if ($_smarty_tpl->tpl_vars['next']->value<$_smarty_tpl->tpl_vars['p_max']->value-1) {?>...<?php }?>
			<?php }?>
			<a href="<?php smarty_template_function_getPageLink($_smarty_tpl,array('page'=>$_smarty_tpl->tpl_vars['p_max']->value));?>
" class="go-to-page last" data-page="<?php echo intval($_smarty_tpl->tpl_vars['p_max']->value);?>
"><?php echo intval($_smarty_tpl->tpl_vars['p_max']->value);?>
</a>
		<?php } else { ?>
			<?php $_smarty_tpl->tpl_vars['next'] = new Smarty_variable($_smarty_tpl->tpl_vars['p_max']->value, null, 0);?>
		<?php }?>
		
	</div>
	<?php }?>
</div>

<?php }} ?>
