<?php /* Smarty version Smarty-3.1.19, created on 2018-09-19 13:35:42
         compiled from "/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/form-group.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15920086755ba1ee3e15eae3-24047304%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '00432931f9563347a957c7b660c40d2ee8b13e4e' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/form-group.tpl',
      1 => 1528149244,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15920086755ba1ee3e15eae3-24047304',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'group_class' => 0,
    'label_class' => 0,
    'input_wrapper_class' => 0,
    'input_class' => 0,
    'field' => 0,
    'name' => 0,
    'form_identifier' => 0,
    'id' => 0,
    'i' => 0,
    'opt' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba1ee3e21e037_75196351',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba1ee3e21e037_75196351')) {function content_5ba1ee3e21e037_75196351($_smarty_tpl) {?>

<?php if (empty($_smarty_tpl->tpl_vars['group_class']->value)) {?><?php $_smarty_tpl->tpl_vars['group_class'] = new Smarty_variable('form-group', null, 0);?><?php }?>
<?php if (empty($_smarty_tpl->tpl_vars['label_class']->value)) {?><?php $_smarty_tpl->tpl_vars['label_class'] = new Smarty_variable('control-label col-lg-3', null, 0);?><?php }?>
<?php if (empty($_smarty_tpl->tpl_vars['input_wrapper_class']->value)) {?><?php $_smarty_tpl->tpl_vars['input_wrapper_class'] = new Smarty_variable('col-lg-3', null, 0);?><?php }?>
<?php if (empty($_smarty_tpl->tpl_vars['input_class']->value)) {?><?php $_smarty_tpl->tpl_vars['input_class'] = new Smarty_variable('', null, 0);?><?php }?>

<div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group_class']->value, ENT_QUOTES, 'UTF-8', true);?>
">
    <label class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['label_class']->value, ENT_QUOTES, 'UTF-8', true);?>
">
        <span<?php if (!empty($_smarty_tpl->tpl_vars['field']->value['tooltip'])) {?> class="label-tooltip" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['tooltip'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
            <?php echo $_smarty_tpl->tpl_vars['field']->value['display_name'];?>
 
        </span>
    </label>
    <div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_wrapper_class']->value, ENT_QUOTES, 'UTF-8', true);?>
">
        <?php if (!empty($_smarty_tpl->tpl_vars['field']->value['input_class'])) {?><?php $_smarty_tpl->tpl_vars['input_class'] = new Smarty_variable((($_smarty_tpl->tpl_vars['input_class']->value).(' ')).($_smarty_tpl->tpl_vars['field']->value['input_class']), null, 0);?><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['field']->value['type']=='switcher') {?>
            <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable(((Tools::str2url($_smarty_tpl->tpl_vars['name']->value)).('-')).($_smarty_tpl->tpl_vars['form_identifier']->value), null, 0);?> 
            <span class="switch prestashop-switch <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_class']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                <input type="radio" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="1"<?php if (!empty($_smarty_tpl->tpl_vars['field']->value['value'])) {?> checked<?php }?> >
                <label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'custombanners'),$_smarty_tpl);?>
</label>
                <input type="radio" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
_0" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="0"<?php if (empty($_smarty_tpl->tpl_vars['field']->value['value'])) {?> checked<?php }?> >
                <label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
_0"><?php echo smartyTranslate(array('s'=>'No','mod'=>'custombanners'),$_smarty_tpl);?>
</label>
                <a class="slide-button btn"></a>
            </span>
        <?php } elseif ($_smarty_tpl->tpl_vars['field']->value['type']=='select') {?>
            <select class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_class']->value, ENT_QUOTES, 'UTF-8', true);?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                <?php  $_smarty_tpl->tpl_vars['opt'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['opt']->_loop = false;
 $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['field']->value['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['opt']->key => $_smarty_tpl->tpl_vars['opt']->value) {
$_smarty_tpl->tpl_vars['opt']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['opt']->key;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['field']->value['value']==$_smarty_tpl->tpl_vars['i']->value) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['opt']->value, ENT_QUOTES, 'UTF-8', true);?>
</option>
                <?php } ?>
            </select>
        <?php } else { ?>
            <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_class']->value, ENT_QUOTES, 'UTF-8', true);?>
">
        <?php }?>
    </div>
</div>
<?php }} ?>
