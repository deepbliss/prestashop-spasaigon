<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 20:12:22
         compiled from "/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/configure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6589674135ba0f9b6846007-64986416%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '00b6ceed429c2ac6b9b05d64d57b4f016a5bb3b5' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/configure.tpl',
      1 => 1528149272,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6589674135ba0f9b6846007-64986416',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'hooks' => 0,
    'hk' => 0,
    'qty' => 0,
    'carousels' => 0,
    'id_wrapper' => 0,
    'ec' => 0,
    'wrapper_carousels' => 0,
    'wrapper_fields' => 0,
    'k' => 0,
    'field' => 0,
    'name' => 0,
    'carousel' => 0,
    'type_names' => 0,
    'howto_tpl_path' => 0,
    'overrides_data' => 0,
    'override' => 0,
    'info_links' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0f9b6a4c051_49215529',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0f9b6a4c051_49215529')) {function content_5ba0f9b6a4c051_49215529($_smarty_tpl) {?>

<div class="easycarousels horizontal-tabs clearfix">
	<a href="#carousels" class="nav-tab-name first active"><i class="icon-image"></i> <?php echo smartyTranslate(array('s'=>'Carousels','mod'=>'easycarousels'),$_smarty_tpl);?>
</a>
	<a href="#importer" class="nav-tab-name"><i class="icon-file-zip-o"></i> <?php echo smartyTranslate(array('s'=>'Import/export','mod'=>'easycarousels'),$_smarty_tpl);?>
</a>
	<a href="#overrides" class="nav-tab-name"><i class="icon-file-text-o"></i> <?php echo smartyTranslate(array('s'=>'Overrides','mod'=>'easycarousels'),$_smarty_tpl);?>
</a>
	<a href="#info" class="nav-tab-name"><i class="icon-info-circle"></i> <?php echo smartyTranslate(array('s'=>'Information','mod'=>'easycarousels'),$_smarty_tpl);?>
</a>
</div>
<div class="easycarousels horizontal-tabs-content">
	<div id="carousels" class="panel active all-carousels clearfix">
	<form class="form-horizontal hook-form clearfix">
		<label class="control-label col-lg-1" for="hookSelector">
			<?php echo smartyTranslate(array('s'=>'Select hook','mod'=>'easycarousels'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-3">
			<select class="hookSelector">
				<?php  $_smarty_tpl->tpl_vars['qty'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['qty']->_loop = false;
 $_smarty_tpl->tpl_vars['hk'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['hooks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['qty']->key => $_smarty_tpl->tpl_vars['qty']->value) {
$_smarty_tpl->tpl_vars['qty']->_loop = true;
 $_smarty_tpl->tpl_vars['hk']->value = $_smarty_tpl->tpl_vars['qty']->key;
?>
					<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
 (<?php echo intval($_smarty_tpl->tpl_vars['qty']->value);?>
) </option>
				<?php } ?>
			</select>
		</div>
		<div class="col-lg-6 hook-settings">
			<a href="#" class="btn btn-default callSettings" data-settings="display">
				<i class="icon-wrench"></i> <?php echo smartyTranslate(array('s'=>'Display settings','mod'=>'easycarousels'),$_smarty_tpl);?>

			</a>
			<a href="#" class="btn btn-default callSettings" data-settings="exceptions">
				<i class="icon-ban"></i> <?php echo smartyTranslate(array('s'=>'Exceptions','mod'=>'easycarousels'),$_smarty_tpl);?>

			</a>
			<a href="#" class="btn btn-default callSettings" data-settings="positions">
				<i class="icon-arrows-alt"></i> <?php echo smartyTranslate(array('s'=>'Module positions','mod'=>'easycarousels'),$_smarty_tpl);?>

			</a>
		</div>
		<button type="button" class="addWrapper btn btn-default pull-right">
			<span class="custom-wrapper-icon"><i class="t-l"></i><i class="t-r"></i><i class="b-r"></i><i class="b-l"></i></span>
			<?php echo smartyTranslate(array('s'=>'New Wrapper','mod'=>'easycarousels'),$_smarty_tpl);?>

		</button>
	</form>
	<div id="settings-content" style="display:none;"></div>
	<?php  $_smarty_tpl->tpl_vars['qty'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['qty']->_loop = false;
 $_smarty_tpl->tpl_vars['hk'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['hooks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['qty']->key => $_smarty_tpl->tpl_vars['qty']->value) {
$_smarty_tpl->tpl_vars['qty']->_loop = true;
 $_smarty_tpl->tpl_vars['hk']->value = $_smarty_tpl->tpl_vars['qty']->key;
?>
	<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="hook-content <?php if ($_smarty_tpl->tpl_vars['hk']->value=='displayHome') {?>active<?php }?>">
		<?php if (substr($_smarty_tpl->tpl_vars['hk']->value,0,19)=='displayEasyCarousel') {?>
		<div class="alert alert-info">
			<?php echo smartyTranslate(array('s'=>'In order to display this hook, insert the following code to any tpl','mod'=>'easycarousels'),$_smarty_tpl);?>
:
			{hook h='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
'}
		</div>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['hk']->value=='displayFooterProduct') {?>
		<div class="alert alert-info">
			<?php echo smartyTranslate(array('s'=>'Here you can display product accessories or other products with same categories/features','mod'=>'easycarousels'),$_smarty_tpl);?>

		</div>
		<?php }?>
		<div class="wrappers-container">
			<?php if (!isset($_smarty_tpl->tpl_vars['carousels']->value[$_smarty_tpl->tpl_vars['hk']->value])) {?>
				<?php $_smarty_tpl->createLocalArrayVariable('carousels', null, 0);
$_smarty_tpl->tpl_vars['carousels']->value[$_smarty_tpl->tpl_vars['hk']->value] = array(0=>array());?>
			<?php }?>
			<?php  $_smarty_tpl->tpl_vars['wrapper_carousels'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['wrapper_carousels']->_loop = false;
 $_smarty_tpl->tpl_vars['id_wrapper'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['carousels']->value[$_smarty_tpl->tpl_vars['hk']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['wrapper_carousels']->key => $_smarty_tpl->tpl_vars['wrapper_carousels']->value) {
$_smarty_tpl->tpl_vars['wrapper_carousels']->_loop = true;
 $_smarty_tpl->tpl_vars['id_wrapper']->value = $_smarty_tpl->tpl_vars['wrapper_carousels']->key;
?>
				<?php $_smarty_tpl->tpl_vars['wrapper_fields'] = new Smarty_variable($_smarty_tpl->tpl_vars['ec']->value->getWrapperFields($_smarty_tpl->tpl_vars['id_wrapper']->value), null, 0);?>
				<div class="c-wrapper<?php if (!$_smarty_tpl->tpl_vars['wrapper_carousels']->value) {?> empty<?php }?>" data-id="<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
">
					<div class="w-actions">
						<div class="w-settings-form pull-left">
							<form>
								<input type="hidden" name="id_wrapper" value="<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
">
								<?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['wrapper_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['field']->key;
?>
								<?php $_smarty_tpl->tpl_vars['name'] = new Smarty_variable($_smarty_tpl->tpl_vars['k']->value, null, 0);?>
								<div class="inline-block wrapper-form-group">
									<label>
										<span<?php if (!empty($_smarty_tpl->tpl_vars['field']->value['tooltip'])) {?> class="label-tooltip" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['tooltip'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
											<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['display_name'], ENT_QUOTES, 'UTF-8', true);?>

										</span>
									</label>
									<div class="inline-block">
										<input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
" class="save-on-the-fly <?php if (!empty($_smarty_tpl->tpl_vars['field']->value['input_class'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['input_class'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
									</div>
								</div>
								<?php } ?>
							</form>
						</div>
						<a href="#" class="addCarousel pull-right">
							<i class="icon-plus"></i>
							<span class="btn-txt"><?php echo smartyTranslate(array('s'=>'New carousel','mod'=>'easycarousels'),$_smarty_tpl);?>
</span>
						</a>
					</div>
					<a href="#" class="deleteWrapper" title="<?php echo smartyTranslate(array('s'=>'Delete wrapper','mod'=>'easycarousels'),$_smarty_tpl);?>
"><i class="icon-trash"></i></a>
					<a href="#" class="dragger w-dragger">
						<i class="icon icon-arrows-v"></i>
					</a>
					<div class="settings-container" style="display:none;"></div>
					<div class="carousel-list">
						<?php  $_smarty_tpl->tpl_vars['carousel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carousel']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wrapper_carousels']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carousel']->key => $_smarty_tpl->tpl_vars['carousel']->value) {
$_smarty_tpl->tpl_vars['carousel']->_loop = true;
?>
							<?php echo $_smarty_tpl->getSubTemplate ("./carousel-form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('carousel'=>$_smarty_tpl->tpl_vars['carousel']->value,'type_names'=>$_smarty_tpl->tpl_vars['type_names']->value,'full'=>0), 0);?>

						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="btn-group bulk-actions dropup">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<?php echo smartyTranslate(array('s'=>'Bulk actions','mod'=>'easycarousels'),$_smarty_tpl);?>
 <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li><a href="#"	class="bulk-select"><i class="icon-check-sign"></i> <?php echo smartyTranslate(array('s'=>'Select all','mod'=>'easycarousels'),$_smarty_tpl);?>
</a></li>
				<li><a href="#" class="bulk-unselect"><i class="icon-check-empty"></i> <?php echo smartyTranslate(array('s'=>'Unselect all','mod'=>'easycarousels'),$_smarty_tpl);?>
</a></li>
				<li class="divider"></li>
				<li><a href="#" data-bulk-act="enable"><i class="icon-check on"></i> <?php echo smartyTranslate(array('s'=>'Enable','mod'=>'easycarousels'),$_smarty_tpl);?>
</a></li>
				<li><a href="#" data-bulk-act="disable"><i class="icon-times off"></i> <?php echo smartyTranslate(array('s'=>'Disable','mod'=>'easycarousels'),$_smarty_tpl);?>
</a></li>
				<li><a href="#" data-bulk-act="group_in_tabs"><i class="icon-plus-square"></i> <?php echo smartyTranslate(array('s'=>'Group in tabs','mod'=>'easycarousels'),$_smarty_tpl);?>
</a></li>
				<li><a href="#" data-bulk-act="ungroup"><i class="icon-minus-square"></i> <?php echo smartyTranslate(array('s'=>'Ungroup','mod'=>'easycarousels'),$_smarty_tpl);?>
</a></li>
				<li class="divider"></li>
				<li><a href="#" data-bulk-act="delete"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'easycarousels'),$_smarty_tpl);?>
</a></li>
			</ul>
		</div>
	</div>
	<?php } ?>
	</div>

	<div id="importer" class="panel importer clearfix">
		<div class="info-note alert-info"><?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['howto_tpl_path']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</div>
		<form method="post" class="export-form" action="" enctype="multipart/form-data">
			<input type="hidden" name="action" value="exportCarousels">
			<button type="submit" class="export btn btn-default">
				<i class="icon-download icon-rotate-180"></i>
				<?php echo smartyTranslate(array('s'=>'Export carousels','mod'=>'easycarousels'),$_smarty_tpl);?>

			</button>
		</form>
		<button class="import btn btn-default">
			<i class="icon-download"></i>
			<?php echo smartyTranslate(array('s'=>'Import carousels','mod'=>'easycarousels'),$_smarty_tpl);?>

		</button>
		<form action="" method="post" enctype="multipart/form-data" style="display:none;">
			<input type="file" name="carousels_data_file" style="display:none;">
		</form>
	</div>

	<div id="overrides" class="panel clearfix">
		<div class="info-note alert-info">
			<?php echo smartyTranslate(array('s'=>'In most cases overrides are processed automatically','mod'=>'easycarousels'),$_smarty_tpl);?>
.
			<?php echo smartyTranslate(array('s'=>'They are used to improve module functionality','mod'=>'easycarousels'),$_smarty_tpl);?>
.<br>
			<span class="b"><?php echo smartyTranslate(array('s'=>'NOTE: These are advanced settings','mod'=>'easycarousels'),$_smarty_tpl);?>
.</span>
			<?php echo smartyTranslate(array('s'=>'Do not change anything here, if you are not sure what are you doing.','mod'=>'easycarousels'),$_smarty_tpl);?>

		</div>
		<div class="overrides-list">
			<?php  $_smarty_tpl->tpl_vars['override'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['override']->_loop = false;
 $_smarty_tpl->tpl_vars['class_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['overrides_data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['override']->key => $_smarty_tpl->tpl_vars['override']->value) {
$_smarty_tpl->tpl_vars['override']->_loop = true;
 $_smarty_tpl->tpl_vars['class_name']->value = $_smarty_tpl->tpl_vars['override']->key;
?>
				<div class="override-item<?php if ($_smarty_tpl->tpl_vars['override']->value['installed']===true) {?> installed<?php } elseif ($_smarty_tpl->tpl_vars['override']->value['installed']===false) {?> not-installed<?php }?> clearfix">
					<span class="override-name b"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['override']->value['path'], ENT_QUOTES, 'UTF-8', true);?>
</span>
					<?php if ($_smarty_tpl->tpl_vars['override']->value['installed']===true||$_smarty_tpl->tpl_vars['override']->value['installed']===false) {?>
						<span class="override-status alert-success"><?php echo smartyTranslate(array('s'=>'Installed','mod'=>'easycarousels'),$_smarty_tpl);?>
</span>
						<span class="override-status alert-danger"><?php echo smartyTranslate(array('s'=>'Not installed','mod'=>'easycarousels'),$_smarty_tpl);?>
</span>
					<?php } else { ?>
						<span class="override-status alert-warning"><?php echo smartyTranslate(array('s'=>'The following methods are already overriden: %s','mod'=>'easycarousels','sprintf'=>array($_smarty_tpl->tpl_vars['override']->value['installed'])),$_smarty_tpl);?>
</span>
						<span class="grey-note pull-right install-manually"><?php echo smartyTranslate(array('s'=>'Should be added manually','mod'=>'easycarousels'),$_smarty_tpl);?>
</span>
					<?php }?>
					<button class="btn btn-default install-override pull-right" data-override="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['override']->value['path'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'Install','mod'=>'easycarousels'),$_smarty_tpl);?>
</button>
					<button class="btn btn-default uninstall-override pull-right" data-override="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['override']->value['path'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'Uninstall','mod'=>'easycarousels'),$_smarty_tpl);?>
</button>
					<div class="grey-note">
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['override']->value['note'], ENT_QUOTES, 'UTF-8', true);?>
.
					</div>
				</div>
			<?php } ?>
		</div>
	</div>

	<div id="info" class="panel clearfix">
		<div class="info-row">
			<?php echo smartyTranslate(array('s'=>'Current version:','mod'=>'easycarousels'),$_smarty_tpl);?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ec']->value->version, ENT_QUOTES, 'UTF-8', true);?>
</b>
		</div>
		<div class="info-row">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_links']->value['changelog'], ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
				<i class="icon-code-fork"></i> <?php echo smartyTranslate(array('s'=>'Changelog','mod'=>'easycarousels'),$_smarty_tpl);?>

			</a>
		</div>
		<div class="info-row">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_links']->value['documentation'], ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
				<i class="icon-file-text"></i> <?php echo smartyTranslate(array('s'=>'Documentation','mod'=>'easycarousels'),$_smarty_tpl);?>

			</a>
		</div>
		<div class="info-row">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_links']->value['contact'], ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
				<i class="icon-envelope"></i> <?php echo smartyTranslate(array('s'=>'Contact us','mod'=>'easycarousels'),$_smarty_tpl);?>

			</a>
		</div>
		<div class="info-row">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_links']->value['modules'], ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
				<i class="icon-download"></i> <?php echo smartyTranslate(array('s'=>'Our modules','mod'=>'easycarousels'),$_smarty_tpl);?>

			</a>
		</div>
	</div>
</div>

<?php }} ?>
