<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:42
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/easycarousels/views/templates/hook/product-item-16.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3786472465bad98a6e684c8-48123477%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2db06540f241e73b1c56ba965e041efcc299f3f2' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/easycarousels/views/templates/hook/product-item-16.tpl',
      1 => 1533535152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3786472465bad98a6e684c8-48123477',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'settings' => 0,
    'has_buttons' => 0,
    'product' => 0,
    'legend' => 0,
    'PS_CATALOG_MODE' => 0,
    'restricted_country_mode' => 0,
    'priceDisplay' => 0,
    'price' => 0,
    'currency_iso_code' => 0,
    'add_prod_display' => 0,
    'static_token' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a722c8b8_31509402',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a722c8b8_31509402')) {function content_5bad98a722c8b8_31509402($_smarty_tpl) {?>


<?php $_smarty_tpl->tpl_vars['has_buttons'] = new Smarty_variable(!empty($_smarty_tpl->tpl_vars['settings']->value['add_to_cart'])||!empty($_smarty_tpl->tpl_vars['settings']->value['view_more']), null, 0);?>
<div class="product-container<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['price'])) {?> has-price<?php }?><?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['stock'])) {?> has-stock<?php }?><?php if ($_smarty_tpl->tpl_vars['has_buttons']->value) {?> has-buttons<?php }?>" itemscope itemtype="http://schema.org/Product">
<div class="left-block">
	
	<div class="product-image-container">
		
		<?php if ($_smarty_tpl->tpl_vars['settings']->value['image_type']!='--') {?>
			<?php $_smarty_tpl->tpl_vars['img_type'] = new Smarty_variable($_smarty_tpl->tpl_vars['settings']->value['image_type'], null, 0);?>
			<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['legend'])) {?><?php $_smarty_tpl->tpl_vars['legend'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['legend'], null, 0);?><?php } else { ?><?php $_smarty_tpl->tpl_vars['legend'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['name'], null, 0);?><?php }?>
			<a class="product_img_link"	href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
				<img
					class="replace-2x<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['second_img_src'])) {?> primary-image<?php }?>"
					src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['img_src'], ENT_QUOTES, 'UTF-8', true);?>
"
					alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['legend']->value, ENT_QUOTES, 'UTF-8', true);?>
"
					
					itemprop="image"
				/>
				<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['second_img_src'])) {?>
	                <img
						class="replace-2x secondary-image"
						src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['second_img_src'], ENT_QUOTES, 'UTF-8', true);?>
"
						alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['legend']->value, ENT_QUOTES, 'UTF-8', true);?>
"
						
					/>
	            <?php }?>
		</a>
		<?php }?>
		
			
				<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['stickers'])&&($_smarty_tpl->tpl_vars['product']->value['allow_oosp']||$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
					<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&!empty($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])) {?>
						<?php if ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='percentage') {?>
						<span class="tag price-percent-reduction">-<?php echo htmlspecialchars(($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100), ENT_QUOTES, 'UTF-8', true);?>
%
							<?php echo smartyTranslate(array('s'=>'Discount','mod'=>'easycarousels'),$_smarty_tpl);?>

						</span>
						<?php } elseif (!empty($_smarty_tpl->tpl_vars['product']->value['on_sale'])&&!empty($_smarty_tpl->tpl_vars['product']->value['show_price'])&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
							<a class="tag sale" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
">
								<?php echo smartyTranslate(array('s'=>'Sale!','mod'=>'easycarousels'),$_smarty_tpl);?>

							</a>
						<?php } elseif (isset($_smarty_tpl->tpl_vars['product']->value['new'])&&$_smarty_tpl->tpl_vars['product']->value['new']==1) {?>
							<a class="tag new" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
">
								<?php echo smartyTranslate(array('s'=>'New','mod'=>'easycarousels'),$_smarty_tpl);?>

							</a>
						<?php }?>
				<?php } else { ?>
					<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['on_sale'])&&!empty($_smarty_tpl->tpl_vars['product']->value['show_price'])&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
						<a class="tag sale" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php echo smartyTranslate(array('s'=>'Sale!','mod'=>'easycarousels'),$_smarty_tpl);?>

						</a>
					<?php } elseif (isset($_smarty_tpl->tpl_vars['product']->value['new'])&&$_smarty_tpl->tpl_vars['product']->value['new']==1) {?>
						<a class="tag new" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
">
							<?php echo smartyTranslate(array('s'=>'New','mod'=>'easycarousels'),$_smarty_tpl);?>

						</a>
					<?php }?>
				<?php }?>
			<?php }?>
			
		
		<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['stock_status'])&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?>
			<span class="tag out product-availability label-<?php if ($_smarty_tpl->tpl_vars['product']->value['stock_status']=='not_available') {?>danger<?php } elseif ($_smarty_tpl->tpl_vars['product']->value['stock_status']=='available_different') {?>warning<?php } else { ?>success<?php }?>">
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['stock_txt'], ENT_QUOTES, 'UTF-8', true);?>

			</span>
		<?php }?>
		
		<div class="wrap-view">
			<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListFunctionalButtons','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl);?>

			
			<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['quick_view'])) {?>
				<a class="quick-view hidden-xs function_btn icon-eye-two" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" rel="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
">
				<span><?php echo smartyTranslate(array('s'=>'Quick view','mod'=>'easycarousels'),$_smarty_tpl);?>
</span>
				</a>
			<?php }?>
			
		</div>
			
			<?php if (isset($_smarty_tpl->tpl_vars['product']->value['color_list'])) {?>
				<div class="color-list-container"><?php echo $_smarty_tpl->tpl_vars['product']->value['color_list'];?>
</div>
			<?php }?>
			
	</div>

	<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['displayProductDeliveryTime'])) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductDeliveryTime",'product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl);?>
<?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['displayProductPriceBlock'])) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"weight"),$_smarty_tpl);?>
<?php }?>

</div>
<div class="right-block clearfix">
		
        <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['product_cat'])&&!empty($_smarty_tpl->tpl_vars['product']->value['cat_url'])) {?>
            <div class="prop-line product-category nowrap">
                <a class="cat-name " href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cat_url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cat_name'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['cat_name'],45,'...'), ENT_QUOTES, 'UTF-8', true);?>
</a>
            </div>
        <?php }?>
        
        <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['product_man'])&&$_smarty_tpl->tpl_vars['product']->value['id_manufacturer']&&$_smarty_tpl->tpl_vars['product']->value['man_name']&&!empty($_smarty_tpl->tpl_vars['product']->value['man_url'])) {?>
            <div class="prop-line product-manufacturer nowrap">
                <a class="man-name" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['man_url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['man_name'], ENT_QUOTES, 'UTF-8', true);?>
">
                <?php if (!empty($_smarty_tpl->tpl_vars['product']->value['man_img_src'])) {?>
                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['man_img_src'], ENT_QUOTES, 'UTF-8', true);?>
" class="product-manufacturer-img">
                <?php } else { ?>
                    <?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['man_name'],45,'...'), ENT_QUOTES, 'UTF-8', true);?>

                <?php }?>
                </a>
            </div>
        <?php }?>

 
		<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['title'])) {?>
			<h5 itemprop="name" class="<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['title_one_line'])) {?>nowrap<?php }?>">
				<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['pack_quantity'])) {?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['pack_quantity']);?>
 x <?php }?>
				<a class="product-name" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
">
					<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['name'],$_smarty_tpl->tpl_vars['settings']->value['title'],'...'), ENT_QUOTES, 'UTF-8', true);?>

				</a>
			</h5>
		<?php }?>
		
	
    <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['reference'])) {?>
        <div class="prop-line product-reference nowrap"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['reference'], ENT_QUOTES, 'UTF-8', true);?>
</span></div>
    <?php }?>
        <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['description'])) {?>
        <div class="prop-line product-description-short" itemprop="description">
            <?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(strip_tags($_smarty_tpl->tpl_vars['product']->value['description_short']),$_smarty_tpl->tpl_vars['settings']->value['description'],'...'), ENT_QUOTES, 'UTF-8', true);?>

        </div>
    <?php }?>
	

	<!--<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['displayProductListReviews'])) {?>
		<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListReviews','product'=>$_smarty_tpl->tpl_vars['product']->value,'hide_thumbnails'=>intval(empty($_smarty_tpl->tpl_vars['settings']->value['thumbnails']))),$_smarty_tpl);?>

	<?php }?>
	-->
    
	<?php if (($_smarty_tpl->tpl_vars['settings']->value['price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&(!empty($_smarty_tpl->tpl_vars['product']->value['show_price'])||!empty($_smarty_tpl->tpl_vars['product']->value['available_for_order'])))) {?>
		<div class="content_price main" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['show_price'])&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?>
				<?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php $_smarty_tpl->tpl_vars['price'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['price'], null, 0);?><?php } else { ?><?php $_smarty_tpl->tpl_vars['price'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['price_tax_exc'], null, 0);?><?php }?>
				<meta itemprop="price" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['price']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
				<meta itemprop="priceCurrency" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency_iso_code']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
				<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['stock_txt'])) {?>
					<meta itemprop="availability" content="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['stock_txt'], ENT_QUOTES, 'UTF-8', true);?>
" />
				<?php }?>
				<span class="price product-price <?php if (isset($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']>0) {?>reduce_style<?php }?>"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['price']->value),$_smarty_tpl);?>
</span>
				<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&!empty($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])) {?>
					<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['displayProductPriceBlock'])) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"old_price"),$_smarty_tpl);?>
<?php }?>
					<span class="old-price product-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['product']->value['price_without_reduction']),$_smarty_tpl);?>
</span>
					
				<?php }?>
				<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['displayProductPriceBlock'])) {?>
					<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"price"),$_smarty_tpl);?>

					<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"unit_price"),$_smarty_tpl);?>

				<?php }?>
			<?php }?>
		</div>
	<?php }?>
	

	
	<?php if ($_smarty_tpl->tpl_vars['has_buttons']->value) {?>
	<div class="button-container">
		<?php if ($_smarty_tpl->tpl_vars['settings']->value['add_to_cart']) {?>
			<?php if (($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['product']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['product']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
				<?php if ((!isset($_smarty_tpl->tpl_vars['product']->value['customization_required'])||!$_smarty_tpl->tpl_vars['product']->value['customization_required'])&&($_smarty_tpl->tpl_vars['product']->value['allow_oosp']||$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
					<?php $_smarty_tpl->_capture_stack[0][] = array('default', null, null); ob_start(); ?>add=1&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {?>&amp;token=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
					<a class="button ajax_add_to_cart_button btn btn-default" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',true,null,Smarty::$_smarty_vars['capture']['default'],false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'easycarousels'),$_smarty_tpl);?>
" data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" data-minimal_quantity="<?php if (isset($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity'])&&$_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']>1) {?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']);?>
<?php } else { ?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['minimal_quantity']);?>
<?php }?>">
						<span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'easycarousels'),$_smarty_tpl);?>
</span>
					</a>
				<?php } else { ?>
					<span class="button ajax_add_to_cart_button btn btn-default disabled">
						<span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'easycarousels'),$_smarty_tpl);?>
</span>
					</span>
				<?php }?>
			<?php }?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['settings']->value['view_more']) {?>
			<a class="button lnk_view btn btn-default" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View','mod'=>'easycarousels'),$_smarty_tpl);?>
">
				<span><?php if ((isset($_smarty_tpl->tpl_vars['product']->value['customization_required'])&&$_smarty_tpl->tpl_vars['product']->value['customization_required'])) {?><?php echo smartyTranslate(array('s'=>'Customize','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'More','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php }?></span>
			</a>
		<?php }?>
		
	</div>
	<?php }?>
	
</div>
</div>


<?php }} ?>
