<?php /* Smarty version Smarty-3.1.19, created on 2018-09-19 13:35:42
         compiled from "/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/custom-file-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3523607725ba1ee3e5977e6-87974728%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c209f290440e67e5f01b100bd07707b08d49ca24' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/custom-file-form.tpl',
      1 => 1528149244,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3523607725ba1ee3e5977e6-87974728',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'shop_names' => 0,
    'code' => 0,
    'type' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba1ee3e5b03e1_88767944',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba1ee3e5b03e1_88767944')) {function content_5ba1ee3e5b03e1_88767944($_smarty_tpl) {?>

<form action="" method="post">
	<?php if ($_smarty_tpl->tpl_vars['shop_names']->value) {?>
	<div class="alert alert-info">
		<?php echo smartyTranslate(array('s'=>'This file will be saved for the following shops: %s','mod'=>'custombanners','sprintf'=>$_smarty_tpl->tpl_vars['shop_names']->value),$_smarty_tpl);?>

	</div>
	<?php }?>
	<div class="form-group">
		<textarea class="form-control textarea-code" rows="3" name="code"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
	</div>
	<div class="p-footer clearfix">
		<input type="hidden" name="file_type" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true);?>
">
		<button class="saveCustomFile btn btn-default pull-right">
			<i class="icon-save"></i>
			<?php echo smartyTranslate(array('s'=>'Save','mod'=>'custombanners'),$_smarty_tpl);?>
		
		</button>
	</div>
</form>
<?php }} ?>
