<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:44
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/blockuserinfo/nav.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12725754615bad98a8291e19-04231018%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c2886eaa58e4c3c1cbe90cf1c8a27faf53453302' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/blockuserinfo/nav.tpl',
      1 => 1529402182,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12725754615bad98a8291e19-04231018',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'order_process' => 0,
    'is_logged' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a82e2406_70587914',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a82e2406_70587914')) {function content_5bad98a82e2406_70587914($_smarty_tpl) {?><!-- Block user information module NAV  -->
<div class="header_user_info desktop-view">
	<label class="drop_links icon-user" for="state-user-1">
		<i class="drop-icon icon-caret-down"></i>
	</label>
	<input id="state-user-1" type="checkbox" class="not-styling"/>
	<ul class="drop_content_user" class="row">
		<li class="user-info__item item-account">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" class="account icon-user" rel="nofollow"><span><?php echo smartyTranslate(array('s'=>'My account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
</span></a>
		</li>
		<li class="user-info__item">
			<a class="wish_link icon-heart" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('blockwishlist','mywishlist',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'My wishlists','mod'=>'blockuserinfo'),$_smarty_tpl);?>
">
			<?php echo smartyTranslate(array('s'=>'Wish lists','mod'=>'blockuserinfo'),$_smarty_tpl);?>

			</a>
		</li>
		<!--li class="user-info__item">
			<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink($_smarty_tpl->tpl_vars['order_process']->value,true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my shopping cart','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow" class="shop_cart_user icon-shopping-cart">
			<?php echo smartyTranslate(array('s'=>'Shopping Cart','mod'=>'blockuserinfo'),$_smarty_tpl);?>

			</a>
		</li-->
		<li class="user-info__item log">
			<?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?>
				<a class="logout log icon-sign-out" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,"mylogout"), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo'),$_smarty_tpl);?>
">
					<?php echo smartyTranslate(array('s'=>'Log out','mod'=>'blockuserinfo'),$_smarty_tpl);?>

				</a>
			<?php } else { ?>
				<a class="login log icon-key" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log in to your customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
">
					<?php echo smartyTranslate(array('s'=>'Login','mod'=>'blockuserinfo'),$_smarty_tpl);?>

				</a>
			<?php }?>
		</li>
	</ul>
</div>
<!-- /Block usmodule NAV -->
<?php }} ?>
