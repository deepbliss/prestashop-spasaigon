<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 19:02:05
         compiled from "/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/item-column.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17927709815ba0e93d39f262-08907500%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62bb63b5d8c682c8d25d27dbca1a77a5c998082c' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/item-column.tpl',
      1 => 1533124444,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17927709815ba0e93d39f262-08907500',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'have_li' => 0,
    'column' => 0,
    'block' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0e93d3f4859_81820863',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0e93d3f4859_81820863')) {function content_5ba0e93d3f4859_81820863($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['have_li']->value) {?>
<li data-id-column="<?php echo intval($_smarty_tpl->tpl_vars['column']->value['id_column']);?>
" class="mm_columns_li item<?php echo intval($_smarty_tpl->tpl_vars['column']->value['id_column']);?>
 column_size_<?php echo intval($_smarty_tpl->tpl_vars['column']->value['column_size']);?>
 <?php if ($_smarty_tpl->tpl_vars['column']->value['is_breaker']) {?>mm_breaker<?php }?>" data-obj="column">
<?php }?>
<div class="mm_buttons">
    <span class="mm_column_delete" title="<?php echo smartyTranslate(array('s'=>'Delete column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Delete','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>
    <span class="mm_duplicate" title="<?php echo smartyTranslate(array('s'=>'Duplicate column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Duplicate','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>
    <span class="mm_column_edit" title="<?php echo smartyTranslate(array('s'=>'Edit column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Edit','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>
    <div class="mm_add_block btn btn-default" data-id-column="<?php echo intval($_smarty_tpl->tpl_vars['column']->value['id_column']);?>
" title="<?php echo smartyTranslate(array('s'=>'Add block','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Add block','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>    
</div>
<ul class="mm_blocks_ul">
    <?php if (isset($_smarty_tpl->tpl_vars['column']->value['blocks'])&&$_smarty_tpl->tpl_vars['column']->value['blocks']) {?>                                                    
        <?php  $_smarty_tpl->tpl_vars['block'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['block']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['column']->value['blocks']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['block']->key => $_smarty_tpl->tpl_vars['block']->value) {
$_smarty_tpl->tpl_vars['block']->_loop = true;
?>
            <li data-id-block="<?php echo intval($_smarty_tpl->tpl_vars['block']->value['id_block']);?>
" class="mm_blocks_li <?php if (!$_smarty_tpl->tpl_vars['block']->value['enabled']) {?>mm_disabled<?php }?> item<?php echo intval($_smarty_tpl->tpl_vars['block']->value['id_block']);?>
" data-obj="block">
                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMMItemBlock','block'=>$_smarty_tpl->tpl_vars['block']->value),$_smarty_tpl);?>

            </li>
        <?php } ?>                                                    
    <?php }?>
</ul>
<?php if ($_smarty_tpl->tpl_vars['have_li']->value) {?>
    </li>
<?php }?><?php }} ?>
