<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:44
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazingsearch//views/templates/hook/amazingsearch-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11817997165bad98a83376c8-70022276%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6fb45c3fa3a6759f8ae9223bf489e578558741da' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazingsearch//views/templates/hook/amazingsearch-top.tpl',
      1 => 1528149756,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11817997165bad98a83376c8-70022276',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'categories' => 0,
    'category' => 0,
    'id_category' => 0,
    'search_query' => 0,
    'lang_id' => 0,
    'shop_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a83752f9_05870361',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a83752f9_05870361')) {function content_5bad98a83752f9_05870361($_smarty_tpl) {?>
<!-- Amazinsearch module TOP -->
<div id="amazing_block_top" class="amazing-search search-box clearfix col-lg-3 col-md-2 col-md-offset-0 col-sm-4 col-xs-offset-2">
	<form method="get" action="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" id="searchbox">
			<?php if ($_smarty_tpl->tpl_vars['categories']->value) {?>
			<select id="amazing_search_select" name="category_id">
				<option value=""><?php echo smartyTranslate(array('s'=>'---','mod'=>'amazingsearch'),$_smarty_tpl);?>
</option>
				<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
					<option <?php if ($_smarty_tpl->tpl_vars['category']->value['id_category']==$_smarty_tpl->tpl_vars['id_category']->value) {?>selected="selected"<?php }?> value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['id_category'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
				<?php } ?>
			</select>
			<?php }?>
			<input type="hidden" name="controller" value="search" />
			<input type="hidden" name="orderby" value="position" />
			<input type="hidden" name="orderway" value="desc" />
			<input class="search_query header_search_input" type="text" id="amazingsearch_query_top" name="search_query" placeholder="<?php echo smartyTranslate(array('s'=>'Search','mod'=>'amazingsearch'),$_smarty_tpl);?>
" value="<?php echo stripslashes(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true));?>
" />
			<button type="submit" name="submit_search" class="button-search icon-search"></button>
	</form>
	<div class="amazingsearch_result"></div>
</div>
<script type="text/javascript">
lang_id = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['lang_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
shop_id = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['shop_id']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
</script>
<!-- /Amazing search module TOP -->
<?php }} ?>
