<?php /* Smarty version Smarty-3.1.19, created on 2018-09-25 21:14:39
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/cookienotification/views/templates/hook/notification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8888552085baa42cfedc897-31441114%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd3619ecfd3256e1c42923be4d7a4f29860a8a391' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/cookienotification/views/templates/hook/notification.tpl',
      1 => 1528149824,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8888552085baa42cfedc897-31441114',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'settings' => 0,
    'form_action' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5baa42cff01d07_13330684',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5baa42cff01d07_13330684')) {function content_5baa42cff01d07_13330684($_smarty_tpl) {?>

<div class="cookie-notification<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['position'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['position'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
	<div class="cookie-notification__close icon-close-o"></div>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_action']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="cookie-accept-form">
	<p>
		<?php echo smartyTranslate(array('s'=>'Our site uses cookies to give you the best user experience. By continuing you accept to receive them.','mod'=>'cookienotification'),$_smarty_tpl);?>

		<span class="button-holder">
			<button class="btn button white_btn btn-default child" type="submit"><?php echo smartyTranslate(array('s'=>'OK','mod'=>'cookienotification'),$_smarty_tpl);?>
</button>
			<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['info_link'])) {?>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['info_link'], ENT_QUOTES, 'UTF-8', true);?>
" class="btn button white_btn btn-default child" target="_blank"><?php echo smartyTranslate(array('s'=>'More info','mod'=>'cookienotification'),$_smarty_tpl);?>
</a>
			<?php }?>
		</span>
	</p>
	</form>
</div><?php }} ?>
