<?php /* Smarty version Smarty-3.1.19, created on 2018-09-26 10:51:35
         compiled from "/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/hook/getContent.tpl" */ ?>
<?php /*%%SmartyHeaderCode:441609395bab02475b1851-69068536%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd3b967ac230eed1109342937d0ddca1a7d636c37' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/hook/getContent.tpl',
      1 => 1536668036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '441609395bab02475b1851-69068536',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'confirmation' => 0,
    'error' => 0,
    'apifaq' => 0,
    'module_version' => 0,
    'msg_confirmation_delete' => 0,
    'search_product_placeholder' => 0,
    'search_category_placeholder' => 0,
    'currency' => 0,
    'id_lang' => 0,
    'controller_url' => 0,
    'controller_name' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bab0247651d09_57807183',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bab0247651d09_57807183')) {function content_5bab0247651d09_57807183($_smarty_tpl) {?>
<div class="clearfix">
<?php if (isset($_smarty_tpl->tpl_vars['confirmation']->value)) {?>
    <div class="alert alert-success"><?php echo smartyTranslate(array('s'=>'Settings updated','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
    <div class="alert alert-danger"><?php echo smartyTranslate(array('s'=>'Oops, there was a problem and your setting weren\'t updated','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
<?php }?>
<div class="col-lg-2">
    <div class="list-group" id="myTabs">
        <a href="#tab_documentation" class="list-group-item" data-toggle="tab"><i class="icon-book"></i> <?php echo smartyTranslate(array('s'=>'Documentation','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</a>
        <a href="#tab_configuration" class="list-group-item active" data-toggle="tab"><i class="icon-cog"></i> <?php echo smartyTranslate(array('s'=>'Configuration','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</a>
        <?php if (($_smarty_tpl->tpl_vars['apifaq']->value!='')) {?>
        <a href="#faq" class="list-group-item" data-toggle="tab"><i class="icon-question"></i> <?php echo smartyTranslate(array('s'=>'Help','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</a>
        <?php }?>
        <a href="#tab_contact" class="list-group-item" data-toggle="tab"><i class="icon-envelope"></i> <?php echo smartyTranslate(array('s'=>'Contact','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</a>
    </div>
    <div class="list-group">
        <a disabled="true" class="list-group-item"><i class="icon-info"></i> <?php echo smartyTranslate(array('s'=>'Version','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_version']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a>
    </div>
</div>
<div class="tab-content col-lg-10">
    <!-- DOCUMENTATION TEMPLATE -->
    <?php echo $_smarty_tpl->getSubTemplate ('../admin/documentation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <!-- CONFIGURATION TEMPLATE -->
    <?php echo $_smarty_tpl->getSubTemplate ('../admin/configuration.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <!-- FAQ TEMPLATE -->
    <?php if (($_smarty_tpl->tpl_vars['apifaq']->value!='')) {?>
    <?php echo $_smarty_tpl->getSubTemplate ('../admin/faq.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>
    <!-- CONTACT TEMPLATE -->
    <?php echo $_smarty_tpl->getSubTemplate ('../admin/contact.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>

<script type="text/javascript">
    msg_confirmation_delete = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['msg_confirmation_delete']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
    search_product_placeholder = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_product_placeholder']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
    search_category_placeholder = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_category_placeholder']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
    currency = '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
';
    id_lang = '<?php echo intval($_smarty_tpl->tpl_vars['id_lang']->value);?>
';
    admin_module_ajax_url = "<?php echo $_smarty_tpl->tpl_vars['controller_url']->value;?>
"; /* url */
    admin_module_controller = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['controller_name']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
";
</script>

</div>
<?php }} ?>
