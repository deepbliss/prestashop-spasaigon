<?php /* Smarty version Smarty-3.1.19, created on 2018-09-26 10:51:35
         compiled from "/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5821944885bab0247e46ae3-68448645%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5d468c0a9263e00d98971b0e6d79e6f9874c3ed7' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/form.tpl',
      1 => 1536668036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5821944885bab0247e46ae3-68448645',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'currency' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bab02480f5370_07899634',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bab02480f5370_07899634')) {function content_5bab02480f5370_07899634($_smarty_tpl) {?>

<form class="form-horizontal" name="form_prestagiftvouchers" action="" id='form_prestagiftvouchers' method="POST">

    <!-- Populated when editing a giftvoucher -->
    <input type="hidden" name="id_giftvoucher" id="id_giftvoucher">

    <!-- Condition Name -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for='name'>
        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo smartyTranslate(array('s'=>'The name of your discount','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Name','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
    </label>
    <div class="col-md-8">
        <div class="col-md-6">
            <div class="input-group">
                <input required name="name" type="text" id="name" value="" class="text form-control">
            </div>
        </div>
    </div>
    </div>
    <hr>
    <!-- Condition Type -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="unlimited">
        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo smartyTranslate(array('s'=>'If you choose Yes, the client will be able to receive discount codes as many times as his orders meet the criteria of this voucher rule.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Unlimited?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
    </label>
    <div class="col-md-8">
        <label class="radio-inline" for="unlimited-1">
            <input name="unlimited" type="radio" id="unlimited-1" value="1" checked="checked"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

        </label>
        <label class="radio-inline" for="unlimited-0">
            <input name="unlimited" type="radio" id="unlimited-0" value="0"><?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

        </label>
    </div>
    </div>
    <div class="form-group row">
        <label class="col-md-4 control-label" for="condition_type">
            <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" title="<?php echo smartyTranslate(array('s'=>'You can choose to generate cart rules based on order total amount, or on specific products to buy, or on specific category of products','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Type of condition','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <div id="condition_type" class="btn-group col-centered" data-toggle="buttons" required>
                <label class="btn btn-default active btn-lg btn-condition-0">
                    <input type="radio" id="condition_type_0" name="condition_type" value="0" autocomplete="off" checked><?php echo smartyTranslate(array('s'=>'Amount','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                </label>
                <label class="btn btn-default btn-lg btn-condition-1">
                    <input type="radio" id="condition_type_1" name="condition_type" value="1" autocomplete="off"><?php echo smartyTranslate(array('s'=>'Product','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                </label>
                <label class="btn btn-default btn-lg btn-condition-2">
                    <input type="radio" id="condition_type_2" name="condition_type" value="2" autocomplete="off"><?php echo smartyTranslate(array('s'=>'Category','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                </label>
            </div>
        </div>
    </div>

    <!-- Required amount -->
    <div class="form-group row" id="form-group-required-amount">
        <label class="col-md-4 control-label" for="amount_required">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Minimum amount necessary to generate a cart rule','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Order amount required','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <div class="col-md-6">
                <div class="input-group">
                    <input id="amount_required" name="amount_required" class="form-control" type="text">
                    <span class="input-group-addon"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
                </div>
            </div>
        </div>
    </div>

    <!-- Shipping included ? -->
    <div class="form-group row" id="form-group-shipping-included">
        <label class="col-md-4 control-label" for="shipping_included">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Do you want to include the shipping fees in this required amount ?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Shipping included in order amount?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <label class="radio-inline" for="shipping_included-0">
                <input type="radio" name="shipping_included" id="shipping_included-0" value="1" checked="checked">
                <?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

            </label>
            <label class="radio-inline" for="shipping_included-1">
                <input type="radio" name="shipping_included" id="shipping_included-1" value="0">
                <?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

            </label>
        </div>
    </div>

    <!-- Required Product -->
    <div class="form-group row" id="form-group-search-product">
        <label class="col-md-4 control-label" for="id_product">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Product to buy to generate a cart rule','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Product name','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-6">
            <input type="hidden" id="id_product" name='id_product' class="form-control" style="width:100%;"/>
        </div>
    </div>

    <!-- Required Category -->
    <div class="form-group row" id="form-group-search-category">
        <label class="col-md-4 control-label" for="id_category">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Product to buy within that category to generate a cart rule','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Category name','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-6">
            <input type="hidden" id="id_category" name='id_category' class="form-control" style="width:100%;"/>
        </div>
    </div>

    <!-- New clients only ? -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="new_clients_only">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Reserve the discount for new clients only. Those who already placed an order in the past won\'t receive it.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Only for new customers?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <label class="radio-inline" for="new_clients_only-0">
                <input type="radio" name="new_clients_only" id="new_clients_only-0" value="1">
                <?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

            </label>
            <label class="radio-inline" for="new_clients_only-1">
                <input type="radio" name="new_clients_only" id="new_clients_only-1" value="0" checked="checked">
                <?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

            </label>
        </div>
    </div>

    <hr>

    <!-- Discount Type -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="reduction_type">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'The type of discount that will be created when an order match your criteria','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Discount Type','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <div id="reduction_type" class="btn-group" data-toggle="buttons" required>
                <label class="btn btn-default active btn-lg">
                    <input type="radio" id="reduction_type-0" name="reduction_type" value="0" autocomplete="off" checked><?php echo smartyTranslate(array('s'=>'Amount','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                </label>
                <label class="btn btn-default btn-lg">
                    <input type="radio" id="reduction_type-1" name="reduction_type" value="1" autocomplete="off"><?php echo smartyTranslate(array('s'=>'Percentage','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                </label>
            </div>
        </div>
    </div>

    <!-- Discount value -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="reduction_value">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Discount voucher amount/percentage (cannot be applied to shipping)','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Discount value','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-4">
            <div class="input-group">
                <input id="reduction_value" name="reduction_value" class="form-control" type="text" required>
                <span class="input-group-addon" id="reduction_value_symbol"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
            </div>
        </div>
    </div>

    <!-- Appended Input : Discount Min Use -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="reduction_min_use">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Minimum order amount required (shipping not included) for the discount voucher to be valid','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Minimum order amount','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <div class="col-md-4">
                <div class="input-group">
                    <input id="reduction_min_use" name="reduction_min_use" class="form-control" type="text" required="">
                    <span class="input-group-addon"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
                </div>
            </div>
        </div>
    </div>

    <!-- Free Shipping ? -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="free_shipping">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'Do you want to offer the shipping fees when using the discount ?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Offer free shipping ?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <label class="radio-inline" for="free_shipping-0">
                <input type="radio" name="free_shipping" id="free_shipping-0" value="1">
                <?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

            </label>
            <label class="radio-inline" for="free_shipping-1">
                <input type="radio" name="free_shipping" id="free_shipping-1" value="0" checked="checked">
                <?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

            </label>
        </div>
    </div>

    <!-- Duration -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="duration">
            <span class="label-tooltip" title="<?php echo smartyTranslate(array('s'=>'How many days do you want the cart rule to be valid ?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Discount voucher validity length','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
        </label>
        <div class="col-md-8">
            <div class="col-md-4">
                <div class="input-group">
                    <input id="duration" name="duration" class="form-control" type="text" required="" min="1">
                    <span class="input-group-addon"><?php echo smartyTranslate(array('s'=>'days','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
                </div>
            </div>
        </div>
    </div>

    <!--</form> is in modal.tpl-->
<?php }} ?>
