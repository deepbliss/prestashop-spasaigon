<?php /* Smarty version Smarty-3.1.19, created on 2018-09-26 10:51:35
         compiled from "/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/modal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4197470175bab0247d9d177-73480337%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c95157f2d6a3abb52a0a28e6efcd7b4411e0b92d' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/modal.tpl',
      1 => 1536668036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4197470175bab0247d9d177-73480337',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bab0247e2a464_22026715',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bab0247e2a464_22026715')) {function content_5bab0247e2a464_22026715($_smarty_tpl) {?>

<div class="modal fade" id="modalEditGiftVoucher">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo smartyTranslate(array('s'=>'Create/Change a discount voucher rule','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</h4>
            </div>
            <div class="modal-body">
                <?php echo $_smarty_tpl->getSubTemplate ("./form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <div class="pull-right" style="line-height: 35px;!important;">
                        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</button>
                        <input type="submit" id="form_prestagiftvouchers" name="form_prestagiftvouchers" class="btn btn-success btn-lg" value=<?php echo smartyTranslate(array('s'=>'Save','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
>
                    </div>
                </div>
            </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }} ?>
