<?php /* Smarty version Smarty-3.1.19, created on 2018-09-26 10:51:36
         compiled from "/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/contact.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16436158335bab02482ac324-87760209%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c98846e25daeb6b46fab5cc3580c076e0ef1f307' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/contact.tpl',
      1 => 1536668036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16436158335bab02482ac324-87760209',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_display' => 0,
    'tracking_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bab0248321802_81627183',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bab0248321802_81627183')) {function content_5bab0248321802_81627183($_smarty_tpl) {?>

<div class="tab-pane" id="tab_contact">
    <div class="clearfix"></div>
    <div class="tab-pane panel" id="contacts">
        <h3><i class="icon-envelope"></i> <?php echo smartyTranslate(array('s'=>'Prestashop Addons','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <small><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small></h3>
        <div class="form-group">
            <b><?php echo smartyTranslate(array('s'=>'Thank you for choosing a module developed by the Addons Team of PrestaShop.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</b><br /><br />

            <?php echo smartyTranslate(array('s'=>'If you encounter a problem using the module, our team is at your service via the ','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <a target="_blank" href="http://addons.prestashop.com/contact-form.php<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tracking_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'contact form','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
.</a><br /><br />

            <?php echo smartyTranslate(array('s'=>'To save you time, before you contact us:','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
<br />
            - <?php echo smartyTranslate(array('s'=>'make sure you have read the documentation well. ','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
<br />
            - <?php echo smartyTranslate(array('s'=>'in the event you would be contacting us via the form, do not hesitate to give us your first message, maximum of details on the problem and its origins (screenshots, reproduce actions to find the bug, etc. ..) ','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
<br /><br />
            <?php echo smartyTranslate(array('s'=>'This module has been developped by PrestaShop and can only be sold through','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <a target="_blank" href="http://addons.prestashop.com<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tracking_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">addons.prestashop.com</a>.<br /><br />

            The PrestaShop Addons Team<br />
        </div>
    </div>
</div>
<?php }} ?>
