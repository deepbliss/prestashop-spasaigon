<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:41
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/custombanners/views/templates/hook/banners.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11311725575bad98a5a2f188-31492516%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fdc00acb7947cf968f9b7e361c5f78753cc5b1d' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/custombanners/views/templates/hook/banners.tpl',
      1 => 1528149828,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11311725575bad98a5a2f188-31492516',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'banners' => 0,
    'hook_name' => 0,
    'w' => 0,
    'settings' => 0,
    'id_wrapper' => 0,
    'encoded_carousel_settings' => 0,
    'banner' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a5afd6c6_53991420',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a5afd6c6_53991420')) {function content_5bad98a5afd6c6_53991420($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['banners']->value) {?>
<div class="custombanners <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true);?>
" data-hook="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true);?>
">
	<?php  $_smarty_tpl->tpl_vars['w'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['w']->_loop = false;
 $_smarty_tpl->tpl_vars['id_wrapper'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banners']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['w']->key => $_smarty_tpl->tpl_vars['w']->value) {
$_smarty_tpl->tpl_vars['w']->_loop = true;
 $_smarty_tpl->tpl_vars['id_wrapper']->value = $_smarty_tpl->tpl_vars['w']->key;
?>
		<?php if (empty($_smarty_tpl->tpl_vars['w']->value['banners'])||empty($_smarty_tpl->tpl_vars['w']->value['settings'])) {?><?php continue 1?><?php }?>
		<?php $_smarty_tpl->tpl_vars['settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['w']->value['settings']['general'], null, 0);?>
		<?php $_smarty_tpl->tpl_vars['encoded_carousel_settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['w']->value['settings']['carousel'], null, 0);?>
		<div class="cb-wrapper clearfix<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['custom_class'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['custom_class'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" data-wrapper="<?php echo intval($_smarty_tpl->tpl_vars['id_wrapper']->value);?>
">
			<?php if ($_smarty_tpl->tpl_vars['settings']->value['display_type']==2) {?>
			<div class="carousel" data-settings="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['encoded_carousel_settings']->value, ENT_QUOTES, 'UTF-8', true);?>
">
			<?php }?>
				<?php  $_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['banner']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['w']->value['banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['banner']->key => $_smarty_tpl->tpl_vars['banner']->value) {
$_smarty_tpl->tpl_vars['banner']->_loop = true;
?>
					<div class="banner-item<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value['class'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['class'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" <?php if (htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayTopColumn') {?>style="background-image: url(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['img'], ENT_QUOTES, 'UTF-8', true);?>
);"<?php }?>>
						<div class="banner-item-content">
							<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value['img'])) {?>
								<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value['link']['href'])) {?>
								<a <?php if (htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayCustomBanners3'||htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayLeftColumn'||htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayCustomBanners2') {?>class="wrap-effect"<?php }?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['link']['href'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if (isset($_smarty_tpl->tpl_vars['banner']->value['link']['_blank'])) {?> target="_blank"<?php }?>>
								<?php } elseif (htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayCustomBanners3'||htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayLeftColumn'||htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayCustomBanners2') {?>
								<div class="wrap-effect">
								<?php }?>
								<?php if (htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)!='displayTopColumn') {?>
									<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['img'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if (isset($_smarty_tpl->tpl_vars['banner']->value['title'])) {?> title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?> class="banner-img">
								<?php }?>
							<?php }?>
							<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value['html'])) {?>
								<div class="custom-html clearfix">
									<?php echo $_smarty_tpl->tpl_vars['banner']->value['html'];?>

								</div>
							<?php }?>
							<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value['img'])) {?>
							<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value['link']['href'])) {?>
								</a>
							<?php } elseif (htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayCustomBanners3'||htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayLeftColumn'||htmlspecialchars($_smarty_tpl->tpl_vars['hook_name']->value, ENT_QUOTES, 'UTF-8', true)=='displayCustomBanners2') {?>
							</div>
							<?php }?>
							<?php }?>
						</div>
					</div>
				<?php } ?>
			<?php if ($_smarty_tpl->tpl_vars['settings']->value['display_type']==2) {?>
			</div>
			<?php }?>
		</div>
	<?php } ?>
</div>
<?php }?>
<?php }} ?>
