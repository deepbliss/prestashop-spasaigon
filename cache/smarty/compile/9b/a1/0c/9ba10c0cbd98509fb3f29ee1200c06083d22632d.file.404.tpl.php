<?php /* Smarty version Smarty-3.1.19, created on 2018-09-20 19:31:12
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/404.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12703711095ba3931027c687-68184086%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ba10c0cbd98509fb3f29ee1200c06083d22632d' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/404.tpl',
      1 => 1528149666,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12703711095ba3931027c687-68184086',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'base_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba3931029a006_71447834',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba3931029a006_71447834')) {function content_5ba3931029a006_71447834($_smarty_tpl) {?>
<div class="pagenotfound">
    	<h1><span><?php echo smartyTranslate(array('s'=>'Ooops!'),$_smarty_tpl);?>
</span></h1>
	<h2><span class="p_404"><?php echo smartyTranslate(array('s'=>'404.'),$_smarty_tpl);?>
</span><span class="not_found"><?php echo smartyTranslate(array('s'=>'Page not found'),$_smarty_tpl);?>
</span></h2>

	<p>
		<?php echo smartyTranslate(array('s'=>'SEEMS LIKE WE LOST ONE'),$_smarty_tpl);?>

	</p>

	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std form_404">
		<fieldset>
			<div>
				<input id="search_query" name="search_query" type="text" placeholder="<?php echo smartyTranslate(array('s'=>'Enter a product name...'),$_smarty_tpl);?>
" class="form-control grey" />
                <button type="submit" name="Submit" value="OK" class="btn btn_border button-small"><span><?php echo smartyTranslate(array('s'=>'Ok'),$_smarty_tpl);?>
</span></button>
			</div>
		</fieldset>
	</form>

	<div class="buttons"><a class="btn btn-default button-medium" href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
"><span><i class="icon-angle-left left"></i><?php echo smartyTranslate(array('s'=>'Back to home'),$_smarty_tpl);?>
</span></a></div>
</div>
<?php }} ?>
