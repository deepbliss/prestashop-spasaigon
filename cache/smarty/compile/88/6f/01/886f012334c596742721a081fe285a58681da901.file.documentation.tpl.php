<?php /* Smarty version Smarty-3.1.19, created on 2018-09-26 10:51:35
         compiled from "/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/documentation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2611250945bab02476b0c12-80300660%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '886f012334c596742721a081fe285a58681da901' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/documentation.tpl',
      1 => 1536668036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2611250945bab02476b0c12-80300660',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_display' => 0,
    'module_dir' => 0,
    'guide_link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bab02477d40b0_36280972',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bab02477d40b0_36280972')) {function content_5bab02477d40b0_36280972($_smarty_tpl) {?>

<div class="tab-pane panel" id="tab_documentation">
    <h3>
        <i class="icon-book"></i> <?php echo smartyTranslate(array('s'=>'Documentation','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <small><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small>
    </h3>
    <p><?php echo smartyTranslate(array('s'=>'The Discount Vouchers module developed by PrestaShop allows you to generate discount vouchers and automatically send them to your customers who have placed a certain type of order.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</p>
    <p><?php echo smartyTranslate(array('s'=>'','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</p>
    <ul>
        <li><?php echo smartyTranslate(array('s'=>'Automatically create cart rules that generate offer codes emailed to your customers','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</li>
        <li><?php echo smartyTranslate(array('s'=>'Customize the percentage or amount of discount vouchers','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</li>
        <li><?php echo smartyTranslate(array('s'=>'Choose the expiration date of your discount vouchers','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</li>
    </ul>
    <p>&nbsp;</p>
    <div class="media">
        <a class="pull-left" target="_blank" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['guide_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <img heigh="64" width="64" class="media-object" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
views/img/pdf.png" alt="" title=""/>
        </a>
        <div class="media-body">
            <h4 class="media-heading"><p><?php echo smartyTranslate(array('s'=>'Attached you will find the documentation for your module. Please consult it to properly configure the module.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</p></h4>
            <p><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['guide_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Discount vouchers user guide','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</a></p>
            <p><?php echo smartyTranslate(array('s'=>'Access to Prestashop free documentation: ','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <a href="http://doc.prestashop.com/" target="_blank"><?php echo smartyTranslate(array('s'=>'Click here','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</a></p>
        </div>
    </div>

    <p>&nbsp;</p>
    <p><?php echo smartyTranslate(array('s'=>'Need help? You will find it on','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <a href="#tab_contact" class="contactus" data-toggle="tab" data-original-title="" title="<?php echo smartyTranslate(array('s'=>'Need help?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Contact tab.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</a></p>
</div>
<?php }} ?>
