<?php /* Smarty version Smarty-3.1.19, created on 2018-09-25 19:16:27
         compiled from "/home/pjmyczpl/public_html/demo/modules/amazzingblog/views/templates/front/category.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18088202725baa271beefb89-42199914%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3faa0fb499a1e80f5fa1dfdcef2bd21f8fef0c73' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/amazzingblog/views/templates/front/category.tpl',
      1 => 1528149062,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18088202725baa271beefb89-42199914',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ab_category' => 0,
    'category' => 0,
    'blog' => 0,
    'ab_cat_parents' => 0,
    'ab_subcategories' => 0,
    'cat' => 0,
    'ab_posts' => 0,
    'ab_settings' => 0,
    'ab_additional_filters' => 0,
    'name' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5baa271c036055_68483961',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5baa271c036055_68483961')) {function content_5baa271c036055_68483961($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['category'] = new Smarty_variable($_smarty_tpl->tpl_vars['ab_category']->value, null, 0);?>
<div class="amazzingblog category-page">
<?php if ($_smarty_tpl->tpl_vars['category']->value&&$_smarty_tpl->tpl_vars['category']->value['active']) {?>
	<?php if (!$_smarty_tpl->tpl_vars['blog']->value->is_17) {?><?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('breadcrumbs.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('parents'=>$_smarty_tpl->tpl_vars['ab_cat_parents']->value,'current_item'=>$_smarty_tpl->tpl_vars['category']->value['title']), 0);?>
<?php }?>
	<h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
</h2>
	<?php if (!empty($_smarty_tpl->tpl_vars['category']->value['description'])) {?>
		<div class="category-description">
			<?php echo $_smarty_tpl->tpl_vars['category']->value['description'];?>
 
		</div>
	<?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['ab_subcategories']->value)) {?>
		<div class="blog-subcategories">
		<?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ab_subcategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
?>
			<div class="blog-subcategory">
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
					<span class="blog-category-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cat']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
</span>
				</a>
				<span class="posts-num"><?php echo smartyTranslate(array('s'=>'%d posts','mod'=>'amazzingblog','sprintf'=>array($_smarty_tpl->tpl_vars['cat']->value['posts_num'])),$_smarty_tpl);?>
</span>
			</div>
		<?php } ?>
		</div>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['ab_posts']->value) {?>
		<div class="dynamic-posts">
			<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('post-list.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('posts'=>$_smarty_tpl->tpl_vars['ab_posts']->value,'settings'=>$_smarty_tpl->tpl_vars['ab_settings']->value), 0);?>

		</div>
	<?php }?>
	<form action="" class="additional-filters hidden">
		<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ab_additional_filters']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['name']->value=='active') {?><?php continue 1?><?php }?>
			<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo htmlspecialchars(implode(',',$_smarty_tpl->tpl_vars['value']->value), ENT_QUOTES, 'UTF-8', true);?>
">
		<?php } ?>
	</form>
<?php } else { ?>
	<div class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'Category not available','mod'=>'amazzingblog'),$_smarty_tpl);?>
</div>
<?php }?>
</div>

<?php }} ?>
