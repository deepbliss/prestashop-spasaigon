<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:43
         compiled from "/home/pjmyczpl/public_html/demo/modules/ybc_newsletter/views/templates/hook/template2.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18758603475bad98a783b045-99237229%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3fa7af83901ca68c2fa6c36c4776f53552e64cdd' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/ybc_newsletter/views/templates/hook/template2.tpl',
      1 => 1536060984,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18758603475bad98a783b045-99237229',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'YBC_NEWSLETTER_MOBILE_HIDE' => 0,
    'YBC_NEWSLETTER_TEMPLATE' => 0,
    'YBC_NEWSLETTER_IMAGE' => 0,
    'YBC_NEWSLETTER_ACTION' => 0,
    'YBC_NEWSLETTER_LOADING_IMG' => 0,
    'YBC_NEWSLETTER_LOGO' => 0,
    'YBC_NEWSLETTER_POPUP_TITLE' => 0,
    'YBC_NEWSLETTER_POPUP_CONTENT' => 0,
    'YBC_NEWSLETTER_fb_url' => 0,
    'YBC_NEWSLETTER_tw_url' => 0,
    'YBC_NEWSLETTER_gg_url' => 0,
    'YBC_NEWSLETTER_pin_url' => 0,
    'YBC_NEWSLETTER_vimeo_url' => 0,
    'YBC_NEWSLETTER_in_url' => 0,
    'YBC_NEWSLETTER_youtb_url' => 0,
    'YBC_NEWSLETTER_li_url' => 0,
    'YBC_NEWSLETTER_rss_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a7902cb5_65655164',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a7902cb5_65655164')) {function content_5bad98a7902cb5_65655164($_smarty_tpl) {?>
<div class="ybc-newsletter-popup<?php if ($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_MOBILE_HIDE']->value) {?> ynp-mobile-hide<?php }?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_TEMPLATE']->value, ENT_QUOTES, 'UTF-8', true);?>
 ybc-mail-wrapper">

        <div class="ynp-div-l2 ybc_animation">
            <div class="ynp-div-l3" >
                <div class="ybc_nlt_content ybc_animation">
                    <span id="ynp-close" class="ynp-close button" title="<?php echo smartyTranslate(array('s'=>'Close popup','mod'=>'ybc_newsletter'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'ybc_newsletter'),$_smarty_tpl);?>
</span>
                    <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_IMAGE']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_IMAGE']->value!='') {?>
                        <div class="img_bg"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_IMAGE']->value, ENT_QUOTES, 'UTF-8', true);?>
" alt="" /></div>
                    <?php }?>
                    <form class="ynp-form ynp-form-popup" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_ACTION']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post">
                        <div class="ynp-inner">
                            <div class="ynp-loading-div">
                                <img class="ynp-loading" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_LOADING_IMG']->value, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo smartyTranslate(array('s'=>'Loading...','mod'=>'ybc_newsletter'),$_smarty_tpl);?>
" />
                            </div>                            
                            <div class="ynp-inner-wrapper">
                                <?php if ($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_LOGO']->value) {?>
                                    <div class="header_logo_center">
                                        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_LOGO']->value, ENT_QUOTES, 'UTF-8', true);?>
" alt="" />                                    
                                    </div>
                                <?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_POPUP_TITLE']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_POPUP_TITLE']->value!='') {?>
                                    <h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_POPUP_TITLE']->value, ENT_QUOTES, 'UTF-8', true);?>
</h4>
                                <?php }?>
                                <?php echo $_smarty_tpl->tpl_vars['YBC_NEWSLETTER_POPUP_CONTENT']->value;?>

                                <div class="ynp-input-row">
                                    <label for="ynp-email-input"><?php echo smartyTranslate(array('s'=>'Email: ','mod'=>'ybc_newsletter'),$_smarty_tpl);?>
 </label>
                                    <input type="text" id="ynp-email-input" class="ynp-email-input" value="" placeholder="<?php echo smartyTranslate(array('s'=>'Enter your email...','mod'=>'ybc_newsletter'),$_smarty_tpl);?>
" />
                                    <input class="button ynp-submit" type="submit" name="ynpSubmit" id="ynp-submit" value="Subscribe" />
                                </div>                                
                                <div class="section_social">
                                    <ul>
                                        <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_fb_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_fb_url']->value!='') {?>
                                			<li class="facebook">
                                				<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_fb_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                					<span><i class="icon-facebook"></i></span>
                                                    <span class="icon_hover"><i class="icon-facebook"></i></span>
                                				</a>
                                			</li>
                                		<?php }?>
                                		<?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_tw_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_tw_url']->value!='') {?>
                                			<li class="twitter">
                                				<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_tw_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                					<span><i class="icon-twitter"></i></span>
                                                            <span class="icon_hover"><i class="icon-twitter"></i></span>
                                        				
                                				</a>
                                			</li>
                                		<?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_gg_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_gg_url']->value!='') {?>
                                        	<li class="google-plus">
                                        		<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_gg_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" rel="publisher">
                                        			<span><i class="fa fa-google"></i></span>
                                                            <span class="icon_hover"><i class="fa fa-google"></i></span>
                                                		
                                        		</a>
                                        	</li>
                                        <?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_pin_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_pin_url']->value!='') {?>
                                        	<li class="pinterest">
                                        		<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_pin_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                        			<span><i class="icon-pinterest-p"></i></span>
                                                            <span class="icon_hover"><i class="icon-pinterest-p"></i></span>
                                                		
                                        		</a>
                                        	</li>
                                        <?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_vimeo_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_vimeo_url']->value!='') {?>
                                        	<li class="vimeo">
                                        		<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_vimeo_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                        			<span><i class="fa fa-vimeo"></i></span>
                                                            <span class="icon_hover"><i class="fa fa-vimeo"></i></span>
                                                		
                                        		</a>
                                        	</li>
                                        <?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_in_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_in_url']->value!='') {?>
                                        	<li class="instagram">
                                        		<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_in_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                                <span><i class="icon-instagram"></i></span>
                                                            <span class="icon_hover"><i class="icon-instagram"></i></span>
                                                	
                                        		</a>
                                        	</li>
                                        <?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_youtb_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_youtb_url']->value!='') {?>
                                        	<li class="youtube">
                                        		<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_youtb_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                        			<span><i class="fa fa-youtube-play"></i></span>
                                                            <span class="icon_hover"><i class="fa fa-youtube-play"></i></span>
                                                		
                                        		</a>
                                        	</li>
                                        <?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_li_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_li_url']->value!='') {?>
                                			<li class="linkedin">
                                				<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_li_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                					<span><i class="icon-linkedin"></i></span>
                                                            <span class="icon_hover"><i class="icon-linkedin"></i></span>
                                        				
                                				</a>
                                			</li>
                                		<?php }?>
                                		<?php if (isset($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_rss_url']->value)&&$_smarty_tpl->tpl_vars['YBC_NEWSLETTER_rss_url']->value!='') {?>
                                			<li class="rss">
                                				<a class="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['YBC_NEWSLETTER_rss_url']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                                                    <span><i class="fa fa-rss"></i></span>
                                					<span class="icon_hover"><i class="fa fa-rss"></i></span>
                                				</a>
                                			</li>
                                		<?php }?>
                                        
                                        
                                        
                                        
                                        
                                    </ul>
                                </div>
                                <div class="ynp-input-checkbox">
                                    <input type="checkbox" id="ynp-input-dont-show" class="ynp-input-dont-show" name="ynpcheckbox" />
                                    <label for="ynp-input-dont-show"><?php echo smartyTranslate(array('s'=>'Do not show this again','mod'=>'ybc_newsletter'),$_smarty_tpl);?>
</label>
                                </div>               
                            </div>
                        </div>
                    </form>
                    <div class="ybc-pp-clear"></div>
                </div>
            </div>
        </div>
</div><?php }} ?>
