<?php /* Smarty version Smarty-3.1.19, created on 2018-09-19 13:35:41
         compiled from "/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/configure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14388117135ba1ee3df02510-07476282%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ebb89236b626525910c15cf193bebe336fc92d2' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/configure.tpl',
      1 => 1528149244,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14388117135ba1ee3df02510-07476282',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'magic_quotes_warning' => 0,
    'multishop_note' => 0,
    'files_update_warnings' => 0,
    'file' => 0,
    'identifier' => 0,
    'custom_files' => 0,
    'type' => 0,
    'name' => 0,
    'hooks' => 0,
    'hk' => 0,
    'qty' => 0,
    'banners' => 0,
    'banners_in_wrapper' => 0,
    'id_wrapper' => 0,
    'bs_classes' => 0,
    'width' => 0,
    'class' => 0,
    'col' => 0,
    'documentation_link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba1ee3e0d5e81_74147813',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba1ee3e0d5e81_74147813')) {function content_5ba1ee3e0d5e81_74147813($_smarty_tpl) {?>

<div class="alert alert-danger general-ajax-errors" style="display:none;"></div>
<?php if ($_smarty_tpl->tpl_vars['magic_quotes_warning']->value) {?>
	<div class="alert alert-danger">
		<?php echo smartyTranslate(array('s'=>'You are advised to turn OFF "magic quotes" in your server configuration. This directive is deprecated since 2009 (when PHP 5.3.0 was released)','mod'=>'custombanners'),$_smarty_tpl);?>

		<i class="icon icon-times" data-dismiss="alert"></i>
	</div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['multishop_note']->value) {?>
	<div class="alert alert-warning">
		<?php echo smartyTranslate(array('s'=>'NOTE: All modifications will be applied to more than one shop','mod'=>'custombanners'),$_smarty_tpl);?>

		<i class="icon icon-times" data-dismiss="alert"></i>
	</div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['files_update_warnings']->value) {?>
	<div class="alert alert-warning">
		<?php echo smartyTranslate(array('s'=>'Some of your customized files have been updated in the new version','mod'=>'custombanners'),$_smarty_tpl);?>

		<ul>
		<?php  $_smarty_tpl->tpl_vars['identifier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['identifier']->_loop = false;
 $_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['files_update_warnings']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['identifier']->key => $_smarty_tpl->tpl_vars['identifier']->value) {
$_smarty_tpl->tpl_vars['identifier']->_loop = true;
 $_smarty_tpl->tpl_vars['file']->value = $_smarty_tpl->tpl_vars['identifier']->key;
?>
			<li>
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value, ENT_QUOTES, 'UTF-8', true);?>

				<span class="warning-advice">
					<?php echo smartyTranslate(array('s'=>'Make sure you update this file in your theme folder, and insert the following code to the last line','mod'=>'custombanners'),$_smarty_tpl);?>
:
					<span class="code"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['identifier']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
				</span>
			</li>
		<?php } ?>
		</ul>
	</div>
<?php }?>
<div class="bootstrap custombanners panel clearfix">
	<h3><i class="icon icon-cogs"></i> <?php echo smartyTranslate(array('s'=>'General settings','mod'=>'custombanners'),$_smarty_tpl);?>
</h3>
	<div class="col-lg-6">
		<?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom_files']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['type']->value = $_smarty_tpl->tpl_vars['name']->key;
?>
			<a href="#" class="custom-code" data-toggle="modal" data-target="#custom-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true);?>
-form" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
">
				<span class="monospace">{}</span> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>

			</a>
		<?php } ?>
	</div>
	<div class="col-lg-6 importer pull-right text-right">
		<form action="" method="post">
			<input type="hidden" name="action" value="exportBannersData">
			<button type="submit" class="export btn btn-default">
				<i class="icon icon-cloud-download icon-lg"></i>
				<?php echo smartyTranslate(array('s'=>'Export all banners','mod'=>'custombanners'),$_smarty_tpl);?>

			</button>
		</form>
		<button class="importBannersData btn btn-default">
			<i class="icon icon-cloud-upload icon-lg"></i>
			<?php echo smartyTranslate(array('s'=>'Import banners','mod'=>'custombanners'),$_smarty_tpl);?>

		</button>
		<form class="zip-uploader" action="" method="post" enctype="multipart/form-data" style="display:none;">
			<input type="file" name="zipped_banners_data">
			<input type="hidden" name="action" value="importBannersData">
			<button type="submit" class="import btn btn-default">
				<span class=""><?php echo smartyTranslate(array('s'=>'GO','mod'=>'custombanners'),$_smarty_tpl);?>
</span>
				<i class="icon icon-refresh icon-spin" style="display:none;"></i>
			</button>
		</form>
		<a href="#" data-toggle="modal" data-target="#modal_importer_info" title="<?php echo smartyTranslate(array('s'=>'About the importer','mod'=>'custombanners'),$_smarty_tpl);?>
">
			<i class="icon-info-circle importer-info"></i>
		</a>
	</div>
</div>
<div class="custombanners all-banners panel">
	<h3><i class="icon icon-image"></i> <?php echo smartyTranslate(array('s'=>'Banners','mod'=>'custombanners'),$_smarty_tpl);?>
</h3>
	<form class="settings form-horizontal row">
		<label class="control-label col-lg-1" for="hookSelector">
			<?php echo smartyTranslate(array('s'=>'Select hook','mod'=>'custombanners'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-3">
			<select class="hookSelector">
				<?php  $_smarty_tpl->tpl_vars['qty'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['qty']->_loop = false;
 $_smarty_tpl->tpl_vars['hk'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['hooks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['qty']->key => $_smarty_tpl->tpl_vars['qty']->value) {
$_smarty_tpl->tpl_vars['qty']->_loop = true;
 $_smarty_tpl->tpl_vars['hk']->value = $_smarty_tpl->tpl_vars['qty']->key;
?>
					<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if (Tools::getValue('hook')==$_smarty_tpl->tpl_vars['hk']->value) {?> selected<?php }?>> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
 (<?php echo intval($_smarty_tpl->tpl_vars['qty']->value);?>
) </option>
				<?php } ?>
			</select>
		</div>
		<div class="col-lg-8 hook-settings">
			<button class="btn btn-default callSettings" data-settings="exceptions">
				<i class="icon-ban"></i> <?php echo smartyTranslate(array('s'=>'Exceptions','mod'=>'custombanners'),$_smarty_tpl);?>

			</button>
			<button class="btn btn-default callSettings" data-settings="positions">
				<i class="icon-arrows-alt"></i> <?php echo smartyTranslate(array('s'=>'Module positions','mod'=>'custombanners'),$_smarty_tpl);?>

			</button>
			<button class="addWrapper btn btn-default pull-right">
				<span class="custom-wrapper-icon">
					<i class="t-l"></i><i class="t-r"></i><i class="b-r"></i><i class="b-l"></i>
				</span>
					<?php echo smartyTranslate(array('s'=>'New Wrapper','mod'=>'custombanners'),$_smarty_tpl);?>

			</button>
		</div>
	</form>
	<div id="settings-content" style="display:none;"></div>
	<?php  $_smarty_tpl->tpl_vars['qty'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['qty']->_loop = false;
 $_smarty_tpl->tpl_vars['hk'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['hooks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['qty']->key => $_smarty_tpl->tpl_vars['qty']->value) {
$_smarty_tpl->tpl_vars['qty']->_loop = true;
 $_smarty_tpl->tpl_vars['hk']->value = $_smarty_tpl->tpl_vars['qty']->key;
?>
	<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="hook-content<?php if ($_smarty_tpl->tpl_vars['hk']->value=='displayHome') {?> active<?php }?>">
		<?php if (substr($_smarty_tpl->tpl_vars['hk']->value,0,20)=='displayCustomBanners') {?>
		<div class="alert alert-info">
			<?php echo smartyTranslate(array('s'=>'In order to display this hook, insert the following code to any tpl','mod'=>'custombanners'),$_smarty_tpl);?>
: <strong>{hook h='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hk']->value, ENT_QUOTES, 'UTF-8', true);?>
'}</strong>
		</div>
		<?php }?>
		<div class="wrappers-container">
			<?php if (isset($_smarty_tpl->tpl_vars['banners']->value[$_smarty_tpl->tpl_vars['hk']->value])) {?>
				<?php  $_smarty_tpl->tpl_vars['banners_in_wrapper'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['banners_in_wrapper']->_loop = false;
 $_smarty_tpl->tpl_vars['id_wrapper'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banners']->value[$_smarty_tpl->tpl_vars['hk']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['banners_in_wrapper']->key => $_smarty_tpl->tpl_vars['banners_in_wrapper']->value) {
$_smarty_tpl->tpl_vars['banners_in_wrapper']->_loop = true;
 $_smarty_tpl->tpl_vars['id_wrapper']->value = $_smarty_tpl->tpl_vars['banners_in_wrapper']->key;
?>
					<?php echo $_smarty_tpl->getSubTemplate ("./wrapper-form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('banners'=>$_smarty_tpl->tpl_vars['banners_in_wrapper']->value,'id_wrapper'=>$_smarty_tpl->tpl_vars['id_wrapper']->value), 0);?>

				<?php } ?>
			<?php }?>
		</div>
	</div>
	<?php } ?>
	<div class="classes-wrapper" style="display:none;">
	<div class="predefined-classes round-border">
		<i class="caret-t"></i>
		<div class="col-md-6">
			<?php  $_smarty_tpl->tpl_vars['width'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['width']->_loop = false;
 $_smarty_tpl->tpl_vars['class'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['bs_classes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['width']->key => $_smarty_tpl->tpl_vars['width']->value) {
$_smarty_tpl->tpl_vars['width']->_loop = true;
 $_smarty_tpl->tpl_vars['class']->value = $_smarty_tpl->tpl_vars['width']->key;
?>
			<div class="row">
				<label class="control-label grey-note col-md-11">
					<?php echo smartyTranslate(array('s'=>'Banner width for displays, wider than %d','mod'=>'custombanners','sprintf'=>$_smarty_tpl->tpl_vars['width']->value),$_smarty_tpl);?>
:
				</label>
				<div class="col-md-1">
					<div class="multiclass">
						<i class="icon-bars cursor-pointer"></i>
						<div class="list" style="display:none;">
							<i class="caret-l"></i>
							<ul>
							<?php $_smarty_tpl->tpl_vars['col'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['col']->step = 1;$_smarty_tpl->tpl_vars['col']->total = (int) ceil(($_smarty_tpl->tpl_vars['col']->step > 0 ? 12+1 - (1) : 1-(12)+1)/abs($_smarty_tpl->tpl_vars['col']->step));
if ($_smarty_tpl->tpl_vars['col']->total > 0) {
for ($_smarty_tpl->tpl_vars['col']->value = 1, $_smarty_tpl->tpl_vars['col']->iteration = 1;$_smarty_tpl->tpl_vars['col']->iteration <= $_smarty_tpl->tpl_vars['col']->total;$_smarty_tpl->tpl_vars['col']->value += $_smarty_tpl->tpl_vars['col']->step, $_smarty_tpl->tpl_vars['col']->iteration++) {
$_smarty_tpl->tpl_vars['col']->first = $_smarty_tpl->tpl_vars['col']->iteration == 1;$_smarty_tpl->tpl_vars['col']->last = $_smarty_tpl->tpl_vars['col']->iteration == $_smarty_tpl->tpl_vars['col']->total;?>
								<li class="" data-class="col-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['class']->value, ENT_QUOTES, 'UTF-8', true);?>
-<?php echo intval($_smarty_tpl->tpl_vars['col']->value);?>
">
									<span class="cl"><span class="fragment">col-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['class']->value, ENT_QUOTES, 'UTF-8', true);?>
-</span><?php echo intval($_smarty_tpl->tpl_vars['col']->value);?>
</span>
									<span class="grey-note"><?php echo floatval(round(($_smarty_tpl->tpl_vars['col']->value*100/12),2));?>
%</span>
								</li>
							<?php }} ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="col-md-6">
			<div class="row">
				<label class="control-label grey-note col-md-8">
					<span class="label-tooltip" data-toggle="tooltip" title="<?php echo smartyTranslate(array('s'=>'Use this class if you want to place a caption over the image','mod'=>'custombanners'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Place HTML over the image','mod'=>'custombanners'),$_smarty_tpl);?>
:</span>
				</label>
				<div class="col-md-4">
					<div class="cl" data-class="html-over">html-over</div>
				</div>
			</div>
			<div class="row">
				<label class="control-label grey-note col-md-8">
					<span class="label-tooltip" data-toggle="tooltip" title="<?php echo smartyTranslate(array('s'=>'Affects all images within banner container if no other overrides are applied','mod'=>'custombanners'),$_smarty_tpl);?>
">
					<?php echo smartyTranslate(array('s'=>'Round borders for the images','mod'=>'custombanners'),$_smarty_tpl);?>
:
					</span>
				</label>
				<div class="col-md-4">
					<div class="cl" data-class="img-rb">img-rb</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="config-footer">
		<div class="btn-group bulk-actions dropup">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<?php echo smartyTranslate(array('s'=>'Bulk actions','mod'=>'custombanners'),$_smarty_tpl);?>
 <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li><a href="#"	class="bulk-select"><i class="icon-check-sign"></i> <?php echo smartyTranslate(array('s'=>'Check all','mod'=>'custombanners'),$_smarty_tpl);?>
</a></li>
				<li><a href="#" class="bulk-unselect"><i class="icon-check-empty"></i> <?php echo smartyTranslate(array('s'=>'Uncheck all','mod'=>'custombanners'),$_smarty_tpl);?>
</a></li>
				<li class="divider"></li>
				<li><a href="#" data-bulk-act="enable"><i class="icon-check on"></i> <?php echo smartyTranslate(array('s'=>'Enable','mod'=>'custombanners'),$_smarty_tpl);?>
</a></li>
				<li><a href="#" data-bulk-act="disable"><i class="icon-times off"></i> <?php echo smartyTranslate(array('s'=>'Disable','mod'=>'custombanners'),$_smarty_tpl);?>
</a></li>
				<li class="dont-hide">
					<a href="#" class="toggle-hook-list"><i class="icon icon-copy"></i> <?php echo smartyTranslate(array('s'=>'Copy to hook','mod'=>'custombanners'),$_smarty_tpl);?>
</a>
					<div class="dynamic-hook-list" style="display:none;">
						<button class="btn btn-default" data-bulk-act="copy"><?php echo smartyTranslate(array('s'=>'OK','mod'=>'custombanners'),$_smarty_tpl);?>
</button>
					</div>
				</li>
				<li class="dont-hide">
					<a href="#" class="toggle-hook-list"><i class="icon icon-arrow-left"></i> <?php echo smartyTranslate(array('s'=>'Move to hook','mod'=>'custombanners'),$_smarty_tpl);?>
</a>
					<div class="dynamic-hook-list" style="display:none;">
						<button class="btn btn-default" data-bulk-act="move"><?php echo smartyTranslate(array('s'=>'OK','mod'=>'custombanners'),$_smarty_tpl);?>
</button>
					</div>
				</li>
				<li class="divider"></li>
				<li><a href="#" data-bulk-act="delete"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'custombanners'),$_smarty_tpl);?>
</a></li>
			</ul>
		</div>
		<button type="button" class="deleteAll btn btn-default pull-right" data-bulk-act="deleteAll">
			<i class="icon icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete all banners','mod'=>'custombanners'),$_smarty_tpl);?>

		</button>
	</div>
</div>


<a id="module-documentation" class="toolbar_btn hidden" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['documentation_link']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank" title="<?php echo smartyTranslate(array('s'=>'Documentation','mod'=>'custombanners'),$_smarty_tpl);?>
">
	<i class="process-icon-t icon-file-text"></i>
	<div><?php echo smartyTranslate(array('s'=>'Documentation','mod'=>'custombanners'),$_smarty_tpl);?>
</div>
</a>

<script type="text/javascript">
	$(document).ready(function(){
		$('ul.nav.nav-pills').prepend('<li class="li-docs"></li>');
		$('#module-documentation').prependTo('.li-docs').removeClass('hidden');
	});
</script>


<?php }} ?>
