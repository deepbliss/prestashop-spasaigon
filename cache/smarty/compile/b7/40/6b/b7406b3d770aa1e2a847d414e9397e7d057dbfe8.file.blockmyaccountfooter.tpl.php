<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:43
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/blockmyaccountfooter/blockmyaccountfooter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18540787715bad98a74b8b08-02571476%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b7406b3d770aa1e2a847d414e9397e7d057dbfe8' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/blockmyaccountfooter/blockmyaccountfooter.tpl',
      1 => 1534411492,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18540787715bad98a74b8b08-02571476',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'returnAllowed' => 0,
    'voucherAllowed' => 0,
    'HOOK_BLOCK_MY_ACCOUNT' => 0,
    'is_logged' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a7595726_86494193',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a7595726_86494193')) {function content_5bad98a7595726_86494193($_smarty_tpl) {?>

<!-- Block myaccount module -->
<section class="footer-block col-xs-12 col-sm-2 user-myaccount">
	<h4><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Manage my customer account','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My account','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></h4>
	<div class="block_content toggle-footer">
		<ul class="bullet">
			<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('history',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'My orders','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My orders','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></li>
			<?php if ($_smarty_tpl->tpl_vars['returnAllowed']->value) {?><li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-follow',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'My merchandise returns','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My merchandise returns','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></li><?php }?>
			<!--<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'My credit slips','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My credit slips','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></li>
-->			<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'My addresses','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My addresses','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></li>
			<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('identity',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Manage my personal information','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My personal info','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></li>
			<?php if ($_smarty_tpl->tpl_vars['voucherAllowed']->value) {?><li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('discount',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'My vouchers','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'My vouchers','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></li><?php }?>
			<?php echo $_smarty_tpl->tpl_vars['HOOK_BLOCK_MY_ACCOUNT']->value;?>

            <?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('index');?>
?mylogout" title="<?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'blockmyaccountfooter'),$_smarty_tpl);?>
</a></li><?php }?>
		</ul>
	</div>
    
<!--    ********************************-->
   <div class="footer-block corporate-sales">
    <h4 class="len"><?php echo smartyTranslate(array('s'=>'Corporate Sales'),$_smarty_tpl);?>
</h4>
    <h4 class="lvn"><?php echo smartyTranslate(array('s'=>'Hợp tác kinh doanh'),$_smarty_tpl);?>
</h4>
    <div class="block_content toggle-footer">
        <ul class="bullet">
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink('8','CMSpage');?>
" title="<?php echo smartyTranslate(array('s'=>'Wholesale','mod'=>'blockcms'),$_smarty_tpl);?>
" class="len"><?php echo smartyTranslate(array('s'=>'Wholesale','mod'=>'blockcms'),$_smarty_tpl);?>
</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink('8','CMSpage');?>
" title="<?php echo smartyTranslate(array('s'=>'Wholesale','mod'=>'blockcms'),$_smarty_tpl);?>
" class="lvn"><?php echo smartyTranslate(array('s'=>'Kinh doanh sỉ','mod'=>'blockcms'),$_smarty_tpl);?>
</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink('9','CMSpage');?>
" title="<?php echo smartyTranslate(array('s'=>'Hotel Amenities','mod'=>'blockcms'),$_smarty_tpl);?>
" class="len"><?php echo smartyTranslate(array('s'=>'Hotel Amenities','mod'=>'blockcms'),$_smarty_tpl);?>
</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink('9','CMSpage');?>
" title="<?php echo smartyTranslate(array('s'=>'Hotel Amenities','mod'=>'blockcms'),$_smarty_tpl);?>
" class="lvn"><?php echo smartyTranslate(array('s'=>'Sản phẩm dành cho khách sạn','mod'=>'blockcms'),$_smarty_tpl);?>
</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink('10','CMSpage');?>
" title="<?php echo smartyTranslate(array('s'=>'Corporate Gifts','mod'=>'blockcms'),$_smarty_tpl);?>
" class="len"><?php echo smartyTranslate(array('s'=>'Corporate Gifts','mod'=>'blockcms'),$_smarty_tpl);?>
</a></li>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink('10','CMSpage');?>
" title="<?php echo smartyTranslate(array('s'=>'Corporate Gifts','mod'=>'blockcms'),$_smarty_tpl);?>
" class="lvn"><?php echo smartyTranslate(array('s'=>'Quà tặng liên kết','mod'=>'blockcms'),$_smarty_tpl);?>
</a></li>
       </ul>
    </div>
</div>
</section>

<!-- /Block myaccount module -->
<?php }} ?>
