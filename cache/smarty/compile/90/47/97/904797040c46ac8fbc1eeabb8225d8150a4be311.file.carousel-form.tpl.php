<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 20:12:50
         compiled from "/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/carousel-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:649186315ba0f9d23aa991-23551840%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '904797040c46ac8fbc1eeabb8225d8150a4be311' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/carousel-form.tpl',
      1 => 1528149274,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '649186315ba0f9d23aa991-23551840',
  'function' => 
  array (
    'renderMultilangField' => 
    array (
      'parameter' => 
      array (
        'label_col' => 3,
        'input_col' => 9,
      ),
      'compiled' => '',
    ),
    'renderSettings' => 
    array (
      'parameter' => 
      array (
        's_type' => '',
        's_name' => '',
        'rows' => 6,
        'cols' => 3,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'carousel' => 0,
    'id_carousel' => 0,
    'ec' => 0,
    'full' => 0,
    'type_names' => 0,
    'group_name' => 0,
    'options' => 0,
    'input_name' => 0,
    'display_name' => 0,
    'label_col' => 0,
    'tooltip' => 0,
    'input_col' => 0,
    'languages' => 0,
    'mce' => 0,
    'lang' => 0,
    'id_lang_current' => 0,
    'value' => 0,
    's_type' => 0,
    'multidevice_settings' => 0,
    's_name' => 0,
    'multiple_devices' => 0,
    'device_types' => 0,
    'd_type' => 0,
    'name' => 0,
    'cols' => 0,
    'fields' => 0,
    'col' => 0,
    'rows' => 0,
    'column_fields' => 0,
    'field' => 0,
    'grid' => 0,
    'device_type' => 0,
    'device_suffix' => 0,
    'k' => 0,
    's_type_with_suffix' => 0,
    'custom_value' => 0,
    'id' => 0,
    'yes_no_types' => 0,
    'img_type' => 0,
    'opt_class' => 0,
    'saved' => 0,
    'val' => 0,
    'key' => 0,
    'selector' => 0,
    'opt' => 0,
    'multishop_warning' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0f9d2777e34_95218636',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0f9d2777e34_95218636')) {function content_5ba0f9d2777e34_95218636($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['id_carousel'] = new Smarty_variable($_smarty_tpl->tpl_vars['carousel']->value['id_carousel'], null, 0);?>
<div class="c-item clearfix" data-id="<?php echo intval($_smarty_tpl->tpl_vars['id_carousel']->value);?>
">
<form method="post" action="" class="form-horizontal desktop">
	<div class="carousel-header clearfix">
		<input type="checkbox" value="<?php echo intval($_smarty_tpl->tpl_vars['id_carousel']->value);?>
" class="carousel-box" title="<?php echo smartyTranslate(array('s'=>'Bulk actions','mod'=>'easycarousels'),$_smarty_tpl);?>
">
		<span class="carousel-name">
			<?php if (empty($_smarty_tpl->tpl_vars['carousel']->value['name'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ec']->value->getCarouselName($_smarty_tpl->tpl_vars['carousel']->value['type']), ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>
			<?php if (!empty($_smarty_tpl->tpl_vars['carousel']->value['exc_note'])) {?><span class="exc-note"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['exc_note'], ENT_QUOTES, 'UTF-8', true);?>
</span><?php }?>
		</span>
		<span class="actions pull-right">
			<label class="label-checkbox">
				<input type="checkbox" name="in_tabs" value="1" class="toggleable_param"<?php if ($_smarty_tpl->tpl_vars['carousel']->value['in_tabs']) {?> checked<?php }?>>
				<?php echo smartyTranslate(array('s'=>'Grouped in tabs','mod'=>'easycarousels'),$_smarty_tpl);?>

			</label>
			<label class="activateCarousel list-action-enable action-<?php if ($_smarty_tpl->tpl_vars['carousel']->value['active']==1) {?>enabled<?php } else { ?>disabled<?php }?>" title="<?php echo smartyTranslate(array('s'=>'Activate/Deactivate','mod'=>'easycarousels'),$_smarty_tpl);?>
">
				<i class="icon-check"></i>
				<i class="icon-remove"></i>
				<input type="checkbox" name="active" value="1" class="toggleable_param hidden"<?php if ($_smarty_tpl->tpl_vars['carousel']->value['active']) {?> checked<?php }?>>
			</label>
			<div class="actions btn-group pull-right">
				<button title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'easycarousels'),$_smarty_tpl);?>
" class="editCarousel btn btn-default" data-id="<?php echo intval($_smarty_tpl->tpl_vars['id_carousel']->value);?>
">
					<i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'easycarousels'),$_smarty_tpl);?>

				</button>
				<button title="<?php echo smartyTranslate(array('s'=>'Scroll Up','mod'=>'easycarousels'),$_smarty_tpl);?>
" class="scrollUp btn btn-default">
					<i class="icon icon-minus"></i> <?php echo smartyTranslate(array('s'=>'Cancel','mod'=>'easycarousels'),$_smarty_tpl);?>

				</button>
				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
					<i class="icon-caret-down"></i>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a class="deleteCarousel" href="#" onclick="event.preventDefault();">
							<i class="icon icon-trash"></i>
							<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'easycarousels'),$_smarty_tpl);?>

						</a>
					</li>
				</ul>
				<i class="dragger icon icon-arrows-v icon-2x"></i>
			</div>
		</span>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['full']->value) {?>
	<div class="carousel-details" style="display:none;">
		<div class="ajax_errors" style="display:none;"></div>
		<div class="col-lg-4">
			<div class="form-group">
				<label class="control-label col-lg-6" for="carousel_type">
					<?php echo smartyTranslate(array('s'=>'Carousel type','mod'=>'easycarousels'),$_smarty_tpl);?>

				</label>
				<div class="col-lg-6">
					<select name="type" id="carousel_type">
						<?php  $_smarty_tpl->tpl_vars['options'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['options']->_loop = false;
 $_smarty_tpl->tpl_vars['group_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['type_names']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['options']->key => $_smarty_tpl->tpl_vars['options']->value) {
$_smarty_tpl->tpl_vars['options']->_loop = true;
 $_smarty_tpl->tpl_vars['group_name']->value = $_smarty_tpl->tpl_vars['options']->key;
?>
						<optgroup label="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['group_name']->value, ENT_QUOTES, 'UTF-8', true);?>
">
							<?php  $_smarty_tpl->tpl_vars['display_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['display_name']->_loop = false;
 $_smarty_tpl->tpl_vars['input_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['display_name']->key => $_smarty_tpl->tpl_vars['display_name']->value) {
$_smarty_tpl->tpl_vars['display_name']->_loop = true;
 $_smarty_tpl->tpl_vars['input_name']->value = $_smarty_tpl->tpl_vars['display_name']->key;
?>
								<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['carousel']->value['type']==$_smarty_tpl->tpl_vars['input_name']->value) {?> selected<?php }?>>
									<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['display_name']->value, ENT_QUOTES, 'UTF-8', true);?>

								</option>
							<?php } ?>
						</optgroup>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>

		<?php if (!function_exists('smarty_template_function_renderMultilangField')) {
    function smarty_template_function_renderMultilangField($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['renderMultilangField']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
			<div class="form-group">
				<label class="control-label col-lg-<?php echo intval($_smarty_tpl->tpl_vars['label_col']->value);?>
" for="carousel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
">
					<span<?php if ($_smarty_tpl->tpl_vars['tooltip']->value) {?> class="label-tooltip" data-toggle="tooltip" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tooltip']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['display_name']->value, ENT_QUOTES, 'UTF-8', true);?>

					</span>
				</label>
				<div class="col-lg-<?php echo intval($_smarty_tpl->tpl_vars['input_col']->value);?>
 has-lang-selector">
					<?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['lang']->_loop = true;
?>
						<?php if (empty($_smarty_tpl->tpl_vars['mce']->value)) {?>
							<?php if (isset($_smarty_tpl->tpl_vars['carousel']->value['multilang'][$_smarty_tpl->tpl_vars['lang']->value['id_lang']][$_smarty_tpl->tpl_vars['input_name']->value])) {?><?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['carousel']->value['multilang'][$_smarty_tpl->tpl_vars['lang']->value['id_lang']][$_smarty_tpl->tpl_vars['input_name']->value], null, 0);?><?php } else { ?><?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable('', null, 0);?><?php }?>
							<input type="text" name="multilang[<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
]" class="multilang lang_<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
<?php if ($_smarty_tpl->tpl_vars['lang']->value['id_lang']!=$_smarty_tpl->tpl_vars['id_lang_current']->value) {?> hidden<?php }?><?php if (!$_smarty_tpl->tpl_vars['id_carousel']->value&&$_smarty_tpl->tpl_vars['input_name']->value=='name') {?> name-not-saved<?php }?>" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8', true);?>
">
						<?php } else { ?>
							<div class="multilang lang_<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
<?php if ($_smarty_tpl->tpl_vars['lang']->value['id_lang']!=$_smarty_tpl->tpl_vars['id_lang_current']->value) {?> hidden<?php }?>">
								<textarea name="multilang[<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
]" id="multilang_<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="mce"><?php if (isset($_smarty_tpl->tpl_vars['carousel']->value['multilang'][$_smarty_tpl->tpl_vars['lang']->value['id_lang']][$_smarty_tpl->tpl_vars['input_name']->value])) {?><?php echo $_smarty_tpl->tpl_vars['carousel']->value['multilang'][$_smarty_tpl->tpl_vars['lang']->value['id_lang']][$_smarty_tpl->tpl_vars['input_name']->value];?>
<?php }?></textarea>
							</div>
						<?php }?>
					<?php } ?>
					<div class="languages pull-right">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							<?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['lang']->_loop = true;
?>
								<span class="multilang lang_<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
<?php if ($_smarty_tpl->tpl_vars['lang']->value['id_lang']!=$_smarty_tpl->tpl_vars['id_lang_current']->value) {?> hidden<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lang']->value['iso_code'], ENT_QUOTES, 'UTF-8', true);?>
</span>
							<?php } ?>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['lang']->_loop = true;
?>
							<li>
								<a href="#" class="lang_switcher" data-id-lang="<?php echo intval($_smarty_tpl->tpl_vars['lang']->value['id_lang']);?>
" onclick="event.preventDefault();">
									<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lang']->value['name'], ENT_QUOTES, 'UTF-8', true);?>

								</a>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>


		<div class="col-lg-8">
			<?php $_smarty_tpl->_capture_stack[0][] = array('display_name', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Carousel title','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
			<?php $_smarty_tpl->_capture_stack[0][] = array('field_tooltip', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'You may leave this field empty for carousels that are not in tabs','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
			<?php smarty_template_function_renderMultilangField($_smarty_tpl,array('input_name'=>'name','display_name'=>Smarty::$_smarty_vars['capture']['display_name'],'tooltip'=>Smarty::$_smarty_vars['capture']['field_tooltip'],'label_col'=>3,'input_col'=>9));?>

		</div>
		<div class="col-lg-12">
			<?php $_smarty_tpl->_capture_stack[0][] = array('display_name', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Carousel description','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
			<?php $_smarty_tpl->_capture_stack[0][] = array('field_tooltip', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'You may leave this field empty','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
			<?php smarty_template_function_renderMultilangField($_smarty_tpl,array('input_name'=>'description','display_name'=>Smarty::$_smarty_vars['capture']['display_name'],'tooltip'=>Smarty::$_smarty_vars['capture']['field_tooltip'],'label_col'=>2,'input_col'=>10,'mce'=>1));?>

		</div>

		<?php if (!function_exists('smarty_template_function_renderSettings')) {
    function smarty_template_function_renderSettings($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['renderSettings']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
		<?php $_smarty_tpl->tpl_vars['multiple_devices'] = new Smarty_variable(isset($_smarty_tpl->tpl_vars['multidevice_settings']->value[$_smarty_tpl->tpl_vars['s_type']->value]), null, 0);?>
		<div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['s_type']->value, ENT_QUOTES, 'UTF-8', true);?>
">
			<?php if ($_smarty_tpl->tpl_vars['s_name']->value) {?>
				<div class="col-lg-12">
					<h4 class="col-lg-10 col-lg-offset-2 settings-title">
						<span class="txt"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['s_name']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
						<?php if ($_smarty_tpl->tpl_vars['multiple_devices']->value) {?>
							<?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['d_type'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['device_types']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['d_type']->value = $_smarty_tpl->tpl_vars['name']->key;
?><a href="#" class="device-type <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['d_type']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8', true);?>
" data-type="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['d_type']->value, ENT_QUOTES, 'UTF-8', true);?>
"><i class="icon-<?php echo $_smarty_tpl->tpl_vars['d_type']->value;?>
"></i></a><?php } ?>
						<?php }?>
					</h4>
				</div>
			<?php }?>
			<?php $_smarty_tpl->tpl_vars['col'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['col']->step = 1;$_smarty_tpl->tpl_vars['col']->total = (int) ceil(($_smarty_tpl->tpl_vars['col']->step > 0 ? ($_smarty_tpl->tpl_vars['cols']->value-1)+1 - (0) : 0-(($_smarty_tpl->tpl_vars['cols']->value-1))+1)/abs($_smarty_tpl->tpl_vars['col']->step));
if ($_smarty_tpl->tpl_vars['col']->total > 0) {
for ($_smarty_tpl->tpl_vars['col']->value = 0, $_smarty_tpl->tpl_vars['col']->iteration = 1;$_smarty_tpl->tpl_vars['col']->iteration <= $_smarty_tpl->tpl_vars['col']->total;$_smarty_tpl->tpl_vars['col']->value += $_smarty_tpl->tpl_vars['col']->step, $_smarty_tpl->tpl_vars['col']->iteration++) {
$_smarty_tpl->tpl_vars['col']->first = $_smarty_tpl->tpl_vars['col']->iteration == 1;$_smarty_tpl->tpl_vars['col']->last = $_smarty_tpl->tpl_vars['col']->iteration == $_smarty_tpl->tpl_vars['col']->total;?>
			<?php $_smarty_tpl->tpl_vars['column_fields'] = new Smarty_variable(array_slice($_smarty_tpl->tpl_vars['fields']->value[$_smarty_tpl->tpl_vars['s_type']->value],($_smarty_tpl->tpl_vars['col']->value*$_smarty_tpl->tpl_vars['rows']->value),$_smarty_tpl->tpl_vars['rows']->value), null, 0);?>
				<?php if ($_smarty_tpl->tpl_vars['column_fields']->value) {?>
				<div class="col-lg-<?php echo 12/intval($_smarty_tpl->tpl_vars['cols']->value);?>
">
				<?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['column_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['field']->key;
?>
					<?php if ($_smarty_tpl->tpl_vars['s_type']->value=='carousel'&&$_smarty_tpl->tpl_vars['col']->value>1) {?><?php $_smarty_tpl->tpl_vars['grid'] = new Smarty_variable(array(10,2), null, 0);?><?php } elseif ($_smarty_tpl->tpl_vars['s_type']->value=='exceptions') {?><?php $_smarty_tpl->tpl_vars['grid'] = new Smarty_variable(array(2,10), null, 0);?><?php } else { ?><?php $_smarty_tpl->tpl_vars['grid'] = new Smarty_variable(array(6,6), null, 0);?><?php }?>
					<?php if (!empty($_smarty_tpl->tpl_vars['field']->value['separator'])) {?>
						<div class="form-group<?php if (isset($_smarty_tpl->tpl_vars['field']->value['class'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['class'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>"><h4 class="field-separator col-lg-offset-<?php echo intval($_smarty_tpl->tpl_vars['grid']->value[1]);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['separator'], ENT_QUOTES, 'UTF-8', true);?>
:</h4></div>
					<?php }?>
					<div class="form-group<?php if (isset($_smarty_tpl->tpl_vars['field']->value['class'])) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['class'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
						<label class="control-label col-lg-<?php echo intval($_smarty_tpl->tpl_vars['grid']->value[0]);?>
">
							<span<?php if (isset($_smarty_tpl->tpl_vars['field']->value['tooltip'])) {?> class="label-tooltip" data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['field']->value['tooltip'];?>
"<?php }?>>
								<?php echo $_smarty_tpl->tpl_vars['field']->value['name'];?>

							</span>
						</label>
						<?php  $_smarty_tpl->tpl_vars['device_type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['device_type']->_loop = false;
 $_from = array_keys($_smarty_tpl->tpl_vars['device_types']->value); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['device_type']->key => $_smarty_tpl->tpl_vars['device_type']->value) {
$_smarty_tpl->tpl_vars['device_type']->_loop = true;
?>
						<?php if ($_smarty_tpl->tpl_vars['device_type']->value=='desktop') {?><?php $_smarty_tpl->tpl_vars['device_suffix'] = new Smarty_variable('', null, 0);?><?php } else { ?><?php $_smarty_tpl->tpl_vars['device_suffix'] = new Smarty_variable(('_').($_smarty_tpl->tpl_vars['device_type']->value), null, 0);?><?php }?>
						<?php if (!$_smarty_tpl->tpl_vars['multiple_devices']->value&&$_smarty_tpl->tpl_vars['device_suffix']->value) {?><?php continue 1?><?php }?>
						<?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable(((($_smarty_tpl->tpl_vars['k']->value).('_')).($_smarty_tpl->tpl_vars['id_carousel']->value)).($_smarty_tpl->tpl_vars['device_suffix']->value), null, 0);?>
						<?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['field']->value['value'], null, 0);?>
						<?php if (isset($_smarty_tpl->tpl_vars['carousel']->value['settings'][$_smarty_tpl->tpl_vars['s_type']->value][$_smarty_tpl->tpl_vars['k']->value])) {?>
							
							<?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['carousel']->value['settings'][$_smarty_tpl->tpl_vars['s_type']->value][$_smarty_tpl->tpl_vars['k']->value], null, 0);?>
						<?php }?>
						<?php $_smarty_tpl->tpl_vars['custom_value'] = new Smarty_variable(false, null, 0);?>
						<?php $_smarty_tpl->tpl_vars['s_type_with_suffix'] = new Smarty_variable(($_smarty_tpl->tpl_vars['s_type']->value).($_smarty_tpl->tpl_vars['device_suffix']->value), null, 0);?>
						<?php if ($_smarty_tpl->tpl_vars['device_suffix']->value&&isset($_smarty_tpl->tpl_vars['carousel']->value['settings'][$_smarty_tpl->tpl_vars['s_type_with_suffix']->value][$_smarty_tpl->tpl_vars['k']->value])) {?>
							
							<?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['carousel']->value['settings'][$_smarty_tpl->tpl_vars['s_type_with_suffix']->value][$_smarty_tpl->tpl_vars['k']->value], null, 0);?>
							<?php $_smarty_tpl->tpl_vars['custom_value'] = new Smarty_variable(true, null, 0);?>
						<?php }?>
						<?php $_smarty_tpl->tpl_vars['input_name'] = new Smarty_variable((((('settings[').($_smarty_tpl->tpl_vars['s_type_with_suffix']->value)).('][')).($_smarty_tpl->tpl_vars['k']->value)).(']'), null, 0);?>
						<div class="col-lg-<?php echo intval($_smarty_tpl->tpl_vars['grid']->value[1]);?>
<?php if ($_smarty_tpl->tpl_vars['device_suffix']->value) {?> device-input-wrapper <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['device_type']->value, ENT_QUOTES, 'UTF-8', true);?>
 <?php if ($_smarty_tpl->tpl_vars['custom_value']->value) {?>un<?php }?>locked<?php }?>">
							<?php if ($_smarty_tpl->tpl_vars['device_suffix']->value) {?>
								<a href="#" class="lock-device-settings"><i class="icon-lock"></i><i class="icon-unlock-alt"></i></a>
								<div class="lock-overlay"><div class="same-note"><?php if ($_smarty_tpl->tpl_vars['grid']->value[1]>3) {?><?php echo smartyTranslate(array('s'=>'Same as on desktop','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php }?></div></div>
							<?php }?>
							<div class="input-content">
							<?php if ($_smarty_tpl->tpl_vars['field']->value['type']=='switcher') {?>
								<span class="switch prestashop-switch">
									<input type="radio" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="1"<?php if ($_smarty_tpl->tpl_vars['value']->value) {?> checked<?php }?> >
									<label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
">
										<?php $_smarty_tpl->tpl_vars['yes_no_types'] = new Smarty_variable(array('randomize','consider_cat','a','ah','normalize_h','l','title_one_line'), null, 0);?>
										<?php if (in_array($_smarty_tpl->tpl_vars['k']->value,$_smarty_tpl->tpl_vars['yes_no_types']->value)) {?><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Show','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php }?>
									</label>
									<input type="radio" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
_0" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="0"<?php if (!$_smarty_tpl->tpl_vars['value']->value) {?> checked<?php }?> >
									<label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
_0">
										<?php if (in_array($_smarty_tpl->tpl_vars['k']->value,$_smarty_tpl->tpl_vars['yes_no_types']->value)) {?><?php echo smartyTranslate(array('s'=>'No','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Hide','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php }?>
									</label>
									<a class="slide-button btn"></a>
								</span>
							<?php } elseif ($_smarty_tpl->tpl_vars['field']->value['type']=='select') {?>
								<select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="select_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'utf-8', true);?>
">
									<?php if ($_smarty_tpl->tpl_vars['k']->value=='image_type') {?>
										<?php  $_smarty_tpl->tpl_vars['opt_class'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['opt_class']->_loop = false;
 $_smarty_tpl->tpl_vars['img_type'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['field']->value['select']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['opt_class']->key => $_smarty_tpl->tpl_vars['opt_class']->value) {
$_smarty_tpl->tpl_vars['opt_class']->_loop = true;
 $_smarty_tpl->tpl_vars['img_type']->value = $_smarty_tpl->tpl_vars['opt_class']->key;
?>
											<?php $_smarty_tpl->tpl_vars['saved'] = new Smarty_variable($_smarty_tpl->tpl_vars['value']->value==$_smarty_tpl->tpl_vars['img_type']->value, null, 0);?>
											<option class="img-type-option <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['opt_class']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['saved']->value) {?> saved<?php }?>" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_type']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['saved']->value) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_type']->value, ENT_QUOTES, 'UTF-8', true);?>
</option>
										<?php } ?>
									<?php } else { ?>
										<?php  $_smarty_tpl->tpl_vars['display_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['display_name']->_loop = false;
 $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['field']->value['select']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['display_name']->key => $_smarty_tpl->tpl_vars['display_name']->value) {
$_smarty_tpl->tpl_vars['display_name']->_loop = true;
 $_smarty_tpl->tpl_vars['val']->value = $_smarty_tpl->tpl_vars['display_name']->key;
?>
											<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['val']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if (($_smarty_tpl->tpl_vars['val']->value).('')==($_smarty_tpl->tpl_vars['value']->value).('')) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['display_name']->value, ENT_QUOTES, 'UTF-8', true);?>
</option>
										<?php } ?>
									<?php }?>
								</select>
							<?php } elseif ($_smarty_tpl->tpl_vars['s_type']->value=='exceptions') {?>
								<?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable(array(), null, 0);?><?php if (isset($_smarty_tpl->tpl_vars['carousel']->value['settings']['exceptions'])) {?><?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['carousel']->value['settings']['exceptions'], null, 0);?><?php }?>
								<?php  $_smarty_tpl->tpl_vars['selector'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['selector']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['field']->value['selectors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['selector']->key => $_smarty_tpl->tpl_vars['selector']->value) {
$_smarty_tpl->tpl_vars['selector']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['selector']->key;
?>
									<div class="exceptions-block <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['ids']) {?> has-ids<?php }?>">
										<select name="settings[exceptions][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
][type]" class="exc <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
">
											<?php  $_smarty_tpl->tpl_vars['opt'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['opt']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['selector']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['opt']->key => $_smarty_tpl->tpl_vars['opt']->value) {
$_smarty_tpl->tpl_vars['opt']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['opt']->key;
?>
												<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['type']==$_smarty_tpl->tpl_vars['k']->value) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['opt']->value, ENT_QUOTES, 'UTF-8', true);?>
</option>
											<?php } ?>
										</select>
										<div class="input-group exc-ids">
											<span class="input-group-addon">
												<span class="label-tooltip" data-toggle="tooltip" title="<?php echo smartyTranslate(array('s'=>'For example: 11,15,18','mod'=>'easycarousels'),$_smarty_tpl);?>
">
													<?php echo smartyTranslate(array('s'=>'IDs','mod'=>'easycarousels'),$_smarty_tpl);?>

												</span>
											</span>
											<input type="text" name="settings[exceptions][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8', true);?>
][ids]" value="<?php if ($_smarty_tpl->tpl_vars['value']->value&&$_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['ids']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value[$_smarty_tpl->tpl_vars['key']->value]['ids'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" class="ids">
										</div>
									</div>
								<?php } ?>
							<?php } else { ?>
								<input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_name']->value, ENT_QUOTES, 'UTF-8', true);?>
" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8', true);?>
">
							<?php }?>
							</div>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
				<?php }?>
			<?php }} ?>
		</div>
		<?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>


		<?php smarty_template_function_renderSettings($_smarty_tpl,array('s_type'=>'special','s_name'=>'','rows'=>6));?>

		<?php smarty_template_function_renderSettings($_smarty_tpl,array('s_type'=>'php','s_name'=>'','rows'=>1));?>

        <?php $_smarty_tpl->_capture_stack[0][] = array('template_settings_txt', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Template settings','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php $_smarty_tpl->_capture_stack[0][] = array('carousel_settings_txt', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Carousel settings','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php $_smarty_tpl->_capture_stack[0][] = array('exceptions_txt', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Exceptions','mod'=>'easycarousels'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
		<?php smarty_template_function_renderSettings($_smarty_tpl,array('s_type'=>'tpl','s_name'=>Smarty::$_smarty_vars['capture']['template_settings_txt'],'rows'=>7));?>

		<?php smarty_template_function_renderSettings($_smarty_tpl,array('s_type'=>'carousel','s_name'=>Smarty::$_smarty_vars['capture']['carousel_settings_txt']));?>

		<?php smarty_template_function_renderSettings($_smarty_tpl,array('s_type'=>'exceptions','s_name'=>Smarty::$_smarty_vars['capture']['exceptions_txt'],'cols'=>1));?>


		<div class="p-footer">
			<input type="hidden" name="hook_name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carousel']->value['hook_name'], ENT_QUOTES, 'UTF-8', true);?>
">
			<input type="hidden" name="position" value="<?php echo intval($_smarty_tpl->tpl_vars['carousel']->value['position']);?>
">
			<input type="hidden" name="id_wrapper" value="<?php echo intval($_smarty_tpl->tpl_vars['carousel']->value['id_wrapper']);?>
">
			<button id="saveCarousel" class="btn btn-default">
				<i class="process-icon-save"></i>
				<?php echo smartyTranslate(array('s'=>'Save','mod'=>'easycarousels'),$_smarty_tpl);?>

			</button>
			<?php if ($_smarty_tpl->tpl_vars['multishop_warning']->value) {?>
				<div class="inline-block alert-warning"><?php echo smartyTranslate(array('s'=>'NOTE: Changes will be saved for more than one shop','mod'=>'easycarousels'),$_smarty_tpl);?>
</div>
			<?php }?>
		</div>
	</div>
	<?php }?>
</form>
</div>

<?php }} ?>
