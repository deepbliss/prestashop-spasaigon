<?php /* Smarty version Smarty-3.1.19, created on 2018-09-27 15:43:13
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/socialsharing/views/templates/hook/socialsharing.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12555645375bac982105b317-14270567%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '93658842c804d29c81527dea82f00b7fbdb68df7' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/socialsharing/views/templates/hook/socialsharing.tpl',
      1 => 1531742988,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12555645375bac982105b317-14270567',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'PS_SC_TWITTER' => 0,
    'PS_SC_FACEBOOK' => 0,
    'PS_SC_GOOGLE' => 0,
    'PS_SC_PINTEREST' => 0,
    'PS_SC_INSTAGRAM' => 0,
    'module_dir' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bac9821122a12_50129160',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bac9821122a12_50129160')) {function content_5bac9821122a12_50129160($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['PS_SC_TWITTER']->value||$_smarty_tpl->tpl_vars['PS_SC_FACEBOOK']->value||$_smarty_tpl->tpl_vars['PS_SC_GOOGLE']->value||$_smarty_tpl->tpl_vars['PS_SC_PINTEREST']->value||$_smarty_tpl->tpl_vars['PS_SC_INSTAGRAM']->value) {?>
	<div class="wrap_share">
		<p class="socialsharing_product list-inline no-print">
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_TWITTER']->value) {?>
				<button data-type="twitter" type="button" class="btn btn-default btn-twitter social-sharing">
					<i class="fa icon-twitter"></i> <span><?php echo smartyTranslate(array('s'=>'Tweet','mod'=>'socialsharing'),$_smarty_tpl);?>
</span>
					<!-- <img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)$_smarty_tpl->tpl_vars['module_dir']->value)."img/twitter.gif");?>
" alt="Tweet" /> -->
				</button>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_FACEBOOK']->value) {?>
				<button data-type="facebook" type="button" class="btn btn-default btn-facebook social-sharing">
					<i class="fa icon-facebook"></i> <span><?php echo smartyTranslate(array('s'=>'Share','mod'=>'socialsharing'),$_smarty_tpl);?>
</span>
					<!-- <img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)$_smarty_tpl->tpl_vars['module_dir']->value)."img/facebook.gif");?>
" alt="Facebook Like" /> -->
				</button>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_GOOGLE']->value) {?>
				<button data-type="google-plus" type="button" class="btn btn-default btn-google-plus social-sharing">
					<i class="fa icon-google-plus"></i> <span><?php echo smartyTranslate(array('s'=>'Google+','mod'=>'socialsharing'),$_smarty_tpl);?>
</span>
					<!-- <img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)$_smarty_tpl->tpl_vars['module_dir']->value)."img/google.gif");?>
" alt="Google Plus" /> -->
				</button>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_PINTEREST']->value) {?>
				<button data-type="pinterest" type="button" class="btn btn-default btn-pinterest social-sharing">
					<i class="fa icon-pinterest"></i> <span><?php echo smartyTranslate(array('s'=>'Pinterest','mod'=>'socialsharing'),$_smarty_tpl);?>
</span>
					<!-- <img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)$_smarty_tpl->tpl_vars['module_dir']->value)."img/pinterest.gif");?>
" alt="Pinterest" /> -->
				</button>
			<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['PS_SC_INSTAGRAM']->value) {?>
                <button data-type="instagram" type="button" class="btn btn-default btn-instagram social-sharing">
                    <i class="fa icon-instagram"></i> <span><?php echo smartyTranslate(array('s'=>'Instagram','mod'=>'socialsharing'),$_smarty_tpl);?>
</span>
                    <!-- <img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)$_smarty_tpl->tpl_vars['module_dir']->value)."img/instagram.gif");?>
" alt="Instagram" /> -->
                </button>
            <?php }?>
		</p>
	</div>
<?php }?><?php }} ?>
