<?php /* Smarty version Smarty-3.1.19, created on 2018-09-26 10:51:35
         compiled from "/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/configuration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14812230425bab024783a417-49913049%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e1f45ba018f9e1a27396bade6cd0ceb2eaa385b' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/prestagiftvouchers/views/templates/admin/configuration.tpl',
      1 => 1536668036,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14812230425bab024783a417-49913049',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_display' => 0,
    'vouchers' => 0,
    'voucher' => 0,
    'currency' => 0,
    'confirm' => 0,
    'is_cumulable' => 0,
    'groupsAvailable' => 0,
    'group' => 0,
    'groupsRestricted' => 0,
    'orderStates' => 0,
    'orderstate' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bab0247d85543_07489843',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bab0247d85543_07489843')) {function content_5bab0247d85543_07489843($_smarty_tpl) {?>
<div class="tab-pane active" id="tab_configuration">
    <div class="panel">
        <h3>
            <i class="icon-cogs"></i> <?php echo smartyTranslate(array('s'=>'Configuration','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <small><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small>
        </h3>
        <?php echo $_smarty_tpl->getSubTemplate ("./modal.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <!-- Create new gift voucher button -->
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <button class="btn btn-default" data-toggle="modal" data-target="#modalEditGiftVoucher">
                        <i class="process-icon-new"></i><?php echo smartyTranslate(array('s'=>'Create a discount voucher','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                    </button>
                </div>
            </div>
        </div>
        <?php if (isset($_smarty_tpl->tpl_vars['vouchers']->value)&&!empty($_smarty_tpl->tpl_vars['vouchers']->value)) {?>
        <hr>
        <div class="alert alert-success fade in hidden" id="successAjax">
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong><?php echo smartyTranslate(array('s'=>'Success!','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 </strong>
            <?php echo smartyTranslate(array('s'=>'Your modifications have been saved.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

        </div>
        <div class="alert alert-danger fade in hidden" id="errorAjax">
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong><?php echo smartyTranslate(array('s'=>'Oops!','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 </strong>
            <?php echo smartyTranslate(array('s'=>'There were a problem when updating your data. Please contact the developer.','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="vouchers_table">
                <thead>
                    <tr>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'ID','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Name','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Status','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Priority','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Cart rule condition','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Only new customers?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Discount voucher amount','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Minimum purchase','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Free shipping?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Unlimited?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Validity period','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                        <td align="center"><?php echo smartyTranslate(array('s'=>'Actions','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</td>
                    </tr>
                </thead>
                <tbody>
                    <?php  $_smarty_tpl->tpl_vars['voucher'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['voucher']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['vouchers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['voucher']->key => $_smarty_tpl->tpl_vars['voucher']->value) {
$_smarty_tpl->tpl_vars['voucher']->_loop = true;
?>
                    <tr class="draggable" data-id-voucher="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['id_giftvoucher'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
                        <td align="center" class="id_giftvoucher"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['id_giftvoucher'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                        <td align="center"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                        <td align="center" data-id-voucher="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['id_giftvoucher'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
                            <!--TODO test isset -->
                            <?php if ($_smarty_tpl->tpl_vars['voucher']->value['active']==0) {?>
                            <i class="icon icon-close" style="color: red"></i>
                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['active']==1) {?>
                            <i class="icon icon-check" style="color: green"></i>
                            <?php } else { ?>
                            <?php echo smartyTranslate(array('s'=>'Undefined','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php }?>
                        </td>
                        <td align="center" class="priority"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['priority'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                        <td align="center" draggable="true">
                            <!--TODO test isset -->
                            <?php if ($_smarty_tpl->tpl_vars['voucher']->value['condition_type']==0) {?>
                            <?php echo mb_convert_encoding(htmlspecialchars(($_smarty_tpl->tpl_vars['voucher']->value['amount_required']).($_smarty_tpl->tpl_vars['currency']->value), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

                            <?php if ($_smarty_tpl->tpl_vars['voucher']->value['shipping_included']==true) {?>
                            &nbsp;<?php echo smartyTranslate(array('s'=>'(shipping included)','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } else { ?>
                            &nbsp;<?php echo smartyTranslate(array('s'=>'(shipping excluded)','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php }?>
                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['condition_type']==1) {?>
                            <?php echo smartyTranslate(array('s'=>'Product','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars(("#").($_smarty_tpl->tpl_vars['voucher']->value['id_product']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 - <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['product_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['condition_type']==2) {?>
                            <?php echo smartyTranslate(array('s'=>'Category','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars(("#").($_smarty_tpl->tpl_vars['voucher']->value['id_category']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 - <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['category_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

                            <?php } else { ?>
                            <?php echo smartyTranslate(array('s'=>'Undefined','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php }?>
                        </td>
                        <td align="center">
                            <!--TODO test isset -->
                            <?php if ($_smarty_tpl->tpl_vars['voucher']->value['new_clients_only']==0) {?>
                            <?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['new_clients_only']==1) {?>
                            <?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } else { ?>
                            <?php echo smartyTranslate(array('s'=>'Undefined','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php }?>
                        </td>
                        <td align="center">
                            <!--TODO test isset -->
                            <?php if ($_smarty_tpl->tpl_vars['voucher']->value['reduction_type']==0) {?>
                            <?php echo mb_convert_encoding(htmlspecialchars(($_smarty_tpl->tpl_vars['voucher']->value['reduction_value']).($_smarty_tpl->tpl_vars['currency']->value), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['reduction_type']==1) {?>
                            <?php echo mb_convert_encoding(htmlspecialchars(($_smarty_tpl->tpl_vars['voucher']->value['reduction_value']).("%"), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

                            <?php } else { ?>
                            <?php echo smartyTranslate(array('s'=>'Undefined','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php }?>
                        </td>
                        <td align="center"><?php echo mb_convert_encoding(htmlspecialchars(($_smarty_tpl->tpl_vars['voucher']->value['reduction_min_use']).($_smarty_tpl->tpl_vars['currency']->value), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                        <td align="center">
                            <!--TODO test isset -->
                            <?php if ($_smarty_tpl->tpl_vars['voucher']->value['free_shipping']==0) {?>
                            <?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['free_shipping']==1) {?>
                            <?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } else { ?>
                            <?php echo smartyTranslate(array('s'=>'Undefined','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php }?>
                        </td>
                        <td align="center">
                            <?php if (!isset($_smarty_tpl->tpl_vars['voucher']->value['unlimited'])) {?>
                            <?php echo smartyTranslate(array('s'=>'Undefined','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['unlimited']==0) {?>
                            <?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } elseif ($_smarty_tpl->tpl_vars['voucher']->value['unlimited']==1) {?>
                            <?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php } else { ?>
                            <?php echo smartyTranslate(array('s'=>'Undefined','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                            <?php }?>
                        </td>
                        <td align="center"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['duration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                        <td align="center">
                            <!-- TODO : DRY this-->
                            <?php if ($_smarty_tpl->tpl_vars['voucher']->value['active']==1) {?>
                            <div class="btn-group-action pull-right group-edit">
                                <div class="btn-group">
                                    <a class="edit btn btn-default editButton" data-id-voucher="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['id_giftvoucher'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
                                        <i class="icon-pencil"></i>&nbsp;<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                                    </a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="toggle_active_voucher">
                                                <i class="icon icon-off toggle_active_voucher"></i> <?php echo smartyTranslate(array('s'=>'Disable','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                                            </a>
                                            <a class="deleteButton" id="deleteButton" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
">
                                                <i class="icon-trash"></i>&nbsp;<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="btn-group-action pull-right group-edit">
                                <div class="btn-group">
                                    <a class="btn btn-default toggle_active_voucher">
                                        <i class="icon icon-off"></i> <?php echo smartyTranslate(array('s'=>'Enable','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                                    </a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="editButton" data-id-voucher="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['voucher']->value['id_giftvoucher'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
                                                <i class="icon-pencil"></i>&nbsp;<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                                            </a>
                                            <a class="deleteButton" id="deleteButton" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
">
                                                <i class="icon-trash"></i>&nbsp;<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php }?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php }?>
        <hr>
    </div>
    <div class="panel">
        <h3>
            <i class="icon-cogs"></i> <?php echo smartyTranslate(array('s'=>'Advanced Parameters','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
 <small><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small>
        </h3>
        <?php if (isset($_smarty_tpl->tpl_vars['confirm']->value)) {?><?php echo $_smarty_tpl->tpl_vars['confirm']->value;?>
<?php }?> 
        <form action="" class="form-horizontal "id="form_cumulable" method="post">
      <!-- CUMULABLE OPTION-->
            <div class="form-group">
                <label class="control-label col-lg-3">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="<?php echo smartyTranslate(array('s'=>'Do you want the cart rules to be usable with other cart rules?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
">
                        <?php echo smartyTranslate(array('s'=>'Cumulable?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>

                    </span>
                </label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="is_cumulable" id="is_cumulable_on" value="1" <?php if ($_smarty_tpl->tpl_vars['is_cumulable']->value==1) {?>checked="checked"<?php }?>>
                        <label for="is_cumulable_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="is_cumulable" id="is_cumulable_off" value="0" <?php if ($_smarty_tpl->tpl_vars['is_cumulable']->value==0) {?>checked="checked"<?php }?>>
                        <label for="is_cumulable_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                </div>
            </div>

      <!-- GROUP RESTRICTION -->
      <div class="form-group">
        <label class="col-lg-3 control-label" for="group_checkboxes">
          <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="<?php echo smartyTranslate(array('s'=>'These groups won\'t generate any cart rule','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
">
            <?php echo smartyTranslate(array('s'=>'Exclude groups?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</label>
          </span>
        <div class="col-lg-9">
          <?php  $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['groupsAvailable']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foreachGroup']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['group']->key => $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foreachGroup']['index']++;
?>
            <div class="checkbox">
              <label for="<?php echo ('group-').($_smarty_tpl->getVariable('smarty')->value['foreach']['foreachGroup']['index']);?>
">
                <input type="checkbox" class="text" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['id_group'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" name="group_checkboxes[]" <?php if (in_array($_smarty_tpl->tpl_vars['group']->value['id_group'],$_smarty_tpl->tpl_vars['groupsRestricted']->value)) {?>checked="checked"<?php }?>>
                <?php echo $_smarty_tpl->tpl_vars['group']->value['name'];?>

              </label>
            </div>
          <?php } ?>
        </div>
      </div>
      <!-- END GROUP RESTRICTION -->

      <!-- ORDER STATE TRIGGER -->
      <div class="form-group">
        <label class="col-lg-3 control-label" for="group_checkboxes">
          <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="<?php echo smartyTranslate(array('s'=>'Which order status should trigger the discount generation?','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
">
            <?php echo smartyTranslate(array('s'=>'Order State Trigger','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</label>
          </span>
        <div class="col-lg-9">
          <select name="order_state_trigger" class="input-small">
            <?php  $_smarty_tpl->tpl_vars['orderstate'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['orderstate']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orderStates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['orderstate']->key => $_smarty_tpl->tpl_vars['orderstate']->value) {
$_smarty_tpl->tpl_vars['orderstate']->_loop = true;
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['orderstate']->value['id_order_state'];?>
" <?php if ($_smarty_tpl->tpl_vars['orderstate']->value['current']==true) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['orderstate']->value['name'];?>
</option>
            <?php } ?>
          </select>
        </div>
      </div>
      <!-- END ORDER STATE TRIGGER -->
            <div class="panel-footer">
                <button name="submitAdvancedParameters" value="1" class="btn btn-default pull-right" type="submit">
                    <i class="process-icon-save"></i>
                    <span><?php echo smartyTranslate(array('s'=>'Save','mod'=>'prestagiftvouchers'),$_smarty_tpl);?>
</span>
                </button>
            </div>
        </form>
    </div>
</div>
<?php }} ?>
