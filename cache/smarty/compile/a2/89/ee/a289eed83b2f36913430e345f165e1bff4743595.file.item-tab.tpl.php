<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 19:01:19
         compiled from "/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/item-tab.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2237888515ba0e90f455cd1-17753142%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a289eed83b2f36913430e345f165e1bff4743595' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/item-tab.tpl',
      1 => 1533124444,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2237888515ba0e90f455cd1-17753142',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'have_li' => 0,
    'tab' => 0,
    'menu' => 0,
    'column' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0e90f53a7b5_24725499',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0e90f53a7b5_24725499')) {function content_5ba0e90f53a7b5_24725499($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['have_li']->value) {?>
    <li data-id-tab="<?php echo intval($_smarty_tpl->tpl_vars['tab']->value['id_tab']);?>
" class="mm_tabs_li item<?php echo intval($_smarty_tpl->tpl_vars['tab']->value['id_tab']);?>
 <?php if (!$_smarty_tpl->tpl_vars['tab']->value['enabled']) {?>mm_disabled<?php }?>" data-obj="tab">
<?php }?>
    <div class="mm_tab_li_content" style="width: <?php if ($_smarty_tpl->tpl_vars['menu']->value['tab_item_width']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['tab_item_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>230px<?php }?>">
        <span class="mm_tab_name mm_tab_toggle">
            <span class="mm_tab_toggle_title">
                <?php if ($_smarty_tpl->tpl_vars['tab']->value['url']) {?>
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
">
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['tab']->value['tab_img_link']) {?>
                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['tab_img_link'], ENT_QUOTES, 'UTF-8', true);?>
" title="" alt="" width="20" />
                <?php } elseif ($_smarty_tpl->tpl_vars['tab']->value['tab_icon']) {?>
                    <i class="fa <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['tab_icon'], ENT_QUOTES, 'UTF-8', true);?>
"></i>
                <?php }?>
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

                <?php if ($_smarty_tpl->tpl_vars['tab']->value['bubble_text']) {?><span class="mm_bubble_text" style="background: <?php if ($_smarty_tpl->tpl_vars['tab']->value['bubble_background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['bubble_background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>#FC4444<?php }?>; color: <?php if (htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['bubble_text_color'], ENT_QUOTES, 'UTF-8', true)) {?><?php echo $_smarty_tpl->tpl_vars['tab']->value['bubble_text_color'];?>
<?php } else { ?>#ffffff<?php }?>;"><?php echo $_smarty_tpl->tpl_vars['tab']->value['bubble_text'];?>
</span><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['tab']->value['url']) {?>
                    </a>
                <?php }?>
            </span>
        </span>
        <div class="mm_buttons" style="left:<?php if ($_smarty_tpl->tpl_vars['menu']->value['tab_item_width']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['tab_item_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>230px<?php }?>;right:<?php if ($_smarty_tpl->tpl_vars['menu']->value['tab_item_width']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['tab_item_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>230px<?php }?>;">
            <span class="mm_tab_delete" title="<?php echo smartyTranslate(array('s'=>'Delete tab','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Delete','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>  
            <span class="mm_duplicate" title="<?php echo smartyTranslate(array('s'=>'Duplicate tab','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Duplicate','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>                      
            <span class="mm_tab_edit" title="<?php echo smartyTranslate(array('s'=>'Edit tab','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Edit','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>                
            <span class="mm_menu_toggle mm_menu_toggle_arrow" title="<?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span> 
            <div class="mm_add_column btn btn-default" title="<?php echo smartyTranslate(array('s'=>'Add column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
" data-id-menu="<?php echo intval($_smarty_tpl->tpl_vars['tab']->value['id_menu']);?>
" data-id-tab="<?php echo intval($_smarty_tpl->tpl_vars['tab']->value['id_tab']);?>
" ><?php echo smartyTranslate(array('s'=>'Add column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div> 
        </div> 
    </div>
    <ul class="mm_columns_ul" style="width:calc(100% - <?php if ($_smarty_tpl->tpl_vars['menu']->value['tab_item_width']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['tab_item_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>230px<?php }?>); left: <?php if ($_smarty_tpl->tpl_vars['menu']->value['tab_item_width']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['tab_item_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>230px<?php }?>;right: <?php if ($_smarty_tpl->tpl_vars['menu']->value['tab_item_width']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['tab_item_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>230px<?php }?>;">
        <?php if ($_smarty_tpl->tpl_vars['tab']->value['columns']) {?>                            
            <?php  $_smarty_tpl->tpl_vars['column'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['column']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value['columns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['column']->key => $_smarty_tpl->tpl_vars['column']->value) {
$_smarty_tpl->tpl_vars['column']->_loop = true;
?>
                <li data-id-column="<?php echo intval($_smarty_tpl->tpl_vars['column']->value['id_column']);?>
" class="mm_columns_li item<?php echo intval($_smarty_tpl->tpl_vars['column']->value['id_column']);?>
 column_size_<?php echo intval($_smarty_tpl->tpl_vars['column']->value['column_size']);?>
 <?php if ($_smarty_tpl->tpl_vars['column']->value['is_breaker']) {?>mm_breaker<?php }?>" data-obj="column">
                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMMItemColumn','column'=>$_smarty_tpl->tpl_vars['column']->value),$_smarty_tpl);?>

                </li>
            <?php } ?>                            
        <?php }?>  
    </ul>
<?php if ($_smarty_tpl->tpl_vars['have_li']->value) {?>
</li>
<?php }?>
<?php }} ?>
