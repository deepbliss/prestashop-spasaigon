<?php /* Smarty version Smarty-3.1.19, created on 2018-09-19 13:35:42
         compiled from "/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/importer-how-to.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5773727625ba1ee3e58b009-86926911%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '010a1ef2704870ee6db3cda1521f87ef5b50caec' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/custombanners/views/templates/admin/importer-how-to.tpl',
      1 => 1528149244,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5773727625ba1ee3e58b009-86926911',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'iso_lang_current' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba1ee3e593a42_32169915',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba1ee3e593a42_32169915')) {function content_5ba1ee3e593a42_32169915($_smarty_tpl) {?>

<div class="modal-body">
	<?php if ($_smarty_tpl->tpl_vars['iso_lang_current']->value=='some_other_lang') {?>
	<?php } else { ?>
		<h4>Basic use:</h4>
		<p>In order to export banners, just click "Export all banners" and save the archive. This archive contains all current banner data including images, custom css/js files, hook positions and page exceptions.</p>
		</p>In order to import, just upload the archive using "Import banners" button. You can use this archive on the same store as a backup, or you can upload it to any other store. When you upload the archive, data is processed in a smart way to synchronize with installed languages/shops.</p>
		<h4>Advanced use:</h4>
		<p>If you want, you can change pre-installed demo content, that is used every time on module reset. Same content is pre-installed when you export module as part of your theme. In order to do that, follow these steps:</p>
		<ol>
			<li>Make a regular export and save the archive</li>
			<li>Rename the archive to "data-custom.zip"</li>
			<li>Move the archive to "/custombanners/defaults/data-custom.zip"</li>
		</ol>
	<?php }?>
</div>
<?php }} ?>
