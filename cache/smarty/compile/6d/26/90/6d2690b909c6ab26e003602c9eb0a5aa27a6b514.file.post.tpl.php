<?php /* Smarty version Smarty-3.1.19, created on 2018-09-25 19:16:36
         compiled from "/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/front/post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1995516515baa27242efae4-70986682%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d2690b909c6ab26e003602c9eb0a5aa27a6b514' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/themes/cosmetico/modules/amazzingblog/views/templates/front/post.tpl',
      1 => 1528149758,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1995516515baa27242efae4-70986682',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ab_post' => 0,
    'ab_post_settings' => 0,
    'post' => 0,
    'blog' => 0,
    'ab_cat_parents' => 0,
    'settings' => 0,
    'author_name' => 0,
    'sn' => 0,
    'ab_comments' => 0,
    'comment' => 0,
    'ab_user_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5baa27244a53c0_13223737',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5baa27244a53c0_13223737')) {function content_5baa27244a53c0_13223737($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/pjmyczpl/public_html/demo/tools/smarty/plugins/modifier.date_format.php';
?>

<?php $_smarty_tpl->tpl_vars['post'] = new Smarty_variable($_smarty_tpl->tpl_vars['ab_post']->value, null, 0);?>
<?php $_smarty_tpl->tpl_vars['settings'] = new Smarty_variable($_smarty_tpl->tpl_vars['ab_post_settings']->value, null, 0);?>
<div class="amazzingblog post-page">
<?php if ($_smarty_tpl->tpl_vars['post']->value&&$_smarty_tpl->tpl_vars['post']->value['active']) {?>
<p class="title-blog"><?php echo smartyTranslate(array('s'=>'Blog','mod'=>'amazzingblog'),$_smarty_tpl);?>
</p>
	<?php if (!$_smarty_tpl->tpl_vars['blog']->value->is_17) {?><?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('breadcrumbs.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('parents'=>$_smarty_tpl->tpl_vars['ab_cat_parents']->value,'current_item'=>$_smarty_tpl->tpl_vars['post']->value['title']), 0);?>
<?php }?>
	<div class="cover">
	<?php if ($_smarty_tpl->tpl_vars['post']->value['main_img']) {?>
		<div class="post-main-image">
			<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['main_img'], ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
">
		</div>
	<?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_date'])) {?>
		<?php if ($_smarty_tpl->tpl_vars['post']->value['publish_from']==$_smarty_tpl->tpl_vars['blog']->value->empty_date) {?><?php $_smarty_tpl->createLocalArrayVariable('post', null, 0);
$_smarty_tpl->tpl_vars['post']->value['publish_from'] = $_smarty_tpl->tpl_vars['post']->value['date_add'];?><?php }?>
		<div class="post-date inline-block"><i class="icon-calendar"></i> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['publish_from']), ENT_QUOTES, 'UTF-8', true);?>
</div>
	<?php }?>
	</div>
	<div class="wrapper-post-content">
		<h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
</h1>
		<div class="post-content"><?php echo $_smarty_tpl->tpl_vars['post']->value['content'];?>
</div>
		<div class="wrapper-info">
			<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_author'])||!empty($_smarty_tpl->tpl_vars['settings']->value['show_views'])) {?>
			<div class="post-info info-block">
				<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_author'])) {?>
					<?php $_smarty_tpl->tpl_vars['author_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['blog']->value->getAuthorNameById($_smarty_tpl->tpl_vars['post']->value['author']), null, 0);?>
					<div class="post-author inline-block">
						
						<span><i class="icon-user"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['author_name']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
					</div>
				<?php }?>
				<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_views'])) {?>
					<div class="post-views inline-block"><i class="icon-eye"></i> <?php echo intval($_smarty_tpl->tpl_vars['post']->value['views']);?>
</div>
				<?php }?>
				<?php if (!empty($_smarty_tpl->tpl_vars['post']->value['tags'])) {?>
					<div class="post-tags inline-block">
						<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('post-tags.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tags'=>$_smarty_tpl->tpl_vars['post']->value['tags'],'no_commas'=>true), 0);?>

					</div>
				<?php }?>
			</div>
		<?php }?>
		<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['social_sharing'])) {?>
			<div class="post-sharing">
				<?php echo smartyTranslate(array('s'=>'Share','mod'=>'amazzingblog'),$_smarty_tpl);?>

				<div class="sharing-icons inline-block">
					<?php  $_smarty_tpl->tpl_vars['sn'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sn']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['settings']->value['social_sharing']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sn']->key => $_smarty_tpl->tpl_vars['sn']->value) {
$_smarty_tpl->tpl_vars['sn']->_loop = true;
?>
						<a href="#" class="social-share" data-network="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sn']->value, ENT_QUOTES, 'UTF-8', true);?>
">
							<i class="icon-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sn']->value, ENT_QUOTES, 'UTF-8', true);?>
"></i>
						</a>
					<?php } ?>
				</div>
			</div>
		<?php }?>
		</div>
	</div>
	<?php if (!empty($_smarty_tpl->tpl_vars['blog']->value->general_settings['user_comments'])) {?>
		<div id="post-comments" class="post-comments">
			<h4><span class="comments-num"><?php echo intval(count($_smarty_tpl->tpl_vars['ab_comments']->value));?>
</span> <?php echo smartyTranslate(array('s'=>'comments','mod'=>'amazzingblog'),$_smarty_tpl);?>
</h4>
			<div class="comments-list">
				<?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ab_comments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
?>
					<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('comment.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('comment'=>$_smarty_tpl->tpl_vars['comment']->value), 0);?>

				<?php } ?>
			</div>
			<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('comment-form.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id_post'=>$_smarty_tpl->tpl_vars['post']->value['id_post'],'user_data'=>$_smarty_tpl->tpl_vars['ab_user_data']->value), 0);?>

		</div>
	<?php }?>
	<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['show_footer_hook'])) {?>
		<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayPostFooter'),$_smarty_tpl);?>

	<?php }?>
<?php } else { ?>
	<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['blog']->value->getTemplatePath('breadcrumbs.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('current_item'=>''), 0);?>

	<div class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'This post is not available','mod'=>'amazzingblog'),$_smarty_tpl);?>
</div>
<?php }?>
</div>

<?php }} ?>
