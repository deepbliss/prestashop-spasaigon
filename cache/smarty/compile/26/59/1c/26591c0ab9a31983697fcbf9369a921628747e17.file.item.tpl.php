<?php /* Smarty version Smarty-3.1.19, created on 2018-09-28 09:57:42
         compiled from "/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/hook/item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13348984355bad98a6e060d9-89326387%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26591c0ab9a31983697fcbf9369a921628747e17' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/hook/item.tpl',
      1 => 1528149274,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13348984355bad98a6e060d9-89326387',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'settings' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bad98a6e5c5c1_88473359',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bad98a6e5c5c1_88473359')) {function content_5bad98a6e5c5c1_88473359($_smarty_tpl) {?>


<div class="item-container">
	
	<?php if ($_smarty_tpl->tpl_vars['item']->value['img_src']) {?>
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
">
			<img class="item-image" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['img_src'], ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
">
		</a>
	<?php }?>
	

	
	<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['title'])) {?>
        <div class="item-title<?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['title_one_line'])) {?> nowrap<?php }?>">
        	<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value['name'],$_smarty_tpl->tpl_vars['settings']->value['title'],'...');?>
</a>
        </div>
    <?php }?>
	

	
    <?php if (!empty($_smarty_tpl->tpl_vars['settings']->value['description'])) {?>
        <div class="item-desc">
            <?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(strip_tags($_smarty_tpl->tpl_vars['item']->value['description']),$_smarty_tpl->tpl_vars['settings']->value['description'],'...'), ENT_QUOTES, 'UTF-8', true);?>

        </div>
    <?php }?>
	

</div>



<?php }} ?>
