<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 20:12:22
         compiled from "/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/warnings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6755301845ba0f9b67df202-84547649%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3a9a5ddb0558627a93bd0504f73f65a25749d0e' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/easycarousels/views/templates/admin/warnings.tpl',
      1 => 1528149272,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6755301845ba0f9b67df202-84547649',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'file_warnings' => 0,
    'file' => 0,
    'identifier' => 0,
    'info_links' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0f9b67fe388_84820764',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0f9b67fe388_84820764')) {function content_5ba0f9b67fe388_84820764($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['file_warnings']->value) {?>
<div class="easycarousels">
	<div class="alert-warning">
		<?php echo smartyTranslate(array('s'=>'Some of files, that you customized, have been updated in the new version','mod'=>'easycarousels'),$_smarty_tpl);?>

		<ul>
		<?php  $_smarty_tpl->tpl_vars['identifier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['identifier']->_loop = false;
 $_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['file_warnings']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['identifier']->key => $_smarty_tpl->tpl_vars['identifier']->value) {
$_smarty_tpl->tpl_vars['identifier']->_loop = true;
 $_smarty_tpl->tpl_vars['file']->value = $_smarty_tpl->tpl_vars['identifier']->key;
?>
			<li>
				<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value, ENT_QUOTES, 'UTF-8', true);?>

				<span class="warning-advice">
					<?php echo smartyTranslate(array('s'=>'Make sure you update this file in your theme folder, and insert the following code to the last line','mod'=>'easycarousels'),$_smarty_tpl);?>
:
					<span class="code"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['identifier']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
					<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['info_links']->value['documentation'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'More info','mod'=>'easycarousels'),$_smarty_tpl);?>
" target="_blank" class="icon-question-circle"></a>
				</span>
			</li>
		<?php } ?>
		</ul>
	</div>
</div>
<?php }?>

<?php }} ?>
