<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 19:01:19
         compiled from "/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/item-menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14663870425ba0e90f33c175-66094354%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '48f21c3fcf4dc4661b7c9c412a89833a191c7164' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/item-menu.tpl',
      1 => 1533124444,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14663870425ba0e90f33c175-66094354',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'have_li' => 0,
    'menu' => 0,
    'tab' => 0,
    'column' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0e90f44cd56_78938688',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0e90f44cd56_78938688')) {function content_5ba0e90f44cd56_78938688($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['have_li']->value) {?>
    <li class="mm_menus_li item<?php echo intval($_smarty_tpl->tpl_vars['menu']->value['id_menu']);?>
 <?php if (!$_smarty_tpl->tpl_vars['menu']->value['enabled']) {?>mm_disabled<?php }?>" data-id-menu="<?php echo intval($_smarty_tpl->tpl_vars['menu']->value['id_menu']);?>
" data-obj="menu">
<?php }?>                        
    <?php if ($_smarty_tpl->tpl_vars['menu']->value['enabled_vertical']) {?>
        <div class="mm_menus_li_content" style="width: <?php if ($_smarty_tpl->tpl_vars['menu']->value['menu_item_width']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['menu_item_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>230px<?php }?>">
            <span class="mm_menu_name mm_menu_toggle">
                <span class="mm_menu_content_title">
                    <?php if ($_smarty_tpl->tpl_vars['menu']->value['menu_img_link']) {?>
                        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['menu_img_link'], ENT_QUOTES, 'UTF-8', true);?>
" title="" alt="" width="20" />
                    <?php } elseif ($_smarty_tpl->tpl_vars['menu']->value['menu_icon']) {?>
                        <i class="fa <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['menu_icon'], ENT_QUOTES, 'UTF-8', true);?>
"></i>
                    <?php }?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

                    <?php if ($_smarty_tpl->tpl_vars['menu']->value['bubble_text']) {?><span class="mm_bubble_text" style="background: <?php if ($_smarty_tpl->tpl_vars['menu']->value['bubble_background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['bubble_background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>#FC4444<?php }?>; color: <?php if (htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['bubble_text_color'], ENT_QUOTES, 'UTF-8', true)) {?><?php echo $_smarty_tpl->tpl_vars['menu']->value['bubble_text_color'];?>
<?php } else { ?>#ffffff<?php }?>;"><?php echo $_smarty_tpl->tpl_vars['menu']->value['bubble_text'];?>
</span><?php }?>
                </span>
            </span>
            <div class="mm_buttons">
                <span class="mm_menu_delete" title="<?php echo smartyTranslate(array('s'=>'Delete menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Delete','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>  
                <span class="mm_duplicate" title="<?php echo smartyTranslate(array('s'=>'Duplicate menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Duplicate','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>                      
                <span class="mm_menu_edit" title="<?php echo smartyTranslate(array('s'=>'Edit menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Edit menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>                
                <span class="mm_menu_toggle mm_menu_toggle_arrow"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span> 
                <div class="mm_add_tab btn btn-default" data-id-menu="<?php echo intval($_smarty_tpl->tpl_vars['menu']->value['id_menu']);?>
" title="<?php echo smartyTranslate(array('s'=>'Add tab','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Add tab','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div> 
            </div> 
        </div>
        
        <div class="mm_tabs_ul">
            <ul class="mm_tabs_ul_content">
                <?php if ($_smarty_tpl->tpl_vars['menu']->value['tabs']) {?>                            
                    <?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu']->value['tabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
                        <li data-id-tab="<?php echo intval($_smarty_tpl->tpl_vars['tab']->value['id_tab']);?>
" class="mm_tabs_li item<?php echo intval($_smarty_tpl->tpl_vars['tab']->value['id_tab']);?>
 <?php if (!$_smarty_tpl->tpl_vars['tab']->value['enabled']) {?>mm_disabled<?php }?>" data-obj="tab">
                            <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMMItemTab','tab'=>$_smarty_tpl->tpl_vars['tab']->value),$_smarty_tpl);?>

                        </li>
                    <?php } ?>                            
                <?php }?>
            </ul>
        </div>
    <?php } else { ?>
        <div class="mm_menus_li_content">
            <span class="mm_menu_name mm_menu_toggle">
                <span class="mm_menu_content_title">
                    <?php if ($_smarty_tpl->tpl_vars['menu']->value['menu_img_link']) {?>
                        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['menu_img_link'], ENT_QUOTES, 'UTF-8', true);?>
" title="" alt="" width="20" />
                    <?php } elseif ($_smarty_tpl->tpl_vars['menu']->value['menu_icon']) {?>
                        <i class="fa <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['menu_icon'], ENT_QUOTES, 'UTF-8', true);?>
"></i>
                    <?php }?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

                    <?php if ($_smarty_tpl->tpl_vars['menu']->value['bubble_text']) {?><span class="mm_bubble_text" style="background: <?php if ($_smarty_tpl->tpl_vars['menu']->value['bubble_background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['bubble_background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>#FC4444<?php }?>; color: <?php if (htmlspecialchars($_smarty_tpl->tpl_vars['menu']->value['bubble_text_color'], ENT_QUOTES, 'UTF-8', true)) {?><?php echo $_smarty_tpl->tpl_vars['menu']->value['bubble_text_color'];?>
<?php } else { ?>#ffffff<?php }?>;"><?php echo $_smarty_tpl->tpl_vars['menu']->value['bubble_text'];?>
</span><?php }?>
                </span>
            </span>
            <div class="mm_buttons">
                <span class="mm_menu_delete" title="<?php echo smartyTranslate(array('s'=>'Delete menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Delete','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>  
                <span class="mm_duplicate" title="<?php echo smartyTranslate(array('s'=>'Duplicate menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Duplicate','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>                      
                <span class="mm_menu_edit" title="<?php echo smartyTranslate(array('s'=>'Edit menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Edit','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span>                
                <span class="mm_menu_toggle mm_menu_toggle_arrow"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</span> 
                <div class="mm_add_column btn btn-default" data-id-menu="<?php echo intval($_smarty_tpl->tpl_vars['menu']->value['id_menu']);?>
" title="<?php echo smartyTranslate(array('s'=>'Add column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Add column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div> 
            </div> 
        </div>
        <ul class="mm_columns_ul">
            <?php if ($_smarty_tpl->tpl_vars['menu']->value['columns']) {?>                            
                <?php  $_smarty_tpl->tpl_vars['column'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['column']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu']->value['columns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['column']->key => $_smarty_tpl->tpl_vars['column']->value) {
$_smarty_tpl->tpl_vars['column']->_loop = true;
?>
                    <li data-id-column="<?php echo intval($_smarty_tpl->tpl_vars['column']->value['id_column']);?>
" class="mm_columns_li item<?php echo intval($_smarty_tpl->tpl_vars['column']->value['id_column']);?>
 column_size_<?php echo intval($_smarty_tpl->tpl_vars['column']->value['column_size']);?>
 <?php if ($_smarty_tpl->tpl_vars['column']->value['is_breaker']) {?>mm_breaker<?php }?>" data-obj="column">
                        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMMItemColumn','column'=>$_smarty_tpl->tpl_vars['column']->value),$_smarty_tpl);?>

                    </li>
                <?php } ?>                            
            <?php }?>  
        </ul> 
    <?php }?> 
<?php if ($_smarty_tpl->tpl_vars['have_li']->value) {?>
</li>
<?php }?>
<script type="text/javascript">
$(document).ready(function(){
    if($('.mm_menus_li.open .mm_tabs_ul .mm_tabs_li.open .mm_columns_ul').length)
    {
       $('.mm_menus_li.open .mm_tabs_ul').css('height',($('.mm_menus_li.open .mm_tabs_ul .mm_tabs_li.open .mm_columns_ul').height()+300)+'px') 
    }
});
</script><?php }} ?>
