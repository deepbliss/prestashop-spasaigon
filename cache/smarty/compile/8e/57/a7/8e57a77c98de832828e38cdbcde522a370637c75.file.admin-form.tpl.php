<?php /* Smarty version Smarty-3.1.19, created on 2018-09-18 19:01:19
         compiled from "/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/admin-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15418910975ba0e90f270ad8-09834764%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8e57a77c98de832828e38cdbcde522a370637c75' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/ets_megamenu/views/templates/hook/admin-form.tpl',
      1 => 1533124444,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15418910975ba0e90f270ad8-09834764',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mm_img_dir' => 0,
    'mmBaseAdminUrl' => 0,
    'mm_backend_layout' => 0,
    'multiLayout' => 0,
    'menus' => 0,
    'menu' => 0,
    'menuForm' => 0,
    'tabForm' => 0,
    'columnForm' => 0,
    'blockForm' => 0,
    'configForm' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ba0e90f331e00_35321069',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ba0e90f331e00_35321069')) {function content_5ba0e90f331e00_35321069($_smarty_tpl) {?>
<script type="text/javascript">
    var mm_img_dir ='<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['mm_img_dir']->value);?>
';
    var mmBaseAdminUrl = '<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['mmBaseAdminUrl']->value);?>
';
    var mmCloseTxt = '<?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmOpenTxt = '<?php echo smartyTranslate(array('s'=>'Open','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDeleteTxt = '<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmEditTxt = '<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDeleteTitleTxt = '<?php echo smartyTranslate(array('s'=>'Delete this item','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmAddMenuTxt = '<?php echo smartyTranslate(array('s'=>'Add new menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmEditMenuTxt = '<?php echo smartyTranslate(array('s'=>'Edit menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmAddColumnTxt = '<?php echo smartyTranslate(array('s'=>'Add new column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmEditColumnTxt = '<?php echo smartyTranslate(array('s'=>'Edit column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDeleteColumnTxt = '<?php echo smartyTranslate(array('s'=>'Delete this column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDeleteBlockTxt = '<?php echo smartyTranslate(array('s'=>'Delete this block','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmEditBlockTxt = '<?php echo smartyTranslate(array('s'=>'Edit this block','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmAddBlockTxt = '<?php echo smartyTranslate(array('s'=>'Add new block','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDuplicateTxt = '<?php echo smartyTranslate(array('s'=>'Duplicate','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDuplicateMenuTxt = '<?php echo smartyTranslate(array('s'=>'Duplicate this menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDuplicateColumnTxt = '<?php echo smartyTranslate(array('s'=>'Duplicate this column','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var mmDuplicateBlockTxt = '<?php echo smartyTranslate(array('s'=>'Duplicate this block','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
    var ets_mm_invalid_file = '<?php echo smartyTranslate(array('s'=>'Image is invalid','mod'=>'ets_megamenu'),$_smarty_tpl);?>
';
</script>
<div class="ets_megamenu mm_view_mode_tab <?php if ($_smarty_tpl->tpl_vars['mm_backend_layout']->value=='rtl') {?>ets-dir-rtl backend-layout-rtl<?php } else { ?>ets-dir-ltr backend-layout-ltr<?php }?> <?php if ($_smarty_tpl->tpl_vars['multiLayout']->value) {?>mm_multi_layout<?php } else { ?>mm_single_layout<?php }?>">
    <div class="mm_menus">
        <?php if ($_smarty_tpl->tpl_vars['menus']->value) {?>
            <ul class="mm_menus_ul">
                <?php  $_smarty_tpl->tpl_vars['menu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['menu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menus']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['menu']->key => $_smarty_tpl->tpl_vars['menu']->value) {
$_smarty_tpl->tpl_vars['menu']->_loop = true;
?>
                    <li class="mm_menus_li item<?php echo intval($_smarty_tpl->tpl_vars['menu']->value['id_menu']);?>
 <?php if (!$_smarty_tpl->tpl_vars['menu']->value['enabled']) {?>mm_disabled<?php }?>" data-id-menu="<?php echo intval($_smarty_tpl->tpl_vars['menu']->value['id_menu']);?>
" data-obj="menu">
                        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMMItemMenu','menu'=>$_smarty_tpl->tpl_vars['menu']->value),$_smarty_tpl);?>

                    </li>
                <?php } ?>
            </ul>
        <?php }?>        
        <div class="mm_useful_buttons">
            <div class="mm_add_menu btn btn-default"><?php echo smartyTranslate(array('s'=>'Add menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
            <div class="mm_view_mm_view_modes">
                <div class="mm_view_mode mm_view_mode_tab_select active" title="<?php echo smartyTranslate(array('s'=>'Preview in tab mode','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Tab','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
                <div class="mm_view_mode mm_view_mode_list_select" title="<?php echo smartyTranslate(array('s'=>'Preview in list mode','mod'=>'ets_megamenu'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'List','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['multiLayout']->value) {?>
                <div class="mm_layout_mode">                
                    <div data-title="&#xE236;" class="mm_layout_ltr mm_change_mode <?php if ($_smarty_tpl->tpl_vars['mm_backend_layout']->value!='rtl') {?>active<?php }?>"><?php echo smartyTranslate(array('s'=>'LTR','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
                    <div data-title="&#xE237;" class="mm_layout_rlt mm_change_mode <?php if ($_smarty_tpl->tpl_vars['mm_backend_layout']->value=='rtl') {?>active<?php }?>"><?php echo smartyTranslate(array('s'=>'RTL','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
                </div>
            <?php }?>
            <div class="mm_import_button btn btn-default"><i class="fa fa-exchange" data-title="&#xE8D4;"></i><?php echo smartyTranslate(array('s'=>'Import/Export','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
            <div class="mm_config_button btn btn-default" data-title="&#xE8B8;"><?php echo smartyTranslate(array('s'=>'Settings','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
        </div>
    </div>
    <div class="mm_loading_icon"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['mm_img_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
ajax-loader.gif" /></div>
    <!-- popup forms -->
    <div class="mm_forms hidden mm_popup_overlay">
        <div class="mm_menu_form hidden mm_pop_up">
            <div class="mm_close"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
            <div class="mm_form"></div>
        </div>
        <div class="mm_menu_form_new hidden"><?php echo $_smarty_tpl->tpl_vars['menuForm']->value;?>
</div>
        <div class="mm_tab_form_new hidden"><?php echo $_smarty_tpl->tpl_vars['tabForm']->value;?>
</div>
        <div class="mm_column_form_new hidden"><?php echo $_smarty_tpl->tpl_vars['columnForm']->value;?>
</div>
        <div class="mm_block_form_new hidden"><?php echo $_smarty_tpl->tpl_vars['blockForm']->value;?>
</div>
    </div>
    <div class="mm_popup_overlay hidden">
        <div class="mm_config_form mm_pop_up">
            <div class="mm_close" ><?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
            <div class="mm_config_form_content"><div class="mm_close"></div><?php echo $_smarty_tpl->tpl_vars['configForm']->value;?>
</div>
        </div>
    </div>
    <div class="mm_export_form hidden mm_popup_overlay">
        <div class="mm_close"></div>
        <div class="mm_export mm_pop_up hidden">
            <div data-title="&#xE14C;" class="mm_close"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
            <div class="mm_export_form_content">            
                <div class="mm_export_option">
                    <div class="export_title"><?php echo smartyTranslate(array('s'=>'Export menu content','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
                    <a class="btn btn-default mm_export_menu" href="<?php echo $_smarty_tpl->tpl_vars['mmBaseAdminUrl']->value;?>
&exportMenu=1" target="_blank">
                        <i class="fa fa-download" data-title="&#xE864;"></i><?php echo smartyTranslate(array('s'=>'Export menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>

                    </a>
                    <p class="mm_export_option_note"><?php echo smartyTranslate(array('s'=>'Export all menu data including images, text and configuration','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</p>
                </div>                       
                <div class="mm_import_option">
                    <div class="export_title"><?php echo smartyTranslate(array('s'=>'Import menu data','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
                    <form action="<?php echo $_smarty_tpl->tpl_vars['mmBaseAdminUrl']->value;?>
" method="post" enctype="multipart/form-data" class="mm_import_option_form">
                        <div class="mm_import_option_updata">
                            <label for="sliderdata"><?php echo smartyTranslate(array('s'=>'Menu data package','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</label>
                            <input id="image" type="file" name="sliderdata" id="sliderdata" />
                        </div>
                        <div class="mm_import_option_clean">
                            <input type="checkbox" value="1" id="importoverride" checked="checked" name="importoverride" />
                            <label for="importoverride"><?php echo smartyTranslate(array('s'=>'Clear all menus before importing','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</label>
                        </div>
                        <div class="mm_import_option_button">
                            <input type="hidden" name="importMenu" value="1" />
                            <div class="mm_import_menu_loading"><img src="<?php echo $_smarty_tpl->tpl_vars['mm_img_dir']->value;?>
loader.gif" /><?php echo smartyTranslate(array('s'=>'Importing data','mod'=>'ets_megamenu'),$_smarty_tpl);?>
</div>
                            <div class="mm_import_menu_submit">
                                <i class="fa fa-compress" data-title="&#xE0C3;"></i>
                                <input type="submit" value="<?php echo smartyTranslate(array('s'=>'Import menu','mod'=>'ets_megamenu'),$_smarty_tpl);?>
" class="btn btn-default mm_import_menu"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div><?php }} ?>
