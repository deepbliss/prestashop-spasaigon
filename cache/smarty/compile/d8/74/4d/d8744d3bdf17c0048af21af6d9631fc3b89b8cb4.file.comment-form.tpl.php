<?php /* Smarty version Smarty-3.1.19, created on 2018-09-25 19:16:36
         compiled from "/home/pjmyczpl/public_html/demo/modules/amazzingblog/views/templates/front/comment-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13827714545baa272456e9e9-09978188%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd8744d3bdf17c0048af21af6d9631fc3b89b8cb4' => 
    array (
      0 => '/home/pjmyczpl/public_html/demo/modules/amazzingblog/views/templates/front/comment-form.tpl',
      1 => 1528149062,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13827714545baa272456e9e9-09978188',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id_post' => 0,
    'user_data' => 0,
    'avatars_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5baa27245d1ed0_16503701',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5baa27245d1ed0_16503701')) {function content_5baa27245d1ed0_16503701($_smarty_tpl) {?>

<form class="new-comment" enctype="multipart/form-data">
	<input type="hidden" name="id_post" value="<?php echo intval($_smarty_tpl->tpl_vars['id_post']->value);?>
">
	<input type="hidden" name="action" value="SubmitComment">
	<div class="comment-item" data-name="content">
		<div class="user-avatar">
			<div class="avatar-img<?php if (empty($_smarty_tpl->tpl_vars['user_data']->value['avatar'])) {?> empty<?php }?>"<?php if (!empty($_smarty_tpl->tpl_vars['user_data']->value['avatar'])) {?> style="background-image:url(<?php echo htmlspecialchars(($_smarty_tpl->tpl_vars['avatars_dir']->value).($_smarty_tpl->tpl_vars['user_data']->value['avatar']), ENT_QUOTES, 'UTF-8', true);?>
)"<?php }?>></div>
			<div class="hidden"><input class="avatar-file" type="file" name="avatar_file"></div>
			<a href="#" class="edit-avatar text-center" title="<?php echo smartyTranslate(array('s'=>'Upload new','mod'=>'amazzingblog'),$_smarty_tpl);?>
"><i class="icon-file-upload"></i></a>
		</div>
		<script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<div class="mce-placeholder"><?php echo smartyTranslate(array('s'=>'Your comment...','mod'=>'amazzingblog'),$_smarty_tpl);?>
</div>
		<div id="new-comment-content" class="mce-input"></div>
	</div>
	<div class="new-comment-footer">
		<span class="input-label"><?php echo smartyTranslate(array('s'=>'Comment as','mod'=>'amazzingblog'),$_smarty_tpl);?>
</span>
		<div class="inline-block" data-name="user_name">
			<input type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['user_name'], ENT_QUOTES, 'UTF-8', true);?>
" name="user_name" placeholder="<?php echo smartyTranslate(array('s'=>'Your Name','mod'=>'amazzingblog'),$_smarty_tpl);?>
">
			<div class="note-below-text">* <?php echo smartyTranslate(array('s'=>'This will be applied to all of your comments','mod'=>'amazzingblog'),$_smarty_tpl);?>
</div>
		</div>
		<button class="btn btn-primary submit-comment"><?php echo smartyTranslate(array('s'=>'Submit','mod'=>'amazzingblog'),$_smarty_tpl);?>
</button>
	</div>
</form>

<?php }} ?>
