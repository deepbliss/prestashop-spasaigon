/**
*  @author    Amazzing
*  @copyright Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)*
*/

var cbResizeTimer,
	cbCarousels = {};

$(document).ready(function(){

	$('.cb-wrapper').find('.carousel').each(function(){
		renderCarousel($(this), false);
	});

	$(window).resize(function(){
		clearTimeout(cbResizeTimer);
		cbResizeTimer = setTimeout(function() {
			for (var idWrapper in cbCarousels){
				renderCarousel(cbCarousels[idWrapper], true);
			}
		}, 200);
	});

	function renderCarousel($container, reload){
		var settings = $container.data('settings');
		var w = $(window).width();
		var itemsNum = 1;
		if (w > 1199) {
			itemsNum = settings.i;
		} else if (w > 991) {
			itemsNum = settings.i_1200;
		} else if (w > 767) {
			itemsNum = settings.i_992;
		} else if (w > 479) {
			itemsNum = settings.i_768;
		} else if (w < 480) {
			itemsNum = settings.i_480;
		}
		var slideWidth = Math.round($container.parent().innerWidth() / itemsNum);
		var params = {
			pager : settings.p == 1 ? true : false,
			controls: parseInt(settings.n) > 0 ? true : false,
			auto: settings.a == 1 ? true : false,
			moveSlides: settings.m,
			speed: settings.s,
			maxSlides: itemsNum,
			minSlides: itemsNum,
			slideWidth: slideWidth,
			responsive: false,
			swipeThreshold: 1,
			useCSS: true,
			oneToOneTouch: false,
			onSlideAfter: function ($slideElement) {
				 $slideElement.addClass('current').siblings('.current').removeClass('current');
			},
		}
		if (reload) {
			$container.reloadSlider(params);
		} else {
			var idWrapper = $container.closest('.cb-wrapper').data('wrapper');
			cbCarousels[idWrapper] = $container.bxSlider(params);
			if (settings.n == 2 && !isMobile) {
				$container.closest('.cb-wrapper').addClass('n-hover');
			}
		}
		$container.closest('.bx-wrapper').css('max-width', '100%');
	}
});
