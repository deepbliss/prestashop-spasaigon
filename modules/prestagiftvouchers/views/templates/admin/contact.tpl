{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<div class="tab-pane" id="tab_contact">
    <div class="clearfix"></div>
    <div class="tab-pane panel" id="contacts">
        <h3><i class="icon-envelope"></i> {l s='Prestashop Addons' mod='prestagiftvouchers'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small></h3>
        <div class="form-group">
            <b>{l s='Thank you for choosing a module developed by the Addons Team of PrestaShop.' mod='prestagiftvouchers'}</b><br /><br />

            {l s='If you encounter a problem using the module, our team is at your service via the ' mod='prestagiftvouchers'} <a target="_blank" href="http://addons.prestashop.com/contact-form.php{$tracking_url|escape:'htmlall':'UTF-8'}">{l s='contact form' mod='prestagiftvouchers'}.</a><br /><br />

            {l s='To save you time, before you contact us:' mod='prestagiftvouchers'}<br />
            - {l s='make sure you have read the documentation well. ' mod='prestagiftvouchers'}<br />
            - {l s='in the event you would be contacting us via the form, do not hesitate to give us your first message, maximum of details on the problem and its origins (screenshots, reproduce actions to find the bug, etc. ..) ' mod='prestagiftvouchers'}<br /><br />
            {l s='This module has been developped by PrestaShop and can only be sold through' mod='prestagiftvouchers'} <a target="_blank" href="http://addons.prestashop.com{$tracking_url|escape:'htmlall':'UTF-8'}">addons.prestashop.com</a>.<br /><br />

            The PrestaShop Addons Team<br />
        </div>
    </div>
</div>
