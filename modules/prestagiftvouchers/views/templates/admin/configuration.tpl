{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<div class="tab-pane active" id="tab_configuration">
    <div class="panel">
        <h3>
            <i class="icon-cogs"></i> {l s='Configuration' mod='prestagiftvouchers'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small>
        </h3>
        {include file="./modal.tpl"}
        <!-- Create new gift voucher button -->
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <button class="btn btn-default" data-toggle="modal" data-target="#modalEditGiftVoucher">
                        <i class="process-icon-new"></i>{l s='Create a discount voucher' mod='prestagiftvouchers'}
                    </button>
                </div>
            </div>
        </div>
        {if isset($vouchers) && !empty($vouchers)}
        <hr>
        <div class="alert alert-success fade in hidden" id="successAjax">
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{l s='Success!' mod='prestagiftvouchers'} </strong>
            {l s='Your modifications have been saved.' mod='prestagiftvouchers'}
        </div>
        <div class="alert alert-danger fade in hidden" id="errorAjax">
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{l s='Oops!' mod='prestagiftvouchers'} </strong>
            {l s='There were a problem when updating your data. Please contact the developer.' mod='prestagiftvouchers'}
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="vouchers_table">
                <thead>
                    <tr>
                        <td align="center">{l s='ID' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Name' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Status' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Priority' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Cart rule condition' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Only new customers?' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Discount voucher amount' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Minimum purchase' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Free shipping?' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Unlimited?' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Validity period' mod='prestagiftvouchers'}</td>
                        <td align="center">{l s='Actions' mod='prestagiftvouchers'}</td>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$vouchers item=voucher}
                    <tr class="draggable" data-id-voucher="{$voucher['id_giftvoucher']|escape:'htmlall':'UTF-8'}">
                        <td align="center" class="id_giftvoucher">{$voucher['id_giftvoucher']|escape:'htmlall':'UTF-8'}</td>
                        <td align="center">{$voucher['name']|escape:'htmlall':'UTF-8'}</td>
                        <td align="center" data-id-voucher="{$voucher['id_giftvoucher']|escape:'htmlall':'UTF-8'}">
                            <!--TODO test isset -->
                            {if $voucher['active'] == 0}
                            <i class="icon icon-close" style="color: red"></i>
                            {elseif $voucher['active'] == 1}
                            <i class="icon icon-check" style="color: green"></i>
                            {else}
                            {l s='Undefined' mod='prestagiftvouchers'}
                            {/if}
                        </td>
                        <td align="center" class="priority">{$voucher['priority']|escape:'htmlall':'UTF-8'}</td>
                        <td align="center" draggable="true">
                            <!--TODO test isset -->
                            {if $voucher['condition_type'] == 0}
                            {$voucher['amount_required']|cat: $currency|escape:'htmlall':'UTF-8'}
                            {if $voucher['shipping_included'] == true}
                            &nbsp;{l s='(shipping included)' mod='prestagiftvouchers'}
                            {else}
                            &nbsp;{l s='(shipping excluded)' mod='prestagiftvouchers'}
                            {/if}
                            {elseif $voucher['condition_type'] == 1}
                            {l s='Product' mod='prestagiftvouchers'} {"#"|cat: $voucher['id_product']|escape:'htmlall':'UTF-8'} - {$voucher['product_name']|escape:'htmlall':'UTF-8'}
                            {elseif $voucher['condition_type'] == 2}
                            {l s='Category' mod='prestagiftvouchers'} {"#"|cat: $voucher['id_category']|escape:'htmlall':'UTF-8'} - {$voucher['category_name']|escape:'htmlall':'UTF-8'}
                            {else}
                            {l s='Undefined' mod='prestagiftvouchers'}
                            {/if}
                        </td>
                        <td align="center">
                            <!--TODO test isset -->
                            {if $voucher['new_clients_only'] == 0}
                            {l s='No' mod='prestagiftvouchers'}
                            {elseif $voucher['new_clients_only'] == 1}
                            {l s='Yes' mod='prestagiftvouchers'}
                            {else}
                            {l s='Undefined' mod='prestagiftvouchers'}
                            {/if}
                        </td>
                        <td align="center">
                            <!--TODO test isset -->
                            {if $voucher['reduction_type'] == 0}
                            {$voucher['reduction_value']|cat: $currency|escape:'htmlall':'UTF-8'}
                            {elseif $voucher['reduction_type'] == 1}
                            {$voucher['reduction_value']|cat: "%"|escape:'htmlall':'UTF-8'}
                            {else}
                            {l s='Undefined' mod='prestagiftvouchers'}
                            {/if}
                        </td>
                        <td align="center">{$voucher['reduction_min_use']|cat: $currency|escape:'htmlall':'UTF-8'}</td>
                        <td align="center">
                            <!--TODO test isset -->
                            {if $voucher['free_shipping'] == 0}
                            {l s='No' mod='prestagiftvouchers'}
                            {elseif $voucher['free_shipping'] == 1}
                            {l s='Yes' mod='prestagiftvouchers'}
                            {else}
                            {l s='Undefined' mod='prestagiftvouchers'}
                            {/if}
                        </td>
                        <td align="center">
                            {if !isset($voucher['unlimited'])}
                            {l s='Undefined' mod='prestagiftvouchers'}
                            {elseif $voucher['unlimited'] == 0}
                            {l s='No' mod='prestagiftvouchers'}
                            {elseif $voucher['unlimited'] == 1}
                            {l s='Yes' mod='prestagiftvouchers'}
                            {else}
                            {l s='Undefined' mod='prestagiftvouchers'}
                            {/if}
                        </td>
                        <td align="center">{$voucher['duration']|escape:'htmlall':'UTF-8'}</td>
                        <td align="center">
                            <!-- TODO : DRY this-->
                            {if $voucher['active'] == 1}
                            <div class="btn-group-action pull-right group-edit">
                                <div class="btn-group">
                                    <a class="edit btn btn-default editButton" data-id-voucher="{$voucher['id_giftvoucher']|escape:'htmlall':'UTF-8'}">
                                        <i class="icon-pencil"></i>&nbsp;{l s='Edit' mod='prestagiftvouchers'}
                                    </a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="toggle_active_voucher">
                                                <i class="icon icon-off toggle_active_voucher"></i> {l s='Disable' mod='prestagiftvouchers'}
                                            </a>
                                            <a class="deleteButton" id="deleteButton" title="{l s='Delete' mod='prestagiftvouchers'}">
                                                <i class="icon-trash"></i>&nbsp;{l s='Delete' mod='prestagiftvouchers'}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {else}
                            <div class="btn-group-action pull-right group-edit">
                                <div class="btn-group">
                                    <a class="btn btn-default toggle_active_voucher">
                                        <i class="icon icon-off"></i> {l s='Enable' mod='prestagiftvouchers'}
                                    </a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="editButton" data-id-voucher="{$voucher['id_giftvoucher']|escape:'htmlall':'UTF-8'}">
                                                <i class="icon-pencil"></i>&nbsp;{l s='Edit' mod='prestagiftvouchers'}
                                            </a>
                                            <a class="deleteButton" id="deleteButton" title="{l s='Delete' mod='prestagiftvouchers'}">
                                                <i class="icon-trash"></i>&nbsp;{l s='Delete' mod='prestagiftvouchers'}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {/if}
                        </td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
        {/if}
        <hr>
    </div>
    <div class="panel">
        <h3>
            <i class="icon-cogs"></i> {l s='Advanced Parameters' mod='prestagiftvouchers'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small>
        </h3>
        {if isset($confirm)}{$confirm}{/if} {* html *}
        <form action="" class="form-horizontal "id="form_cumulable" method="post">
      <!-- CUMULABLE OPTION-->
            <div class="form-group">
                <label class="control-label col-lg-3">
                    <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='Do you want the cart rules to be usable with other cart rules?' mod='prestagiftvouchers'}">
                        {l s='Cumulable?' mod='prestagiftvouchers'}
                    </span>
                </label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="is_cumulable" id="is_cumulable_on" value="1" {if $is_cumulable==1}checked="checked"{/if}>
                        <label for="is_cumulable_on">{l s='Yes' mod='prestagiftvouchers'}</label>
                        <input type="radio" name="is_cumulable" id="is_cumulable_off" value="0" {if $is_cumulable==0}checked="checked"{/if}>
                        <label for="is_cumulable_off">{l s='No' mod='prestagiftvouchers'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                </div>
            </div>

      <!-- GROUP RESTRICTION -->
      <div class="form-group">
        <label class="col-lg-3 control-label" for="group_checkboxes">
          <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='These groups won\'t generate any cart rule' mod='prestagiftvouchers'}">
            {l s='Exclude groups?' mod='prestagiftvouchers'}</label>
          </span>
        <div class="col-lg-9">
          {foreach from=$groupsAvailable item=group name=foreachGroup}
            <div class="checkbox">
              <label for="{'group-'|cat: $smarty.foreach.foreachGroup.index}">
                <input type="checkbox" class="text" value="{$group.id_group|escape:'htmlall':'UTF-8'}" name="group_checkboxes[]" {if in_array($group.id_group, $groupsRestricted)}checked="checked"{/if}>
                {$group.name}
              </label>
            </div>
          {/foreach}
        </div>
      </div>
      <!-- END GROUP RESTRICTION -->

      <!-- ORDER STATE TRIGGER -->
      <div class="form-group">
        <label class="col-lg-3 control-label" for="group_checkboxes">
          <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='Which order status should trigger the discount generation?' mod='prestagiftvouchers'}">
            {l s='Order State Trigger' mod='prestagiftvouchers'}</label>
          </span>
        <div class="col-lg-9">
          <select name="order_state_trigger" class="input-small">
            {foreach from=$orderStates item=orderstate}
            <option value="{$orderstate.id_order_state}" {if $orderstate.current == true}selected{/if}>{$orderstate.name}</option>
            {/foreach}
          </select>
        </div>
      </div>
      <!-- END ORDER STATE TRIGGER -->
            <div class="panel-footer">
                <button name="submitAdvancedParameters" value="1" class="btn btn-default pull-right" type="submit">
                    <i class="process-icon-save"></i>
                    <span>{l s='Save' mod='prestagiftvouchers'}</span>
                </button>
            </div>
        </form>
    </div>
</div>
