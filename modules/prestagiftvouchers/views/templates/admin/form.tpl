{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<form class="form-horizontal" name="form_prestagiftvouchers" action="" id='form_prestagiftvouchers' method="POST">

    <!-- Populated when editing a giftvoucher -->
    <input type="hidden" name="id_giftvoucher" id="id_giftvoucher">

    <!-- Condition Name -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for='name'>
        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" title="{l s='The name of your discount' mod='prestagiftvouchers'}">{l s='Name' mod='prestagiftvouchers'}</span>
    </label>
    <div class="col-md-8">
        <div class="col-md-6">
            <div class="input-group">
                <input required name="name" type="text" id="name" value="" class="text form-control">
            </div>
        </div>
    </div>
    </div>
    <hr>
    <!-- Condition Type -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="unlimited">
        <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" title="{l s='If you choose Yes, the client will be able to receive discount codes as many times as his orders meet the criteria of this voucher rule.' mod='prestagiftvouchers'}">{l s='Unlimited?' mod='prestagiftvouchers'}</span>
    </label>
    <div class="col-md-8">
        <label class="radio-inline" for="unlimited-1">
            <input name="unlimited" type="radio" id="unlimited-1" value="1" checked="checked">{l s='Yes' mod='prestagiftvouchers'}
        </label>
        <label class="radio-inline" for="unlimited-0">
            <input name="unlimited" type="radio" id="unlimited-0" value="0">{l s='No' mod='prestagiftvouchers'}
        </label>
    </div>
    </div>
    <div class="form-group row">
        <label class="col-md-4 control-label" for="condition_type">
            <span class="label-tooltip" data-toggle="tooltip" data-placement="bottom" title="{l s='You can choose to generate cart rules based on order total amount, or on specific products to buy, or on specific category of products' mod='prestagiftvouchers'}">{l s='Type of condition' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <div id="condition_type" class="btn-group col-centered" data-toggle="buttons" required>
                <label class="btn btn-default active btn-lg btn-condition-0">
                    <input type="radio" id="condition_type_0" name="condition_type" value="0" autocomplete="off" checked>{l s='Amount' mod='prestagiftvouchers'}
                </label>
                <label class="btn btn-default btn-lg btn-condition-1">
                    <input type="radio" id="condition_type_1" name="condition_type" value="1" autocomplete="off">{l s='Product' mod='prestagiftvouchers'}
                </label>
                <label class="btn btn-default btn-lg btn-condition-2">
                    <input type="radio" id="condition_type_2" name="condition_type" value="2" autocomplete="off">{l s='Category' mod='prestagiftvouchers'}
                </label>
            </div>
        </div>
    </div>

    <!-- Required amount -->
    <div class="form-group row" id="form-group-required-amount">
        <label class="col-md-4 control-label" for="amount_required">
            <span class="label-tooltip" title="{l s='Minimum amount necessary to generate a cart rule' mod='prestagiftvouchers'}">{l s='Order amount required' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <div class="col-md-6">
                <div class="input-group">
                    <input id="amount_required" name="amount_required" class="form-control" type="text">
                    <span class="input-group-addon">{$currency|escape:'htmlall':'UTF-8'}</span>
                </div>
            </div>
        </div>
    </div>

    <!-- Shipping included ? -->
    <div class="form-group row" id="form-group-shipping-included">
        <label class="col-md-4 control-label" for="shipping_included">
            <span class="label-tooltip" title="{l s='Do you want to include the shipping fees in this required amount ?' mod='prestagiftvouchers'}">{l s='Shipping included in order amount?' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <label class="radio-inline" for="shipping_included-0">
                <input type="radio" name="shipping_included" id="shipping_included-0" value="1" checked="checked">
                {l s='Yes' mod='prestagiftvouchers'}
            </label>
            <label class="radio-inline" for="shipping_included-1">
                <input type="radio" name="shipping_included" id="shipping_included-1" value="0">
                {l s='No' mod='prestagiftvouchers'}
            </label>
        </div>
    </div>

    <!-- Required Product -->
    <div class="form-group row" id="form-group-search-product">
        <label class="col-md-4 control-label" for="id_product">
            <span class="label-tooltip" title="{l s='Product to buy to generate a cart rule' mod='prestagiftvouchers'}">{l s='Product name' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-6">
            <input type="hidden" id="id_product" name='id_product' class="form-control" style="width:100%;"/>
        </div>
    </div>

    <!-- Required Category -->
    <div class="form-group row" id="form-group-search-category">
        <label class="col-md-4 control-label" for="id_category">
            <span class="label-tooltip" title="{l s='Product to buy within that category to generate a cart rule' mod='prestagiftvouchers'}">{l s='Category name' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-6">
            <input type="hidden" id="id_category" name='id_category' class="form-control" style="width:100%;"/>
        </div>
    </div>

    <!-- New clients only ? -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="new_clients_only">
            <span class="label-tooltip" title="{l s='Reserve the discount for new clients only. Those who already placed an order in the past won\'t receive it.' mod='prestagiftvouchers'}">{l s='Only for new customers?' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <label class="radio-inline" for="new_clients_only-0">
                <input type="radio" name="new_clients_only" id="new_clients_only-0" value="1">
                {l s='Yes' mod='prestagiftvouchers'}
            </label>
            <label class="radio-inline" for="new_clients_only-1">
                <input type="radio" name="new_clients_only" id="new_clients_only-1" value="0" checked="checked">
                {l s='No' mod='prestagiftvouchers'}
            </label>
        </div>
    </div>

    <hr>

    <!-- Discount Type -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="reduction_type">
            <span class="label-tooltip" title="{l s='The type of discount that will be created when an order match your criteria' mod='prestagiftvouchers'}">{l s='Discount Type' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <div id="reduction_type" class="btn-group" data-toggle="buttons" required>
                <label class="btn btn-default active btn-lg">
                    <input type="radio" id="reduction_type-0" name="reduction_type" value="0" autocomplete="off" checked>{l s='Amount' mod='prestagiftvouchers'}
                </label>
                <label class="btn btn-default btn-lg">
                    <input type="radio" id="reduction_type-1" name="reduction_type" value="1" autocomplete="off">{l s='Percentage' mod='prestagiftvouchers'}
                </label>
            </div>
        </div>
    </div>

    <!-- Discount value -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="reduction_value">
            <span class="label-tooltip" title="{l s='Discount voucher amount/percentage (cannot be applied to shipping)' mod='prestagiftvouchers'}">{l s='Discount value' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-4">
            <div class="input-group">
                <input id="reduction_value" name="reduction_value" class="form-control" type="text" required>
                <span class="input-group-addon" id="reduction_value_symbol">{$currency|escape:'htmlall':'UTF-8'}</span>
            </div>
        </div>
    </div>

    <!-- Appended Input : Discount Min Use -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="reduction_min_use">
            <span class="label-tooltip" title="{l s='Minimum order amount required (shipping not included) for the discount voucher to be valid' mod='prestagiftvouchers'}">{l s='Minimum order amount' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <div class="col-md-4">
                <div class="input-group">
                    <input id="reduction_min_use" name="reduction_min_use" class="form-control" type="text" required="">
                    <span class="input-group-addon">{$currency|escape:'htmlall':'UTF-8'}</span>
                </div>
            </div>
        </div>
    </div>

    <!-- Free Shipping ? -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="free_shipping">
            <span class="label-tooltip" title="{l s='Do you want to offer the shipping fees when using the discount ?' mod='prestagiftvouchers'}">{l s='Offer free shipping ?' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <label class="radio-inline" for="free_shipping-0">
                <input type="radio" name="free_shipping" id="free_shipping-0" value="1">
                {l s='Yes' mod='prestagiftvouchers'}
            </label>
            <label class="radio-inline" for="free_shipping-1">
                <input type="radio" name="free_shipping" id="free_shipping-1" value="0" checked="checked">
                {l s='No' mod='prestagiftvouchers'}
            </label>
        </div>
    </div>

    <!-- Duration -->
    <div class="form-group row">
        <label class="col-md-4 control-label" for="duration">
            <span class="label-tooltip" title="{l s='How many days do you want the cart rule to be valid ?' mod='prestagiftvouchers'}">{l s='Discount voucher validity length' mod='prestagiftvouchers'}</span>
        </label>
        <div class="col-md-8">
            <div class="col-md-4">
                <div class="input-group">
                    <input id="duration" name="duration" class="form-control" type="text" required="" min="1">
                    <span class="input-group-addon">{l s='days' mod='prestagiftvouchers'}</span>
                </div>
            </div>
        </div>
    </div>

    <!--</form> is in modal.tpl-->
