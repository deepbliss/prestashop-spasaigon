{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<div class="modal fade" id="modalEditGiftVoucher">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{l s='Create/Change a discount voucher rule' mod='prestagiftvouchers'}</h4>
            </div>
            <div class="modal-body">
                {include file="./form.tpl"}
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <div class="pull-right" style="line-height: 35px;!important;">
                        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">{l s='Close' mod='prestagiftvouchers'}</button>
                        <input type="submit" id="form_prestagiftvouchers" name="form_prestagiftvouchers" class="btn btn-success btn-lg" value={l s='Save' mod='prestagiftvouchers'}>
                    </div>
                </div>
            </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
