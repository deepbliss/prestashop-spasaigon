{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<div class="tab-pane panel" id="tab_documentation">
    <h3>
        <i class="icon-book"></i> {l s='Documentation' mod='prestagiftvouchers'} <small>{$module_display|escape:'htmlall':'UTF-8'}</small>
    </h3>
    <p>{l s='The Discount Vouchers module developed by PrestaShop allows you to generate discount vouchers and automatically send them to your customers who have placed a certain type of order.' mod='prestagiftvouchers'}</p>
    <p>{l s='' mod='prestagiftvouchers'}</p>
    <ul>
        <li>{l s='Automatically create cart rules that generate offer codes emailed to your customers' mod='prestagiftvouchers'}</li>
        <li>{l s='Customize the percentage or amount of discount vouchers' mod='prestagiftvouchers'}</li>
        <li>{l s='Choose the expiration date of your discount vouchers' mod='prestagiftvouchers'}</li>
    </ul>
    <p>&nbsp;</p>
    <div class="media">
        <a class="pull-left" target="_blank" href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link|escape:'htmlall':'UTF-8'}">
            <img heigh="64" width="64" class="media-object" src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/pdf.png" alt="" title=""/>
        </a>
        <div class="media-body">
            <h4 class="media-heading"><p>{l s='Attached you will find the documentation for your module. Please consult it to properly configure the module.' mod='prestagiftvouchers'}</p></h4>
            <p><a href="{$module_dir|escape:'htmlall':'UTF-8'}{$guide_link|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Discount vouchers user guide' mod='prestagiftvouchers'}</a></p>
            <p>{l s='Access to Prestashop free documentation: ' mod='prestagiftvouchers'} <a href="http://doc.prestashop.com/" target="_blank">{l s='Click here' mod='prestagiftvouchers'}</a></p>
        </div>
    </div>

    <p>&nbsp;</p>
    <p>{l s='Need help? You will find it on' mod='prestagiftvouchers'} <a href="#tab_contact" class="contactus" data-toggle="tab" data-original-title="" title="{l s='Need help?' mod='prestagiftvouchers'}">{l s='Contact tab.' mod='prestagiftvouchers'}</a></p>
</div>
