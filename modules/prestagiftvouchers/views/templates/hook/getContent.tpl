{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<div class="clearfix">
{if isset($confirmation)}
    <div class="alert alert-success">{l s='Settings updated' mod='prestagiftvouchers'}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
{/if}
{if isset($error)}
    <div class="alert alert-danger">{l s='Oops, there was a problem and your setting weren\'t updated' mod='prestagiftvouchers'}
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>
{/if}
<div class="col-lg-2">
    <div class="list-group" id="myTabs">
        <a href="#tab_documentation" class="list-group-item" data-toggle="tab"><i class="icon-book"></i> {l s='Documentation' mod='prestagiftvouchers'}</a>
        <a href="#tab_configuration" class="list-group-item active" data-toggle="tab"><i class="icon-cog"></i> {l s='Configuration' mod='prestagiftvouchers'}</a>
        {if ($apifaq != '')}
        <a href="#faq" class="list-group-item" data-toggle="tab"><i class="icon-question"></i> {l s='Help' mod='prestagiftvouchers'}</a>
        {/if}
        <a href="#tab_contact" class="list-group-item" data-toggle="tab"><i class="icon-envelope"></i> {l s='Contact' mod='prestagiftvouchers'}</a>
    </div>
    <div class="list-group">
        <a disabled="true" class="list-group-item"><i class="icon-info"></i> {l s='Version' mod='prestagiftvouchers'} {$module_version|escape:'htmlall':'UTF-8'}</a>
    </div>
</div>
<div class="tab-content col-lg-10">
    <!-- DOCUMENTATION TEMPLATE -->
    {include file='../admin/documentation.tpl'}
    <!-- CONFIGURATION TEMPLATE -->
    {include file='../admin/configuration.tpl'}
    <!-- FAQ TEMPLATE -->
    {if ($apifaq != '')}
    {include file='../admin/faq.tpl'}
    {/if}
    <!-- CONTACT TEMPLATE -->
    {include file='../admin/contact.tpl'}
</div>
{literal}
<script type="text/javascript">
    msg_confirmation_delete = "{/literal}{$msg_confirmation_delete|escape:'htmlall':'UTF-8'}{literal}";
    search_product_placeholder = "{/literal}{$search_product_placeholder|escape:'htmlall':'UTF-8'}{literal}";
    search_category_placeholder = "{/literal}{$search_category_placeholder|escape:'htmlall':'UTF-8'}{literal}";
    currency = '{/literal}{$currency|escape:'htmlall':'UTF-8'}{literal}';
    id_lang = '{/literal}{$id_lang|intval}{literal}';
    admin_module_ajax_url = "{/literal}{$controller_url}{literal}"; /* url */
    admin_module_controller = "{/literal}{$controller_name|escape:'htmlall'}{literal}";
</script>
{/literal}
</div>
