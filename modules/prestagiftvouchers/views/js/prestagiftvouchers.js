/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
$(document).ready(function(){
    /**
     * Toggle giftvoucher status
     */
    $('.toggle_active_voucher').on('click', function(){
        $.ajax({
            type: 'POST',
            url: admin_module_ajax_url,
            context: this,
            dataType: 'json',
            data: {
                controller: admin_module_controller,
                ajax: true,
                action: 'ToggleActiveGiftVoucher',
                id_giftvoucher: $(this).closest('tr').data('id-voucher')
            },
            success: function() {
                location.reload(true);
            },
            error: function() {
                showErrorAlert();
            }
        });
    });

    /**
     * This makes Jquery Validation Plugin compatible with Bootstrap 3 forms
     */
    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.help-block').html(' ');
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    /**
     * Set validation rules in the form
     */
    $('#form_prestagiftvouchers').validate({
        // Don't ignore hidden fields, useful for select2 hidden input
        ignore: 'hidden:not(#id_product, #id_category)',
        rules: {
            amount_required: {
                number: true,
                min: 0
            },
            duration: {
                number: true,
                digits: true,
                min: 1
            },
            reduction_min_use: {
                number: true,
                min: 0
            },
            reduction_value: {
                number: true,
                min: 0
            }
        }
    });

    /**
     * Update validator rules for discount value (amount/percentage validation)
     */
    $('input:radio[name="reduction_type"]').change(
        function(){
            if ($(this).prop('checked') && $(this).val() === '0') {
              console.log('test');
                $('#reduction_value_symbol').html(currency);
                $('#reduction_value').rules('remove', 'max');
            } else if ($(this).prop('checked') && $(this).val() === '1') {
              console.log('test1');
                $('#reduction_value_symbol').html('%');
                $('#reduction_value').rules('add', {max: 100});
            }
        }
    );

    /**
     * active/inactive class management for left menu tabs
     */
    $(".list-group-item").on('click', function() {
        var $el = $(this).parent().closest(".list-group").children(".active");
        if ($el.hasClass("active")) {
            $el.removeClass("active");
            $(this).addClass("active");
        }
    });

    /**
     * Hide/show appropriate inputs when a condition type is being selected
     */
    $('#condition_type_0').change(function(){
        $('#form-group-required-amount').show('slow');
        $('#amount_required').rules('add', {required: true});
        $('#form-group-shipping-included').show('slow');
        $('input[name="shipping_included"]').rules('add', {required: true});
        $('#form-group-search-category').hide('slow');
        $('#id_category').rules('remove', 'required');
        $('#form-group-search-product').hide('slow');
        $('#id_product').rules('remove', 'required');
    });
    $('#condition_type_1').change(function() {
        $('#form-group-search-product').show('slow');
        $('#id_product').rules('add', {required: true});
        $('#form-group-search-category').hide('slow');
        $('#id_category').rules('remove', 'required');
        $('#form-group-required-amount').hide('slow');
        $('#amount_required').rules('remove', 'required');
        $('#form-group-shipping-included').hide('slow');
        $('input[name="shipping_included"]').rules('remove', 'required');
    });
    $('#condition_type_2').change(function() {
        $('#form-group-search-category').show('slow');
        $('#id_category').rules('add', {required: true});
        $('#form-group-search-product').hide('slow');
        $('#id_product').rules('remove', 'required');
        $('#form-group-required-amount').hide('slow');
        $('#amount_required').rules('remove', 'required');
        $('#form-group-shipping-included').hide('slow');
        $('input[name="shipping_included"]').rules('remove', 'required');
    });

    /**
     * Helper function to keep table row from collapsing when being sorted
     */
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index){
            $(this).width($originals.eq(index).width());
        });
        return $helper;
    };

    /**
     * Makes vouchers table sortable
     */
    $("#vouchers_table tbody").sortable({
        helper: fixHelperModified,
        stop: function(event,ui) {renumber_table('#vouchers_table');}
    }).disableSelection();

    /**
     * AJAX request to edit a giftvoucher
     */
    $('.editButton').on('click', function(){
        $('#id_giftvoucher').rules('add', {required:true});
        $.ajax({
            type: 'POST',
            url: admin_module_ajax_url,
            dataType: 'json',
            data: {
                controller: admin_module_controller,
                ajax: true,
                action: 'GetGiftVoucher',
                id_giftvoucher: $(this).data('id-voucher')
            },
            success: function(data){
              console.log(data);
                for (var attribute in data[0]) {
                    if (data[0].hasOwnProperty(attribute)) {
                        $('#form_prestagiftvouchers [name='+attribute+']').val([data[0][attribute]]);
                    }
                }
                if (data[0].condition_type === '0') {
                  console.log("condition 0");
                    $('#condition_type_0').trigger('click');
                    $('.btn-condition-1').addClass('disabled');
                    $('.btn-condition-2').addClass('disabled');
                } else if (data[0].condition_type === '1') {
                    $('#condition_type_1').trigger('click');
                    $('.btn-condition-0').addClass('disabled');
                    $('.btn-condition-2').addClass('disabled');
                    $('#id_product').select2("val", data[0].id_product);
                } else {
                    $('#condition_type_2').trigger('click');
                    $('.btn-condition-0').addClass('disabled');
                    $('.btn-condition-1').addClass('disabled');
                    $('#id_category').select2("val", data[0].id_category);
                }
                $('#modalEditGiftVoucher').modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    });

    /**
     * Reset form on modal close event
     */
    $('#modalEditGiftVoucher').on('hidden.bs.modal', function () {
        $('#id_giftvoucher').removeAttr('value');
        $('#id_giftvoucher').rules('remove', 'required');
        $('div#condition_type').children().removeClass('disabled');
        $('#condition_type_0').trigger('click');
        $('#form_prestagiftvouchers').get(0).reset();
        $('#id_product').select2('val', '');
        $('#id_category').select2('val', '');
    });

    /**
     * AJAX request to delete a giftvoucher
     */
    $('.deleteButton').on('click', function(){
        if (confirm(msg_confirmation_delete)) {
            $.ajax({
                type: 'POST',
                url: admin_module_ajax_url,
                dataType: 'html',
                context: this,
                data: {
                    controller: admin_module_controller,
                    ajax: true,
                    action: 'DeleteGiftVoucher',
                    id_giftvoucher: $(this).closest('tr').data('id-voucher')
                },
                success: function() {
                    $(this).closest('tr').hide('slow').remove();
                    renumber_table('#vouchers_table');
                    showSuccessAlert();
                },
                error: function() {
                    showErrorAlert();
                }
            });
        }
    });

    /**
     * Product search in the modal form
     * cf. select2 + AJAX
     */
    $('#id_product').select2({
        placeholder: search_product_placeholder,
        minimumInputLength: 2,
        ajax: {
            url: admin_module_ajax_url,
            dataType: 'json',
            data: function (term) {
                var queryParameters = {
                    user_input: term,
                    id_lang: id_lang,
                    controller: admin_module_controller,
                    ajax: true,
                    action: 'SearchProducts'
                };
                return queryParameters;
            },
            processResults: function(data) {
                return {
                    results: data
                };
            }
        },
        initSelection: function(element, callback) {
            var id = $(element).val();
            if (id !== "") {
                $.ajax({
                    type: 'POST',
                    url: admin_module_ajax_url,
                    dataType: 'json',
                    data: {
                        controller: admin_module_controller,
                        ajax: true,
                        action: 'GetProduct',
                        id_product: id,
                        id_lang: id_lang
                    }
                }).done(function(data) {
                    callback(data[0]);
                });
            }
        },
        formatResult: productFormat,
        formatSelection: productFormat
    });

    /**
     * Category Search in the modal form
     * cf. select2 + AJAX
     */
    $('#id_category').select2({
        placeholder: search_category_placeholder,
        minimumInputLength: 2,
        ajax: {
            url: admin_module_ajax_url,
            dataType: 'json',
            data: function (term) {
                var queryParameters = {
                    user_input: term,
                    id_lang: id_lang,
                    controller: admin_module_controller,
                    ajax: true,
                    action: 'SearchCategories'
                };
                return queryParameters;
            },
            processResults: function(data) {
                return {
                    results: data
                };
            }
        },
        initSelection: function(element, callback) {
            var id = $(element).val();
            if (id !== "") {
                $.ajax({
                    type: 'POST',
                    url: admin_module_ajax_url,
                    dataType: 'json',
                    data: {
                        controller: admin_module_controller,
                        ajax: true,
                        action: 'GetCategory',
                        id_category: id,
                        id_lang: id_lang
                    }
                }).done(function(data) {
                    callback(data[0]);
                });
            }
        },
        formatResult: categoryFormat,
        formatSelection: categoryFormat
    });

    /**
     * FAQ collapse/extend
     */
    $('.faq-item-prestagiftvouchers').click(function(){
            if ($(this).find('.faq-content').is(':visible')) {
                $(this).find('.faq-content').hide('fast');
                $(this).find('.expand').html('+');
            } else {
                $('.faq-content').each(function() {
                    $(this).prev().text('+');
                    $(this).hide('fast');
                });
                $(this).find('.expand').html('-');
                $(this).find('.faq-content').show('fast');
            }

        }
    );

}); // END $(document).ready()

/**
 * Format the categories search results
 * @param category
 * @returns {string}
 */
function categoryFormat (category) {
    var markup = '#' + category.id + ' - ' + category.name;
    return markup;
}

/**
 * Format the products search results
 * @param product
 * @returns {*}
 */
function productFormat (product) {
    var markup = $.parseHTML(
        '<div class="media">' +
        '<div class="media-left">' +
        '<img class="media-object" style="width:98px;height:98px;vertical-align:middle;margin-right:3px;" src="' + product.img_url + '"/>' +
        '</div>' +
        '<div class="media-body">' +
        '<div class="media-right"><span>#' + product.id + ' - ' + product.name + '</span></div>' +
        '</div>' +
        '</div>');
    return markup;
}

/**
 * Update voucher priorities in the given table
 * @param tableID
 */
function renumber_table(tableID) {
    $(tableID).find('tbody').find('tr').each(function() {
        var count = $(this).parent().children().index($(this)) + 1;
        var current_priority = $(this).find('.priority').html(); //.html(count);
        if (count != current_priority) {
            $(this).find('.priority').html(count);
            $.ajax({
                type: 'POST',
                url: admin_module_ajax_url,
                dataType: 'html',
                data: {
                    controller: admin_module_controller,
                    ajax: true,
                    action: 'UpdatePriority',
                    id_giftvoucher: $(this).find('.id_giftvoucher').text(),
                    new_priority: count
                },
                success: function() {
                    showSuccessAlert();
                },
                error: function() {
                    showErrorAlert();
                    return;
                }
            });
        }
    });
}

/**
 * Shows success message after an AJAX request
 */
function showErrorAlert() {
    $("#errorAjax").removeClass('hidden').alert();
    $("#errorAjax").fadeTo(2000, 500).slideUp(500, function(){
        $("#errorAjax").hide();
    });
}
/**
 * Shows error message after an AJAX request
 */
function showSuccessAlert() {
    $("#successAjax").removeClass('hidden').alert();
    $("#successAjax").fadeTo(2000, 500).slideUp(500, function(){
        $("#successAjax").hide();
    });
}
