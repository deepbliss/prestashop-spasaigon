<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminPrestaGiftVoucherController extends ModuleAdminController
{
    /**
     * Handles GetCategory AJAX request
     *
     * @return void
     */
    public function ajaxProcessGetCategory()
    {
        $id_category = Tools::getValue('id_category');
        $id_lang = Tools::getValue('id_lang');
        $category = PrestaGiftVoucher::getCategoryForInit($id_category, $id_lang);

        exit(Tools::jsonEncode($category));
    }

    /**
     * Handles GetProduct AJAX request
     *
     * @return void
     */
    public function ajaxProcessGetProduct()
    {
        $id_product = Tools::getValue('id_product');
        $id_lang = Tools::getValue('id_lang');
        $product = PrestaGiftVoucher::getProductForInit($id_product, $id_lang);

        exit(Tools::jsonEncode($product));
    }

    /**
     * Handles ToggleActiveGiftVoucher AJAX request
     *
     * @return void
     */
    public function ajaxProcessToggleActiveGiftVoucher()
    {
        $id_giftvoucher = Tools::getValue('id_giftvoucher');
        $id_lang = Tools::getValue('id_lang');
        $result = PrestaGiftVoucher::toggleActiveState($id_giftvoucher, $id_lang);

        exit($result);
    }

    /**
     * Handles SearchProducts AJAX request
     *
     * @return void
     */
    public function ajaxProcessSearchProducts()
    {
        $user_search = Tools::getValue('user_input');
        $id_lang = Tools::getValue('id_lang');
        $result_search = PrestaGiftVoucher::searchProductByName($user_search, $id_lang);
        exit(Tools::jsonEncode($result_search));
    }

    /**
     * Handles SearchCategories AJAX request
     *
     * @return void
     */
    public function ajaxProcessSearchCategories()
    {
        $user_search = Tools::getValue('user_input');
        $id_lang = Tools::getValue('id_lang');
        $result_search = PrestaGiftVoucher::searchCategoryByName($user_search, $id_lang);

        exit(Tools::jsonEncode($result_search));
    }

    /**
     * Handles DeleteGiftVoucher AJAX request
     *
     * @return void
     */
    public function ajaxProcessDeleteGiftVoucher()
    {
        $id_giftvoucher = Tools::getValue('id_giftvoucher');
        exit(PrestaGiftVoucher::deleteGiftVoucher($id_giftvoucher));
    }

    /**
     * Handles GetGiftVoucher AJAX request
     *
     * @return void
     */
    public function ajaxProcessGetGiftVoucher()
    {
        $id_giftvoucher = Tools::getValue('id_giftvoucher');
        $giftvoucher = PrestaGiftVoucher::getGiftVoucher($id_giftvoucher);

        exit(Tools::jsonEncode($giftvoucher));
    }

    /**
     * Handles UpdatePriority AJAX request
     *
     * @return void
     */
    public function ajaxProcessUpdatePriority()
    {
        $id_giftvoucher = Tools::getValue('id_giftvoucher');
        $new_priority = Tools::getValue('new_priority');
        exit(PrestaGiftVoucher::updatePriority($id_giftvoucher, $new_priority));
    }
}
