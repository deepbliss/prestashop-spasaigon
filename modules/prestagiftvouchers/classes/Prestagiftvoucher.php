<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class PrestaGiftVoucher
{
    /**
     * Does the given order match with the given gift voucher ?
     *
     * @param Order $order       the order Instance
     * @param array $giftvoucher the voucher rule
     *
     * @return bool
     */
    public static function doesOrderMatch($order, $giftvoucher)
    {
        // If the voucher is reserved for new clients only, cancel procedure for customer
        // with nb_orders > 1 (1=current order)
        if ((bool)$giftvoucher['new_clients_only'] === true && Order::getCustomerNbOrders($order->id_customer) > 1) {
            return (false);
        }
        switch ($giftvoucher['condition_type']) {
            case 0:
                return (self::isMatchingAmount($order, $giftvoucher));
            case 1:
                return (self::isMatchingProduct($order, $giftvoucher));
            case 2:
                return (self::isMatchingCategory($order, $giftvoucher));
            default:
                return (false);
        }
    }

    /**
     * Check if the given order reach the required amount in the given gift voucher
     *
     * @param Order $order       the order instance
     * @param array $giftvoucher the voucher rule
     *
     * @return bool
     */
    public static function isMatchingAmount($order, $giftvoucher)
    {
        // If the order has been place in another currency, convert it in the current
        $order_currency = new Currency($order->id_currency);
        $store_currency = Context::getContext()->currency;
        if ($order_currency->iso_code != $store_currency->iso_code) {
            $total_paid = Tools::convertPriceFull($order->total_paid, $order_currency, $store_currency);
            $total_shipping = Tools::convertPriceFull($order->total_shipping, $order_currency, $store_currency);
        } else {
            $total_paid = $order->total_paid;
            $total_shipping = $order->total_shipping;
        }

        if ($giftvoucher['shipping_included'] == true) {
            return ($total_paid >= $giftvoucher['amount_required']);
        } else {
            return ($total_paid - $total_shipping >= $giftvoucher['amount_required']);
        }
    }

    /**
     * Check if the given order contains the product specified in the given gift voucher
     *
     * @param Order $order       the order instance
     * @param array $giftvoucher the voucher rule
     *
     * @return bool
     */
    public function isMatchingProduct($order, $giftvoucher)
    {
        foreach ($order->getProducts() as $orderProduct) {
            if ($orderProduct['product_id'] == $giftvoucher['id_product']) {
                return (true);
            }
        }
        return (false);
    }

    /**
     * Check if the given order contains a product within the category specified in the given gift voucher
     *
     * @param Order $order       the order instance
     * @param array $giftvoucher the voucher rule
     *
     * @return bool
     */
    public function isMatchingCategory($order, $giftvoucher)
    {
        foreach ($order->getProducts() as $orderProduct) {
            $product = new Product($orderProduct['id_product'], true, Context::getContext()->language->id);
            foreach ($product->getCategories() as $orderProductCategory) {
                if ($orderProductCategory == $giftvoucher['id_category']) {
                    return (true);
                }
            }
            unset($product);
        }
        return (false);
    }

    /**
     * Toggle giftvoucher status
     *
     * @param int $id_giftvoucher the voucher rule to toggle
     *
     * @return bool
     */
    public static function toggleActiveState($id_giftvoucher)
    {
        $query  = 'UPDATE `'. _DB_PREFIX_ .'prestagiftvouchers` ';
        $query .= 'SET active = !active ';
        $query .= 'WHERE id_giftvoucher = '. (int)$id_giftvoucher;

        return (Db::getInstance()->execute($query));
    }

    /**
     * Get the picture of the given product
     *
     * @param int      $id_lang        the lang
     * @param int      $id_product     the product
     * @param null|int $id_combination the product combination
     *
     * @return bool|string
     */
    public static function getProductImageUrl($id_lang, $id_product, $id_combination = null)
    {
        $cover_dir = Image::getImages($id_lang, $id_product, $id_combination);

        if (!isset($cover_dir[0]['id_image'])) {
            $cover_dir = Image::getImages($id_lang, $id_product);
        }

        if (!isset($cover_dir[0]['id_image'])) {
            return (false);
        }

        $img_dir = implode('/', str_split($cover_dir[0]['id_image']));
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            return (_PS_BASE_URL_._PS_IMG_.'p/'.$img_dir.'/'.$cover_dir[0]['id_image'].'-'.
            ImageType::getFormatedName('small').'.jpg');
        } else {
            // ImageType::getFormatedName is now ImageType::getFormattedName
            return (_PS_BASE_URL_._PS_IMG_.'p/'.$img_dir.'/'.$cover_dir[0]['id_image'].'-'.
            ImageType::getFormattedName('small').'.jpg');
        }
    }

    /**
     * Get product id, name and img url for select2 initialization
     *
     * @param int $id_product the product
     * @param int $id_lang    the lang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getProductForInit($id_product, $id_lang)
    {
        $query  = 'SELECT DISTINCT pl.id_product as id, pl.name as name ';
        $query .= 'FROM `'._DB_PREFIX_.'product_lang` pl ';
        $query .= 'JOIN `'._DB_PREFIX_.'product` p ON pl.id_product = p.id_product ';
        $query .= 'WHERE pl.id_lang = '.(int)$id_lang;
        $query .= ' AND p.id_product = '. (int)$id_product;

        $result = Db::getInstance()->executeS($query);
        if (!empty($result)) {
            $result[0]['img_url'] = self::getProductImageUrl($id_lang, $id_product);
        }
        return ($result);
    }

    /**
     * Get category id and name for select2 initialization
     *
     * @param int $id_category the category
     * @param int $id_lang     the lang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getCategoryForInit($id_category, $id_lang)
    {
        $query  = 'SELECT DISTINCT cl.id_category as id, cl.name as name ';
        $query .= 'FROM `'._DB_PREFIX_.'category_lang` cl ';
        $query .= 'JOIN `'._DB_PREFIX_.'category` c ON cl.id_category = c.id_category ';
        $query .= 'WHERE cl.id_lang = '.(int)$id_lang;
        $query .= ' AND c.id_category = '. (int)$id_category;

        $result = Db::getInstance()->executeS($query);
        return ($result);
    }

    /**
     * Search for active products by name
     *
     * @param string $value   the name to find
     * @param int    $id_lang the lang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function searchProductByName($value, $id_lang)
    {
        $query  = 'SELECT DISTINCT pl.id_product as id, pl.name as name ';
        $query .= 'FROM `'._DB_PREFIX_.'product_lang` pl ';
        $query .= 'JOIN `'._DB_PREFIX_.'product` p ON pl.id_product = p.id_product ';
        $query .= 'WHERE pl.id_lang = '.(int)$id_lang;
        $query .= ' AND pl.name LIKE "%'.pSQL($value).'%" ';
        $query .= 'AND p.active = 1';

        $results = Db::getInstance()->ExecuteS($query);
        if (!empty($results)) {
            for ($i = 0; isset($results[$i]); $i++) {
                $results[$i]['img_url'] = self::getProductImageUrl(
                    Context::getContext()->language->id,
                    $results[$i]['id']
                );
            }
        }
        return ($results);
    }

    /**
     * Search for active categories by name
     *
     * @param string $value   the name to find
     * @param int    $id_lang the lang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function searchCategoryByName($value, $id_lang)
    {
        $query  = 'SELECT DISTINCT cl.id_category as id, cl.name as name ';
        $query .= 'FROM `'._DB_PREFIX_.'category_lang` cl ';
        $query .= 'JOIN `'._DB_PREFIX_.'category` c ON cl.id_category = c.id_category ';
        $query .= 'WHERE cl.id_lang = '.(int)$id_lang;
        $query .= ' AND cl.name LIKE "%'.pSQL($value).'%" ';
        return Db::getInstance()->ExecuteS($query);
    }

    /**
     * Update priority of the given giftvoucher
     *
     * @param int $id_giftvoucher the voucher rule to update
     * @param int $new_priority   the priority value to set
     *
     * @return bool
     */
    public static function updatePriority($id_giftvoucher, $new_priority)
    {
        $req  = 'UPDATE `'._DB_PREFIX_.'prestagiftvouchers` ';
        $req .= 'SET `priority`=' . (int)$new_priority;
        $req .= ' WHERE `id_giftvoucher`=' . (int)$id_giftvoucher . ';';

        return (Db::getInstance()->execute($req));
    }

    /**
     * Search and disable cart rules that have been created by the given order
     *
     * @param Order $order the order instance
     *
     * @return void
     * @throws PrestaShopDatabaseException
     */
    public static function cancelCartRulesAfterRefund($order)
    {
        $req  = 'SELECT * ';
        $req .= 'FROM `' . _DB_PREFIX_ .'cart_rule` c ';
        $req .= 'WHERE STRCMP(SUBSTRING(c.description, 7), "' . (int)$order->id . '") = 0';
        //TODO Fix this
        $result = Db::getInstance()->executeS($req);
        $r = '';
        if (!empty($result)) {
            $req  = 'UPDATE `' . _DB_PREFIX_ .'cart_rule` c ';
            $req .= 'SET active = 0 ';
            $req .= 'WHERE c.id_cart_rule = ' . (int)$r['id_cart_rule'];
            foreach ($result as $r) {
                Db::getInstance()->execute($req);
            }
        }
    }

    /**
     * Check if the client can receive a discount
     * Pattern : GV[id]
     *
     * @param Order $order       the order object
     * @param array $giftvoucher the voucher rule
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function isAllowedToBeGifted($order, $giftvoucher)
    {
        // Is the client in a forbidden group?
        $forbiddenGroups = Tools::jsonDecode(Configuration::get('PRESTAGIFTVOUCHERS_GROUP_RESTRICTION'));
        $forbiddenGroups = implode(',', $forbiddenGroups);
        if (isset($forbiddenGroups) && !empty($forbiddenGroups)) {
            $req = 'SELECT * FROM `'._DB_PREFIX_.'customer_group` WHERE `id_customer`='.(int)$order->id_customer.'
                    AND `id_group` in ('.$forbiddenGroups.')';
            if (Db::getInstance()->getRow($req)) {
                return false;
            }
        }

        // Has the client already generated a cart rule ?
        $req = 'SELECT * FROM `'._DB_PREFIX_.'prestagiftvoucher_cart_rule` WHERE `id_order`='.(int)$order->id;
        if (Db::getInstance()->getRow($req)) {
            return false;
        }

        $customer = new Customer($order->id_customer);
        // Hotfix : we don't want to generate discounts for orders coming from Amazon
        if (Validate::isLoadedObject($customer) && strstr($customer->email, "marketplace.amazon.fr")) {
            return (false);
        }
        if (isset($giftvoucher['unlimited']) && $giftvoucher['unlimited'] == true) {
            return (true);
        } else {
            $db = Db::getInstance();
            $match = 'GV' . $giftvoucher['id_giftvoucher'];
            $descriptions = $db->ExecuteS('SELECT description FROM `'._DB_PREFIX_.'cart_rule`
                            WHERE id_customer='.(int)$order->id_customer);
            foreach ($descriptions as $d) {
                if (strstr($d['description'], $match)) {
                    return (false);
                }
            }
        }
        return (true);
    }

    /**
     * Get all active gift vouchers ordered by priority
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getActiveGiftVouchers()
    {
        $req  = 'SELECT * ';
        $req .= 'FROM `'._DB_PREFIX_.'prestagiftvouchers` ';
        $req .= 'WHERE `active` IS TRUE ';
        $req .= 'AND `id_shop` = ' . (int)Context::getContext()->shop->id;
        $req .= ' ORDER BY `priority` ASC';

        return (Db::getInstance()->executeS($req));
    }

    /**
     * Get all gift vouchers ordered by priority
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getGiftVouchers()
    {
        $query  = 'SELECT * ';
        $query .= 'FROM `'._DB_PREFIX_.'prestagiftvouchers` ';
        $query .= 'ORDER BY `priority` ASC';

        $result = Db::getInstance()->executeS($query);
        return (self::injectProductOrCategoryName($result));
    }

    /**
     * Insert product or CategoryName if they exists for each vouchers
     *
     * @param array $giftvouchers_array the voucher rules in DB
     *
     * @return array
     */
    public static function injectProductOrCategoryName($giftvouchers_array)
    {
        if (empty($giftvouchers_array)) {
            return;
        }
        foreach ($giftvouchers_array as &$giftvoucher) {
            if ($giftvoucher['condition_type'] == 0) {
                continue;
            } elseif ($giftvoucher['condition_type'] == 1) {
                $giftvoucher['product_name'] = Product::getProductName($giftvoucher['id_product']);
            } elseif ($giftvoucher['condition_type'] == 2) {
                $category = new Category($giftvoucher['id_category']);
                $giftvoucher['category_name'] = $category->getName();
            }
        }
        return ($giftvouchers_array);
    }

    /**
     * Get the number of giftvouchers in DB
     *
     * @return false|null|string
     */
    public static function getGiftVouchersCount()
    {
        $query = 'SELECT COUNT(*) FROM `'._DB_PREFIX_.'prestagiftvouchers`';

        return (Db::getInstance()->getValue($query));
    }

    /**
     * Get one particular voucher
     *
     * @param int $id_voucher the voucher to pick
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getGiftVoucher($id_voucher)
    {
        $query  = 'SELECT * ';
        $query .= 'FROM `'._DB_PREFIX_.'prestagiftvouchers` ';
        $query .= 'WHERE id_giftvoucher = ' . (int)$id_voucher;

        $result = Db::getInstance()->executeS($query);

        return ($result);
    }

    /**
     * Create a giftvoucher based on order reaching a specific amount
     *
     * @param array $data the voucher details
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function addGiftVoucherAmount($data)
    {
        $to_insert = array(
                'name'              => pSQL($data['name']),
                'unlimited'         => (bool)$data['unlimited'],
                'id_shop'           => (int)Context::getContext()->shop->id,
                'new_clients_only'  => (bool)$data['new_clients_only'],
                'condition_type'    => 0,
                'shipping_included' => (bool)$data['shipping_included'],
                'amount_required'   => (float)$data['amount_required'],
                'reduction_type'    => (int)$data['reduction_type'],
                'reduction_value'   => (int)$data['reduction_value'],
                'reduction_min_use' => (int)$data['reduction_min_use'],
                'free_shipping'     => (bool)$data['free_shipping'],
                'duration'          => (int)$data['duration'],
                'priority'          => (int)self::getGiftVouchersCount() + 1,
                'active'            => true
            );

        return (Db::getInstance()->insert('prestagiftvouchers', $to_insert));
    }

    /**
     * Create a giftvoucher based on orders containing a specific product
     *
     * @param array $data the voucher details
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function addGiftVoucherProduct($data)
    {
        $to_insert = array(
            'name'              => pSQL($data['name']),
            'unlimited'         => (bool)$data['unlimited'],
            'id_shop'           => (int)Context::getContext()->shop->id,
            'new_clients_only'  => (bool)$data['new_clients_only'],
            'condition_type'    => 1,
            'id_product'        => (int)$data['id_product'],
            'reduction_type'    => (int)$data['reduction_type'],
            'reduction_value'   => (int)$data['reduction_value'],
            'reduction_min_use' => (int)$data['reduction_min_use'],
            'free_shipping'     => (bool)$data['free_shipping'],
            'duration'          => (int)$data['duration'],
            'priority'          => (int)self::getGiftVouchersCount() + 1,
            'active'            => true
        );

        return (Db::getInstance()->insert('prestagiftvouchers', $to_insert));
    }

    /**
     * Create a giftvoucher based on orders containing products
     * within a specific category
     *
     * @param array $data the voucher details
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function addGiftVoucherCategory($data)
    {
        $to_insert = array(
            'name'              => pSQL($data['name']),
            'unlimited'         => (bool)$data['unlimited'],
            'id_shop'           => (int)Context::getContext()->shop->id,
            'new_clients_only'  => (bool)$data['new_clients_only'],
            'condition_type'    => 2,
            'id_category'       => (int)$data['id_category'],
            'reduction_type'    => (int)$data['reduction_type'],
            'reduction_value'   => (int)$data['reduction_value'],
            'reduction_min_use' => (int)$data['reduction_min_use'],
            'free_shipping'     => (bool)$data['free_shipping'],
            'duration'          => (int)$data['duration'],
            'priority'          => (int)self::getGiftVouchersCount() + 1,
            'active'            => true
        );

        return (Db::getInstance()->insert('prestagiftvouchers', $to_insert));
    }

    /**
     * Create a giftvoucher (entry point of the creation)
     *
     * @param array $data the voucher details
     *
     * @return bool
     */
    public static function addGiftVoucher($data)
    {
        switch ($data['condition_type']) {
            case 0:
                return (self::addGiftVoucherAmount($data));
            case 1:
                return (self::addGiftVoucherProduct($data));
            case 2:
                return (self::addGiftVoucherCategory($data));
            default:
                return (false);
        }
    }

    /**
     * Edit a giftvoucher based on orders reaching a specific product
     *
     * @param array $data the voucher details
     *
     * @return bool
     */
    public static function editGiftVoucherAmount($data)
    {
        $to_update = array(
            'name'              => pSQL($data['name']),
            'unlimited'         => (bool)$data['unlimited'],
            'id_shop'           => (int)Context::getContext()->shop->id,
            'new_clients_only'  => (bool)$data['new_clients_only'],
            'condition_type'    => 0,
            'shipping_included' => (bool)$data['shipping_included'],
            'amount_required'   => (float)$data['amount_required'],
            'reduction_type'    => (int)$data['reduction_type'],
            'reduction_value'   => (int)$data['reduction_value'],
            'reduction_min_use' => (int)$data['reduction_min_use'],
            'free_shipping'     => (bool)$data['free_shipping'],
            'duration'          => (int)$data['duration'],
            'active'            => true
        );
        $where = '`id_giftvoucher` ='. (int)$data['id_giftvoucher'];

        return (Db::getInstance()->update('prestagiftvouchers', $to_update, $where));
    }

    /**
     * Edit a giftvoucher based on orders containing a specific product
     *
     * @param array $data the voucher details
     *
     * @return bool
     */
    public static function editGiftVoucherProduct($data)
    {
        $to_update = array(
            'name'              => pSQL($data['name']),
            'unlimited'         => (bool)$data['unlimited'],
            'id_shop'           => (int)Context::getContext()->shop->id,
            'new_clients_only'  => (bool)$data['new_clients_only'],
            'condition_type'    => 1,
            'id_product'        => (int)$data['id_product'],
            'reduction_type'    => (int)$data['reduction_type'],
            'reduction_value'   => (int)$data['reduction_value'],
            'reduction_min_use' => (int)$data['reduction_min_use'],
            'free_shipping'     => (bool)$data['free_shipping'],
            'duration'          => (int)$data['duration'],
            'priority'          => (int)self::getGiftVouchersCount() + 1,
            'active'            => true
        );
        $where = '`id_giftvoucher` ='. (int)$data['id_giftvoucher'];

        return (Db::getInstance()->update('prestagiftvouchers', $to_update, $where));
    }

    /**
     * Edit a giftvoucher based on orders containing products within a specific category
     *
     * @param array $data the voucher details
     *
     * @return bool
     */
    public static function editGiftVoucherCategory($data)
    {
        $to_update = array(
            'name'              => pSQL($data['name']),
            'unlimited'         => (bool)$data['unlimited'],
            'id_shop'           => (int)Context::getContext()->shop->id,
            'new_clients_only'  => (bool)$data['new_clients_only'],
            'condition_type'    => 2,
            'id_category'       => (int)$data['id_category'],
            'reduction_type'    => (int)$data['reduction_type'],
            'reduction_value'   => (int)$data['reduction_value'],
            'reduction_min_use' => (int)$data['reduction_min_use'],
            'free_shipping'     => (bool)$data['free_shipping'],
            'duration'          => (int)$data['duration'],
            'priority'          => (int)self::getGiftVouchersCount() + 1,
            'active'            => true
        );
        $where = '`id_giftvoucher` ='. (int)$data['id_giftvoucher'];

        return (Db::getInstance()->update('prestagiftvouchers', $to_update, $where));
    }

    /**
     * Edit a giftvoucher (entry point of edition)
     *
     * @param array $data the voucher details
     *
     * @return bool
     */
    public static function editGiftVoucher($data)
    {
        switch ($data['condition_type']) {
            case 0:
                return (self::editGiftVoucherAmount($data));
            case 1:
                return (self::editGiftVoucherProduct($data));
            case 2:
                return (self::editGiftVoucherCategory($data));
            default:
                return (false);
        }
    }


    /**
     * Deletes a voucher rule
     *
     * @param int $id_giftvoucher the voucher rule to delete
     *
     * @return bool
     */
    public static function deleteGiftVoucher($id_giftvoucher)
    {
        $where = 'id_giftvoucher = ' . (int)$id_giftvoucher;
        return (Db::getInstance()->delete('prestagiftvouchers', $where));
    }

    /**
     * Get the number of cart rules generated by the module
     *
     * @return int
     * @see    self::createCartRule()
     */
    public static function getTotalCartRulesGenerated()
    {
        $query  = 'SELECT DISTINCT COUNT(id_cart_rule) ';
        $query .= 'FROM `' . _DB_PREFIX_ . 'cart_rule` c ';
        $query .= 'WHERE STRCMP(LEFT(c.description,2), "GV") = 0';
        $result = Db::getInstance()->getValue($query);

        return ((int)$result[0] + 1);
    }

    /**
     * Create a cart rule according to the given data
     *
     * @param Order    $order       the order instance
     * @param Customer $customer    the customer instance
     * @param array    $giftvoucher a voucher rule
     *
     * @return bool
     */
    public static function createCartRule($order, $customer, $giftvoucher)
    {
        $cartrule = new CartRule();
        $cartrule->id_customer = $order->id_customer;
        $cartrule->date_from = date('Y-m-d H:i:s', strtotime('today 00:00'));
        $cartrule->date_to = date('Y-m-d H:i:s', strtotime('+' . $giftvoucher['duration'] . 'days 00:00'));
        $cartrule->description = 'GV' . $giftvoucher['id_giftvoucher'] . $customer->lastname .
                                (int)self::getTotalCartRulesGenerated() . ' -- From order #' . $order->id;
        $cartrule->quantity = 1;
        $cartrule->quantity_per_user = 1;
        $cartrule->partial_use = false;
        $cartrule->code = Tools::strtoupper(Tools::passwdGen(8));
        $cartrule->minimum_amount = (float)$giftvoucher['reduction_min_use'];
        $cartrule->minimum_amount_tax = true;
        $cartrule->minimum_amount_currency = Context::getContext()->currency->id;
        $cartrule->minimum_amount_shipping = $giftvoucher['shipping_included'];
        $cartrule->country_restriction = false;
        $cartrule->carrier_restriction = false;
        $cartrule->group_restriction = false;
        $cartrule->product_restriction = false;
        if (!Configuration::get('PRESTAGIFTVOUCHER_IS_CUMULABLE')) {
            $cartrule->cart_rule_restriction = true;
        } else {
            $cartrule->cart_rule_restriction = false;
        }
        $cartrule->shop_restriction = 0;
        $cartrule->free_shipping = $giftvoucher['free_shipping'];
        if ($giftvoucher['reduction_type'] == 0) {
            $cartrule->reduction_amount = $giftvoucher['reduction_value'];
        } else {
            $cartrule->reduction_percent = $giftvoucher['reduction_value'];
        }
        $cartrule->reduction_tax = 1;
        $cartrule->reduction_currency = Context::getContext()->currency->id;
        $cartrule->highlight = false;
        $cartrule->active = true;
        $cartrule->highlight = 0;
        /* Lang fields */
        foreach (Language::getLanguages() as $language) {
            $cartrule->name[$language['id_lang']] = $giftvoucher['name']. '-' . $customer->lastname;
        }
        if ($cartrule->add()) {
            self::registerPrestagiftvoucherCartrule($order->id, $cartrule->id);
            Logger::addLog('Module Discount Voucher : cart rule #'. $cartrule->id . ' created for customer #'
                .$order->id_customer . ', order #' . $order->id);
            if (Configuration::get('PRESTAGIFTVOUCHER_IS_CUMULABLE')) {
                $ids = Db::getInstance()->ExecuteS('SELECT id_cart_rule FROM '. _DB_PREFIX_ .'cart_rule
                                                    WHERE active=1 AND id_cart_rule != '. $cartrule->id);
                if (is_array($ids) && count($ids)) {
                    $values = array();
                    foreach ($ids as $id) {
                        $values[] = '('.(int)$cartrule->id.','.(int)$id['id_cart_rule'].')';
                    }
                    Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'cart_rule_combination`
                                                (`id_cart_rule_1`, `id_cart_rule_2`)
                                                VALUES '.implode(',', $values));
                }
                // The new rule has no cart rule restriction, so it must be added to the white list of the other cart
                // rules that have restrictions
                Db::getInstance()->execute(
                    'INSERT INTO `'._DB_PREFIX_.'cart_rule_combination` (`id_cart_rule_1`, `id_cart_rule_2`) (
                        SELECT id_cart_rule, '.(int)$cartrule->id.' FROM `'._DB_PREFIX_.'cart_rule`
                        WHERE cart_rule_restriction = 1
                    )'
                );
            } else {
                $ruleCombinations = Db::getInstance()->executeS(
                    'SELECT cr.id_cart_rule
                    FROM '._DB_PREFIX_.'cart_rule cr
                    WHERE cr.id_cart_rule != '.(int)$cartrule->id.'
                    AND cr.cart_rule_restriction = 0
                    AND NOT EXISTS (
                        SELECT 1
                        FROM '._DB_PREFIX_.'cart_rule_combination
                        WHERE cr.id_cart_rule = '._DB_PREFIX_.'cart_rule_combination.id_cart_rule_2
                        AND '.(int)$cartrule->id.' = id_cart_rule_1
                    )
                    AND NOT EXISTS (
                        SELECT 1
                        FROM '._DB_PREFIX_.'cart_rule_combination
                        WHERE cr.id_cart_rule = '._DB_PREFIX_.'cart_rule_combination.id_cart_rule_1
                        AND '.(int)$cartrule->id.' = id_cart_rule_2
                    )'
                );
                foreach ($ruleCombinations as $incompatibleRule) {
                    Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'cart_rule` SET cart_rule_restriction = 1
                        WHERE id_cart_rule = '.(int)$incompatibleRule['id_cart_rule'].' LIMIT 1');
                    Db::getInstance()->execute(
                        'INSERT IGNORE INTO `'._DB_PREFIX_.'cart_rule_combination`
                            (`id_cart_rule_1`, `id_cart_rule_2`) (
                            SELECT id_cart_rule, '.(int)$incompatibleRule['id_cart_rule'].'
                            FROM `'._DB_PREFIX_.'cart_rule`
                            WHERE active = 1
                            AND id_cart_rule != '.(int)$cartrule->id.'
                            AND id_cart_rule != '.(int)$incompatibleRule['id_cart_rule'].'
                        )'
                    );
                }
            }
            return ($cartrule->id);
        }
        Logger::addLog(
            'Module Discount Voucher : there was an error when creating a cart rule for order #' . $order->id
        );
        return (false);
    }

    /**
     * Updates the cumulable option
     *
     * @param bool $is_cumulable the value to set
     *
     * @return void|bool
     */
    public static function manageCumulableOption($is_cumulable)
    {
        if (Validate::isBool($is_cumulable)) {
            Configuration::updateValue('PRESTAGIFTVOUCHER_IS_CUMULABLE', $is_cumulable);
        } else {
            return (false);
        }
    }

    public static function registerPrestagiftvoucherCartrule($orderId, $cartruleId)
    {
        $to_insert = array(
            'id_order'     => (int)$orderId,
            'id_cart_rule' => (int)$cartruleId,
        );

        return (Db::getInstance()->insert('prestagiftvoucher_cart_rule', $to_insert));
    }

    public static function manageGroupRestrictionOption(array $post_data)
    {
        if (isset($post_data['group_checkboxes']) && !empty($post_data['group_checkboxes'])) {
            $forbiddenGroups = array_values($post_data['group_checkboxes']);
            Configuration::updateValue('PRESTAGIFTVOUCHERS_GROUP_RESTRICTION', Tools::jsonEncode($forbiddenGroups));
        } else {
            Configuration::updateValue('PRESTAGIFTVOUCHERS_GROUP_RESTRICTION', Tools::jsonEncode(array()));
        }
    }
}
