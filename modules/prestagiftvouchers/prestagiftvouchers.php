<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once dirname(__FILE__) . '/classes/Prestagiftvoucher.php';

/**
 * Class PrestaGiftVouchers
 *
 * @category Prestagiftvouchers
 * @package  Prestagiftvouchers
 * @author   PrestaShop SA <contact@prestashop.com>
 * @license  http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
 * @link     http://addons.prestashop.com/2376-coupons-discount-vouchers.html
 * @see      ModuleCore
 */
class PrestaGiftVouchers extends Module
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->author                 = 'PrestaShop';
        $this->name                   = 'prestagiftvouchers';
        $this->version                = '2.3.5';
        $this->bootstrap              = true;
        $this->tab                    = 'pricing_promotion';
        $this->module_key             = '2096267b6f9def42848cdfb17b45be0d';
        $this->author_address         = '0x64aa3c1e4034d07015f639b0e171b0d7b27d01aa';
        $this->id_shop                = Context::getContext()->shop->id;
        $this->controller_name        = 'AdminPrestaGiftVoucher';
        $this->ps_versions_compliancy = array(
            'min' => '1.6',
            'max' => '1.7'
        );

        parent::__construct();

        $this->displayName = $this->l('Discount Vouchers');
        $this->css_path    = $this->_path . 'views/css/';
        $this->js_path     = $this->_path . 'views/js/';
        $this->description = $this->l('With this module you can automatically generate discount vouchers that your
            customers will receive by email after their purchases');
        $this->currency    = $this->context->currency;
    }

    /**
     * PrestaShop required install method
     *
     * @see    ModuleCore::install()
     * @return bool
     */
    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if (!parent::install()
            || !Configuration::updateValue('PRESTAGIFTVOUCHERS_ORDER_STATE_TRIGGER', _PS_OS_PAYMENT_)
            || !Configuration::updateValue('PRESTAGIFTVOUCHER_IS_CUMULABLE', 0)
            || !Configuration::updateValue('PRESTAGIFTVOUCHERS_GROUP_RESTRICTION', Tools::jsonEncode(array()))
            || !Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_en', 'You\'ve got a gift!')
            || !Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_fr', 'Vous avez reçu un cadeau!')
            || !Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_de', 'Ein Geschenk wartet auf Sie!')
            || !Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_it', 'Ti aspetta un regalo!')
            || !Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_es', '¡Tenemos un regalo para ti!')
            || !$this->registerHook('updateOrderStatus')
            || !$this->installDB()
            || !$this->installTab()
        ) {
            return (false);
        }
        return (true);
    }

    /**
     * Module's DB requirements
     *
     * @return bool
     */
    public function installDB()
    {
        $install_sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'prestagiftvouchers` (
            `name` VARCHAR(255) NOT NULL,
            `unlimited` BOOLEAN,
            `id_giftvoucher` INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_shop` INT UNSIGNED NOT NULL DEFAULT 1,
            `new_clients_only` BOOLEAN NOT NULL DEFAULT TRUE,
            `condition_type` INT UNSIGNED NOT NULL DEFAULT 0,
            `shipping_included` BOOLEAN NOT NULL DEFAULT TRUE,
            `amount_required` FLOAT DEFAULT NULL,
            `id_product` INT UNSIGNED DEFAULT NULL,
            `id_category` INT UNSIGNED DEFAULT NULL,
            `reduction_type` INT UNSIGNED NOT NULL DEFAULT 0,
            `reduction_value` INT UNSIGNED NOT NULL,
            `reduction_min_use` INT UNSIGNED NOT NULL DEFAULT 0,
            `free_shipping` BOOLEAN NOT NULL DEFAULT FALSE,
            `duration` INT UNSIGNED NOT NULL DEFAULT 365,
            `priority` INT UNSIGNED NOT NULL,
            `active` BOOLEAN NOT NULL,
            PRIMARY KEY (`id_giftvoucher`)
        ) DEFAULT CHARSET=utf8;';

        $req = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'prestagiftvoucher_cart_rule` (
            `id_order` INT NOT NULL,
            `id_cart_rule` INT NOT NULL
        ) DEFAULT CHARSET=utf8;';
        return (Db::getInstance()->Execute($install_sql) && Db::getInstance()->Execute($req));
    }

    /**
     * Install an admin tab
     *
     * @return boolean
     */
    public function installTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = $this->controller_name;
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'prestagiftvouchers';
        }
        unset($lang);
        $tab->id_parent = -1;
        $tab->module = $this->name;
        $result = $tab->add();
        return ($result);
    }

    /**
     * Prestashop uninstall method
     *
     * @see    ModuleCore::uninstall()
     * @return bool
     */
    public function uninstall()
    {
        if (!parent::uninstall()
            || !Configuration::deleteByName('MOD_PRESTAGIFTVOUCHERS_OBJ_en')
            || !Configuration::deleteByName('MOD_PRESTAGIFTVOUCHERS_OBJ_fr')
            || !Configuration::deleteByName('MOD_PRESTAGIFTVOUCHERS_OBJ_es')
            || !Configuration::deleteByName('MOD_PRESTAGIFTVOUCHERS_OBJ_de')
            || !Configuration::deleteByName('MOD_PRESTAGIFTVOUCHERS_OBJ_it')
            || !Configuration::deleteByName('PRESTAGIFTVOUCHERS_GROUP_RESTRICTION')
            || !Configuration::deleteByName('PRESTAGIFTVOUCHER_IS_CUMULABLE')
            || !Configuration::deleteByName('PRESTAGIFTVOUCHERS_ORDER_STATE_TRIGGER')
            || !$this->unregisterHook('updateOrderStatus')
            || !$this->uninstallDB()
            || !$this->uninstallTab()
        ) {
            return (false);
        }
        return (true);
    }

    /**
     * Uninstall module's DB requirements
     *
     * @return bool
     */
    public function uninstallDB()
    {
        $uninstall_sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'prestagiftvouchers`';
        return (Db::getInstance()->Execute($uninstall_sql));
    }

    /**
     * Uninstall Tab
     *
     * @return boolean
     */
    public function uninstallTab()
    {
        $id_tab = (int)Tab::getIdFromClassName('Adminprestagiftvoucher');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            if (Validate::isLoadedObject($tab)) {
                return ($tab->delete());
            } else {
                return (false);
            }
        } else {
            return (true);
        }
    }

    /**
     * Returns the order states available on the store
     *
     * @return Array
     */
    public function getOrderStates()
    {
        $query        = new DbQuery();
        $idLanguage   = Context::getContext()->language->id;
        $queryResults = array();

        $query->select('os.id_order_state, osl.name');
        $query->from('order_state', 'os');
        $query->innerJoin('order_state_lang', 'osl', 'os.id_order_state = osl.id_order_state
                        AND osl.id_lang = '.(int)$idLanguage);
        $query->orderBy('id_order_state');

        $queryResults = Db::getInstance()->executeS($query);
        if (!$queryResults || empty($queryResults)) {
            return array();
        }

        foreach ($queryResults as &$res) {
            if ($res['id_order_state'] == Configuration::get('PRESTAGIFTVOUCHERS_ORDER_STATE_TRIGGER')) {
                $res['current'] = true;
            } else {
                $res['current'] = false;
            }
        }

        return $queryResults;
    }

    /**
     * Entry point of the module
     *
     * @see    ModuleCore::getContent()
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function getContent()
    {
        // API FAQ Update
        include_once('classes/APIFAQClass.php');
        $api = new APIFAQ();
        $faq = $api->getData($this->module_key, $this->version);

        $context = Context::getContext();
        $this->processConfiguration();
        $this->loadAssets();
        $vouchers = Prestagiftvoucher::getGiftVouchers();

        $admin_token = '&token=' . Tools::getAdminTokenLite($this->controller_name);
        $controller_url = 'index.php?tab=' . $this->controller_name.$admin_token;

        $doc_lang = $this->getLangForDoc();
        $groupsAvailable = Group::getGroups($context->language->id, $context->shop->id);
        $groupsRestricted = Tools::jsonDecode(Configuration::get('PRESTAGIFTVOUCHERS_GROUP_RESTRICTION'));

        // Passing variables to the back-office template
        $this->context->smarty->assign(
            array(
                'orderStates'                 => $this->getOrderStates(),
                'is_cumulable'                => Configuration::get('PRESTAGIFTVOUCHER_IS_CUMULABLE'),
                'guide_link'                  => 'docs/doc_'.$this->name.'_'.$doc_lang.'.pdf',
                'module_version'              => $this->version,
                'controller_url'              => $controller_url,
                'id_lang'                     => $context->language->id,
                'controller_name'             => $this->controller_name,
                'tracking_url'                => '?utm_source=back-office&utm_medium=module&utm_campaign=
                                                 back-office-FR&utm_content='.$this->name,
                'module_display'              => $this->displayName,
                'multishop'                   => (int)Shop::isFeatureActive(),
                'currency'                    => $this->context->currency->getSign(),
                'vouchers'                    => $vouchers,
                'search_product_placeholder'  => $this->l('Search for a product...'),
                'search_category_placeholder' => $this->l('Search for a category...'),
                'msg_confirmation_delete'     => $this->l('Are you sure you want to delete this item?'),
                'groupsAvailable'             => $groupsAvailable,
                'groupsRestricted'            => $groupsRestricted,
                'apifaq'                      => $faq,
            )
        );
        return ($this->display(__FILE__, 'views/templates/hook/getContent.tpl'));
    }

    /**
     * Handles the form submission
     *
     * @return bool
     */
    public function processConfiguration()
    {
        $post_data = self::getAllValues();
        if (Tools::isSubmit('form_prestagiftvouchers')) {
            if ($this->formIsValid($post_data)) {
                if (!empty($post_data['id_giftvoucher'])) {
                    PrestaGiftVoucher::editGiftVoucher($post_data);
                } else {
                    Prestagiftvoucher::addGiftVoucher($post_data);
                }
                Tools::redirect($_SERVER['HTTP_REFERER']);
                $this->context->smarty->assign(array('confirmation' => 'ok'));
            } else {
                Tools::redirect($_SERVER['HTTP_REFERER']);
                $this->context->smarty->assign(array('error' => 'ok'));
            }
        } elseif (Tools::isSubmit('submitAdvancedParameters')) {
            PrestaGiftVoucher::manageGroupRestrictionOption($post_data);
            PrestaGiftVoucher::manageCumulableOption(Tools::getValue('is_cumulable'));
            Configuration::updateValue(
                'PRESTAGIFTVOUCHERS_ORDER_STATE_TRIGGER',
                Tools::getValue('order_state_trigger')
            );
            $this->smarty->assign(
                'confirm',
                $this->displayConfirmation($this->l('Your modifications have been saved'))
            );
        }
    }

    /**
     * Get all values from $_POST/$_GET
     *
     * @return mixed
     */
    public static function getAllValues()
    {
        return ($_POST + $_GET);
    }

    /**
     * Server-side form validation
     *
     * @param array $post_data $_POST
     *
     * @return bool
     */
    public function formIsValid($post_data)
    {
        if (Validate::isInt($post_data['condition_type'])
            && Validate::isBool($post_data['new_clients_only'])
            && Validate::isBool($post_data['unlimited'])
            && Validate::isInt($post_data['reduction_type'])
            && Validate::isFloat($post_data['reduction_value'])
            && Validate::isFloat($post_data['reduction_min_use'])
            && Validate::isBool($post_data['free_shipping'])
            && Validate::isInt($post_data['duration'])
        ) {
            return ($this->isValidAccordingToCondition($post_data));
        } else {
            return (false);
        }
    }

    /**
     * If we want to create a gift voucher based on a certain condition,
     * we need to check if $_POST contains the relevant data
     *
     * @param array $post_data $_POST
     *
     * @return bool
     */
    public function isValidAccordingToCondition($post_data)
    {
        switch ($post_data['condition_type']) {
            case 0:
                return (Validate::isFloat($post_data['amount_required']) &&
                Validate::isBool($post_data['shipping_included']));
            case 1:
                return (Validate::isInt($post_data['id_product']) &&
                Product::existsInDatabase($post_data['id_product'], 'product'));
            case 2:
                return (Validate::isInt($post_data['id_category']) &&
                Category::categoryExists($post_data['id_category']));
            default:
                return (false);
        }
    }

    /**
     * Load CSS/JS dependencies according to version
     *
     * @return void
     */
    public function loadAssets()
    {
        if (method_exists($this->context->controller, 'addJquery')) {
            $this->context->controller->addJquery('2.1.0', $this->js_path . '');
        }

        $css = array(
            $this->css_path . 'prestagiftvouchers.css',
            $this->css_path . 'select2-bootstrap.css',
            $this->css_path . 'faq.css'
        );

        $js = array(
            $this->js_path . 'prestagiftvouchers.js',
            $this->js_path . 'select2/select2.min.js',
            $this->js_path . 'jquery.validate.js',
            $this->js_path . 'jquery.ui.sortable.min.js',
            $this->js_path . 'faq.js'
        );

        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            $css_retrocompatibility = array(
                $this->css_path . 'jquery-ui.min.css',
                $this->css_path . 'bootstrap.min.css',
                $this->css_path . 'bootstrap.extend.css',
                $this->css_path . 'fontawesome.min.css',
            );
            $js_retrocompatibility = array(
                $this->js_path . 'bootstrap.min.js',
                $this->js_path . 'jquery-ui.min.js',
            );
            $css = array_merge($css, $css_retrocompatibility);
            $js = array_merge($js, $js_retrocompatibility);
        }

        $this->context->controller->addJS($js);
        $this->context->controller->addCSS($css);

        $this->addJsLocalizationPacks();
    }

    /**
     * If the doc is available in the current language, return the current
     * language ISO. If not, return 'EN'
     *
     * @see    self::getContent()
     * @return string
     */
    public function getLangForDoc()
    {
        $doc_langs_available = array('FR', 'ES', 'EN');
        foreach ($doc_langs_available as $lang) {
            if (Tools::strtoupper($this->context->language->iso_code) == $lang) {
                return ($lang);
            }
        }
        return ('EN');
    }

    /**
     * Load localization packs
     *
     * @return void
     */
    public function addJsLocalizationPacks()
    {
        $lang_iso = Context::getContext()->language->iso_code;
        $this->context->controller->addJS(
            _PS_JS_DIR_ . 'jquery/plugins/validate/localization/messages_' . $lang_iso . '.js'
        );
        $this->context->controller->addJS($this->_path . 'views/js/select2/i18n/select2_locale_' . $lang_iso . ".js");
    }

    /**
     * The magic happens when an order status is being updated
     *
     * @param array $params Hook parameters
     *
     * @return bool
     */
    public function hookUpdateOrderStatus($params)
    {
        $orderStateTrigger = Configuration::get('PRESTAGIFTVOUCHERS_ORDER_STATE_TRIGGER');

        if (!Validate::isLoadedObject($params['newOrderStatus'])) {
            return (false);
        }

        $order = new Order((int)$params['id_order']);
        if (!$order || !Validate::isLoadedObject($order)) {
            return (false);
        }

        // If the order is being refunded, search for cart rules generated by it and disable them
        if ($params['newOrderStatus']->template == "refund") {
            Prestagiftvoucher::cancelCartRulesAfterRefund($order);
        }

        // We don't do anything if the order isn't in the right status
        if ($params['newOrderStatus']->id != $orderStateTrigger) {
            return (false);
        }

        $giftvouchers = Prestagiftvoucher::getActiveGiftVouchers();
        foreach ($giftvouchers as $giftvoucher) {
            if (Prestagiftvoucher::isAllowedToBeGifted($order, $giftvoucher)
                && Prestagiftvoucher::doesOrderMatch($order, $giftvoucher)
            ) {
                $customer = new Customer($order->id_customer);
                if ($id = Prestagiftvoucher::createCartRule($order, $customer, $giftvoucher)) {
                    $cart_rule = new CartRule($id);
                }
                $order_currency = Currency::getCurrency($order->id_currency);
                $data = array(
                    '{firstname}'         => $customer->firstname,
                    '{lastname}'          => $customer->lastname,
                    '{code}'              => $cart_rule->code,
                    '{converted_value}'   => $giftvoucher['reduction_type'] == 0 && $order_currency['iso_code'] != $this->currency->iso_code ? '(' . Tools::displayPrice(Tools::convertPriceFull((float)$giftvoucher['reduction_value'], $this->context->currency, new Currency($order_currency['id_currency'])), $order_currency) . ')' : '',
                    '{value}'             => $giftvoucher['reduction_value'],
                    '{symbol}'            => $giftvoucher['reduction_type'] == 0 ? $this->currency->getSign() : '%',
                    '{min_use}'           => $giftvoucher['reduction_min_use'] == 0 ? '' : Tools::displayPrice($giftvoucher['reduction_min_use'], new Currency($order->id_currency)),
                    '{min_use_converted}' => $giftvoucher['reduction_min_use'] != 0 && $order_currency['iso_code'] != $this->context->currency->iso_code ? ' (' . Tools::displayPrice(Tools::convertPriceFull((float)$giftvoucher['reduction_min_use'], $this->context->currency, new Currency($order_currency['id_currency'])), Currency::getCurrency($order->id_currency)). ')' : '',
                    '{free_shipping}'     => $giftvoucher['free_shipping'],
                    '{duration}'          => $giftvoucher['duration']
                );
                $min_use = $giftvoucher['reduction_min_use'];
                self::sendTheGift($customer, $data, $order, $min_use);
                return (true);
            }
        }
    }

    /**
     * Make sure that there is a translation for the customer's language
     * If not, send in english.
     *
     * @param Customer $customer Customer instance
     * @param array    $data     Voucher rules
     * @param Order    $order    Order instance
     *
     * @return void
     */
    public function sendTheGift($customer, $data, $order, $min_use)
    {
        $langs_available = array_diff(Tools::scandir(dirname(__FILE__) . '/mails/', null), array('..', '.'));
        $lang_order = Language::getLanguage($order->id_lang);
        $lang_order_iso = $lang_order['iso_code'];
        $template = "discount-voucher-" . $this->chooseMailTemplate($data, $min_use);
        if (Configuration::get('MOD_PRESTAGIFTVOUCHERS_OBJ_' . $lang_order_iso)) {
            $object = Configuration::get('MOD_PRESTAGIFTVOUCHERS_OBJ_' . $lang_order_iso);
        } else {
            $object = Configuration::get('MOD_PRESTAGIFTVOUCHERS_OBJ_en');
        }


        foreach ($langs_available as $lang) {
            if ($lang == $lang_order_iso) {
                Mail::Send((int)$order->id_lang, $template, $object, $data, $customer->email, $customer->firstname . ' ' . $customer->lastname, Configuration::get('PS_SHOP_EMAIL'), Configuration::get('PS_SHOP_NAME'), null, null, dirname(__FILE__) . '/mails/');
                return;
            }
        }
        $id_lang_en = Language::getIdByIso('en');
        Mail::Send($id_lang_en, $template, $object, $data, $customer->email, $customer->firstname . ' ' . $customer->lastname, Configuration::get('PS_SHOP_EMAIL'), Configuration::get('PS_SHOP_NAME'), null, null, dirname(__FILE__) . '/mails/');
    }

    /**
     * Determines the mail's template number
     *
     * @param array $data Voucher rules
     *
     * @return int
     */
    public function chooseMailTemplate($data, $min_use)
    {
        if ($data['{value}'] == 0 && $data['{free_shipping}'] == true && $min_use == 0) {
            // Free shipping without minimum purchase
            return (1);
        } elseif ($data['{value}'] == 0 && $data['{free_shipping}'] == true && $min_use > 0) {
            // Free shipping with minimum purchase
            return (2);
        } elseif ($data['{value}'] > 0 && $data['{free_shipping}'] == false && $min_use == 0) {
            // Voucher without minimum purchase
            return (3);
        } elseif ($data['{value}'] > 0 && $data['{free_shipping}'] == false && $min_use > 0) {
            // Voucher with minimum purchase
            return (4);
        } elseif ($data['{value}'] > 0 && $data['{free_shipping}'] == true && $min_use == 0) {
            // Voucher without minimum purchase and free shipping
            return (5);
        } elseif ($data['{value}'] > 0 && $data['{free_shipping}'] == true && $min_use > 0) {
            // Voucher with free_shipping
            return (6);
        } else {
            Logger::AddLog('[discount_voucher_module] There is no template for this order scenario');
        }
    }
}
