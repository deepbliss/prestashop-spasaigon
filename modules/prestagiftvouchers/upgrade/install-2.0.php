<?php
/**
 * 2007-2016 PrestaShop
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2014 PrestaShop SA
 * @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
 * International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Remove old templates which are named "giftvoucher-thanks"
 * @param $dir
 *
 * @return bool
 */
function removeOldMailTemplates($dir)
{
    if (empty($dir)) {
        return (false);
    }
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        if (is_dir("$dir/$file")) {
            removeOldMailTemplates("$dir/$file");
        } elseif (strstr(basename("$dir/$file"), "-thanks")) {
            unlink("$dir/$file");
        } else {
            continue;
        }
    }
    return (true);
}

/**
 * Recursively remove a non-empty directory.
 * @param $dir
 *
 * @return bool
 */
function recursiveRmdir($dir)
{
    if (empty($dir)) {
        return (false);
    }
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        if (is_dir("$dir/$file") && !is_link($dir)) {
            recursiveRmdir("$dir/$file");
        } else {
            unlink("$dir/$file");
        }
    }
    return (rmdir($dir));
}

/**
 * Remove files on the server that are not needed anymore
 * @return bool
 */
function removeDeprecatedFiles()
{
    return (recursiveRmdir(dirname(dirname(__FILE__)) . "/backward_compatibility") &&
        unlink(dirname(dirname(__FILE__)) . "/ChequeCadeau.png") &&
        unlink(dirname(dirname(__FILE__)) . "/fr.php") &&
        unlink(dirname(dirname(__FILE__)) . "/logo.gif") &&
        unlink(dirname(dirname(__FILE__)) . "/prestagiftvouchers.png") &&
        unlink(dirname(dirname(__FILE__)) . "/prestagifvouchers.png") &&
        unlink(dirname(dirname(__FILE__)) . "/product.png") &&
        recursiveRmdir(dirname(dirname(__FILE__) . "/backward_compatibility")) &&
        removeOldMailTemplates(dirname(dirname(__FILE__)) . "/mails"));
}

/**
 * Will be called if there is an older version already installed on the shop
 * @param $object
 *
 * @return bool
 */
function upgrade_module_2_0($object)
{
    $db = Db::getInstance();
    $db->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'prestagiftvouchers`');
    $install_sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'prestagiftvouchers` (
            `id_giftvoucher` INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_shop` INT UNSIGNED NOT NULL DEFAULT 1,
            `new_clients_only` BOOLEAN NOT NULL DEFAULT TRUE,
            `condition_type` INT UNSIGNED NOT NULL DEFAULT 0,
            `shipping_included` BOOLEAN NOT NULL DEFAULT TRUE,
            `amount_required` FLOAT DEFAULT NULL,
            `id_product` INT UNSIGNED DEFAULT NULL,
            `id_category` INT UNSIGNED DEFAULT NULL,
            `reduction_type` INT UNSIGNED NOT NULL DEFAULT 0,
            `reduction_value` INT UNSIGNED NOT NULL,
            `reduction_min_use` INT UNSIGNED NOT NULL DEFAULT 0,
            `free_shipping` BOOLEAN NOT NULL DEFAULT FALSE,
            `duration` INT UNSIGNED NOT NULL DEFAULT 365,
            `priority` INT UNSIGNED NOT NULL,
            `active` BOOLEAN NOT NULL,
            PRIMARY KEY (`id_giftvoucher`)
        ) DEFAULT CHARSET=utf8;';

    $db->execute($install_sql);

    $tab = new Tab();
    $tab->active = 1;
    $tab->class_name = 'AdminPrestaGiftVoucher';
    $tab->name = array();
    foreach (Language::getLanguages(true) as $lang) {
        $tab->name[$lang['id_lang']] = 'prestagiftvouchers';
    }
    unset($lang);
    $tab->id_parent = -1;
    $tab->module = 'prestagiftvouchers';
    $result = $tab->add();

    Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_en', 'You\'ve got a gift!');
    Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_fr', 'Vous avez reçu un cadeau!');
    Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_de', 'Ein Geschenk wartet auf Sie!');
    Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_it', 'Ti aspetta un regalo!');
    Configuration::updateValue('MOD_PRESTAGIFTVOUCHERS_OBJ_es', '¡Tenemos un regalo para ti!');

    return (removeDeprecatedFiles() && $result && $object->unregisterHook('payment'));
}
