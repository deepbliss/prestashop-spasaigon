<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Upgrade method from 2.0.X
 *
 * @param Object $object Module instance
 *
 * @return bool
**/
function upgrade_module_2_1_0()
{
    $sql  = 'ALTER TABLE `' . _DB_PREFIX_ . 'prestagiftvouchers` ';
    $sql .= 'ADD (`name` VARCHAR(255) DEFAULT "gift", `unlimited` BOOLEAN DEFAULT 0)';
    Db::getInstance()->execute($sql);

    $req = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'prestagiftvoucher_cart_rule` (
        `id_order` INT NOT NULL,
        `id_cart_rule` INT NOT NULL
    ) DEFAULT CHARSET=utf8;';
    Db::getInstance()->execute($req);
    Configuration::updateValue('PRESTAGIFTVOUCHER_IS_CUMULABLE', 0);
    return (true);
}
