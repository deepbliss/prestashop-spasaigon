{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="bootstrap panel">
	<h3>{l s='Content types' mod='autotranslator'}</h3>
	<form method="post" action="" class="form-horizontal clearfix">
		<div class="at_ct col-lg-3">
			<select name="at_ct">
			{foreach $content_types as $val => $name}
				<option value="{$val|escape:'html':'UTF-8'}"{if $current_ct == $val} selected{/if}>{$name|escape:'html':'UTF-8'}</option>
			{/foreach}
			</select>
		</div>
		<div class="at_cat col-lg-2">
			{function renderOptions values = [] nesting_prefix = ''}
				{foreach $values as $id_category => $value}
					<option value="{$id_category|intval}"{if $id_category == $id_category_selected} selected{/if}>{$nesting_prefix|escape:'html':'UTF-8'}{$value.name|escape:'html':'UTF-8'}</option>
					{if !empty($structured_categories[$id_category])}
						{renderOptions values = $structured_categories[$id_category] nesting_prefix = $nesting_prefix|cat:'--'}
					{/if}
				{/foreach}
			{/function}
			<select name="id_category">
				<option value="0" class="first">{l s='Select category' mod='autotranslator'}</option>
				{if !empty($structured_categories[$id_root])}{renderOptions values = $structured_categories[$id_root]}{/if}
			</select>
		</div>
		<div class="at_lang col-lg-2">
			<select name="at_lang">
			{foreach $languages as $lang}
				{if $lang.id_lang}
					<option value="{$lang.id_lang|intval}"{if $current_lang_id == $lang.id_lang} selected{/if}>{$lang.name|escape:'html':'UTF-8'}</option>
				{/if}
			{/foreach}
			</select>
		</div>
		<div class="at_loc col-lg-2">
			<select name="at_loc">
			<option value="core">{l s='Core (no theme)' mod='autotranslator'}</option>
			{foreach $themes as $theme}
				<option value="{$theme|escape:'html':'UTF-8'}"{if $current_at_loc == $theme} selected{/if}>{$theme|escape:'html':'UTF-8'}</option>
			{/foreach}
			</select>
		</div>
		<input type="submit" value="{l s='Select' mod='autotranslator'}" class="btn btn-default">
		<div class="col-lg-12 additional-options">
			<label class="display-block"><input type="checkbox" name="overwrite_existing"{if !empty($overwrite_existing)} checked{/if}> {l s='Overwrite existing translations' mod='autotranslator'}</label>
		</div>
	</form>
</div>
