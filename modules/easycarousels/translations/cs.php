<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{easycarousels}prestashop>easycarousels_32871ea1cb70a11c23fe04e9e1b8f8a6'] = 'Snadné kolotoče';
$_MODULE['<{easycarousels}prestashop>easycarousels_2ff9f29db96838564c9d309d604f937c'] = 'Vytvořte si vlastní kolotoče v několika málo kliknutí';
$_MODULE['<{easycarousels}prestashop>easycarousels_91367c1e965d2b0d1028c5bcf62ca148'] = 'Kolotoče pro všechny stránky';
$_MODULE['<{easycarousels}prestashop>easycarousels_9ff0635f5737513b1a6f559ac2bff745'] = 'Nové produkty';
$_MODULE['<{easycarousels}prestashop>easycarousels_babba85d52b4221d3f44a6912d1b8e4f'] = 'Nejprodávanější';
$_MODULE['<{easycarousels}prestashop>easycarousels_ca7d973c26c57b69e0857e7a0332d545'] = 'Doporučené produkty';
$_MODULE['<{easycarousels}prestashop>easycarousels_588907ab2d492aca0b07b5bf9c931eea'] = 'Na prodej';
$_MODULE['<{easycarousels}prestashop>easycarousels_aed50620e59a6e60c59e38b53517c91f'] = 'Produkty výrobce';
$_MODULE['<{easycarousels}prestashop>easycarousels_851c8e30a474423da18ff2d8a5450303'] = 'Produkty dodavatele';
$_MODULE['<{easycarousels}prestashop>easycarousels_a16edefd971e6d5a532dd52043f2e992'] = 'Produkty z vybrané kategorie';
$_MODULE['<{easycarousels}prestashop>easycarousels_2377be3c2ad9b435ba277a73f0f1ca76'] = 'Výrobci';
$_MODULE['<{easycarousels}prestashop>easycarousels_1814d65a76028fdfbadab64a5a8076df'] = 'Dodavatelé';
$_MODULE['<{easycarousels}prestashop>easycarousels_bf9fa6548187158b6132eca915be9241'] = 'Kolotoče pro produktové stránce';
$_MODULE['<{easycarousels}prestashop>easycarousels_278c58c0f1d720a7b88944fbe47a8902'] = 'Další produkty ze stejné kategorie';
$_MODULE['<{easycarousels}prestashop>easycarousels_7df3c78b9fc0148bd7eacfca7bf8fd64'] = 'Další výrobky se stejným funkce';
$_MODULE['<{easycarousels}prestashop>easycarousels_7505e94c1dfd1f96d49e9afcddc1696c'] = 'Produkt příslušenství';
$_MODULE['<{easycarousels}prestashop>easycarousels_cbb81506a7fe3ef03f7a89c76c52131a'] = 'Stránkování';
$_MODULE['<{easycarousels}prestashop>easycarousels_aa7abc3b7e4323f9e1e9a2eb488bbcef'] = 'Navigační šipky';
$_MODULE['<{easycarousels}prestashop>easycarousels_f3219a6326279e143641ac2d81adc6c8'] = 'Umožní autoplay';
$_MODULE['<{easycarousels}prestashop>easycarousels_89d7b10cb4238977d2b523dfd9ea7745'] = 'Smyčka';
$_MODULE['<{easycarousels}prestashop>easycarousels_dee7cae0a61d46454861d5fc218842ba'] = 'Rychlost animace (ms)';
$_MODULE['<{easycarousels}prestashop>easycarousels_5b0c09424b0a5c8bcf3da06216a174a8'] = 'Snímky tah';
$_MODULE['<{easycarousels}prestashop>easycarousels_b233070537062759877a03e3678b704c'] = 'Počet snímků pohyboval na přechodu. Nastavení 0 přesunout všechny viditelné snímky';
$_MODULE['<{easycarousels}prestashop>easycarousels_b32e1c711760694b93c5bc4a4625d663'] = 'Celkem položek';
$_MODULE['<{easycarousels}prestashop>easycarousels_db63fe222e38f94b3536962fb529cf61'] = 'Viditelné řádky';
$_MODULE['<{easycarousels}prestashop>easycarousels_646160d6de0c9ae4f0cacdfe81ac85a3'] = 'Můžete střídat několik řádků najednou';
$_MODULE['<{easycarousels}prestashop>easycarousels_3eab5d12656f2f4462f6594019e77355'] = 'Viditelné sloupce';
$_MODULE['<{easycarousels}prestashop>easycarousels_2e6732be22b99c669d901fef63bf4e55'] = 'Za to, jak \"viditelné položky\", pokud máte jen jeden řádek';
$_MODULE['<{easycarousels}prestashop>easycarousels_39d97eb6fdd16f4d0fa6c7b8c2e4fb57'] = 'Min slide šířka (px)';
$_MODULE['<{easycarousels}prestashop>easycarousels_254e8b1c0fa2e2477ef364c8f32af9f3'] = 'Pokud dynamický snímek šířka stává méně, že tato hodnota, pak počet viditelných snímků bude být snížil.';
$_MODULE['<{easycarousels}prestashop>easycarousels_375e0e331b507d50d75ed594c66f472b'] = 'Viditelné sloupce na displeji &lt; 1200px';
$_MODULE['<{easycarousels}prestashop>easycarousels_50a29c2e3c776bb43542bd1a24cc3e2b'] = 'Pokud je zobrazení šířka je menší než 1200px, tento počet položek, které budou viditelné.';
$_MODULE['<{easycarousels}prestashop>easycarousels_0c360640497d0cbf7dea1a620d0b8ac8'] = 'Viditelné sloupce na displeji &lt; 992px';
$_MODULE['<{easycarousels}prestashop>easycarousels_2384c1c4f26e307f14eb35370d6a1409'] = 'Viditelné sloupce na displeji &lt; 768px';
$_MODULE['<{easycarousels}prestashop>easycarousels_303df147513a5cc3d6fa356f367dba6b'] = 'Viditelné sloupce na displeji &lt; 480px';
$_MODULE['<{easycarousels}prestashop>easycarousels_3bf35d21fe54598e7e729fc1914a89c4'] = 'Funkce filtru';
$_MODULE['<{easycarousels}prestashop>easycarousels_4f4dc73ef2eddef0431667450e4dc8ca'] = 'Kategorie ids';
$_MODULE['<{easycarousels}prestashop>easycarousels_d0bd55fcead85262f5fe440bd654f73b'] = 'Zadejte kategorie ids, oddělených čárkou (1,2,3 ...)';
$_MODULE['<{easycarousels}prestashop>easycarousels_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Výrobce';
$_MODULE['<{easycarousels}prestashop>easycarousels_ec136b444eede3bc85639fac0dd06229'] = 'Dodavatel';
$_MODULE['<{easycarousels}prestashop>easycarousels_83429a930ec621178350c42d9d220aaa'] = 'Náhodné objednání';
$_MODULE['<{easycarousels}prestashop>easycarousels_ea548e8b5df8a1de46ae12e9e28963cd'] = 'Zvažte kategorie';
$_MODULE['<{easycarousels}prestashop>easycarousels_f1bfec9c84e7fb16814df65e045ba7be'] = 'Zobrazit produkty pouze z aktuální kategorii, pokud kolotoč se zobrazí na stránce kategorie';
$_MODULE['<{easycarousels}prestashop>easycarousels_95ff0dbd3f00cfcd9ee25dadae9911c0'] = 'Vlastní třídy';
$_MODULE['<{easycarousels}prestashop>easycarousels_8630a2ad30255f2c9aef32cea462bec8'] = 'Vlastní třídu, která bude přidána do kolotoč kontejneru';
$_MODULE['<{easycarousels}prestashop>easycarousels_2dd1148046499f9c2422ce1045a4accf'] = 'Image type (typ obrazu';
$_MODULE['<{easycarousels}prestashop>easycarousels_6bca13036cf0d76bef80c5f722e54771'] = 'Titul v jednom řádku';
$_MODULE['<{easycarousels}prestashop>easycarousels_6c4e205eade6e29daaba33be35820001'] = 'Zkrátit titulu, pokud jeho délka překrývá první linii';
$_MODULE['<{easycarousels}prestashop>easycarousels_ad26a9565e1f5dda1a4b1dfaf1fec6ef'] = 'Délka názvu (symboly)';
$_MODULE['<{easycarousels}prestashop>easycarousels_a8e17c5997f72e100c0851915f72f234'] = 'Nastavení 0, pokud nechcete zobrazit název';
$_MODULE['<{easycarousels}prestashop>easycarousels_08f69c5cc401288d657e7a924bb722bc'] = 'Popis délka';
$_MODULE['<{easycarousels}prestashop>easycarousels_3625cdf19abd4ec330b3db80a9d03c50'] = 'Kategorie produktu';
$_MODULE['<{easycarousels}prestashop>easycarousels_bfd9531106654d6e0f5cce1e57104b6e'] = 'Výrobce produktu';
$_MODULE['<{easycarousels}prestashop>easycarousels_3601146c4e948c32b6424d2c0a7f0118'] = 'Cena';
$_MODULE['<{easycarousels}prestashop>easycarousels_4777b0079dc485209cabce4a34d9275e'] = 'Přidat do košíku tlačítka';
$_MODULE['<{easycarousels}prestashop>easycarousels_b24344b6a2c4a94c9b0d0ebd4602aeca'] = 'Zobrazit více';
$_MODULE['<{easycarousels}prestashop>easycarousels_c91e4ee170226d66e90f99ba917e4c20'] = 'Přehled';
$_MODULE['<{easycarousels}prestashop>easycarousels_ea11632f28670540cf12d2ffc3c2ba9d'] = 'Sklad dat';
$_MODULE['<{easycarousels}prestashop>easycarousels_03ca6f76b331d062c0e45ccad57180cd'] = 'Samolepky';
$_MODULE['<{easycarousels}prestashop>easycarousels_a9b3e10480bddec685149d8ec4bbda16'] = 'Odkaz na všechny položky';
$_MODULE['<{easycarousels}prestashop>easycarousels_58ef46de58adc47c156e31af4bcb5aa8'] = 'Dodací lhůta hák';
$_MODULE['<{easycarousels}prestashop>easycarousels_ba484178d46738caeb45aa00bbc6fb86'] = 'Všechny údaje závislý na displayProductDeliveryTime';
$_MODULE['<{easycarousels}prestashop>easycarousels_ec9039e0d6dd9f8df8af3ed881e210c3'] = 'Cena blok hák';
$_MODULE['<{easycarousels}prestashop>easycarousels_595b4b087251fcbd32c79ecc6d8d0349'] = 'Všechny údaje závislý na displayProductPriceBlock';
$_MODULE['<{easycarousels}prestashop>easycarousels_ac6a5b2000c6465aed88ac3041ab261e'] = 'Recenze hák';
$_MODULE['<{easycarousels}prestashop>easycarousels_5f95e6101e17ad76bbda26dd59212237'] = 'Všechny údaje závislý na displayProductListReviews';
$_MODULE['<{easycarousels}prestashop>easycarousels_ee3d12e7bd9387bc685c110a1ff2b984'] = 'Produkt miniatury';
$_MODULE['<{easycarousels}prestashop>easycarousels_d2d72c90bb88220a1e17c2eae0b8c4db'] = 'Databázové tabulky nebyl nainstalován správně';
$_MODULE['<{easycarousels}prestashop>easycarousels_d7c8c85bf79bbe1b7188497c32c3b0ca'] = 'Nepodařilo';
$_MODULE['<{easycarousels}prestashop>easycarousels_248336101b461380a4b2391a7625493d'] = 'Uložit';
$_MODULE['<{easycarousels}prestashop>easycarousels_729a51874fe901b092899e9e8b31c97a'] = 'Jste si jisti?';
$_MODULE['<{easycarousels}prestashop>easycarousels_1031c68cf7898404584e8a7f4ef2b1af'] = 'Jak používat dovozce';
$_MODULE['<{easycarousels}prestashop>easycarousels_786862a902f7e77bdfa1203980ee65c5'] = 'Soubor nebyl nahrán';
$_MODULE['<{easycarousels}prestashop>easycarousels_ab3d03c70df263cad38a3bde08397a0e'] = 'Tento soubor nelze použít pro import. Důvod: Databázové tabulky se neshodují (%s).';
$_MODULE['<{easycarousels}prestashop>easycarousels_49c5c413d095a0fd0a34fe3d808f91ea'] = 'Nic import';
$_MODULE['<{easycarousels}prestashop>easycarousels_cdd8da9a1cd74d9d932d184c92924387'] = 'Data byla úspěšně importovány';
$_MODULE['<{easycarousels}prestashop>easycarousels_88a68108a272f2a1db61755b65c5faae'] = 'Vyskytla se chyba při importu dat';
$_MODULE['<{easycarousels}prestashop>easycarousels_216adaf4e98dc62ec3abeab51b9fc57f'] = 'Vyberte prosím alespoň jeden produkt';
$_MODULE['<{easycarousels}prestashop>easycarousels_90723749917a82bf3ff8a73247d32ffb'] = 'Nelze přidat více než %d produkt(y) produktu srovnání';
$_MODULE['<{easycarousels}prestashop>easycarousels_10bbe116f8dc86085035eed028ecff55'] = 'Tento typ nastavení není podporováno';
$_MODULE['<{easycarousels}prestashop>easycarousels_7646713729e6ae6c9ba844f2eae1f334'] = 'Hlavní stránky';
$_MODULE['<{easycarousels}prestashop>easycarousels_e334486a68366ba780f5140cfe8668ac'] = 'Modul stránky';
$_MODULE['<{easycarousels}prestashop>easycarousels_323d37371f4741043543e5bc49f4f710'] = 'Prosím, aby výběr';
$_MODULE['<{easycarousels}prestashop>easycarousels_abaf8f95fbd02bd1f97e2b8e7253c9b5'] = 'Vyplňte, prosím, kolotoč jméno alespoň pro následující jazyky: ';
$_MODULE['<{easycarousels}prestashop>easycarousels_5f27b4088038edb25ba848ae9b93433f'] = 'Prosím, přidejte alespoň jednu kategorii id';
$_MODULE['<{easycarousels}prestashop>easycarousels_240213c123e4ef2ba0a5283068e26605'] = 'Vyberte prosím výrobce';
$_MODULE['<{easycarousels}prestashop>easycarousels_3adaf6c593f1ea64d6468e4fd2fc003e'] = 'Vyberte si, prosím, dodavatel';
$_MODULE['<{easycarousels}prestashop>easycarousels_63cbe187a04193b85d7350f2df0a53b1'] = 'Prosím zvolte funkci';
$_MODULE['<{easycarousels}prestashop>easycarousels_0a12939793473f298aeda137fbb08f37'] = 'Kolotoč není uložen';
$_MODULE['<{easycarousels}prestashop>easycarousels_96d42fccb9e338294a01b5793c06fcd4'] = 'Parametry, které nejsou poskytovány správně';
$_MODULE['<{easycarousels}prestashop>easycarousels_76f4eb0a24b07260fca07c260323529a'] = 'Objednání nepodařilo';
$_MODULE['<{easycarousels}prestashop>carousel-form_9ffa4765d2da990741800bbe1ad4e7f8'] = 'Hromadné akce';
$_MODULE['<{easycarousels}prestashop>carousel-form_423e3b8a42924be08fd940da1570a8ed'] = 'Kolotoč %d';
$_MODULE['<{easycarousels}prestashop>carousel-form_1793e8dbd0fc37015589e85d3805e366'] = 'V kartách';
$_MODULE['<{easycarousels}prestashop>carousel-form_320b39251600fbebe992446192156507'] = 'Aktivovat/Deaktivovat';
$_MODULE['<{easycarousels}prestashop>carousel-form_7dce122004969d56ae2e0245cb754d35'] = 'Edit';
$_MODULE['<{easycarousels}prestashop>carousel-form_739fe21361b62c493605f8a740f361d7'] = 'Přejděte Nahoru';
$_MODULE['<{easycarousels}prestashop>carousel-form_ea4788705e6873b424c65e91c2846b19'] = 'Zrušit';
$_MODULE['<{easycarousels}prestashop>carousel-form_f2a6c498fb90ee345d997f888fce3b18'] = 'Odstranit';
$_MODULE['<{easycarousels}prestashop>carousel-form_cf256645026c89884fd0b89c899614fe'] = 'Kolotoč typ';
$_MODULE['<{easycarousels}prestashop>carousel-form_7ed17bca2875204cfc569d8ebe5887f4'] = 'Můžete nechat toto pole prázdné pro kolotoče, které nejsou v kartách';
$_MODULE['<{easycarousels}prestashop>carousel-form_6e0b2c71de45f5edd974fd6492e73318'] = 'Kolotoč hlavy';
$_MODULE['<{easycarousels}prestashop>carousel-form_93cba07454f06a4a960172bbd6e2a435'] = 'Ano';
$_MODULE['<{easycarousels}prestashop>carousel-form_498f79c4c5bbde77f1bceb6c86fd0f6d'] = 'Ukázat';
$_MODULE['<{easycarousels}prestashop>carousel-form_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Č.';
$_MODULE['<{easycarousels}prestashop>carousel-form_62a5e490880a92eef74f167d9dc6dca0'] = 'Skrýt';
$_MODULE['<{easycarousels}prestashop>carousel-form_02c66783006879c4c682df4b1fcbe10b'] = 'Nastavení šablony';
$_MODULE['<{easycarousels}prestashop>carousel-form_af3bff2510b21c30f8cd01945c84da9f'] = 'Kolotoč nastavení';
$_MODULE['<{easycarousels}prestashop>carousel-form_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
$_MODULE['<{easycarousels}prestashop>configure_e1b00c2e9cbd41470804e255b1ca827c'] = 'POZNÁMKA: Všechny změny budou vztahovat na více než jeden obchod';
$_MODULE['<{easycarousels}prestashop>configure_aa56a03e86e0970c3d778c36c4d1bb4d'] = 'Kolotoče';
$_MODULE['<{easycarousels}prestashop>configure_5a5ee220c12193cbe869af4fc6faaa09'] = 'Vyberte hák';
$_MODULE['<{easycarousels}prestashop>configure_61c032d0dffc3fcd3196e4bc036677f0'] = 'Háček nastavení';
$_MODULE['<{easycarousels}prestashop>configure_ebf78b512222fe4dcd14e7d5060a15b0'] = 'zobrazení';
$_MODULE['<{easycarousels}prestashop>configure_2d4ea419afe77dcf9a083e17854e0226'] = 'stránku výjimky';
$_MODULE['<{easycarousels}prestashop>configure_4c2dae2e28ba0156a7c74690928a35c2'] = 'modul pozice';
$_MODULE['<{easycarousels}prestashop>configure_3e1cd8c322f52423c04f6acca24e6afc'] = 'Pro zobrazení této háček, vložte následující kód na každou tpl';
$_MODULE['<{easycarousels}prestashop>configure_670a1670b6f117f366900ac7634c277b'] = 'Zde můžete zobrazit produktu, příslušenství nebo jiných výrobků, se stejnou kategorií/funkce';
$_MODULE['<{easycarousels}prestashop>configure_9ffa4765d2da990741800bbe1ad4e7f8'] = 'Hromadné akce';
$_MODULE['<{easycarousels}prestashop>configure_4c41e0bd957698b58100a5c687d757d9'] = 'Vyberte všechny';
$_MODULE['<{easycarousels}prestashop>configure_237c7b6874386141a095e321c9fdfd38'] = 'Odznačit vše';
$_MODULE['<{easycarousels}prestashop>configure_2faec1f9f8cc7f8f40d521c4dd574f49'] = 'Povolit';
$_MODULE['<{easycarousels}prestashop>configure_bcfaccebf745acfd5e75351095a5394a'] = 'Zakázat';
$_MODULE['<{easycarousels}prestashop>configure_6fed20a6f04a9eff5d4a704b45af19a6'] = 'Skupina v kartách';
$_MODULE['<{easycarousels}prestashop>configure_3de527cc41c9c96e79b2132bc366b1c1'] = 'Oddělit';
$_MODULE['<{easycarousels}prestashop>configure_f2a6c498fb90ee345d997f888fce3b18'] = 'Odstranit';
$_MODULE['<{easycarousels}prestashop>configure_4dc1a4b64bcfa2bcb3868976ef6ad613'] = 'Přidat nový kolotoč';
$_MODULE['<{easycarousels}prestashop>configure_15ffab04714cffc08a5e91a1fb62e4ee'] = 'Dovozce';
$_MODULE['<{easycarousels}prestashop>configure_02632e557b7687e24d1662520c05b4ce'] = 'O dovozce';
$_MODULE['<{easycarousels}prestashop>configure_f890cfc36b02a77cd696ade2bf0b4f2f'] = 'Export kolotoče';
$_MODULE['<{easycarousels}prestashop>configure_0048a3cd545d662b18c6775dd8229008'] = 'Import kolotoče';
$_MODULE['<{easycarousels}prestashop>hook-display-form_793929ec614d5ec29816eb3b5f5b8477'] = 'Zobrazení nastavení pro %s';
$_MODULE['<{easycarousels}prestashop>hook-display-form_62a5e490880a92eef74f167d9dc6dca0'] = 'Skrýt';
$_MODULE['<{easycarousels}prestashop>hook-display-form_6a8fbf96063371a1461c15099becfed4'] = 'Vlastní třídy, které budou použity, aby nádoba všechny kolotoče v tento háček';
$_MODULE['<{easycarousels}prestashop>hook-display-form_c45c3c8328818253d3d3526fbb320090'] = 'Kontejner třídy';
$_MODULE['<{easycarousels}prestashop>hook-display-form_c4a8346a8107cdb2a34aa46a5ada247e'] = 'Pokud kartu jména přesah kontejneru, budou dynamicky mění kompaktní rozbalovací seznam';
$_MODULE['<{easycarousels}prestashop>hook-display-form_42bd25485cf9f7621c8e8957dd4efd78'] = 'Kompaktní karty';
$_MODULE['<{easycarousels}prestashop>hook-display-form_93cba07454f06a4a960172bbd6e2a435'] = 'Ano';
$_MODULE['<{easycarousels}prestashop>hook-display-form_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Č.';
$_MODULE['<{easycarousels}prestashop>hook-display-form_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
$_MODULE['<{easycarousels}prestashop>hook-exceptions-form_cfaa81441f80273faab1c1e94c0317d5'] = 'Stránku výjimky pro %s';
$_MODULE['<{easycarousels}prestashop>hook-exceptions-form_62a5e490880a92eef74f167d9dc6dca0'] = 'Skrýt';
$_MODULE['<{easycarousels}prestashop>hook-exceptions-form_fea535d691590510c151359f75f85fc3'] = 'Zkontrolujte, zda stránky, kde se tento háček by neměl být zobrazeny';
$_MODULE['<{easycarousels}prestashop>hook-exceptions-form_b7d7436a18bf546dd6438d92218868c1'] = 'Zkontrolujte, zda všechny';
$_MODULE['<{easycarousels}prestashop>hook-exceptions-form_228f41f001607dfbaea2724510ed9f98'] = 'Unheck všechny';
$_MODULE['<{easycarousels}prestashop>hook-exceptions-form_a72d672334d4ad8f7a658a1919ab98ff'] = 'Invertovat výběr';
$_MODULE['<{easycarousels}prestashop>hook-exceptions-form_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_c4c5f1721a89671c04c0d6c29ad5f344'] = 'Modul pozic pro %s';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_62a5e490880a92eef74f167d9dc6dca0'] = 'Skrýt';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_fb1e08f1a7c910a432e0f904d86480e8'] = 'Přetáhněte modulů v seznamu níže a měnit jejich pořadí';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_06df33001c1d7187fdd81ea1f5b277aa'] = 'Akce';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_d97628f4b34d5ca2a77cc1bfa7914aa9'] = 'Vyjmutí';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_bcfaccebf745acfd5e75351095a5394a'] = 'Zakázat';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_a27dfe771799a09fd55fea73286eb6ab'] = 'Odinstalovat';
$_MODULE['<{easycarousels}prestashop>hook-positions-form_2faec1f9f8cc7f8f40d521c4dd574f49'] = 'Povolit';
$_MODULE['<{easycarousels}prestashop>carousel_c70ad5f80e4c6f299013e08cabc980df'] = 'Více o %s';
$_MODULE['<{easycarousels}prestashop>carousel_c91e4ee170226d66e90f99ba917e4c20'] = 'Přehled';
$_MODULE['<{easycarousels}prestashop>carousel_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nové';
$_MODULE['<{easycarousels}prestashop>carousel_bb63f16d5ebfcfa8a651642a7bb2ea5c'] = 'Prodej!';
$_MODULE['<{easycarousels}prestashop>carousel_2d0f6b8300be19cf35e89e66f0677f95'] = 'Přidat do košíku';
$_MODULE['<{easycarousels}prestashop>carousel_4351cfebe4b61d8aa5efa1d020710005'] = 'Pohled';
$_MODULE['<{easycarousels}prestashop>carousel_63a78ed4647f7c63c2929e35ec1c95e3'] = 'Přizpůsobit';
$_MODULE['<{easycarousels}prestashop>carousel_d3da97e2d9aee5c8fbe03156ad051c99'] = 'Více';
$_MODULE['<{easycarousels}prestashop>carousel_69d08bd5f8cf4e228930935c3f13e42f'] = 'Skladem';
$_MODULE['<{easycarousels}prestashop>carousel_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'Skladem';
$_MODULE['<{easycarousels}prestashop>carousel_cb3c718c905f00adbb6735f55bfb38ef'] = 'Produkt je k dispozici s různými možnostmi';
$_MODULE['<{easycarousels}prestashop>carousel_0b4db271fc4624853e634ef6882ea8be'] = 'Zobrazit všechny';
