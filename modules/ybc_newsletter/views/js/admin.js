/**
 * 2007-2018 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <etssoft.jsc@gmail.com>
 *  @copyright  2007-2018 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */
$(document).ready(function(){
    //alert(ybc_newsletter_module_path);
    if($('select[name="YBC_NEWSLETTER_PAGE[]"] option[value="all"]').is(':selected'))
        $('select[name="YBC_NEWSLETTER_PAGE[]"] option').prop('selected',true);
    $('select[name="YBC_NEWSLETTER_PAGE[]"] option').click(function(){
        if($(this).val()=='all' && !$('select[name="YBC_NEWSLETTER_PAGE[]"][value="all"]').is(':selected'))
            $('select[name="YBC_NEWSLETTER_PAGE[]"] option').prop('selected',true);
    });
    if($('select[name="YBC_NEWSLETTER_TEMPLATE"]').val()=='ynpt1'||$('select[name="YBC_NEWSLETTER_TEMPLATE"]').val()=='ynpt3'||$('select[name="YBC_NEWSLETTER_TEMPLATE"]').val()=='ynpt4'||$('select[name="YBC_NEWSLETTER_TEMPLATE"]').val()=='ynpt5')
    {
        $('.form-group.logo_newsletter').hide();
    }
    $('select[name="YBC_NEWSLETTER_TEMPLATE"]').change(function(){
        if(confirm('Do you want to load new template?'))
        {
            window.location = $('#module_form').attr('action')+'&loadteamplate='+$(this).val();
        }
        else
            return false;
    });
    $('.ybc_newsletter_form_tab .ybc_newsletter_'+current_tab_active).addClass('active'); 
    $('.ybc_newsletter_form .ybc_newsletter_form_'+current_tab_active).addClass('active');
    $(document).on('click','.ybc_newsletter_form_tab > li',function(){
        if(!$(this).hasClass('active'))
        {
            $('.ybc_newsletter_form > div, .ybc_newsletter_form_tab > li').removeClass('active');
            $(this).addClass('active');
            $('.ybc_newsletter_form > div.ybc_newsletter_form_'+$(this).attr('data-tab')).addClass('active');
            $('#ETS_TAB_CURENT_ACTIVE').val($(this).attr('data-tab'));
        }        
    });
      
    function GetFilename(url)
    {
       if (url){
          var m = url.toString().match(/.*\/(.+?)\./);
          if (m && m.length > 1){
             return m[1];
          }
       }
       return "";
    }
    $("area[rel^='prettyPhoto']").prettyPhoto();    
    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: false});
    $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});    
    $('.ybc-templates').click(function(){ 
        var loadtemp = $('#YBC_NEWSLETTER_TEMPLATE option:selected').val();
        $('.gallery li a img[data-select='+loadtemp+']').click();
        setTimeout(function(){
          $('.pp_close').before('<span class="bt_select">Select this template</span>');  
            
        },1000);        
        $(document).on('click','.bt_select',function(){
            var template = GetFilename($(this).closest('.pp_content').find('#fullResImage').attr('src'));
            $('select[name="YBC_NEWSLETTER_TEMPLATE"]').val(template);
            $('select[name="YBC_NEWSLETTER_TEMPLATE"]').trigger('change');
        }); 
        
        
    }); 
    
});
