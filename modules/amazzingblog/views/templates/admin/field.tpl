{*
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*
*}

{$type = 'text'}{if !empty($field.type)}{$type = $field.type}{/if}
{$name = $k}{if !empty($field.input_name)}{$name = $field.input_name}{/if}
{if $k == 'restrictions'}
	{$restrictions = $field.value}
	<select name="settings[restrictions][type]" class="col-lg-3">
		{foreach $field.options as $k => $opt}
			<option value="{$k|escape:'html':'UTF-8'}"{if !empty($restrictions.type) && $restrictions.type == $k} selected{/if}>{$opt|escape:'html':'UTF-8'}</option>
		{/foreach}
	</select>
	<div class="input-group col-lg-9 restriction-ids">
		<span class="input-group-addon">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='For example: 11, 15, 18' mod='amazzingblog'}">
				<i class="icon-info-circle"></i>
			</span>
			{l s='IDs' mod='amazzingblog'}
		</span>
		<input type="text" name="settings[restrictions][ids]" value="{if !empty($restrictions.ids)}{$restrictions.ids|escape:'html':'UTF-8'}{/if}">
	</div>
{else if !empty($field.options)}
	<select class="{$k|escape:'html':'UTF-8'}{if !empty($field.input_class)} {$field.input_class|escape:'html':'UTF-8'}{/if}" name="{$name|escape:'html':'UTF-8'}">
		{foreach $field.options as $i => $opt}
			<option value="{$i|escape:'html':'UTF-8'}"{if $field.value == $i} selected{/if}>{$opt|escape:'html':'UTF-8'}</option>
		{/foreach}
	</select>
{else if $type == 'checkbox'}
	<div class="checkboxes-list">
	{foreach $field.boxes as $i => $label}
		<label class="control-label"><input type="checkbox" name="{$name|escape:'html':'UTF-8'}" value="{$i|escape:'html':'UTF-8'}"{if in_array($i, $field.value)} checked{/if}> {$label|escape:'html':'UTF-8'}</label>
	{/foreach}
	</div>
{else if $type == 'switcher'}
	<span class="switch prestashop-switch">
		<input type="radio" id="{$k|escape:'html':'UTF-8'}" name="{$name|escape:'html':'UTF-8'}" value="1"{if !empty($field.value)} checked{/if} >
		<label for="{$k|escape:'html':'UTF-8'}">{l s='Yes' mod='amazzingblog'}</label>
		<input type="radio" id="{$k|escape:'html':'UTF-8'}_0" name="{$name|escape:'html':'UTF-8'}" value="0"{if empty($field.value)} checked{/if} >
		<label for="{$k|escape:'html':'UTF-8'}_0">{l s='No' mod='amazzingblog'}</label>
		<a class="slide-button btn"></a>
	</span>
{else}
	<input type="text" name="{$name|escape:'html':'UTF-8'}" value="{$field.value|escape:'html':'UTF-8'}" class="{$k|escape:'html':'UTF-8'}{if !empty($field.input_class)} {$field.input_class|escape:'html':'UTF-8'}{/if}{if $type == 'datepicker'} datepicker{/if}"{if !empty($field.readonly)} readonly{/if}>
{/if}
{* since 1.2.0 *}
