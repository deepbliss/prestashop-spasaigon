{*
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*
*}

<div class="item clearfix {$block.hook_name|escape:'html':'UTF-8'}" data-id="{$block.id_block|intval}">
<form method="post" action="" class="form-horizontal">
	<input type="hidden" name="id_block" value="{$block.id_block|intval}">
	<input type="hidden" name="active" value="{$block.active|intval}">
	<input type="hidden" name="hook_name" value="{$block.hook_name|escape:'html':'UTF-8'}">
	{include file='./item-header.tpl' item=$block type='block'}
	{if !empty($full)}
	<div class="details" style="display:none;">
		{foreach $block.editable_fields as $k => $field}
		<div class="form-group {if !empty($field.class)}{$field.class|escape:'html':'UTF-8'}{/if}">
			<label class="control-label col-lg-3">
    			<span{if !empty($field.tooltip)} class="label-tooltip" data-toggle="tooltip" title="{$field.tooltip|escape:'html':'UTF-8'}"{/if}>
    				{$field.display_name} {* can not be escaped, because of html entities like lt *}
				</span>
            </label>
			<div class="col-lg-5">
			{if empty($field.multilang)}
				{include file="./field.tpl" field=$field k=$k}
			{else}
				{include file="./multilang-field.tpl" field=$field k=$k}
			{/if}
			</div>
		</div>
		{/foreach}
		<div class="col-lg-offset-3 item-footer">
			<button class="btn btn-default save"><i class="icon-save"></i> {l s='Save block' mod='amazzingblog'}</button>
			<button class="btn btn-default save dont-scroll-up"><i class="icon-save"></i> {l s='Save and stay' mod='amazzingblog'}</button>
		</div>
	</div>
	{/if}
</form>
</div>
{* since 1.2.0 *}
