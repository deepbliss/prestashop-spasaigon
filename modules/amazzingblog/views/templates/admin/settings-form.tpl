{*
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*
*}

<form method="post" action="" class="form-horizontal">
	{foreach $settings as $k => $field}
	{if !empty($field.h)}
		<h4 class="subtitle col-lg-12">{$field.h|escape:'html':'UTF-8'}</h4>
	{/if}
	<div class="form-group col-lg-12{if !empty($field.class)} {$field.class|escape:'html':'UTF-8'}{/if}">
		<label class="control-label col-lg-3">
			<span{if !empty($field.tooltip)} class="label-tooltip" data-toggle="tooltip" title="{$field.tooltip|escape:'html':'UTF-8'}"{/if}>
				{$field.display_name|escape:'html':'UTF-8'}
			</span>
		</label>
		<div class="col-lg-3">
		{if empty($field.multilang)}
			{include file="./field.tpl" field=$field k=$k}
		{else}
			{include file="./multilang-field.tpl" field=$field k=$k}
		{/if}
		{if !empty($field.help_box)}
			<div class="help-box">
				{if (!is_array($field.help_box))}{$field.help_box = [$field.help_box]}{/if}
				{foreach $field.help_box as $k => $line}
					{if $k}<br>{/if}{$line|escape:'html':'UTF-8'}.
				{/foreach}
			</div>
		{/if}
		</div>
		{if !empty($field.regenerate)}
			<div class="col-lg-1">
				<div class="progress hidden">
				  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0"
				  aria-valuemin="0" aria-valuemax="100" style="width:0%">
				  </div>
				</div>
				<a href="#" class="regenerate-thumbnails" data-type="{$k|escape:'html':'UTF-8'}">{l s='Regenerate' mod='amazzingblog'}</a>
			</div>
		{/if}
	</div>
	{/foreach}
	<div class="col-lg-12">
		<button class="btn btn-default saveSettings" data-type="{$type|escape:'html':'UTF-8'}">
			<i class="process-icon-save icon-save"></i>
			{l s='Save' mod='amazzingblog'}
		</button>
	</div>
</form>
