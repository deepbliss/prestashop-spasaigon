{*
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*
*}

{if $posts}
	<div class="post-list{if !empty($settings.display_type)} {$settings.display_type|escape:'html':'UTF-8'}{/if}">
	{foreach $posts as $k => $post}
	{$link = $blog->getPostLink($post.id_post, $post.link_rewrite)}
	<div class="post-item-wrapper">
		<div class="post-item-compact">
            {if !empty($settings.show_cover)}
                {$cover_src = $blog->getImgSrc('post', $post.id_post, $settings.cover_type, $post.cover)}
    			{if !empty($cover_src)}
    				<div class="post-item-cover-compact">
                        {if !empty($settings.link_cover)}<a href="{$link|escape:'html':'UTF-8'}">{/if}
                        <img src="{$cover_src|escape:'html':'UTF-8'}">
                        {if !empty($settings.link_cover)}</a>{/if}
    				</div>
    			{/if}
            {/if}
            {if !empty($settings.show_title)}
                <h5 class="post-item-title-compact overflow-ellipsis b">
    				{if !empty($settings.link_title)}<a href="{$link|escape:'html':'UTF-8'}">{/if}
    					{if !empty($settings.title_truncate)}{$post.title = $post.title|truncate:$settings.title_truncate:'...'}{/if}
    					{$post.title|escape:'html':'UTF-8'}
    				{if !empty($settings.link_title)}</a>{/if}
    			</h5>
            {/if}
            {if !empty($settings.show_intro)}
                <div class="post-item-content-compact">
                    {$post.content|strip_tags|truncate:$settings.truncate:'...'|escape:'html':'UTF-8'}
                    {if !empty($settings.show_readmore)}
    					<a href="{$link|escape:'html':'UTF-8'}" title="{l s='Read more' mod='amazzingblog'}" class="item-readmore-compact">
                            <i class="icon-angle-right"></i>
                            <i class="icon-angle-right second"></i>
    					</a>
    				{/if}
                </div>
            {/if}
			{$show_date = !empty($blog->general_settings.date) && !empty($settings.show_date)}
            {if $show_date || !empty($settings.show_author) || !empty($settings.show_views) || !empty($settings.show_comments)}
				<div class="post-item-infos-compact clearfix">
                    {if $show_date}
                        {$date = $post[$blog->general_settings.date]}
                        <span class="post-item-info date">
                            {$date|date_format|escape:'html':'UTF-8'}
                        </span>
                    {/if}
                    {if !empty($settings.show_author)}
						<span class="post-item-info author">
							<i class="icon-user"></i>
							{$post.firstname|escape:'html':'UTF-8'} {$post.lastname|escape:'html':'UTF-8'}
						</span>
					{/if}
					{if !empty($settings.show_views)}
						<span class="post-item-info views-num pull-right">
							<i class="icon-eye"></i>
							{$post.views|intval}
						</span>
					{/if}
					{if !empty($settings.show_comments)}
						<a href="{$link|escape:'html':'UTF-8'}#post-comments" class="post-item-info comments-num pull-right">
							<i class="icon-comment"></i>
							{$post.comments|intval}
						</a>
					{/if}
				</div>
            {/if}
		</div>
	</div>
	{/foreach}
	</div>
{else}
	<div class="alert-warning">{l s='No posts' mod='amazzingblog'}</div>
{/if}
