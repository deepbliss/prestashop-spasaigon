<?php
/**
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class AdminBlogController extends ModuleAdminController
{
    public function __construct()
    {
        parent::__construct();
        $link = 'index.php?controller=AdminModules&configure='.$this->module->name.
        '&token='.Tools::getAdminTokenLite('AdminModules');
        Tools::redirectAdmin($link);
    }
}
