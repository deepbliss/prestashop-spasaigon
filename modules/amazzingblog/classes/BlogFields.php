<?php
/**
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class BlogFields extends AmazzingBlog
{
    public function getPostFields($only_multilang = false)
    {
        $fields = array(
            'title' => array(
                'tab' => 'content',
                'display_name' => $this->l('Title'),
                'input_name' => 'multilang[title]',
                'multilang' => 1,
                'value' => array(
                    $this->context->language->id => '',
                ),
                'required' => 1,
            ),
            'meta_title' => array(
                'tab' => 'seo',
                'display_name' => $this->l('Meta title'),
                'input_name' => 'multilang[meta_title]',
                'multilang' => 1,
                'class' => 'meta-field autofill',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'validate' => 'isGenericName',
            ),
            'link_rewrite' => array(
                'tab' => 'seo',
                'display_name' => $this->l('Friendly URL'),
                'input_name' => 'multilang[link_rewrite]',
                'multilang' => 1,
                'class' => 'autofill',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'required' => 1,
                'validate' => 'isLinkRewrite',
            ),
            'meta_description' => array(
                'tab' => 'seo',
                'display_name' => $this->l('Meta description'),
                'input_name' => 'multilang[meta_description]',
                'multilang' => 1,
                'class' => 'meta-field',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'validate' => 'isGenericName',
            ),
            'meta_keywords' => array(
                'tab' => 'seo',
                'display_name' => $this->l('Meta keywords'),
                'input_name' => 'multilang[meta_keywords]',
                'multilang' => 1,
                'class' => 'meta-field',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'validate' => 'isGenericName',
            ),
            'categories' => array(
                'tab' => 'categories',
                'display_name' => $this->l('Selected categories'),
                'input_name' => 'cat_ids[]',
                'value' => '',
            ),
            'images' => array(
                'tab' => 'content',
                'display_name' => $this->l('Images'),
                'value' => array(),
            ),
            'content' => array(
                'tab' => 'content',
                'display_name' => $this->l('Text'),
                'input_name' => 'multilang[content]',
                'multilang' => 1,
                'type' => 'mce',
                'value' => array(
                    $this->context->language->id => '',
                ),
            ),
            'tags' => array(
                'tab' => 'content',
                'display_name' => $this->l('Tags'),
                'input_name' => 'multilang[tags]',
                'input_class' => 'tagify',
                'multilang' => 1,
                'value' => array(
                    $this->context->language->id => '',
                ),
            ),
            'author' => array(
                'tab' => 'publishing',
                'display_name' => $this->l('Author'),
                'input_name' => 'author',
                'type' => 'select',
                'options' => $this->getAuthorOptions(),
                'value' => $this->context->employee->id,
                'required' => 1,
            ),
            'publish_from' => array(
                'tab' => 'publishing',
                'display_name' => $this->l('Start publication on'),
                'input_name' => 'publish_from',
                'type' => 'datepicker',
                'value' => '',
                'required' => 1,
            ),
            'publish_to' => array(
                'tab' => 'publishing',
                'display_name' => $this->l('End publication on'),
                'tooltip' => $this->l('You can leave it empty'),
                'input_name' => 'publish_to',
                'type' => 'datepicker',
                'value' => '',
                'required' => 1,
            ),
            'date_add' => array(
                'tab' => 'publishing',
                'display_name' => $this->l('Created on'),
                'input_name' => 'date_add',
                'readonly' => 1,
                'value' => '',
                'required' => 1,
            ),
            'date_upd' => array(
                'tab' => 'publishing',
                'display_name' => $this->l('Last updated on'),
                'input_name' => 'date_upd',
                'readonly' => 1,
                'value' => '',
                'required' => 1,
            ),
            // 'related_products' => array(
            //     'tab' => 'related',
            //     'display_name' => $this->l('Related products'),
            //     'tooltip' => $this->l('Will be displayed below post content'),
            //     'value' => array(),
            // ),
        );
        if ($only_multilang) {
            // tags are processed separately
            unset($fields['tags']);
            foreach ($fields as $k => $n) {
                if (empty($n['multilang'])) {
                    unset($fields[$k]);
                }
            }
        }
        return $fields;
    }

    public function getPostTabs()
    {
        $tabs = array(
            'content' => $this->l('Content'),
            'categories' => $this->l('Categories'),
            'seo' => $this->l('SEO'),
            'publishing' => $this->l('Publishing options'),
            // 'related' => $this->l('Related products'),
        );
        return $tabs;
    }


    public function getImgOptions()
    {
        if (empty($this->img_options)) {
            $img_fields = $this->getImgSettingsFields();
            $saved_img_settings = $this->getSettings('img');
            $this->img_options = array();
            foreach ($img_fields as $k => $f) {
                $value = isset($saved_img_settings[$k]) ? $saved_img_settings[$k] : $f['value'];
                $this->img_options[$k] = $f['display_name'].' ('.$value.')';
            }
        }
        return $this->img_options;
    }

    public function getPostSettingsFields()
    {
        $fields = array(
            'display_column_left' => array(
                'display_name' => $this->l('Show left column'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'display_column_right' => array(
                'display_name' => $this->l('Show right column'),
                'type' => 'switcher',
                'value' => 0,
            ),
            'show_date' =>  array(
                'display_name' => $this->l('Show publication date'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_author' =>  array(
                'display_name' => $this->l('Show author'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_views' =>  array(
                'display_name' => $this->l('Show number of views'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_tags' =>  array(
                'display_name' => $this->l('Show tags'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_footer_hook' =>  array(
                'display_name' => $this->l('Show hook displayPostFooter'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'main_img_type' => array(
                'display_name' => $this->l('Main image type'),
                'options' => $this->getImgOptions(),
                'value' => 'l',
            ),
            'social_sharing' => array(
                'display_name' => $this->l('Social sharing'),
                'type' => 'checkbox',
                'input_name' => 'social_sharing[]',
                'boxes' => array(
                    'facebook' => 'Facebook',
                    'vk' => 'VK',
                    'odnoklassniki' => 'Odnoklassniki',
                    'twitter' => 'Twitter',
                    'google-plus' => 'Google+',
                    'linkedin' => 'LinkedIn',
                    'pinterest' => 'Pinterest',
                ),
                'value' => array(),
            ),
        );
        return $fields;
    }

    public function getCategoryFields($id_parent = false, $only_multilang = false)
    {
        $fields = array(
            'id_parent' => array(
                'display_name' => $this->l('Parent category'),
                'input_name' => 'id_parent',
                'value' => $id_parent ? $id_parent : $this->root_id,
            ),
            'title' => array(
                'display_name' => $this->l('Title'),
                'input_name' => 'multilang[title]',
                'multilang' => 1,
                'value' => array(
                    $this->context->language->id => '',
                ),
                'required' => 1,
                'validate' => 'isGenericName',
            ),
            'link_rewrite' => array(
                'display_name' => $this->l('Friendly URL'),
                'input_name' => 'multilang[link_rewrite]',
                'multilang' => 1,
                'class' => 'autofill',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'required' => 1,
                'validate' => 'isLinkRewrite',
            ),
            'meta_title' => array(
                'display_name' => $this->l('Meta title'),
                'input_name' => 'multilang[meta_title]',
                'multilang' => 1,
                'class' => 'meta-field autofill hidden',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'validate' => 'isGenericName',
            ),
            'meta_description' => array(
                'display_name' => $this->l('Meta description'),
                'input_name' => 'multilang[meta_description]',
                'multilang' => 1,
                'class' => 'meta-field hidden',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'validate' => 'isGenericName',
            ),
            'meta_keywords' => array(
                'display_name' => $this->l('Meta keywords'),
                'input_name' => 'multilang[meta_keywords]',
                'multilang' => 1,
                'class' => 'meta-field hidden',
                'value' => array(
                    $this->context->language->id => '',
                ),
                'validate' => 'isGenericName',
            ),
            'description' => array(
                'display_name' => $this->l('Description'),
                'input_name' => 'multilang[description]',
                'multilang' => 1,
                'type' => 'mce',
                'value' => array(
                    $this->context->language->id => '',
                ),
            ),
        );
        if ($only_multilang) {
            foreach ($fields as $k => $n) {
                if (empty($n['multilang'])) {
                    unset($fields[$k]);
                }
            }
        }
        return $fields;
    }

    public function getCategorySettingsFields()
    {
        $fields = array(
            'display_column_left' => array(
                'display_name' => $this->l('Show left column'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'display_column_right' => array(
                'display_name' => $this->l('Show right column'),
                'type' => 'switcher',
                'value' => 0,
            ),
            'p_type' => array(
                'display_name' => $this->l('Pagination type'),
                'options' => array(
                    'regular' => $this->l('Regular'),
                    'ajax' => $this->l('Ajax'),
                    // 'load_more' => $this->l('Load more button'),
                    // 'scroll' => $this->l('Load posts on page scroll'),
                ),
                'value' => 'regular',
            ),
            'show_subcategories' => array(
                'display_name' => $this->l('Display subcategories'),
                'options' => array(
                    0 => $this->l('None'),
                    1 => $this->l('Only having posts'),
                    2 => $this->l('All available'),
                ),
                'value' => 2,
            ),
            'include_all' => array(
                'display_name' => $this->l('Include all posts from subcategories'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'display_type' => array(
                'display_name' => $this->l('Display posts as'),
                'options' => array(
                    'grid' => $this->l('Grid'),
                    'list' => $this->l('List'),
                ),
                'value' => 'list',
                'h' => $this->l('Posts list on category page'),
            ),
            'posts_per_page' => array(
                'display_name' => $this->l('Posts per page'),
                'options' => $this->getNppOptions(),
                'value' => 10,
            ),
            'col_num' => array(
                'display_name' => $this->l('Number of columns'),
                'class' => 'display-type-option grid hidden',
                'options' => array(
                    2 => 2,
                    3 => 3,
                    4 => 4,
                ),
                'value' => 3,
            ),
            'order_by' => array(
                'display_name' => $this->l('Order by'),
                'options' => $this->getSortingOptions(),
                'value' => 'publish_from',
            ),
            'show_cover' => array(
                'display_name' => $this->l('Show cover image'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'cover_type' => array(
                'display_name' => $this->l('Cover image type'),
                'options' => $this->getImgOptions(),
                'class' => 'cover-option hidden',
                'value' => 'xl',
            ),
            'link_cover' => array(
                'display_name' => $this->l('Bind link to cover'),
                'type' => 'switcher',
                'class' => 'cover-option hidden',
                'value' => 1,
            ),
            'show_title' =>  array(
                'display_name' => $this->l('Show title'),
                'input_name' => 'settings[show_title]',
                'type' => 'switcher',
                'value' => 1,
            ),
            'title_truncate' => array(
                'display_name' => $this->l('Title max length'),
                'class' => 'title-option hidden',
                'value' => 100,
            ),
            'link_title' =>  array(
                'display_name' => $this->l('Bind link to title'),
                'type' => 'switcher',
                'class' => 'title-option hidden',
                'value' => 1,
            ),
            'show_intro' => array(
                'display_name' => $this->l('Show intro text'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'truncate' =>  array(
                'display_name' => $this->l('Intro text length'),
                'class' => 'intro-option hidden',
                'value' => 100,
            ),
            'show_readmore' =>  array(
                'display_name' => $this->l('Read more button'),
                'type' => 'switcher',
                'class' => 'intro-option hidden',
                'value' => 1,
            ),
            'show_tags' =>  array(
                'display_name' => $this->l('Show tags'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_date' =>  array(
                'display_name' => $this->l('Show publication date'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_author' =>  array(
                'display_name' => $this->l('Show author'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_views' =>  array(
                'display_name' => $this->l('Show views num'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'show_comments' =>  array(
                'display_name' => $this->l('Show comments num'),
                'type' => 'switcher',
                'value' => 1,
            ),
        );
        return $fields;
    }

    public function getBlockFields()
    {
        // get some fileds from general settings in order to avoid duplicates
        $category_settings_fields = $this->getCategorySettingsFields();
        foreach ($category_settings_fields as $k => &$field) {
            $field['input_name'] = 'settings['.$k.']';
            if ($k == 'cover_type') {
                $field['value'] = 's';
            } elseif ($k == 'show_author' || $k == 'show_tags') {
                $field['value'] = 0;
            }
        }
        $carousel_items_num_array = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6);

        $fields = array(
            'title' => array(
                'display_name' => $this->l('Title'),
                'input_name' => 'multilang[title]',
                'multilang' => 1,
                'value' => array(
                    $this->context->language->id => $this->l('Latest Posts'),
                ),
                'required' => 1,
            ),
            'type' => array(
                'display_name' => $this->l('Type'),
                'input_name' => 'settings[type]',
                'options' => array(
                    'latest' => $this->l('Latest posts'),
                    'selectedposts' => $this->l('Selected posts'),
                    'mostviewed' => $this->l('Most viewed posts'),
                    'random' => $this->l('Random posts'),
                    'related' => $this->l('Other posts, related to current post'),
                    // 'related_p' => $this->l('Posts, related to current product'),
                    // 'selectedcategories' => $this->l('Posts from selected categories'),
                    // 'selectedauthors' => $this->l('Posts by selected authors'),
                    // 'tagcloud' => $this->l('Tag cloud'),
                    // 'categories' => $this->l('Blog catetories'),
                    // 'featured' => $this->l('Featured posts'),
                ),
                'value' => 'latest',
            ),
            'post_ids' => array(
                'display_name' => $this->l('Post ids'),
                'input_name' => 'settings[post_ids]',
                'class' => 'type-option selectedposts hidden',
                'value' => '',
            ),
            'related_category' => array(
                'display_name' => $this->l('Include posts from same category'),
                'input_name' => 'settings[related][category]',
                'class' => 'type-option related hidden',
                'type' => 'switcher',
                'value' => 1,
            ),
            'related_tag' => array(
                'display_name' => $this->l('Include posts having same tags'),
                'input_name' => 'settings[related][tag]',
                'class' => 'type-option related hidden',
                'type' => 'switcher',
                'value' => 1,
            ),
            'num' =>  array(
                'display_name' => $this->l('Number of posts'),
                'input_name' => 'settings[num]',
                'value' => 3,
            ),
            'display_type' => array(
                'display_name' => $this->l('Display Type'),
                'input_name' => 'settings[display_type]',
                'options' => array(
                    // 'presentation' => $this->l('Presentation'),
                    'carousel' => $this->l('Carousel'),
                    'grid' => $this->l('Grid'),
                    'list' => $this->l('List'),
                ),
                'value' => 'list',
            ),
            'compact' => array(
                'display_name' => $this->l('Compact layout'),
                'input_name' => 'settings[compact]',
                'type' => 'switcher',
                'value' => 0,
            ),
            'col_num' => array(
                'display_name' => $this->l('Number of columns'),
                'input_name' => 'settings[col_num]',
                'class' => 'display-type-option grid hidden',
                'options' => array(
                    2 => 2,
                    3 => 3,
                    4 => 4,
                ),
                'value' => 3,
            ),
            'carousel_i' => array(
                'display_name' => $this->l('Visible items on displays > 1999px'),
                'input_name' => 'settings[carousel][i]',
                'class' => 'display-type-option carousel hidden',
                'options' => $carousel_items_num_array,
                'value' => 4,
            ),
            'carousel_i_1200' => array(
                'display_name' => $this->l('Visible items on displays > 1200px'),
                'input_name' => 'settings[carousel][i_1200]',
                'class' => 'display-type-option carousel hidden',
                'options' => $carousel_items_num_array,
                'value' => 4,
            ),
            'carousel_i_992' => array(
                'display_name' => $this->l('Visible items on displays < 992px'),
                'input_name' => 'settings[carousel][i_992]',
                'class' => 'display-type-option carousel hidden',
                'options' => $carousel_items_num_array,
                'value' => 3,
            ),
            'carousel_i_768' => array(
                'display_name' => $this->l('Visible items on displays < 768px'),
                'input_name' => 'settings[carousel][i_768]',
                'class' => 'display-type-option carousel hidden',
                'options' => $carousel_items_num_array,
                'value' => 2,
            ),
            'carousel_i_480' => array(
                'display_name' => $this->l('Visible items on displays < 480px'),
                'input_name' => 'settings[carousel][i_480]',
                'class' => 'display-type-option carousel hidden',
                'options' => $carousel_items_num_array,
                'value' => 1,
            ),
            'carousel_n' => array(
                'display_name' => $this->l('Display navigation arrows'),
                'input_name' => 'settings[carousel][n]',
                'class' => 'display-type-option carousel hidden',
                'options' => array(
                    '0' => $this->l('Never'),
                    '1' => $this->l('Always'),
                    '2' => $this->l('Only on hover'),
                ),
                'value' => 2,
            ),
            'carousel_p' => array(
                'display_name' => $this->l('Display pagination'),
                'input_name' => 'settings[carousel][p]',
                'class' => 'display-type-option carousel hidden',
                'type' => 'switcher',
                'value' => 0,
            ),
            'carousel_a' => array(
                'display_name' => $this->l('Autoplay'),
                'input_name' => 'settings[carousel][a]',
                'class' => 'display-type-option carousel hidden',
                'type' => 'switcher',
                'value' => 1,
            ),
            'carousel_l' => array(
                'display_name' => $this->l('Infinite loop'),
                'input_name' => 'settings[carousel][l]',
                'class' => 'display-type-option carousel hidden',
                'type' => 'switcher',
                'value' => 1,
            ),
            'carousel_s' => array(
                'display_name' => $this->l('Animation speed(ms)'),
                'input_name' => 'settings[carousel][s]',
                'class' => 'display-type-option carousel hidden',
                'value' => 100,
                'required' => 1,
            ),
            'main_item_img_type' => array(
                'display_name' => $this->l('Main item image type'),
                'input_name' => 'settings[main_item_img_type]',
                'class' => 'display-type-option presentation hidden',
                'options' => $category_settings_fields['cover_type']['options'],
                'value' => 'l',
            ),
            'main_item_truncate' => array(
                'display_name' => $this->l('Main item intro text length'),
                'input_name' => 'settings[main_item_truncate]',
                'class' => 'display-type-option presentation hidden',
                'value' => 150,
            ),
            'class' => array(
                'display_name' => $this->l('Container class'),
                'input_name' => 'settings[class]',
                'value' => '',
            ),
            'restrictions' => array(
                'display_name' => $this->l('Restrictions'),
                'tooltip' => $this->l('Display only for selected products/categories'),
                'input_name' => 'settings[restrictions]',
                'options' => array(
                    '0' => $this->l('None'),
                    'product' => $this->l('By products'),
                    'category' => $this->l('By categories'),
                    'manufacturer' => $this->l('By manufacturers'),
                    'supplier' => $this->l('By suppliers'),
                    'cms' => $this->l('By CMS pages'),
                ),
                'value' => '',
            ),
            'show_cover' =>  $category_settings_fields['show_cover'],
            'cover_type' => $category_settings_fields['cover_type'],
            'link_cover' =>  $category_settings_fields['link_cover'],
            'show_title' => $category_settings_fields['show_title'],
            'title_truncate' => $category_settings_fields['title_truncate'],
            'link_title' => $category_settings_fields['link_title'],
            'show_intro' =>  $category_settings_fields['show_intro'],
            'truncate' => $category_settings_fields['truncate'],
            'show_readmore' => $category_settings_fields['show_readmore'],
            'show_date' => $category_settings_fields['show_date'],
            'show_author' => $category_settings_fields['show_author'],
            'show_views' => $category_settings_fields['show_views'],
            'show_comments' => $category_settings_fields['show_comments'],
            'show_tags' => $category_settings_fields['show_tags'],
            'all_link' => array(
                'display_name' => $this->l('Link to all posts'),
                'input_name' => 'settings[all_link]',
                'type' => 'switcher',
                'value' => 1,
            ),
        );
        return $fields;
    }

    public function getCommentSettingsFields()
    {
        $fields = array(
            'instant_publish' => array(
                'display_name' => $this->l('Instant publish'),
                'type' => 'switcher',
                'value' => 1,
                'class' => 'user-comments',
            ),
            'avatar' => array(
                'display_name' => $this->l('Avatar dimentions'),
                'tooltip' => $this->l('For example: 55*55'),
                'value' => '55*55',
                'class' => 'user-comments',
            ),
            'max_chars' => array(
                'display_name' => $this->l('Maximum characters limit'),
                'value' => '512',
                'class' => 'user-comments',
            ),
            'max_comments' => array(
                'display_name' => $this->l('Max comments within 1 hour'),
                'value' => '10',
                'class' => 'user-comments',
            ),
            'notif_email' => array(
                'display_name' => $this->l('Email for notifications'),
                'tooltip' => $this->l('Leave it empty if you want to disable notifications'),
                'type' => 'text',
                'value' => Configuration::get('PS_SHOP_EMAIL'),
                'class' => 'user-comments',
            ),
        );
        return $fields;
    }

    public function getGeneralSettingsFields()
    {
        $fields = array(
            'user_comments' => array(
                'display_name' => $this->l('Enable comments'),
                'type' => 'switcher',
                'value' => 1,
            ),
            'load_icon_fonts' => array(
                'display_name' => $this->l('Load icon fonts'),
                'help_box' => array(
                    $this->l('You can turn it off, if your theme supports \'icon-xx\' classes'),
                    $this->l('Pay attention on  \'icon-google-plus\' and \'icon-odnoklassniki\''),
                ),
                'type' => 'switcher',
                'value' => 0,
            ),
        );
        return $fields;
    }

    public function getImgSettingsFields()
    {
        $fields = array(
            'xs' => array(
                'display_name' => $this->l('Extra small'),
                'value' => '55*55',
                'regenerate' => 1,
            ),
            's' => array(
                'display_name' => $this->l('Small'),
                'value' => '375*125',
                'regenerate' => 1,
            ),
            'm' => array(
                'display_name' => $this->l('Medium'),
                'value' => '450*450',
                'regenerate' => 1,
            ),
            'l' => array(
                'display_name' => $this->l('Large'),
                'value' => '800*350',
                'regenerate' => 1,
            ),
            'xl' => array(
                'display_name' => $this->l('Extra large'),
                'value' => '1280*520',
                'regenerate' => 1,
            ),
        );
        return $fields;
    }
}
