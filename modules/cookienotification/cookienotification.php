<?php
/**
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class CookieNotification extends Module
{
    public function __construct()
    {
        if (!defined('_PS_VERSION_'))
            exit;
        $this->name = 'cookienotification';
        $this->tab = 'front_office_features';
        $this->version = '1.1.0';
        $this->author = 'Amazzing';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = 'e8aa251fa630d358180cbbe514570e7d';

        parent::__construct();

        $this->displayName = $this->l('EU Cookie law notification');
        $this->description = $this->l('Shows notification and blocks selected modules until cookies are accepted');
        $this->db = Db::getInstance();
        $this->cookie_identifier = Configuration::get('CN_COOKIE_IDENTIFIER');
    }

    public function install()
    {
    if (!parent::install()
        || !$this->registerHook('displayHeader')
        || !$this->registerHook('displayFooter')
        || !$this->saveSettings()
        || !Configuration::updateGlobalValue('CN_COOKIE_IDENTIFIER', 'cookies_accepted'))
                return false;
        return true;
    }

    public function uninstall()
    {
        return parent::uninstall() && $this->deleteConfigurationValues();
    }

    public function deleteConfigurationValues()
    {
        $ret = Configuration::deleteByName('CN_SETTINGS');
        $ret &= Configuration::deleteByName('CN_BLOCKED_MODULE_IDS');
        $ret &= Configuration::deleteByName('CN_COOKIE_IDENTIFIER');
        return $ret;
    }


    public function getContent()
    {
        $this->context->controller->addJS($this->_path.'views/js/back.js');
        $this->context->controller->addCSS($this->_path.'views/css/back.css', 'all');
        $this->saved_text = $this->l('Saved');
        $this->error_text = $this->l('Error');
        if (Tools::isSubmit('ajax') && $action = Tools::getValue('action'))
            $this->ajaxAction($action);

        // plain js for retro-compatibility
        $js_variables = '
            <script type="text/javascript">
                var saved_text = \''.$this->saved_text.'\',
                    error_text = \''.$this->error_text.'\';
            </script>
        ';

        $this->context->smarty->assign(array(
            'fields' => $this->getSettingsFields(),
            'modules' => $this->getModules(),
            'label_col' => 2,
            'input_col' => 2,
        ));

        $lang_iso = $this->context->language->iso_code;
        $more_info_path = 'views/templates/admin/more-info-'.$lang_iso.'.tpl';
        if (!file_exists($this->local_path.$more_info_path))
            $more_info_path = 'views/templates/admin/more-info-en.tpl';
        $this->context->controller->modals[] = array(
            'modal_id' => 'modal_more_info',
            'modal_class' => 'modal-md',
            'modal_title' => $this->l('What modules should be blocked'),
            'modal_content' => $this->display($this->local_path, $more_info_path),
        );
        return $js_variables.$this->display(__FILE__, 'views/templates/admin/configure.tpl');
    }

    public function getSettingsFields($fill_saved_values = true)
    {
        $fields = array(
            'position' =>  array(
                'type' => 'select',
                'label' => $this->l('Notification position'),
                'value' => 'bottom',
                'options' => array(
                    'top' => $this->l('Top, full width'),
                    'bottom' => $this->l('Bottom, full width'),
                    'top right' => $this->l('Top right'),
                    'bottom right' => $this->l('Bottom right'),
                    'bottom left' => $this->l('Bottom left'),
                    'top left' => $this->l('Top left'),
                )
            ),
            'id_cms' =>  array(
                'type' => 'select',
                'value' => '0',
                'label' => $this->l('More info page'),
                'options' => $this->getCMSOptions(),
            ),
            'show_once' =>  array(
                'type' => 'switcher',
                'value' => '1',
                'label' => $this->l('Show warning just once'),
                'tooltip' => $this->l('Warning will be displayed only on first page load.'),
            ),
        );
        if ($fill_saved_values)
        {
            $saved_values = Tools::jsonDecode(Configuration::get('CN_SETTINGS'), true);
            foreach ($fields as $name => &$field)
            {
                if (!empty($saved_values[$name]))
                    $field['value'] = $saved_values[$name];
            }
        }
        return $fields;
    }

    public function getModules()
    {
        $modules = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'module');
        $blocked_module_ids = Configuration::get('CN_BLOCKED_MODULE_IDS');
        $blocked_module_ids = $blocked_module_ids ? Tools::jsonDecode($blocked_module_ids, true) : array();
        foreach ($modules as &$m)
        {
            $m['blocked'] = in_array($m['id_module'], $blocked_module_ids);
            $logo = '';
            if (file_exists(_PS_MODULE_DIR_.$m['name'].'/logo.png'))
                $logo = _MODULE_DIR_.$m['name'].'/logo.png';
            $m['logo'] = $logo;
        }
        return $modules;
    }

    public function getCMSOptions()
    {
        $pages = CMS::getCMSPages($this->context->language->id);
        $options = array('0' => $this->l('None'));
        foreach ($pages as $page)
            $options[$page['id_cms']] = $page['meta_title'];
        return $options;
    }

    public function ajaxAction($action)
    {
        $ret = array();
        switch ($action)
        {
            case 'SaveSettings':
                $settings = Tools::getValue('settings');
                $ret['saved'] = $this->saveSettings($settings);
                break;
        }
        exit(Tools::jsonEncode($ret));
    }

    public function saveSettings($submitted_settings = array())
    {
        $required_fields = $this->getSettingsFields(false);
        $settings_to_save = array();
        foreach ($required_fields as $name => $field)
        {
            if (isset($submitted_settings[$name]))
                $settings_to_save[$name] = pSQL($submitted_settings[$name]);
            else
                $settings_to_save[$name] = $field['value'];
        }
        $settings_to_save = Tools::jsonEncode($settings_to_save);
        $success = Configuration::updateGlobalValue('CN_SETTINGS', $settings_to_save);
        if ($blocked_module_ids = Tools::getValue('blocked_modules'))
            $success &= $this->saveBlockedModuleIds($blocked_module_ids);
        return $success;
    }

    public function saveBlockedModuleIds($ids)
    {
        foreach ($ids as &$id)
            $id = (int)$id;
        $ids = Tools::jsonEncode($ids);
        return Configuration::updateGlobalValue('CN_BLOCKED_MODULE_IDS', $ids);
    }

    public function hookDisplayHeader()
    {
        if (empty($_COOKIE) ||
            !empty($this->context->cookie->{$this->cookie_identifier}) ||
            !$this->settings = Tools::jsonDecode(Configuration::get('CN_SETTINGS'), true))
            return '';
        if (!empty($this->settings['show_once']))
            $this->context->cookie->__set($this->cookie_identifier, 1);
        $this->context->controller->addCSS($this->_path.'views/css/front.css', 'all');
        $this->context->controller->addJS($this->_path.'views/js/front.js');
    }

    public function hookDisplayFooter()
    {
        if (empty($this->settings))
            return;
        if (!empty($this->settings['id_cms']))
            $this->settings['info_link'] = $this->context->link->getCMSLink($this->settings['id_cms']);
        $s = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? 's' : '';
        $action_params = array(
            'accept_cookies' => 1,
            'redirect_url' => urlencode('http'.$s.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']),
        );
        $this->context->smarty->assign(array(
            'settings' => $this->settings,
            'form_action' => $this->context->link->getModuleLink($this->name, 'accept', $action_params),
        ));
        return $this->display(__FILE__, 'views/templates/hook/notification.tpl');
    }
}
