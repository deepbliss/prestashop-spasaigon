{*
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author	  Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license	  http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="panel">
	<h3><i class="icon-cogs"></i> {l s='Settings' mod='cookienotification'}</h3>
	<form class="form-horizontal cn-form">
	{foreach $fields as $k => $field}
		{$name = 'settings['|cat:$k|cat:']'}
		<div class="form-group">
			<label class="control-label col-lg-{$label_col|intval}">
				<span{if !empty($field.tooltip)} class="label-tooltip"data-toggle="tooltip" title="{$field.tooltip|escape:'html':'UTF-8'}"{/if}>
				{$field.label|escape:'html':'UTF-8'}<span>
			</label>
			<div class="col-lg-{$input_col|intval}">
				{if $field.type == 'select' && !empty($field.options)}
					<select name="{$name|escape:'html':'UTF-8'}">
						{foreach $field.options as $value => $displayed_name}
							<option value="{$value|escape:'html':'UTF-8'}"{if $value == $field.value} selected{/if}>{$displayed_name|escape:'html':'UTF-8'}</option>
						{/foreach}
					</select>
				{else if $field.type == 'switcher'}
					<span class="switch prestashop-switch">
						<input type="radio" id="{$k|escape:'html':'UTF-8'}" name="{$name|escape:'html':'UTF-8'}" value="1"{if !empty($field.value)} checked{/if} >
						<label for="{$k|escape:'html':'UTF-8'}">{l s='Yes' mod='cookienotification'}</label>
						<input type="radio" id="{$k|escape:'html':'UTF-8'}_0" name="{$name|escape:'html':'UTF-8'}" value="0"{if empty($field.value)} checked{/if} >
						<label for="{$k|escape:'html':'UTF-8'}_0">{l s='No' mod='cookienotification'}</label>
						<a class="slide-button btn"></a>
					</span>
				{else}
					<input type="text" name="{$name|escape:'html':'UTF-8'}" value="{$field.value|escape:'html':'UTF-8'}">
				{/if}
			</div>
		</div>
	{/foreach}
	<div class="alert alert-info">{l s='Select modules, that should be bocked until user accepts to receive cookies.' mod='cookienotification'} <a href="#" data-toggle="modal" data-target="#modal_more_info">{l s='More information' mod='cookienotification'}</a></div>
	<a href="#" class="chk-action checkall">{l s='Check all' mod='cookienotification'}</a>
	<a href="#" class="chk-action uncheckall">{l s='Unheck all' mod='cookienotification'}</a>
	<a href="#" class="chk-action invert">{l s='Invert selection' mod='cookienotification'}</a>
	<div class="module-list clearfix">
		{foreach $modules as $m}
			<div class="col-lg-3">
				<label class="control-label">
					<input type="checkbox" name="blocked_modules[]" value="{$m.id_module|intval}"{if $m.blocked} checked{/if}>
					{if !empty($m.logo)}
						<img src="{$m.logo|escape:'html':'UTF-8'}" class="module-logo">
					{/if}
					{$m.name|escape:'html':'UTF-8'}
				</label>
			</div>
		{/foreach}
	</div>
	</form>
	<div class="panel-footer">
		<button class="btn btn-default pull-right save-settings">
			<i class="process-icon-save"></i>
			{l s='Save' mod='cookienotification'}
		</button>
	</div>
</div>