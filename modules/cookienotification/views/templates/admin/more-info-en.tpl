{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="modal-body">
	<p>Basing on EU cookie law, you must inform people if you set cookies, and clearly explain what the cookies do and why. You can only store cookies on user computer after getting the consent.</p>
	<p>There is an exception for cookies that are essential to provide an online service. These cookies can be used for storing products in cart, keeping user selected language etc.</p>
	<p>Initially Prestashop uses one essential cookie that keeps all required information for running the store properly. It is usually called something like “PrestaShop-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx”.</p>
	<p>Some third-party modules can set their own cookies. In order to comply with the law you have to get user consent before storing those cookies. </p>
	<p>There is no any exact list of modules, that set additional cookies. You have to make an individual selection for your store. A typical example is Google Analytics module. </p>
	<p>There is a full list of available modules under this popup. You should carefully review it and select items that should get blocked before user gives consent.</p>
	<p>In order to find out what cookies are used on your store, you can use browser developer tools, or an external service like <a href="http://cookie-checker.com/" target="_blank">cookie-checker.com</a></p>
	<p>More information about Cookie Law:
	<br><a href="https://ico.org.uk/for-organisations/guide-to-pecr/cookies-and-similar-technologies/" target="_blank">Guidance on the rules on use of cookies and similar technologies by the ICO</a>
	<br><a href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm" target="_blank">Cookie use guide</a>
	</p>
</div>
