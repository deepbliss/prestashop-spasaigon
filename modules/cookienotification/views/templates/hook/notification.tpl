{*
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author	  Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license	  http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="cookie-notification{if !empty($settings.position)} {$settings.position|escape:'html':'UTF-8'}{/if}">
	<form action="{$form_action|escape:'html':'UTF-8'}" method="post" class="cookie-accept-form">
	<p>
		{l s='Our site uses cookies to give you the best user experience. By continuing you accept to receive them.' mod='cookienotification'}
		<span class="button-holder">
			<button class="btn btn-default child" type="submit">{l s='OK' mod='cookienotification'}</button>
			{if !empty($settings.info_link)}
				<a href="{$settings.info_link|escape:'html':'UTF-8'}" class="btn btn-default child" target="_blank">{l s='More info' mod='cookienotification'}</a>
			{/if}
		</span>
	</p>
	</form>
</div>