/**
*  @author    Amazzing
*  @copyright Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)*
*/

var ajax_action_path = window.location.href.split('#')[0]+'&ajax=1';
$(document).ready(function(){
	$('.save-settings').on('click', function(e){
		e.preventDefault();
		var formdata = $('.cn-form').serialize();
		$.ajax({
			type: 'POST',
			url: ajax_action_path+'&action=SaveSettings',
			dataType : 'json',
			data: formdata,
			success: function(r)
			{
				if('saved' in r) {
					$.growl.notice({ title: '', message: saved_text});
				} else {
					$.growl.error({ title: '', message: error_text});
				}
			},
			error: function(r)
			{
				$.growl.error({ title: '', message: error_text});
				console.warn(r.responseText);
			}
		});
	});
	
	$(document).on('click', '.chk-action', function(e){
		e.preventDefault();
		var $checkboxes = $(this).siblings('.module-list').find('input[type="checkbox"]');
		if ($(this).hasClass('checkall')){
			$checkboxes.each(function(){
				$(this).prop('checked', true);
			});
		} else if ($(this).hasClass('uncheckall')){
			$checkboxes.each(function(){
				$(this).prop('checked', false);
			});
		} else if ($(this).hasClass('invert')){
			$checkboxes.each(function(){
				$(this).prop('checked', !$(this).prop('checked'));
			});
		}
	});

	// ajax progress
	$('body').append('<div id="re-progress"><div class="progress-inner"></div></div>');
	$(document).ajaxStart(function(){
		$('#re-progress .progress-inner').width(0).fadeIn('fast').animate({'width':'70%'},500);
	})
	.ajaxSuccess(function(){
		$('#re-progress .progress-inner').animate({'width':'100%'},500,function(){
			$(this).fadeOut('fast');
		});
	})

});