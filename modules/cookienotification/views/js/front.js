/**
*  @author    Amazzing
*  @copyright Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)*
*/

$(document).ready(function(){
	$('.cookie-accept-form').on('submit', function(e){
		e.preventDefault();
		var $form = $(this);
		$.ajax({
			type: 'POST',
			url: $form.attr('action'),
			dataType : 'json',
			data: {ajax: 1},
			success: function(r) {
				if (r.success) {
					$form.parent().slideUp();
				}
			},
			error: function(r) {
				console.warn(r.responseText);
			}
		});
	});
});