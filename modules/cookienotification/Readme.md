Module is installed in a regular way – simply upload your archive and click install.  

CHANGELOG:
===========================
v 1.1.0 (December 8, 2015)
===========================
- [+] Possibility to block selected modules until user accepts to receive cookies
- [*] Minor code fixes

Files modified: 
-----
- /cookienotification.php
- /views/templates/admin/configure.tpl
- /views/templates/js/back.js
- /views/templates/css/back.css
- /Readme.md

Files added:
-----
- /views/templates/admin/more-info-en.tpl
- /override/classes/Hook.php
- /override/classes/index.php
- /override/index.php

Initial relesase
===========================
v 1.0.0 (November 29, 2015)
===========================
