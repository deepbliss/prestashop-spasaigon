<?php
/**
* 2007-2017 Amazzing
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
*
*  @author    Amazzing <mail@amazzing.ru>
*  @copyright 2007-2017 Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class CookieNotificationAcceptModuleFrontController extends ModuleFrontControllerCore
{
    public function initContent()
    {
        $this->context = Context::getContext();
        if (Tools::getValue('accept_cookies'))
        {
            $this->context->cookie->__set($this->module->cookie_identifier, 1);
            if (Tools::isSubmit('ajax'))
            {
                $ret = array('success' => 1);
                exit(Tools::jsonEncode($ret));
            }
            else
            {
                $redirect_url = urldecode(Tools::getValue('redirect_url'));
                Tools::redirect($redirect_url);
            }
        }
    }
}
