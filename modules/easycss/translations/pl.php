<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{easycss}prestashop>easycss_15ea502c0c3df59ddd33334ae6a9420b'] = 'Łatwo w CSS';
$_MODULE['<{easycss}prestashop>easycss_d7c8c85bf79bbe1b7188497c32c3b0ca'] = 'Nie udało';
$_MODULE['<{easycss}prestashop>easycss_248336101b461380a4b2391a7625493d'] = 'Uratował';
$_MODULE['<{easycss}prestashop>easycss_12e1ce6966d38525bc96c0f558ee6df3'] = 'Proszę wybrać prawidłowy obraz';
$_MODULE['<{easycss}prestashop>easycss_729a51874fe901b092899e9e8b31c97a'] = 'Jesteś pewien?';
$_MODULE['<{easycss}prestashop>easycss_e8081116db6145064c613944d82690e7'] = 'Proszę wypełnić pole wyboru';
$_MODULE['<{easycss}prestashop>easycss_41d3bba0af74d2624f6b309888a0309a'] = 'Brak obrazu, aby zapisać';
$_MODULE['<{easycss}prestashop>easycss_1bc564651413b9c31c46314a88d1d6d3'] = 'Błąd: plik nie jest kopiowany';
$_MODULE['<{easycss}prestashop>configure_0c3cd33e7bf626da4897316a7158a7a4'] = 'Wybierak';
$_MODULE['<{easycss}prestashop>configure_be53a0541a6d36f6ecb879fa2c584b08'] = 'Zdjęcia';
$_MODULE['<{easycss}prestashop>configure_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nowe';
