<link rel="stylesheet" href="{$base_dir}modules/extratabs/extratabs.css" />
<script language="javascript" type="text/javascript" src="{$base_dir}modules/extratabs/js/TabSwitcher.js"></script>
<div id="msTabHolder">
<div id="msTabHolder_tabs"></div>
<div id="msTabHolder_contents"></div>
</div>
<script language="javascript">
	TabSwitcher.init("{$ms_tabs}", "{$base_dir}modules/extratabs/");
</script>