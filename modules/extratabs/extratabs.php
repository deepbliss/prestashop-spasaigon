<?php

class extraTabs extends Module
{
	function __construct()
	{
		$this->name = 'extratabs';
		$this->tab = 'Products';
		$this->version = 1.0;

		parent::__construct(); // The parent construct is required for translations

		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Extra Tabs');
		$this->description = $this->l('Adds tabs on product page.<br />(www.marghoobsuleman.com | www.giftlelo.com)');
	}

	function install()
	{
        if (parent::install() == false 
				OR $this->registerHook('productTab') == false
				OR $this->registerHook('productTabContent') == false)
			return (false);
		return (true);
	}


	public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitextraTabs'))
		{
			$tabpages = Tools::getValue('tabpages');
			if ($tabpages != 0 AND $tabpages != 1)
				$output .= '<div class="alert error">'.$this->l('tabpages : Invalid choice.').'</div>';
			else
			{
				Configuration::updateValue('PS_TAB_PAGES', $tabpages);
			}
				$output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings updated').'</div>';
		}
		
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		return '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>
				<legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('Enter titles and pages').'</label>
				<div class="margin-form">
					<input size="100" type="text" name="tabpages"  value="'.(Tools::getValue('tabpages', Configuration::get('PS_TAB_PAGES'))).'" />
					<p class="clear">'.$this->l('Enter title, file name in above text field. use semicolon as a separator if you are using more than one file.<br />e.g. Sizing Chart,content1.html;Make a request,content2.html').'</p>
				</div>
				<center><input type="submit" name="submitextraTabs" value="'.$this->l('Save').'" class="button" /></center>
			</fieldset>
		</form>';
	}

	/**
	* Returns module content
	*
	* @param array $params Parameters
	* @return string Content
	*/

	function hookProductTab($params)
	{
		global $smarty;
		//$smarty->assign('ms_tabs', intval(Configuration::get('PS_TAB_PAGES')));
		return $this->display(__FILE__, 'tab.tpl');
	}
    public function hookProductTabContent($params)
    {
		global $smarty;
		$smarty->assign('ms_tabs', Configuration::get('PS_TAB_PAGES'));
		return ($this->display(__FILE__, '/tabContents.tpl'));
	}	

}

?>