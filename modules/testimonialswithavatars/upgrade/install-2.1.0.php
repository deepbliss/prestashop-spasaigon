<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_2_1_0($module_obj)
{
	if (file_exists(_PS_MODULE_DIR_.$module_obj->name.'/changelog.txt'))
		unlink(_PS_MODULE_DIR_.$module_obj->name.'/changelog.txt');

	// "/css/", "/js/", "/img/" are moved to 'views'
	$current_avatars = glob(_PS_MODULE_DIR_.$module_obj->name.'/img/avatars/*.jpg');
	foreach ($current_avatars as $avatar_path)
		Tools::copy($avatar_path, _PS_MODULE_DIR_.$module_obj->name.'/views/img/avatars/'.basename($avatar_path));

	foreach (array('css', 'js', 'img') as $folder_name)
		recursiveRemove(_PS_MODULE_DIR_.$module_obj->name.'/'.$folder_name);
	return true;
}

function recursiveRemove($dir, $top_level = false)
{
	$files_to_keep = array();
	if ($top_level)
		$files_to_keep = array('index.php');
	$structure = glob(rtrim($dir, '/').'/*');
	if (is_array($structure))
	{
		foreach ($structure as $file)
			if (is_dir($file))
				recursiveRemove($file);
			else if (is_file($file) && !in_array(basename($file), $files_to_keep))
				unlink($file);
	}
	if (!$top_level && is_dir($dir))
		rmdir($dir);
}