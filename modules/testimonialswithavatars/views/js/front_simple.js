/**
*  @author    Amazzing
*  @copyright Amazzing
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)*
*/

$(document).ready(function(){

	var carousels = initialClasses = {},	
		resizeTimer;

	// activating hooks in editable areas
	for (c = 1; c <= 3; c++){
		var orig_value = '{hook h=\'testimonials'+c+'\'}';
		var new_value = '<div class="dynamic_testimonials" data-hook="testimonials'+c+'"></div>';
		replaceTextInDom(orig_value, new_value);
	}

	$('.dynamic_testimonials').each(function(){
		var $el = $(this);
		var hook = $el.data('hook');
		$.ajax({
			type: 'POST',
			url: twa_ajax_path,
			dataType : 'json',
			data: {
				hook: hook,
				ajaxAction: 'loadDynamicTestimonials'
			},
			success: function(r)
			{
				// console.dir(r);
				$el.html(r.html);
				prepareCarousels();
				normalizeColumns(true);
			},
			error: function(r)
			{
				console.warn(r);
			}
		});
	});

	prepareCarousels();
	normalizeColumns(true);

	$(document).on('click', '.twa_posts .expand', function(){

		var parent = $(this).closest('.post');
		var origHeight = parent.find('.content_wrapper').outerHeight();
		parent.toggleClass('expanded');
		var newHeight = parent.find('.content_wrapper').outerHeight();
		var topPos = parent.offset().top;
		var expanded  = parent.hasClass('expanded');

		//normalize columns in same row
		parent.nextAll().each(function(){
			if ($(this).offset().top != topPos)
				return false;
			if (expanded)
				$(this).find('.content_wrapper').css('margin-bottom', (newHeight-origHeight)+'px');
			else
				$(this).find('.content_wrapper').css('margin-bottom', '0');
		});
		parent.prevAll().each(function(){
			if ($(this).offset().top != topPos)
				return false;
			if (expanded)
				$(this).find('.content_wrapper').css('margin-bottom', (newHeight-origHeight)+'px');
			else
				$(this).find('.content_wrapper').css('margin-bottom', '0');

		});
	});

	$(window).resize(function(){
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			$('.twa_posts.rendered').removeClass('rendered');
			prepareCarousels();
		}, 200);
	});

	function prepareCarousels()
	{
		$('.twa_in_hook.carousel .twa_posts').not('.rendered').each(function(){
			var $container = $(this);
			var id = this.id;
			itemsNum = 2;
			if ($(window).width() < 980 || $container.closest('.twa_in_hook').innerWidth() < 700)
				itemsNum = 1;
			var slideWidth = Math.round($container.closest('.twa_in_hook').innerWidth() / itemsNum);
			var params = {
				pager : true,
				controls: false,
				moveSlides: 1,
				speed: 200,
				maxSlides: itemsNum,
				minSlides: itemsNum,
				slideWidth: slideWidth,
				responsive: false,
				swipeThreshold: 1,
				useCSS: true,
				oneToOneTouch: false,
				infiniteLoop: 1,
				onSliderLoad: function(){
					$container.attr('class', initialClasses[id]+' rendered items-num-'+itemsNum).closest('.bx-wrapper').css('max-width', '100%').children('.bx-viewport').css('height', 'auto');
					// normalize heights in carousel
					var carouselHeights = [];
					$container.find('.post_content').each(function(){
						if ($(this).prop('scrollHeight') != $(this).prop('offsetHeight'))
							$(this).closest('.post').addClass('expandable');
						carouselHeights.push($(this).outerHeight());
						if ($(this).closest('.post').next().length < 1){
							var h = Math.max.apply(Math,carouselHeights);
							$(this).css('height', h+'px').closest('.post').prevAll().find('.post_content').css('height', h+'px');
							carouselHeights = [];
						}
					});
				 },
			};

			if (id in carousels)
				carousels[id].reloadSlider(params);
			else {
				initialClasses[id] = $container.attr('class');
				carousels[id] = $container.bxSlider(params);
			}
		});
	}
});

function normalizeColumns(keepAdjusted){

	if (!keepAdjusted)
		$('.post.adjusted').removeClass('adjusted');

	var $elements = $('.twa_posts.grid, .twa_posts.list').find('.content_wrapper').not('.adjusted');
	var colsNum = 0;
	var currentCol = 0;
	var rowHeights = [];

	$elements.each(function(){
		if (!colsNum)
			colsNum = Math.floor($(this).closest('.twa_posts').outerWidth() / $(this).outerWidth());
		var $post_content = $(this).find('.post_content');
		if ($post_content.prop('scrollHeight') != $post_content.prop('offsetHeight'))
			$(this).closest('.post').addClass('expandable');

		if (currentCol == 0)
			$(this).closest('.post').prevAll().addClass('adjusted');

		rowHeights.push($(this).outerHeight());
		currentCol++;

		if (currentCol == colsNum || $(this).closest('.post').next().length < 1){
			var h = Math.max.apply(Math, rowHeights);
			var newCSS = {'min-height': h+'px'};
			$(this).css(newCSS).closest('.post').prevAll().not('.adjusted').find('.content_wrapper').css(newCSS);
			rowHeights = [];
			currentCol = 0;
			if ($(this).closest('.post').next().length < 1)
				colsNum = 0;
		}
	});
}

function replaceTextInDom(original_value, new_value){
	var reg_exp =  new RegExp(original_value, 'g');
	$('div:contains('+original_value+')').each(function(){
		if ($(this).clone().children('div').remove().end().text().indexOf(original_value) >= 0)
			$(this).html($(this).html().replace(reg_exp, new_value));
	});
}
