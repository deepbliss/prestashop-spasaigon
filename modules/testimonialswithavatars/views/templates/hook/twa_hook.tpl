{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="twa_in_hook{if $in_column} block{/if}{if $displayType == 1} carousel{/if}">
	<h2 class="{if $in_column}title_block{/if}">{l s='Testimonials' mod='testimonialswithavatars'}</h2>
	<div id="twa_{$hook_name|escape:'html':'UTF-8'}" class="twa_posts{if $displayType == 2 && !$in_column} grid{else} list{/if}">
		{include file="./../front/post-list.tpl" posts=$posts displayType=$displayType}
	</div>
	{if !empty($view_all_link)}
	<a class="view_all neat" href="{$view_all_link|escape:'html':'UTF-8'}" title="{l s='View all' mod='testimonialswithavatars'}">
		{l s='View all' mod='testimonialswithavatars'}
	</a>
	{/if}
</div>
